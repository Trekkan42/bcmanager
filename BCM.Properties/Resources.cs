using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace BCM.Properties
{
    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [DebuggerNonUserCode]
    [CompilerGenerated]
    internal class Resources
    {
        private static ResourceManager resourceMan;

        private static CultureInfo resourceCulture;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static ResourceManager ResourceManager
        {
            get
            {
                if (resourceMan == null)
                {
                    resourceMan = new ResourceManager("BCM.Properties.Resources", typeof(Resources).Assembly);
                }
                return resourceMan;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string BCActiveChunks => ResourceManager.GetString("BCActiveChunks", resourceCulture);

        internal static string BCAdmins => ResourceManager.GetString("BCAdmins", resourceCulture);

        internal static string BCAssets => ResourceManager.GetString("BCAssets", resourceCulture);

        internal static string BCBlock => ResourceManager.GetString("BCBlock", resourceCulture);

        internal static string BCChatColor => ResourceManager.GetString("BCChatColor", resourceCulture);

        internal static string BCChatMaxLength => ResourceManager.GetString("BCChatMaxLength", resourceCulture);

        internal static string BCChunkInfo => ResourceManager.GetString("BCChunkInfo", resourceCulture);

        internal static string BCChunkObserver => ResourceManager.GetString("BCChunkObserver", resourceCulture);

        internal static string BCConfig => ResourceManager.GetString("BCConfig", resourceCulture);

        internal static string BCEditMode => ResourceManager.GetString("BCEditMode", resourceCulture);

        internal static string BCEntities => ResourceManager.GetString("BCEntities", resourceCulture);

        internal static string BCEvents => ResourceManager.GetString("BCEvents", resourceCulture);

        internal static string BCExport => ResourceManager.GetString("BCExport", resourceCulture);

        internal static string BCGameObjects => ResourceManager.GetString("BCGameObjects", resourceCulture);

        internal static string BCGetSpawn => ResourceManager.GetString("BCGetSpawn", resourceCulture);

        internal static string BCGiveBuffToEntity => ResourceManager.GetString("BCGiveBuffToEntity", resourceCulture);

        internal static string BCGiveBuffToPlayer => ResourceManager.GetString("BCGiveBuffToPlayer", resourceCulture);

        internal static string BCGiveItemToPlayer => ResourceManager.GetString("BCGiveItemToPlayer", resourceCulture);

        internal static string BCGiveQuestToPlayer => ResourceManager.GetString("BCGiveQuestToPlayer", resourceCulture);

        internal static string BCGiveSkillPointsToPlayer => ResourceManager.GetString("BCGiveSkillPointsToPlayer", resourceCulture);

        internal static string BCGiveXpToPlayer => ResourceManager.GetString("BCGiveXpToPlayer", resourceCulture);

        internal static string BCHelp => ResourceManager.GetString("BCHelp", resourceCulture);

        internal static string BCHideChatCommandsPrefix => ResourceManager.GetString("BCHideChatCommandsPrefix", resourceCulture);

        internal static string BCHordeSpawners => ResourceManager.GetString("BCHordeSpawners", resourceCulture);

        internal static string BCImport => ResourceManager.GetString("BCImport", resourceCulture);

        internal static string BCLocation => ResourceManager.GetString("BCLocation", resourceCulture);

        internal static string BCMemTrash => ResourceManager.GetString("BCMemTrash", resourceCulture);

        internal static string BCMutePlayer => ResourceManager.GetString("BCMutePlayer", resourceCulture);

        internal static string BCParties => ResourceManager.GetString("BCParties", resourceCulture);

        internal static string BCPlayerFiles => ResourceManager.GetString("BCPlayerFiles", resourceCulture);

        internal static string BCPlayerLogs => ResourceManager.GetString("BCPlayerLogs", resourceCulture);

        internal static string BCPlayerName => ResourceManager.GetString("BCPlayerName", resourceCulture);

        internal static string BCPlayers => ResourceManager.GetString("BCPlayers", resourceCulture);

        internal static string BCPlayersGamestage => ResourceManager.GetString("BCPlayersGamestage", resourceCulture);

        internal static string BCPlayersId => ResourceManager.GetString("BCPlayersId", resourceCulture);

        internal static string BCPlayersPosition => ResourceManager.GetString("BCPlayersPosition", resourceCulture);

        internal static string BCPrefab => ResourceManager.GetString("BCPrefab", resourceCulture);

        internal static string BCProtect => ResourceManager.GetString("BCProtect", resourceCulture);

        internal static string BCRemove => ResourceManager.GetString("BCRemove", resourceCulture);

        internal static string BCRemoveBuffFromEntity => ResourceManager.GetString("BCRemoveBuffFromEntity", resourceCulture);

        internal static string BCRemoveBuffFromPlayer => ResourceManager.GetString("BCRemoveBuffFromPlayer", resourceCulture);

        internal static string BCRemoveQuestFromPlayer => ResourceManager.GetString("BCRemoveQuestFromPlayer", resourceCulture);

        internal static string BCReset => ResourceManager.GetString("BCReset", resourceCulture);

        internal static string BCResetSkills => ResourceManager.GetString("BCResetSkills", resourceCulture);

        internal static string BCSetSkillOnPlayer => ResourceManager.GetString("BCSetSkillOnPlayer", resourceCulture);

        internal static string BCSleeper => ResourceManager.GetString("BCSleeper", resourceCulture);

        internal static string BCSpawn => ResourceManager.GetString("BCSpawn", resourceCulture);

        internal static string BCTask => ResourceManager.GetString("BCTask", resourceCulture);

        internal static string BCTeleport => ResourceManager.GetString("BCTeleport", resourceCulture);

        internal static string BCTest => ResourceManager.GetString("BCTest", resourceCulture);

        internal static string BCTileEntity => ResourceManager.GetString("BCTileEntity", resourceCulture);

        internal static string BCTime => ResourceManager.GetString("BCTime", resourceCulture);

        internal static string BCUndo => ResourceManager.GetString("BCUndo", resourceCulture);

        internal static string BCUnlockAll => ResourceManager.GetString("BCUnlockAll", resourceCulture);

        internal static string BCVersions => ResourceManager.GetString("BCVersions", resourceCulture);

        internal static string BCVisitRegion => ResourceManager.GetString("BCVisitRegion", resourceCulture);

        internal static string BCWorldBlocks => ResourceManager.GetString("BCWorldBlocks", resourceCulture);

        internal static string BCWorldBlocksRPC => ResourceManager.GetString("BCWorldBlocksRPC", resourceCulture);

        internal Resources()
        {
        }
    }
}
