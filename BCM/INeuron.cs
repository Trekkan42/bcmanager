namespace BCM
{
    public interface INeuron
    {
        void Fire(int b);
    }
}
