namespace BCM
{
    public enum TargetType
    {
        Position,
        Passive,
        Tracked
    }
}
