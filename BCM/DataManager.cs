using System;
using System.Reflection;
using BCM.PersistentData;

namespace BCM
{
    internal static class DataManager
    {
        public static void SavePlayerData(ClientInfo cInfo, PlayerDataFile playerDataFile)
        {
            try
            {
                BCMPersist.Instance.WorldPlayersData[cInfo.playerId, true]?.Update(playerDataFile);
            }
            catch (Exception arg)
            {
                Log.Out(string.Format("{0} Error in DataManager.{1}: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
            }
        }
    }
}
