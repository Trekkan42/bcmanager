using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace BCM
{
    public static class ObjectCopier
    {
        public static T Clone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                Log.Out("(BCM) Clone object requires a serializable object");
            }
            if (source == null)
            {
                return default(T);
            }
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0L, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
