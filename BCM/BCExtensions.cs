using System;
using System.Collections.Generic;
using System.Linq;
using BCM.Commands;
using UnityEngine;

namespace BCM
{
    public static class BCExtensions
    {
        public static string ToStringRgb(this Color c)
        {
            return $"{c.r:F3}, {c.g:F3}, {c.b:F3}, {c.a:F3}";
        }

        public static string ToStringRgbHex(this Color c, bool hash = true)
        {
            return string.Format("{0}{1:X2}{2:X2}{3:X2}", hash ? "#" : "", (int)(c.r * 255f), (int)(c.g * 255f), (int)(c.b * 255f));
        }

        public static string ToUtcStr(this DateTime d)
        {
            return d.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }

        public static string ParseHelp(this string text, BCCommandAbstract command)
        {
            string[] commands = command.GetCommands();
            return "(BCM) " + text.Replace("{commands}", string.Join(", ", commands)).Replace("{defaultoptions}", (command.BaseOptions != null) ? string.Join(" ", command.BaseOptions.Select((KeyValuePair<string, string> kvp) => (kvp.Value == null) ? ("/" + kvp.Key) : ("/" + kvp.Key + "=" + kvp.Value)).ToArray()) : "None").Replace("{command}", commands.First());
        }
    }
}
