using BCM.PersistentData;
using JetBrains.Annotations;

namespace BCM
{
    public struct PlayerInfo
    {
        [NotNull]
        public string SteamId;

        [CanBeNull]
        public EntityPlayer EP;

        [CanBeNull]
        public PlayerDataFile PDF;

        [CanBeNull]
        public BCMWorldPlayerData WPD;

        [CanBeNull]
        public BCMServerPlayerConfig SPD;

        [CanBeNull]
        public ClientInfo CI;

        [CanBeNull]
        public PersistentPlayerData PPD;
    }
}
