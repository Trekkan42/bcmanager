using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using BCM.PersistentData;
using JetBrains.Annotations;

namespace BCM
{
    [UsedImplicitly]
    public class API : IModApi
    {
        public const string ModPrefix = "(BCM)";

        private static readonly EntitySpawner EntitySpawner = new EntitySpawner();

        public static bool IsAwake;

        public void InitMod()
        {
            StateManager.InitMod();
            ModEvents.GameAwake.RegisterHandler(GameAwake);
            ModEvents.GameStartDone.RegisterHandler(GameStartDone);
            ModEvents.GameUpdate.RegisterHandler(GameUpdate);
            ModEvents.GameShutdown.RegisterHandler(GameShutdown);
            ModEvents.PlayerLogin.RegisterHandler(PlayerLogin);
            ModEvents.PlayerSpawning.RegisterHandler(PlayerSpawning);
            ModEvents.PlayerSpawnedInWorld.RegisterHandler(PlayerSpawnedInWorld);
            ModEvents.PlayerDisconnected.RegisterHandler(PlayerDisconnected);
            ModEvents.SavePlayerData.RegisterHandler(SavePlayerData);
            ModEvents.GameMessage.RegisterHandler(GameMessage);
            ModEvents.ChatMessage.RegisterHandler(ChatMessage);
            ModEvents.CalcChunkColorsDone.RegisterHandler(CalcChunkColorsDone);
            ModEvents.EntityKilled.RegisterHandler(EntityKilled);
        }

        private static void GameAwake()
        {
        }

        private static void GameStartDone()
        {
            StateManager.GameStartDone();
            IsAwake = true;
            AIEvents.Synaptogenesis();
            AIEvents.Activate();
        }

        private static void GameUpdate()
        {
            if (IsAwake)
            {
                EntitySpawner.ProcessSpawnQueue();
            }
        }

        private static void GameShutdown()
        {
            AIEvents.IsAlive = false;
            StateManager.Shutdown();
        }

        private static bool PlayerLogin(ClientInfo clientInfo, string compatibilityVersion, StringBuilder kickReason)
        {
            return true;
        }

        private void PlayerSpawning(ClientInfo clientInfo, int chunkViewDim, PlayerProfile playerProfile)
        {
            try
            {
                BCMPersist.Instance.ServerPlayersConfig[clientInfo.playerId, true]?.SetOnline(clientInfo);
            }
            catch (Exception ex)
            {
                Log.Out(string.Format("{0} Error in {1}.{2}: {3}", "(BCM)", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
            }
        }

        private static void PlayerSpawnedInWorld(ClientInfo clientInfo, RespawnType respawnReason, Vector3i position)
        {
        }

        private void PlayerDisconnected(ClientInfo clientInfo, bool bShutdown)
        {
            try
            {
                BCMPersist.Instance.ServerPlayersConfig[clientInfo.playerId, true]?.SetOffline(clientInfo);
            }
            catch (Exception ex)
            {
                Log.Out(string.Format("{0} Error in {1}.{2}: {3}", "(BCM)", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
            }
        }

        private static void SavePlayerData(ClientInfo clientInfo, PlayerDataFile playerDataFile)
        {
            DataManager.SavePlayerData(clientInfo, playerDataFile);
        }

        private static bool GameMessage(ClientInfo clientInfo, EnumGameMessages messageType, string message, string mainName, bool localiseMain, string secondaryName, bool localiseSecondary)
        {
            return true;
        }

        private static bool ChatMessage(ClientInfo clientInfo, EChatType chatType, int senderEntityId, string message, string mainName, bool localiseMain, List<int> recipientEntityIds)
        {
            return BCChat.Parse(clientInfo, chatType, senderEntityId, message, mainName, localiseMain, recipientEntityIds);
        }

        private static void CalcChunkColorsDone(Chunk chunk)
        {
        }

        private static void EntityKilled(Entity killedEntity, Entity killerEntity)
        {
        }
    }
}
