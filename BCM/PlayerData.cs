using BCM.PersistentData;

namespace BCM
{
    public static class PlayerData
    {
        public static PlayerInfo PlayerInfo(string steamId)
        {
            World world = GameManager.Instance.World;
            PlayerInfo playerInfo;
            if (world == null)
            {
                playerInfo = default(PlayerInfo);
                return playerInfo;
            }
            playerInfo = default(PlayerInfo);
            playerInfo.SteamId = steamId;
            playerInfo.WPD = BCMPersist.Instance.WorldPlayersData[steamId, false];
            playerInfo.SPD = BCMPersist.Instance.ServerPlayersConfig[steamId, false];
            playerInfo.CI = SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.ForPlayerId(steamId);
            playerInfo.PPD = GameManager.Instance.persistentPlayers.GetPlayerData(steamId);
            playerInfo.PDF = null;
            playerInfo.EP = null;
            PlayerInfo result = playerInfo;
            if (result.CI != null && world.Entities.dict.ContainsKey(result.CI.entityId))
            {
                result.EP = world.Entities.dict[result.CI.entityId] as EntityPlayer;
            }
            PlayerDataFile playerDataFile = new PlayerDataFile();
            playerDataFile.Load(GameUtils.GetPlayerDataDir(), steamId);
            if (result.EP == null)
            {
                result.PDF = playerDataFile;
            }
            return result;
        }
    }
}
