using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BCM
{
    public static class PlayerStore
    {
        public static bool GetId(string param, out string steamId, string el = "")
        {
            ClientInfo _cInfo;
            int num = ConsoleHelper.ParseParamPartialNameOrId(param, out steamId, out _cInfo, _sendError: false);
            if (_cInfo == null && num > 0 && !IsStoredPlayer(steamId))
            {
                num = 0;
            }
            if (num == 1)
            {
                return true;
            }
            Error(el, (num > 1) ? "(BCM) Multiple matches found." : "(BCM) Entity not found.");
            return false;
        }

        private static bool IsStoredPlayer(string steamId)
        {
            return File.Exists(GameUtils.GetPlayerDataDir() + Path.DirectorySeparatorChar + steamId + ".ttp");
        }

        public static IEnumerable<string> GetAll(Dictionary<string, string> options)
        {
            string playerDataDir = GameUtils.GetPlayerDataDir();
            List<string> list = new List<string>();
            if (!Directory.Exists(playerDataDir))
            {
                return list;
            }
            string[] files = Directory.GetFiles(playerDataDir);
            List<ClientInfo> list2 = new List<ClientInfo>();
            if (options != null && (options.ContainsKey("online") || options.ContainsKey("offline")) && SingletonMonoBehaviour<ConnectionManager>.Instance.ClientCount() > 0)
            {
                list2 = SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.List.ToList();
            }
            if (options == null)
            {
                for (int num = files.Length - 1; num >= 0; num--)
                {
                    if (!(Path.GetExtension(files[num]) != ".ttp"))
                    {
                        list.Add(Path.GetFileNameWithoutExtension(files[num]));
                    }
                }
            }
            else
            {
                if (options.ContainsKey("players"))
                {
                    List<string> list3 = options["players"].Split(',').ToList();
                    for (int num2 = files.Length - 1; num2 >= 0; num2--)
                    {
                        if (!(Path.GetExtension(files[num2]) != ".ttp"))
                        {
                            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(files[num2]);
                            if (list3.Contains(fileNameWithoutExtension))
                            {
                                list.Add(fileNameWithoutExtension);
                            }
                        }
                    }
                }
                if (options.ContainsKey("online"))
                {
                    for (int num3 = files.Length - 1; num3 >= 0; num3--)
                    {
                        if (!(Path.GetExtension(files[num3]) != ".ttp"))
                        {
                            string steamId2 = Path.GetFileNameWithoutExtension(files[num3]);
                            if (list2.Find((ClientInfo x) => x.playerId == steamId2) != null)
                            {
                                list.Add(steamId2);
                            }
                        }
                    }
                }
                else if (options.ContainsKey("offline"))
                {
                    for (int num4 = files.Length - 1; num4 >= 0; num4--)
                    {
                        if (!(Path.GetExtension(files[num4]) != ".ttp"))
                        {
                            string steamId = Path.GetFileNameWithoutExtension(files[num4]);
                            if (list2.Find((ClientInfo x) => x.playerId == steamId) == null)
                            {
                                list.Add(steamId);
                            }
                        }
                    }
                }
                else
                {
                    for (int num5 = files.Length - 1; num5 >= 0; num5--)
                    {
                        if (!(Path.GetExtension(files[num5]) != ".ttp"))
                        {
                            string fileNameWithoutExtension2 = Path.GetFileNameWithoutExtension(files[num5]);
                            list.Add(fileNameWithoutExtension2);
                        }
                    }
                }
            }
            return list;
        }

        private static void Error(string el, string err)
        {
            switch (el)
            {
                case "CON":
                    SingletonMonoBehaviour<SdtdConsole>.Instance.Output(err);
                    break;
                case "LOG":
                    Log.Out(err);
                    break;
                case "ALL":
                    SingletonMonoBehaviour<SdtdConsole>.Instance.Output(err);
                    Log.Out(err);
                    break;
            }
        }
    }
}
