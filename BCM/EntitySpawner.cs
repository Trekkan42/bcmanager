using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace BCM
{
    public class EntitySpawner
    {
        public static readonly Queue<Spawn> SpawnQueue = new Queue<Spawn>();

        public static readonly Dictionary<string, HordeSpawner> HordeSpawners = new Dictionary<string, HordeSpawner>();

        private long _lastTick = DateTime.UtcNow.Ticks;

        public void ProcessSpawnQueue()
        {
            if (DateTime.UtcNow.Ticks <= _lastTick + 500000)
            {
                return;
            }
            _lastTick = DateTime.UtcNow.Ticks;
            if (SpawnQueue.Count == 0)
            {
                return;
            }
            lock (SpawnQueue)
            {
                MicroStopwatch microStopwatch = new MicroStopwatch();
                while (SpawnQueue.Count > 0 && microStopwatch.ElapsedTicks < 250000)
                {
                    try
                    {
                        Spawn spawn = SpawnQueue.Dequeue();
                        World world = GameManager.Instance.World;
                        if (world.GetRandomSpawnPositionMinMaxToPosition(new Vector3(spawn.SpawnPos.x, spawn.SpawnPos.y, spawn.SpawnPos.z), spawn.MinRange, spawn.MaxRange, 1, _bConsiderBedrolls: false, out var _position))
                        {
                            EntityEnemy entityEnemy = EntityFactory.CreateEntity(spawn.EntityClassId, _position) as EntityEnemy;
                            if (entityEnemy == null)
                            {
                                continue;
                            }
                            lock (HordeSpawners)
                            {
                                spawn.EntityId = entityEnemy.entityId;
                                if (HordeSpawners.ContainsKey(spawn.SpawnerId.ToString()))
                                {
                                    HordeSpawners[spawn.SpawnerId.ToString()].Spawns.Add(entityEnemy.entityId.ToString(), spawn);
                                }
                                else
                                {
                                    HordeSpawners.Add(spawn.SpawnerId.ToString(), new HordeSpawner
                                    {
                                        Spawns = new Dictionary<string, Spawn>
                                        {
                                            {
                                                entityEnemy.entityId.ToString(),
                                                spawn
                                            }
                                        }
                                    });
                                }
                            }
                            entityEnemy.bIsChunkObserver = spawn.IsObserver;
                            entityEnemy.moveSpeedAggro *= spawn.SpeedMul;
                            if (spawn.SpeedBase > 0f)
                            {
                                entityEnemy.moveSpeedAggro = spawn.SpeedBase * spawn.SpeedMul;
                            }
                            if (spawn.NightRun)
                            {
                                entityEnemy.moveSpeedNight *= spawn.SpeedMul;
                            }
                            else
                            {
                                entityEnemy.moveSpeedNight = entityEnemy.moveSpeedAggro;
                            }
                            Log.Out(string.Format("{0} Spawning {1}({2}):{3} @{4} {5} {6} => {7} {8} {9}", "(BCM)", entityEnemy.entityType, entityEnemy.entityId, entityEnemy.EntityName, _position.x, _position.y, _position.z, spawn.TargetPos.x, spawn.TargetPos.y, spawn.TargetPos.z));
                            world.AddEntityToMap(entityEnemy);
                            world.Entities.Add(entityEnemy.entityId, entityEnemy);
                            if (entityEnemy.IsEntityAttachedToChunk && !entityEnemy.addedToChunk)
                            {
                                (world.GetChunkFromWorldPos(entityEnemy.GetBlockPosition()) as Chunk)?.AddEntityToChunk(entityEnemy);
                            }
                            world.audioManager?.EntityAddedToWorld(entityEnemy, world);
                            WeatherManager.EntityAddedToWorld(entityEnemy);
                            LightManager.EntityAddedToWorld(entityEnemy, world);
                            entityEnemy.OnAddedToWorld();
                            world.entityDistributer.Add(entityEnemy);
                            entityEnemy.Spawned = true;
                            world.aiDirector.AddEntity(entityEnemy);
                            entityEnemy.SetSpawnerSource(EnumSpawnerSource.Dynamic);
                            entityEnemy.IsHordeZombie = true;
                            entityEnemy.bIsChunkObserver = true;
                            entityEnemy.SetInvestigatePosition(new Vector3(spawn.TargetPos.x, spawn.TargetPos.y, spawn.TargetPos.z), 6000);
                            continue;
                        }
                        Log.Out(string.Format("{0} Unable to find Spawn Point near {1}, min:{2}, max:{3}", "(BCM)", spawn.TargetPos, spawn.MinRange, spawn.MaxRange));
                    }
                    catch (Exception ex)
                    {
                        Log.Out(string.Format("{0} Error in {1}.{2}: {3}", "(BCM)", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
                    }
                }
                microStopwatch.Stop();
            }
        }
    }
}
