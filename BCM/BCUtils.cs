using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using BCM.Commands;
using BCM.Models;
using LitJson;
using UnityEngine;

namespace BCM
{
    public static class BCUtils
    {
        public static bool CheckWorld(out World world)
        {
            world = GameManager.Instance?.World;
            if (world != null)
            {
                return true;
            }
            BCCommandAbstract.SendOutput("World not loaded");
            return false;
        }

        public static bool CheckWorld()
        {
            if (GameManager.Instance?.World != null)
            {
                return true;
            }
            BCCommandAbstract.SendOutput("World not loaded");
            return false;
        }

        public static void NormaliseXZ(ref int x1, ref int z1, ref int x2, ref int z2)
        {
            int num = Math.Min(x1, x2);
            int num2 = Math.Max(x1, x2);
            int num3 = Math.Min(z1, z2);
            int num4 = Math.Max(z1, z2);
            x1 = num;
            x2 = num2;
            z1 = num3;
            z2 = num4;
        }

        public static void NormaliseXYZ(ref int x1, ref int y1, ref int z1, ref int x2, ref int y2, ref int z2)
        {
            int num = Math.Min(y1, y2);
            int num2 = Math.Max(y1, y2);
            y1 = num;
            y2 = num2;
            NormaliseXZ(ref x1, ref z1, ref x2, ref z2);
        }

        public static Vector3i GetMaxPos(Vector3i pos, Vector3i size)
        {
            return new Vector3i(pos.x + size.x - 1, pos.y + size.y - 1, pos.z + size.z - 1);
        }

        public static Vector3i GetSize(Vector3i pos1, Vector3i pos2)
        {
            return new Vector3i(Math.Abs(pos1.x - pos2.x) + 1, Math.Abs(pos1.y - pos2.y) + 1, Math.Abs(pos1.z - pos2.z) + 1);
        }

        public static void WriteVector3i(Vector3i v, JsonWriter w, string format)
        {
            switch (format)
            {
                case "V":
                    w.WriteObjectStart();
                    w.WritePropertyName("x");
                    w.Write(v.x);
                    w.WritePropertyName("y");
                    w.Write(v.y);
                    w.WritePropertyName("z");
                    w.Write(v.z);
                    w.WriteObjectEnd();
                    break;
                case "S":
                    w.Write($"{v.x} {v.y} {v.z}");
                    break;
                case "W":
                    w.Write(string.Format("{0}{1} {2}{3}", Math.Abs(v.x), (v.x < 0) ? "W" : "E", Math.Abs(v.z), (v.z > 0) ? "N" : "S"));
                    break;
                case "C":
                    w.Write($"{v.x}, {v.y}, {v.z}");
                    break;
            }
        }

        public static void WriteVector3(Vector3 v, JsonWriter w, string format)
        {
            switch (format)
            {
                case "V":
                    w.WriteObjectStart();
                    w.WritePropertyName("x");
                    w.Write(Math.Round(v.x, 2));
                    w.WritePropertyName("y");
                    w.Write(Math.Round(v.y, 2));
                    w.WritePropertyName("z");
                    w.Write(Math.Round(v.z, 2));
                    w.WriteObjectEnd();
                    break;
                case "S":
                    w.Write($"{Math.Floor(v.x)} {Math.Floor(v.y)} {Math.Floor(v.z)}");
                    break;
                case "W":
                    w.Write(string.Format("{0:0}{1} {2:0}{3}", Math.Abs(v.x), (v.x < 0f) ? "W" : "E", Math.Abs(v.z), (v.z > 0f) ? "N" : "S"));
                    break;
                case "C":
                    w.Write($"{Math.Floor(v.x)}, {Math.Floor(v.y)}, {Math.Floor(v.z)}");
                    break;
            }
        }

        public static object GetVectorObj(BCMVector3 p, IDictionary<string, string> o)
        {
            if (o.ContainsKey("strpos"))
            {
                return p.x + " " + p.y + " " + p.z;
            }
            if (o.ContainsKey("worldpos"))
            {
                return p.x + p.y + p.z;
            }
            if (o.ContainsKey("csvpos"))
            {
                return new int[3]
                {
                    p.x,
                    p.y,
                    p.z
                };
            }
            return p;
        }

        public static string UIntToHex(uint c)
        {
            return ColorToHex(UIntToColor(c));
        }

        public static Color UIntToColor(uint c)
        {
            byte r = (byte)(c >> 16);
            byte g = (byte)(c >> 8);
            byte b = (byte)c;
            return new Color32(r, g, b, byte.MaxValue);
        }

        public static string ColorToHex(Color color)
        {
            return $"{(int)(color.r * 255f):X02}{(int)(color.g * 255f):X02}{(int)(color.b * 255f):X02}";
        }

        public static string GetPublicIPAddress()
        {
            return new WebClient().DownloadString("http://bot.whatismyipaddress.com");
        }

        public static string GetIPAddress()
        {
            IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            foreach (IPAddress iPAddress in addressList)
            {
                if (iPAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    return iPAddress.ToString();
                }
            }
            return "";
        }

        public static bool ParseColor(string rawColor, out string color)
        {
            color = "";
            if (6 != rawColor.Length)
            {
                return false;
            }
            color = "[" + rawColor.ToUpper() + "]";
            return true;
        }

        public static bool GetXZPair(string[] Params, out Vector2i pos1, out Vector2i pos2, out string error)
        {
            error = string.Empty;
            pos1 = default(Vector2i);
            pos2 = default(Vector2i);
            if (Params.Length < 4)
            {
                error = "Incorrect number of params";
                return false;
            }
            if (!int.TryParse(Params[0], out var result))
            {
                error = "Unable to parse " + Params[0] + " as a number";
                return false;
            }
            if (!int.TryParse(Params[1], out var result2))
            {
                error = "Unable to parse " + Params[1] + " as a number";
                return false;
            }
            if (!int.TryParse(Params[2], out var result3))
            {
                error = "Unable to parse " + Params[2] + " as a number";
                return false;
            }
            if (!int.TryParse(Params[3], out var result4))
            {
                error = "Unable to parse " + Params[3] + " as a number";
                return false;
            }
            NormaliseXZ(ref result, ref result2, ref result3, ref result4);
            pos1.Set(result, result2);
            pos2.Set(result3, result4);
            return true;
        }

        public static bool GetLoadedChunksInArea(World world, Vector2i pos1, Vector2i pos2, out Dictionary<long, Chunk> chunks, out string error)
        {
            chunks = new Dictionary<long, Chunk>();
            error = string.Empty;
            for (int i = pos1.x; i <= pos2.x; i++)
            {
                for (int j = pos1.y; j <= pos2.y; j++)
                {
                    long key = WorldChunkCache.MakeChunkKey(World.toChunkXZ(i), World.toChunkXZ(j), 0);
                    if (!chunks.ContainsKey(key))
                    {
                        Chunk chunkSync = world.ChunkCache.GetChunkSync(key);
                        if (chunkSync == null)
                        {
                            error = $"Chunk not loaded {WorldChunkCache.extractX(key)},{WorldChunkCache.extractZ(key)}";
                            return false;
                        }
                        chunks.Add(key, chunkSync);
                    }
                }
            }
            return true;
        }
    }
}
