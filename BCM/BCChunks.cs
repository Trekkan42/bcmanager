using System;
using System.Collections.Generic;

namespace BCM
{
    public static class BCChunks
    {
        public static int ReloadForClients(Dictionary<long, Chunk> chunks, string steamId = "")
        {
            World world = GameManager.Instance.World;
            if (world == null)
            {
                return 0;
            }
            ResetStability(world, chunks);
            Dictionary<ClientInfo, List<long>> dictionary = new Dictionary<ClientInfo, List<long>>();
            foreach (ClientInfo item in SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.List)
            {
                try
                {
                    if ((steamId != "" && steamId != item.playerId) || !world.Entities.dict.ContainsKey(item.entityId))
                    {
                        continue;
                    }
                    EntityPlayer entityPlayer = world.Entities.dict[item.entityId] as EntityPlayer;
                    if (entityPlayer == null)
                    {
                        continue;
                    }
                    HashSetLong chunksLoaded = entityPlayer.ChunkObserver.chunksLoaded;
                    if (chunksLoaded == null)
                    {
                        continue;
                    }
                    foreach (long item2 in chunksLoaded)
                    {
                        if (chunks.ContainsKey(item2))
                        {
                            if (dictionary.ContainsKey(item))
                            {
                                dictionary[item].Add(item2);
                            }
                            else
                            {
                                dictionary.Add(item, new List<long>
                                {
                                    item2
                                });
                            }
                            item.SendPackage(NetPackageManager.GetPackage<NetPackageChunk>().Setup(chunks[item2], _bOverwriteExisting: true));
                        }
                    }
                    if (dictionary.ContainsKey(item))
                    {
                        Log.Out(string.Format("{0} Reloading {1}/{2} chunks for {3}", "(BCM)", dictionary[item].Count, entityPlayer.ChunkObserver.chunksLoaded.Count, item.playerName));
                    }
                }
                catch (Exception arg)
                {
                    Log.Out(string.Format("{0} Error reloading chunks for {1}:\n{2}", "(BCM)", item.playerName, arg));
                }
            }
            return dictionary.Count;
        }

        public static void ResetStability(World world, Dictionary<long, Chunk> chunks)
        {
            StabilityInitializer stabilityInitializer = new StabilityInitializer(world);
            foreach (Chunk value in chunks.Values)
            {
                value?.ResetStability();
            }
            foreach (Chunk value2 in chunks.Values)
            {
                if (value2 != null)
                {
                    stabilityInitializer.DistributeStability(value2);
                    value2.NeedsRegeneration = true;
                    value2.NeedsLightCalculation = true;
                }
            }
        }
    }
}
