using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using BCM.PersistentData;

namespace BCM
{
    public static class AIEvents
    {
        public delegate void Neuron(int beatCount);

        public static Dictionary<string, INeuron> NeuronList = new Dictionary<string, INeuron>();

        public static bool IsAlive;

        public static int Bpm = 60;

        private static int _beatCount;

        public static ConfigValue Config => BCMPersist.Instance.ServerConfig["AIEvents"];

        public static event Neuron Neurons;

        public static void Activate()
        {
            IsAlive = true;
            if (IsAlive)
            {
                ThreadManager.StartThread(NeuronInvoker, ThreadPriority.Lowest);
                Log.Out("(BCM) It's Alive!!! (AIEvents Activated)");
            }
        }

        private static void NeuronInvoker(ThreadManager.ThreadInfo ti)
        {
            while (IsAlive && !ti.TerminationRequested())
            {
                _beatCount++;
                AIEvents.Neurons?.Invoke(_beatCount);
                Thread.Sleep(60000 / Bpm);
            }
            Terminate();
        }

        private static void Terminate()
        {
            Log.Out("(BCM) It's Dead Jim! (AIEvents Disabled)");
        }

        public static void Synaptogenesis()
        {
            Type typeFromHandle = typeof(INeuron);
            foreach (Assembly loadedAssembly in ModManager.GetLoadedAssemblies())
            {
                try
                {
                    Type[] types = loadedAssembly.GetTypes();
                    foreach (Type type in types)
                    {
                        if (!type.IsClass || type.IsAbstract || !typeFromHandle.IsAssignableFrom(type))
                        {
                            continue;
                        }
                        try
                        {
                            ConstructorInfo constructor = type.GetConstructor(new Type[0]);
                            if (constructor != null)
                            {
                                Log.Out("(BCM) Registering event: " + type.Name);
                                INeuron neuron = (INeuron)constructor.Invoke(new object[0]);
                                NeuronList.Add(type.Name, neuron);
                                Neurons += neuron.Fire;
                            }
                            else
                            {
                                Log.Warning("(BCM) Event class " + type.Name + " does not contain a parameter-less constructor, skipping");
                            }
                        }
                        catch (Exception e)
                        {
                            Log.Error("(BCM) Could not register event: " + type.Name);
                            Log.Exception(e);
                        }
                    }
                }
                catch (Exception e2)
                {
                    Log.Error("(BCM) Error registering events from assembly " + loadedAssembly.Location);
                    Log.Exception(e2);
                }
            }
        }
    }
}
