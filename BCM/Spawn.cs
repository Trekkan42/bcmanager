using UnityEngine;

namespace BCM
{
    public class Spawn
    {
        public int EntityClassId;

        public long SpawnerId;

        public Vector3 SpawnPos;

        public Vector3 TargetPos;

        public int EntityId = -1;

        public int TargetId = -1;

        public int MinRange = 40;

        public int MaxRange = 60;

        public bool IsObserver = true;

        public bool IsFeral;

        public float SpeedMul = 1f;

        public float SpeedBase;

        public bool NightRun;
    }
}
