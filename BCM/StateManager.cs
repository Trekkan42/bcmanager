using System;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;

namespace BCM
{
    public static class StateManager
    {
        public static void InitMod()
        {
            try
            {
                BCMPersist.LoadServer();
            }
            catch (Exception arg)
            {
                Log.Out(string.Format("{0} Error in StateManager.{1}: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
            }
        }

        public static void GameStartDone()
        {
            try
            {
                BCMPersist.LoadWorld();
            }
            catch (Exception arg)
            {
                Log.Out(string.Format("{0} Error in StateManager.{1}: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
            }
        }

        public static void Shutdown()
        {
            try
            {
                BCMPersist.Instance.Save(BCMSaveType.All);
            }
            catch (Exception arg)
            {
                Log.Out(string.Format("{0} Error in StateManager.{1}: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
            }
        }
    }
}
