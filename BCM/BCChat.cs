using System.Collections.Generic;
using System.Linq;
using BCM.Commands;
using BCM.PersistentData;

namespace BCM
{
    public static class BCChat
    {
        private class PNPrefix
        {
            public readonly string Color;

            public readonly string Prefix;

            public PNPrefix(string color, string prefix)
            {
                Color = color;
                Prefix = prefix;
            }
        }

        public static string ServerPM = "Server (PM)";

        public static string ServerGlobal = "Server";

        public static bool Parse(ClientInfo clientInfo, EChatType chatType, int senderEntityId, string message, string mainName, bool localiseMain, List<int> recipientEntityIds)
        {
            if (clientInfo == null)
            {
                return true;
            }
            if (!senderEntityId.Equals(clientInfo.entityId))
            {
                return true;
            }
            Log.Out(string.Format("{0} {1}:{2}:{3}: {4}", "(BCM)", chatType.ToStringCached(), clientInfo.playerName, senderEntityId, message));
            if (BCChatMaxLength.ChatMaxLength > 0 && message.Length > BCChatMaxLength.ChatMaxLength)
            {
                clientInfo.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Whisper, -1, $"Your message was blocked. Restrict message to {BCChatMaxLength.ChatMaxLength} characters", ServerPM, _localizeMain: false, null));
                Log.Out(string.Format("{0} Suppressed long message from {1}, length {2}", "(BCM)", clientInfo.playerName, message.Length));
                return false;
            }
            BCMServerPlayerConfig bCMServerPlayerConfig = BCMPersist.Instance.ServerPlayersConfig[clientInfo.playerId, true];
            if (bCMServerPlayerConfig.Muted && chatType.Equals(EChatType.Global))
            {
                clientInfo.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Whisper, -1, "Your message was blocked. You have been muted", ServerPM, _localizeMain: false, null));
                Log.Out("(BCM) Suppressed message from " + clientInfo.playerName + ", muted");
                return false;
            }
            if (!string.IsNullOrEmpty(BCHideChatCommandsPrefix.ChatCommandPrefix) && message.Trim().StartsWith(BCHideChatCommandsPrefix.ChatCommandPrefix))
            {
                clientInfo.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Whisper, -1, "Logging Command: " + message.Trim(), ServerPM, _localizeMain: false, null));
                Log.Out("(BCM) Command from '" + clientInfo.playerName + "': " + message.Trim());
                return false;
            }
            bool flag = BCPlayerName.MemberLookup.Contains(clientInfo.playerId);
            if (!string.IsNullOrEmpty(bCMServerPlayerConfig.ChatColor) || clientInfo.playerName != bCMServerPlayerConfig.ChatName || flag)
            {
                PNPrefix pNPrefix = null;
                if (flag)
                {
                    pNPrefix = (from g in BCPlayerName.Groups.Values
                                where g.Members.Contains(clientInfo.playerId)
                                select new PNPrefix(g.Color, g.Prefix)).FirstOrDefault();
                }
                GameManager.Instance.ChatMessageServer(clientInfo, chatType, -1, (bCMServerPlayerConfig.ChatColorFullText ? bCMServerPlayerConfig.ChatColor : ChatTypeColor(chatType)) + message + "[FFFFFF]", ((pNPrefix != null) ? (pNPrefix.Color + pNPrefix.Prefix + " ") : "") + (bCMServerPlayerConfig.ChatColor ?? ChatTypeColor(chatType)) + (string.IsNullOrEmpty(bCMServerPlayerConfig.ChatName) ? clientInfo.playerName : bCMServerPlayerConfig.ChatName) + "[FFFFFF]", _localizeMain: false, recipientEntityIds);
                return false;
            }
            return true;
        }

        private static string ChatTypeColor(EChatType _chatType)
        {
            return _chatType switch
            {
                EChatType.Global => "[ffffff]",
                EChatType.Friends => "[00bb00]",
                EChatType.Party => "[ffcc00]",
                EChatType.Whisper => "[d00000]",
                _ => "",
            };
        }
    }
}
