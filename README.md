BCManager Server Module
=======================

A server module for controlling various aspects about your 7 Days to Die Server. This project was originally
located on GitHub. The author has since discontinued support. This is a clean decompilation and recompilation
effort to re-produce the original source code from the assembly artifacts.

# Building

Provided you have the correct version of dotNET Core installed, the 7 Days to Die game installed somewhere,
and a suitable .NET Framework to build with you should be able to build this project with the *`dotnet`* command.

## Build Prerequisites

As of this writing the following tools were used to reproduce a DLL artifact suitable for use with the
dedicated server.

* [dotNET Core **5.0.100**](https://dotnet.microsoft.com/download/dotnet-core/5.0)
* [.NET Framework Version **4.7.2**](https://dotnet.microsoft.com/download/dotnet-framework/net472)
* [7 Days to Die A19.2(b4)](https://store.steampowered.com/app/251570/7_Days_to_Die/) installation via Steam.

Once you have the three items installed and verified working, you must update the `.csproj` file to refer to
the hinted assembly file locations for the 7 Days to Die Dedicated Server Mono/Unity files wherever you installed
the game on your system. We used a Windows 10 machine with the game installed to the `Z:` drive as indicated in
the `.csproj` file.

## Performing a Build

Assuming the above requirements are met and functional, out of the box, this project _should_ compile.

```powershell
dotnet restore
dotnet build
```

You should be greeted with similar output and a deployable artifact (DLL File) for the dedicated server.

```powershell
PS E:\BCManager1> dotnet restore
  Determining projects to restore...
  All projects are up-to-date for restore.
PS E:\BCManager1> dotnet build
Microsoft (R) Build Engine version 16.8.0+126527ff1 for .NET
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  All projects are up-to-date for restore.
  BCManager -> E:\BCManager1\bin\Debug\net472\BCManager.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:00.92
```
