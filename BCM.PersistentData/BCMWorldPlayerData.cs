using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using BCM.Models;
using UnityEngine;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMWorldPlayerData
	{
		private readonly string _steamId;

		private string _name;

		private string _ip;

		private long _totalPlayTime;

		private DateTime _lastOnline;

		private int _gamestage;

		[OptionalField]
		private BCMVector3 _lastPos;

		[OptionalField]
		private DateTime _lastUpdate;

		[NonSerialized]
		private ClientInfo _clientInfo;

		[NonSerialized]
		private PlayerDataFile _playerData;

		public bool IsOnline => _clientInfo != null;

		public string Name => _name ?? string.Empty;

		public string Ip => _ip ?? string.Empty;

		public long TotalPlayTime
		{
			get
			{
				if (!IsOnline)
				{
					return _totalPlayTime;
				}
				return _totalPlayTime + (long)(DateTime.UtcNow - _lastOnline).TotalSeconds;
			}
		}

		public DateTime LastOnline
		{
			get
			{
				if (!IsOnline)
				{
					return _lastOnline;
				}
				return DateTime.UtcNow;
			}
		}

		public int Gamestage => _gamestage;

		public BCMVector3 LastLogoutPos => _lastPos;

		public DateTime LastSaveUtc => _lastUpdate;

		public string SteamId => _steamId;

		public string ClientName => _clientInfo?.playerName;

		public PlayerDataFile DataCache => _playerData ?? (_playerData = new PlayerDataFile());

		public BCMWorldPlayerData(string steamId)
		{
			_steamId = steamId;
		}

		public void SetOffline(ClientInfo ci)
		{
			if (_clientInfo != null)
			{
				_totalPlayTime += (long)(DateTime.UtcNow - _lastOnline).TotalSeconds;
				_lastOnline = DateTime.UtcNow;
				Dictionary<int, EntityPlayer> dict = GameManager.Instance.World.Players.dict;
				EntityPlayer entityPlayer = (dict.ContainsKey(ci.entityId) ? dict[ci.entityId] : null);
				_gamestage = ((entityPlayer != null) ? entityPlayer.gameStage : (-1));
				_lastPos = new BCMVector3((entityPlayer != null) ? entityPlayer.position : Vector3.zero);
				_clientInfo = null;
				_playerData = null;
				BCMPersist.Instance.Save(BCMSaveType.WorldPlayersData);
			}
		}

		public void SetOnline(ClientInfo ci)
		{
			_clientInfo = ci;
			_name = ci.playerName;
			_ip = ci.ip;
			_lastOnline = DateTime.UtcNow;
			BCMPersist.Instance.Save(BCMSaveType.WorldPlayersData);
		}

		public void Update(PlayerDataFile dataFile)
		{
			_lastUpdate = DateTime.UtcNow;
			DataCache.ecd = dataFile.ecd;
			DataCache.bag = dataFile.bag;
			DataCache.craftingData = dataFile.craftingData;
			DataCache.favoriteRecipeList = dataFile.favoriteRecipeList;
			DataCache.unlockedRecipeList = dataFile.unlockedRecipeList;
			DataCache.questJournal = dataFile.questJournal;
			DataCache.markerPosition = dataFile.markerPosition;
		}
	}
}
