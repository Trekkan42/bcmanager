using System;
using BCM.Models;

namespace BCM.PersistentData
{
	[Serializable]
	public class LogData
	{
		public string d;

		public string r;

		public LogData(BCMVector4 posrot, string reason)
		{
			d = $"{posrot}";
			r = reason ?? "";
		}
	}
}
