using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMPlayerLogs
	{
		private readonly Dictionary<string, PlayerLog> _playerlogs = new Dictionary<string, PlayerLog>();

		public int Count => _playerlogs.Count;

		[CanBeNull]
		public PlayerLog this[string steamId, bool create]
		{
			get
			{
				if (string.IsNullOrEmpty(steamId))
				{
					return null;
				}
				if (_playerlogs.ContainsKey(steamId))
				{
					return _playerlogs[steamId];
				}
				if (!create || steamId.Length != 17)
				{
					return null;
				}
				PlayerLog playerLog = new PlayerLog(steamId);
				_playerlogs.Add(steamId, playerLog);
				return playerLog;
			}
		}

		public IEnumerable<string> AllKeys()
		{
			return _playerlogs.Keys;
		}
	}
}
