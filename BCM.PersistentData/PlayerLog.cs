using System;
using System.Collections.Generic;
using BCM.Models;
using UnityEngine;

namespace BCM.PersistentData
{
	[Serializable]
	public class PlayerLog
	{
		public string SteamId;

		public readonly Dictionary<string, LogData> LogDataCache = new Dictionary<string, LogData>();

		private BCMVector3 last;

		public PlayerLog(string steamId)
		{
			SteamId = steamId;
		}

		private void LogPosition(BCMVector3 position, int rotation)
		{
			if (!position.Equals(last))
			{
				string key = $"{DateTime.UtcNow:yyyy-MM-dd_HH_mm_ss.fffZ}";
				if (!LogDataCache.ContainsKey(key))
				{
					LogDataCache.Add(key, new LogData(new BCMVector4(position, rotation), "M"));
				}
				last = position;
			}
		}

		public void LogPosition(Vector4 position, int rotation)
		{
			LogPosition(new BCMVector3(position), rotation);
		}
	}
}
