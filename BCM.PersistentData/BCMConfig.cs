using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMConfig
	{
		private readonly Dictionary<string, ConfigValue> _configValues = new Dictionary<string, ConfigValue>();

		[NotNull]
		public ConfigValue this[string name]
		{
			get
			{
				if (string.IsNullOrEmpty(name))
				{
					throw new NullReferenceException(name + " can not be null or empty.");
				}
				if (_configValues.ContainsKey(name))
				{
					return _configValues[name];
				}
				ConfigValue configValue = new ConfigValue();
				_configValues.Add(name, configValue);
				return configValue;
			}
		}

		[CanBeNull]
		public ConfigValue this[string name, bool create]
		{
			get
			{
				if (string.IsNullOrEmpty(name))
				{
					return null;
				}
				if (_configValues.ContainsKey(name))
				{
					return _configValues[name];
				}
				if (!create)
				{
					return null;
				}
				ConfigValue configValue = new ConfigValue();
				_configValues.Add(name, configValue);
				return configValue;
			}
		}
	}
}
