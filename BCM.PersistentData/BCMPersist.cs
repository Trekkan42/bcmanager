using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using BCM.Models;
using JetBrains.Annotations;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMPersist
	{
		public static class BCPrefs
		{
			public static string SaveGameFolder => GamePrefs.GetString(EnumUtils.Parse<EnumGamePrefs>("SaveGameFolder"));

			public static string GameWorld => GamePrefs.GetString(EnumUtils.Parse<EnumGamePrefs>("GameWorld"));

			public static string GameName => GamePrefs.GetString(EnumUtils.Parse<EnumGamePrefs>("GameName"));
		}

		private const string BCMVersionedDir = "BCMDataV4.0.0";

		private static readonly char _s;

		private static readonly BinaryFormatter BFormatter;

		private static readonly string ServerDir;

		private static readonly string ServerConfigFile;

		private static readonly string ServerPlayersFile;

		private static string _worldDir;

		private static string _worldConfigFile;

		private static string _worldPlayersFile;

		public static string PlayerDataDir;

		public static string UndoCacheDir;

		public static string ProfileBackupDir;

		private static BCMPersist _instance;

		private BCMConfig _serverConfig;

		private BCMConfig _worldConfig;

		private BCMServerPlayersConfig _serverPlayersConfig;

		private BCMWorldPlayersData _worldPlayersData;

		[NotNull]
		public static BCMPersist Instance => _instance ?? (_instance = new BCMPersist());

		[NotNull]
		public BCMConfig ServerConfig => _serverConfig ?? (_serverConfig = new BCMConfig());

		[NotNull]
		public BCMConfig WorldConfig => _worldConfig ?? (_worldConfig = new BCMConfig());

		[NotNull]
		public BCMServerPlayersConfig ServerPlayersConfig => _serverPlayersConfig ?? (_serverPlayersConfig = new BCMServerPlayersConfig());

		[NotNull]
		public BCMWorldPlayersData WorldPlayersData => _worldPlayersData ?? (_worldPlayersData = new BCMWorldPlayersData());

		static BCMPersist()
		{
			_s = Path.DirectorySeparatorChar;
			BFormatter = new BinaryFormatter();
			ServerDir = string.Format("{0}{1}{2}{3}", BCPrefs.SaveGameFolder, _s, "BCMDataV4.0.0", _s);
			ServerConfigFile = ServerDir + "ServerConfig.bin";
			ServerPlayersFile = ServerDir + "PlayersConfig.bin";
			try
			{
				Directory.CreateDirectory(ServerDir ?? "");
			}
			catch (Exception arg)
			{
				Log.Error(string.Format("{0} Error in PersistentContainer.{1} while creating save directory folders: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
			}
		}

		public void Save(BCMSaveType saveType)
		{
			if (saveType == BCMSaveType.All || saveType == BCMSaveType.ServerConfig)
			{
				try
				{
					Log.Out("(BCM) Saving server config to " + ServerConfigFile);
					Stream stream = File.Open(ServerConfigFile, FileMode.Create);
					BFormatter.Serialize(stream, ServerConfig);
					stream.Close();
				}
				catch (Exception arg)
				{
					Log.Error(string.Format("{0} Error in PersistentContainer.{1} while saving server config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
				}
			}
			if ((saveType == BCMSaveType.All || saveType == BCMSaveType.ServerPlayersConfig) && ServerPlayersConfig.Count > 0)
			{
				try
				{
					Log.Out("(BCM) Saving server players config to " + ServerPlayersFile);
					Stream stream2 = File.Open(ServerPlayersFile, FileMode.Create);
					BFormatter.Serialize(stream2, ServerPlayersConfig);
					stream2.Close();
				}
				catch (Exception arg2)
				{
					Log.Error(string.Format("{0} Error in PersistentContainer.{1} while saving server players config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg2));
				}
			}
			if (saveType == BCMSaveType.All || saveType == BCMSaveType.WorldSaveConfig)
			{
				try
				{
					Log.Out("(BCM) Saving world config to " + _worldConfigFile);
					Stream stream3 = File.Open(_worldConfigFile, FileMode.Create);
					BFormatter.Serialize(stream3, WorldConfig);
					stream3.Close();
				}
				catch (Exception arg3)
				{
					Log.Error(string.Format("{0} Error in PersistentContainer.{1} while saving world config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg3));
				}
			}
			if ((saveType == BCMSaveType.All || saveType == BCMSaveType.WorldPlayersData) && WorldPlayersData.Count > 0)
			{
				try
				{
					Log.Out("(BCM) Saving world players data to " + _worldPlayersFile);
					Stream stream4 = File.Open(_worldPlayersFile, FileMode.Create);
					BFormatter.Serialize(stream4, WorldPlayersData);
					stream4.Close();
				}
				catch (Exception arg4)
				{
					Log.Error(string.Format("{0} Error in PersistentContainer.{1} while saving players server config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg4));
				}
			}
		}

		public static void LoadServer()
		{
			BCMPersist bCMPersist = new BCMPersist();
			Log.Out("(BCM) Loading server config.");
			try
			{
				Stream stream = File.Open(ServerConfigFile, FileMode.OpenOrCreate);
				if (stream.Length > 0)
				{
					bCMPersist._serverConfig = (BCMConfig)BFormatter.Deserialize(stream);
				}
				stream.Close();
			}
			catch (Exception arg)
			{
				Log.Error(string.Format("{0} Error in PersistentContainer.{1} while loading server config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
			}
			try
			{
				Stream stream2 = File.Open(ServerPlayersFile, FileMode.OpenOrCreate);
				if (stream2.Length > 0)
				{
					bCMPersist._serverPlayersConfig = (BCMServerPlayersConfig)BFormatter.Deserialize(stream2);
				}
				stream2.Close();
			}
			catch (Exception arg2)
			{
				Log.Error(string.Format("{0} Error in PersistentContainer.{1} while loading server players config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg2));
			}
			_instance = bCMPersist;
		}

		public static void LoadWorld()
		{
			Log.Out("(BCM) Loading world config.");
			_worldDir = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}", BCPrefs.SaveGameFolder, _s, BCPrefs.GameWorld, _s, BCPrefs.GameName, _s, "BCMDataV4.0.0", _s);
			PlayerDataDir = $"{_worldDir}PlayerData{_s}";
			UndoCacheDir = $"{_worldDir}UndoCache{_s}";
			ProfileBackupDir = $"{_worldDir}ProfileBackup{_s}";
			_worldConfigFile = _worldDir + "WorldConfig.bin";
			_worldPlayersFile = _worldDir + "PlayersData.bin";
			Directory.CreateDirectory(_worldDir ?? "");
			Directory.CreateDirectory(UndoCacheDir ?? "");
			Directory.CreateDirectory(ProfileBackupDir ?? "");
			Directory.CreateDirectory(PlayerDataDir ?? "");
			try
			{
				Stream stream = File.Open(_worldConfigFile, FileMode.OpenOrCreate);
				if (stream.Length > 0)
				{
					_instance._worldConfig = (BCMConfig)BFormatter.Deserialize(stream);
				}
				stream.Close();
			}
			catch (Exception arg)
			{
				Log.Error(string.Format("{0} Error in PersistentContainer.{1} while loading save config: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg));
			}
			try
			{
				Stream stream2 = File.Open(_worldPlayersFile, FileMode.OpenOrCreate);
				if (stream2.Length > 0)
				{
					_instance._worldPlayersData = (BCMWorldPlayersData)BFormatter.Deserialize(stream2);
				}
				stream2.Close();
			}
			catch (Exception arg2)
			{
				Log.Error(string.Format("{0} Error in PersistentContainer.{1} while loading world players data: {2}", "(BCM)", MethodBase.GetCurrentMethod().Name, arg2));
			}
		}
	}
}
