using System;
using System.Collections.Generic;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMServerPlayersConfig
	{
		private readonly Dictionary<string, BCMServerPlayerConfig> _players = new Dictionary<string, BCMServerPlayerConfig>();

		public BCMServerPlayerConfig this[string steamId, bool create]
		{
			get
			{
				if (string.IsNullOrEmpty(steamId))
				{
					return null;
				}
				if (_players.ContainsKey(steamId))
				{
					return _players[steamId];
				}
				if (!create || steamId.Length != 17)
				{
					return null;
				}
				Log.Out("(BCM) Created new server player config entry for ID: " + steamId);
				BCMServerPlayerConfig bCMServerPlayerConfig = new BCMServerPlayerConfig(steamId);
				_players.Add(steamId, bCMServerPlayerConfig);
				return bCMServerPlayerConfig;
			}
		}

		public int Count => _players.Count;

		public IEnumerable<BCMServerPlayerConfig> All()
		{
			return _players.Values;
		}
	}
}
