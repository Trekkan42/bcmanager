using System;
using System.Collections.Generic;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMWorldPlayersData
	{
		private readonly Dictionary<string, BCMWorldPlayerData> _players = new Dictionary<string, BCMWorldPlayerData>();

		public BCMWorldPlayerData this[string steamId, bool create]
		{
			get
			{
				if (string.IsNullOrEmpty(steamId))
				{
					return null;
				}
				if (_players.ContainsKey(steamId))
				{
					return _players[steamId];
				}
				if (!create || steamId.Length != 17)
				{
					return null;
				}
				Log.Out("(BCM) Created new server player config entry for ID: " + steamId);
				BCMWorldPlayerData bCMWorldPlayerData = new BCMWorldPlayerData(steamId);
				_players.Add(steamId, bCMWorldPlayerData);
				return bCMWorldPlayerData;
			}
		}

		public int Count => _players.Count;

		public IEnumerable<BCMWorldPlayerData> All()
		{
			return _players.Values;
		}
	}
}
