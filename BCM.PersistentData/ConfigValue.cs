using System;
using System.Collections.Generic;

namespace BCM.PersistentData
{
	[Serializable]
	public class ConfigValue
	{
		public readonly Dictionary<string, object> Settings = new Dictionary<string, object>();

		public void SetItem(string key, object value)
		{
			if (Settings.ContainsKey(key))
			{
				Settings[key] = value;
			}
			else
			{
				Settings.Add(key, value);
			}
		}

		public object GetItem(string key, object defaultValue = null)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return Settings[key];
		}

		public bool GetItem(string key, bool defaultValue = false)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return (bool)Settings[key];
		}

		public int GetItem(string key, int defaultValue = 0)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return (int)Settings[key];
		}

		public string GetItem(string key, string defaultValue = null)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return Settings[key].ToString();
		}

		public string[] GetItem(string key, string[] defaultValue = null)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return (string[])Settings[key];
		}

		public List<string> GetItem(string key, List<string> defaultValue = null)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return (List<string>)Settings[key];
		}

		public Dictionary<string, string> GetItem(string key, Dictionary<string, string> defaultValue = null)
		{
			if (!Settings.ContainsKey(key))
			{
				Settings.Add(key, defaultValue);
			}
			return (Dictionary<string, string>)Settings[key];
		}
	}
}
