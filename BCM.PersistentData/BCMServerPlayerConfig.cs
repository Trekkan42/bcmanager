using System;
using BCM.Models;

namespace BCM.PersistentData
{
	[Serializable]
	public class BCMServerPlayerConfig
	{
		private readonly string _steamId;

		private string _name;

		private bool _chatColorFullText;

		private string _chatColor;

		private bool _muted;

		private string _overrideName;

		[NonSerialized]
		private ClientInfo _clientInfo;

		public string SteamId => _steamId;

		public string Name => _name ?? string.Empty;

		public bool ChatColorFullText
		{
			get
			{
				return _chatColorFullText;
			}
			set
			{
				_chatColorFullText = value;
			}
		}

		public string ChatColor
		{
			get
			{
				return _chatColor ?? "";
			}
			set
			{
				_chatColor = value;
			}
		}

		public bool Muted
		{
			get
			{
				return _muted;
			}
			set
			{
				_muted = value;
			}
		}

		public bool HasCustomChatName
		{
			get
			{
				if (_overrideName != Name)
				{
					return _overrideName != null;
				}
				return false;
			}
		}

		public string ChatName
		{
			get
			{
				return _overrideName ?? _clientInfo?.playerName ?? "";
			}
			set
			{
				_overrideName = value;
			}
		}

		public string ClientName => _clientInfo?.playerName;

		public BCMServerPlayerConfig(string steamId)
		{
			_steamId = steamId;
		}

		public void SetOffline(ClientInfo ci)
		{
			if (_clientInfo != null)
			{
				_clientInfo = null;
				BCMPersist.Instance.Save(BCMSaveType.ServerPlayersConfig);
			}
		}

		public void SetOnline(ClientInfo ci)
		{
			_clientInfo = ci;
			_name = ci.playerName;
			BCMPersist.Instance.Save(BCMSaveType.ServerPlayersConfig);
		}
	}
}
