using System;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCSpawn : BCCommandAbstract
	{
		public BCSpawn()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCSpawn"];
			DefaultCommands = new string[1]
			{
				"bc-spawn"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCSpawn.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "horde":
			{
				if (!GetPos(world, out var position3) || !GetGroup(out var groupName) || !GetCount(out var count2) || !GetMinMax(out var min3, out var max3) || !GetSpawnPos(position3, out var targetPos2))
				{
					break;
				}
				long ticks2 = DateTime.UtcNow.Ticks;
				int lastClassId = -1;
				for (int i = 0; i < count2; i++)
				{
					int randomFromGroup = EntityGroups.GetRandomFromGroup(groupName, ref lastClassId);
					if (!EntityClass.list.ContainsKey(randomFromGroup))
					{
						BCCommandAbstract.SendOutput($"Entity class not found '{randomFromGroup}', from group '{groupName}'");
						return;
					}
					Spawn item2 = new Spawn
					{
						EntityClassId = randomFromGroup,
						SpawnerId = ticks2,
						SpawnPos = position3,
						TargetPos = targetPos2,
						MinRange = min3,
						MaxRange = max3
					};
					EntitySpawner.SpawnQueue.Enqueue(item2);
				}
				BCCommandAbstract.SendOutput($"Spawning horde of {count2} around {position3.x} {position3.y} {position3.z}");
				if (targetPos2 != position3)
				{
					BCCommandAbstract.SendOutput($"Moving towards {targetPos2.x} {targetPos2.y} {targetPos2.z}");
				}
				break;
			}
			case "entity":
				if (BCCommandAbstract.Params.Count != 2 && BCCommandAbstract.Params.Count != 5)
				{
					BCCommandAbstract.SendOutput("Spawn entity requires an entity class name");
				}
				else
				{
					if (!GetPos(world, out var position2) || !GetMinMax(out var min2, out var max2) || !GetSpawnPos(position2, out var targetPos))
					{
						break;
					}
					long ticks = DateTime.UtcNow.Ticks;
					int hashCode = BCCommandAbstract.Params[1].GetHashCode();
					if (!EntityClass.list.ContainsKey(hashCode))
					{
						BCCommandAbstract.SendOutput("Entity class not found '" + BCCommandAbstract.Params[1] + "'");
						break;
					}
					EntitySpawner.SpawnQueue.Enqueue(new Spawn
					{
						EntityClassId = hashCode,
						SpawnerId = ticks,
						SpawnPos = position2,
						TargetPos = position2,
						MinRange = min2,
						MaxRange = max2
					});
					BCCommandAbstract.SendOutput($"Spawning entity {BCCommandAbstract.Params[1]} around {position2.x} {position2.y} {position2.z}");
					if (targetPos != position2)
					{
						BCCommandAbstract.SendOutput($"Moving towards {targetPos.x} {targetPos.y} {targetPos.z}");
					}
				}
				break;
			case "item":
			{
				if (!GetPos(world, out var position) || !GetItemValue(out var item) || !GetCount(out var count, 1) || !GetMinMax(out var min, out var max, "qual"))
				{
					break;
				}
				if (item == null || item.type == ItemValue.None.type)
				{
					BCCommandAbstract.SendOutput("Item class not found'");
					break;
				}
				ItemValue itemValue = new ItemValue(item.type, _bCreateDefaultParts: true);
				int quality = UnityEngine.Random.Range(min, max);
				if (itemValue.HasQuality)
				{
					count = 1;
					itemValue.Quality = quality;
				}
				ItemStack itemStack = new ItemStack(itemValue, count);
				GameManager.Instance.ItemDropServer(itemStack, position, Vector3.zero);
				BCCommandAbstract.SendOutput($"Spawning {count}x {itemValue.ItemClass.Name} at {position.x} {position.y} {position.z}");
				break;
			}
			default:
				BCCommandAbstract.SendOutput("Unknown Sub Command " + BCCommandAbstract.Params[0]);
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static bool GetPos(World world, out Vector3 position)
		{
			position = new Vector3(0f, 0f, 0f);
			switch (BCCommandAbstract.Params.Count)
			{
			case 5:
			{
				if (!int.TryParse(BCCommandAbstract.Params[2], out var result10) || !int.TryParse(BCCommandAbstract.Params[3], out var result11) || !int.TryParse(BCCommandAbstract.Params[4], out var result12))
				{
					BCCommandAbstract.SendOutput("Unable to parse x y z for numbers");
					return false;
				}
				position = new Vector3(result10, result11, result12);
				return true;
			}
			case 4:
			{
				if (!int.TryParse(BCCommandAbstract.Params[1], out var result7) || !int.TryParse(BCCommandAbstract.Params[2], out var result8) || !int.TryParse(BCCommandAbstract.Params[3], out var result9))
				{
					BCCommandAbstract.SendOutput("Unable to parse x y z for numbers");
					return false;
				}
				position = new Vector3(result7, result8, result9);
				return true;
			}
			default:
				if (BCCommandAbstract.Options.ContainsKey("player"))
				{
					ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Options["player"], out var _, out var _cInfo);
					if (_cInfo == null)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from remote client");
						return false;
					}
					Vector3? vector = world.Players.dict[_cInfo.entityId]?.position;
					if (!vector.HasValue)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from client entity");
						return false;
					}
					position = vector.Value;
					return true;
				}
				if (BCCommandAbstract.Options.ContainsKey("p"))
				{
					string[] array = BCCommandAbstract.Options["p"].Split(',');
					if (array.Length != 3)
					{
						BCCommandAbstract.SendOutput("Unable to get position from " + BCCommandAbstract.Options["p"] + ", incorrect number of co-ords: /p=x,y,z");
						return false;
					}
					if (!int.TryParse(array[0], out var result) || !int.TryParse(array[1], out var result2) || !int.TryParse(array[2], out var result3))
					{
						BCCommandAbstract.SendOutput("Unable to get x y z for " + BCCommandAbstract.Options["p"]);
						return false;
					}
					position = new Vector3(result, result2, result3);
					return true;
				}
				if (BCCommandAbstract.Options.ContainsKey("position"))
				{
					string[] array2 = BCCommandAbstract.Options["position"].Split(',');
					if (array2.Length != 3)
					{
						BCCommandAbstract.SendOutput("Unable to get position from '" + BCCommandAbstract.Options["position"] + "', incorrect number of co-ords: /position=x,y,z");
						return false;
					}
					if (!int.TryParse(array2[0], out var result4) || !int.TryParse(array2[1], out var result5) || !int.TryParse(array2[2], out var result6))
					{
						BCCommandAbstract.SendOutput("Unable to get x y z from '" + BCCommandAbstract.Options["position"] + "'");
						return false;
					}
					position = new Vector3(result4, result5, result6);
					return true;
				}
				if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
				{
					ClientInfo remoteClientInfo = BCCommandAbstract.SenderInfo.RemoteClientInfo;
					if (remoteClientInfo == null)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from remote client");
						return false;
					}
					Vector3? vector2 = world.Players.dict[remoteClientInfo.entityId]?.position;
					if (!vector2.HasValue)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from client entity");
						return false;
					}
					position = vector2.Value;
					return true;
				}
				return false;
			}
		}

		private static bool GetGroup(out string groupName)
		{
			groupName = "ZombiesAll";
			if (BCCommandAbstract.Options.ContainsKey("group"))
			{
				groupName = BCCommandAbstract.Options["group"];
			}
			if (EntityGroups.list.ContainsKey(groupName))
			{
				return true;
			}
			BCCommandAbstract.SendOutput("Entity group not found '" + groupName + "'");
			return false;
		}

		private static bool GetItemValue(out ItemValue item)
		{
			string text = "";
			item = null;
			if (BCCommandAbstract.Options.ContainsKey("name"))
			{
				text = BCCommandAbstract.Options["name"];
				item = ItemClass.GetItem(text, _caseInsensitive: true);
				if (ItemClass.list[item.type] != null)
				{
					return true;
				}
			}
			BCCommandAbstract.SendOutput("ItemClass not found '" + text + "'");
			return false;
		}

		private static bool GetCount(out int count, int c = 25)
		{
			count = c;
			if (!BCCommandAbstract.Options.ContainsKey("count"))
			{
				return true;
			}
			if (int.TryParse(BCCommandAbstract.Options["count"], out count))
			{
				return true;
			}
			BCCommandAbstract.SendOutput("Unable to parse count '" + BCCommandAbstract.Options["count"] + "'");
			return false;
		}

		private static bool GetMinMax(out int min, out int max, string mode = "")
		{
			bool result = true;
			min = ((mode == "qual") ? 1 : 40);
			max = ((mode == "qual") ? 6 : 60);
			if (!BCCommandAbstract.Options.ContainsKey("min") && !BCCommandAbstract.Options.ContainsKey("max"))
			{
				return true;
			}
			if (BCCommandAbstract.Options.ContainsKey("min") && !int.TryParse(BCCommandAbstract.Options["min"], out min))
			{
				BCCommandAbstract.SendOutput("Unable to parse min '" + BCCommandAbstract.Options["min"] + "'");
				result = false;
			}
			if (BCCommandAbstract.Options.ContainsKey("max") && !int.TryParse(BCCommandAbstract.Options["max"], out max))
			{
				BCCommandAbstract.SendOutput("Unable to parse max '" + BCCommandAbstract.Options["max"] + "'");
				result = false;
			}
			return result;
		}

		private static bool GetSpawnPos(Vector3 position, out Vector3 targetPos)
		{
			targetPos = position;
			if (BCCommandAbstract.Options.ContainsKey("target") || BCCommandAbstract.Options.ContainsKey("t"))
			{
				string[] array = (BCCommandAbstract.Options.ContainsKey("t") ? BCCommandAbstract.Options["t"] : BCCommandAbstract.Options["target"]).Split(',');
				if (array.Length == 3)
				{
					if (!int.TryParse(array[0], out var result) || !int.TryParse(array[1], out var result2) || !int.TryParse(array[2], out var result3))
					{
						BCCommandAbstract.SendOutput("Unable to parse target");
						return false;
					}
					targetPos = new Vector3(result, result2, result3);
				}
			}
			else if (BCCommandAbstract.Options.ContainsKey("vector") || BCCommandAbstract.Options.ContainsKey("v"))
			{
				string[] array2 = (BCCommandAbstract.Options.ContainsKey("v") ? BCCommandAbstract.Options["v"] : BCCommandAbstract.Options["vector"]).Split(',');
				if (array2.Length == 2)
				{
					if (!int.TryParse(array2[0], out var result4) || !int.TryParse(array2[1], out var result5))
					{
						BCCommandAbstract.SendOutput("Unable to parse spawnvector");
						return false;
					}
					double num = Math.Sin((double)result5 * Math.PI / 180.0) * (double)result4;
					double num2 = Math.Cos((double)result5 * Math.PI / 180.0) * (double)result4;
					targetPos = new Vector3(position.x + Mathf.Round((float)num), position.y, position.z + Mathf.Round((float)num2));
				}
			}
			return true;
		}
	}
}
