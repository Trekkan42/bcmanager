using System;
using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCVisitRegion : BCCommandAbstract
	{
		private MapVisitor _mapVisitor;

		private ClientInfo _lastSender;

		private int _hash;

		private static int completePercent;

		private string commandStr;

		private BCMTask bcmTask;

		public BCVisitRegion()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCVisitRegion"];
			DefaultCommands = new string[2]
			{
				"bc-visitregion",
				"visit"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCVisitRegion.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("stop"))
			{
				if (_mapVisitor == null)
				{
					BCCommandAbstract.SendOutput("VisitRegion not running.");
					return;
				}
				if (bcmTask != null)
				{
					bcmTask.Output = new
					{
						Message = "Visitor was stopped before it completed"
					};
					bcmTask.Command = new BCCmd
					{
						Command = commandStr
					};
					bcmTask.Status = BCMTaskStatus.Aborted;
					bcmTask.Completion = DateTime.UtcNow;
					bcmTask.Duration = bcmTask.Completion - bcmTask.Timestamp;
				}
				_mapVisitor.Stop();
				_mapVisitor = null;
				BCCommandAbstract.SendOutput("VisitRegion stopped.");
				return;
			}
			if (_mapVisitor != null && _mapVisitor.IsRunning())
			{
				BCCommandAbstract.SendOutput($"VisitRegion already running ({completePercent}%). You can stop it with \"bc-visitregion /stop\".");
				return;
			}
			if (BCCommandAbstract.Params.Count < 2)
			{
				BCCommandAbstract.SendOutput("VisitRegion isn't running. Provide some co-ords to explore some regions");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[0], out var result))
			{
				BCCommandAbstract.SendOutput("The given x1 coordinate is not a valid integer");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[1], out var result2))
			{
				BCCommandAbstract.SendOutput("The given z1 coordinate is not a valid integer");
				return;
			}
			if (result > 19 || result < -20)
			{
				BCCommandAbstract.SendOutput("Note: The given x1 coordinate is beyond the recommended range (-20 to 19)");
			}
			if (result2 > 19 || result2 < -20)
			{
				BCCommandAbstract.SendOutput("Note: The given z1 coordinate is beyond the recommended range (-20 to 19)");
			}
			completePercent = 0;
			int result3 = result;
			int result4 = result2;
			switch (BCCommandAbstract.Params.Count)
			{
			case 2:
				BCCommandAbstract.SendOutput($"Sending a visitor to region {result},{result2}");
				break;
			case 3:
			{
				if (!int.TryParse(BCCommandAbstract.Params[2], out var result5))
				{
					BCCommandAbstract.SendOutput("The given radius is not a valid integer");
				}
				if (result5 < 0)
				{
					BCCommandAbstract.SendOutput("The given radius can't be less than 0, the recommended range is 0 to 20");
					return;
				}
				if (result5 > 20)
				{
					BCCommandAbstract.SendOutput("Note: The given radius is beyond the recommended range (0 to 20)");
				}
				result -= result5;
				result2 -= result5;
				result3 += result5;
				result4 += result5;
				BCCommandAbstract.SendOutput($"Sending a visitor to regions between {result},{result2} and {result3},{result4}");
				break;
			}
			case 4:
				if (!int.TryParse(BCCommandAbstract.Params[2], out result3))
				{
					BCCommandAbstract.SendOutput("The given x2 coordinate is not a valid integer");
				}
				else if (!int.TryParse(BCCommandAbstract.Params[3], out result4))
				{
					BCCommandAbstract.SendOutput("The given z2 coordinate is not a valid integer");
				}
				if (result3 > 19 || result3 < -20)
				{
					BCCommandAbstract.SendOutput("Note: The given x2 coordinate is beyond the recommended range (-20 to 19)");
				}
				if (result4 > 19 || result4 < -20)
				{
					BCCommandAbstract.SendOutput("Note: The given z2 coordinate is beyond the recommended range (-20 to 19)");
				}
				BCCommandAbstract.SendOutput($"Sending a visitor to regions between {result},{result2} and {result3},{result4}");
				break;
			default:
				BCCommandAbstract.SendOutput("Invalid param count");
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
			_lastSender = BCCommandAbstract.SenderInfo.RemoteClientInfo;
			commandStr = "";
			if (BCCommandAbstract.Params.Count > 0 || BCCommandAbstract.Options.Count > 0)
			{
				string str = commandStr;
				string[] first = new string[1]
				{
					GetCommands()[0]
				};
				IEnumerable<string> second;
				if (BCCommandAbstract.Options.Count <= 0)
				{
					IEnumerable<string> @params = BCCommandAbstract.Params;
					second = @params;
				}
				else
				{
					second = BCCommandAbstract.Params.Concat(BCCommandAbstract.Options.Select((KeyValuePair<string, string> o) => "/" + o.Key + ((o.Value != null) ? ("=" + o.Value) : "")));
				}
				commandStr = str + string.Join(" ", first.Concat(second).ToArray());
			}
			_mapVisitor = new MapVisitor(new Vector3i(result * 512, 0, result2 * 512), new Vector3i(result3 * 512 + 511, 0, result4 * 512 + 511));
			_hash = _mapVisitor.GetHashCode();
			BCTask.AddTask("BCVisitRegion", _mapVisitor.GetHashCode());
			bcmTask = BCTask.GetTask("BCVisitRegion", _hash);
			if (bcmTask != null)
			{
				bcmTask.Command = new BCCmd
				{
					Command = commandStr
				};
				bcmTask.Output = new
				{
					Count = 0,
					Total = 0,
					Perc = 0.0,
					Time = 0.0
				};
			}
			_mapVisitor.OnVisitChunk += ReportStatus;
			_mapVisitor.OnVisitChunk += GetMapColors;
			_mapVisitor.OnVisitMapDone += ReportCompletion;
			_mapVisitor.Start();
		}

		private void ReportStatus(Chunk chunk, int count, int total, float elapsedTime)
		{
			if (bcmTask != null)
			{
				bcmTask.Output = new
				{
					Count = count,
					Total = total,
					Perc = Math.Round((double)count / (double)total * 100.0, 2),
					Time = Math.Round(elapsedTime, 2)
				};
			}
			if (count % 128 == 0)
			{
				completePercent = Mathf.RoundToInt(100f * ((float)count / (float)total));
				Log.Out($"VisitRegion ({completePercent:00}%): {count} / {total} chunks done (estimated time left {(float)(total - count) * (elapsedTime / (float)count):0.00} seconds)");
			}
		}

		private static void GetMapColors(Chunk chunk, int count, int total, float elapsedTime)
		{
			chunk.GetMapColors();
		}

		private void ReportCompletion(int total, float elapsedTime)
		{
			if (bcmTask != null)
			{
				bcmTask.Status = BCMTaskStatus.Complete;
				bcmTask.Output = $"VisitRegion done, visited {total} chunks in {elapsedTime:0.00} seconds (average {(float)total / elapsedTime:0.00} chunks/sec).";
				BCTask.DelTask("BCVisitRegion", _hash);
			}
			Log.Out($"VisitRegion done, visited {total} chunks in {elapsedTime:0.00} seconds (average {(float)total / elapsedTime:0.00} chunks/sec).");
			if (_lastSender != null)
			{
				_lastSender.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Whisper, -1, "Visit Region Completed", BCChat.ServerPM, _localizeMain: false, null));
				_lastSender = null;
			}
			_mapVisitor = null;
		}
	}
}
