using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using LitJson;

namespace BCM.Commands
{
	public class BCConfig : BCCommandAbstract
	{
		public class Editable : Attribute
		{
			public string Description;

			public Editable()
			{
			}

			public Editable(string description)
			{
				Description = description;
			}
		}

		public static class SubCommand
		{
			public const string ResetAll = "resetall";

			public const string Command = "command";

			public const string DefaultOpts = "option";

			public const string Events = "event";
		}

		public BCConfig()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCConfig"];
			DefaultCommands = new string[1]
			{
				"bc-config"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCConfig.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "resetall":
				BCCommandAbstract.SendOutput("Resetting command aliases and options for all BCM commands. Reboot the server for the changes to take effect.");
				foreach (BCCommandAbstract item in SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommands().OfType<BCCommandAbstract>())
				{
					item.Commands = item.GetDefaults();
					item.BaseOptions = item.DefaultOptions;
					string name4 = item.GetType().Name;
					BCMPersist.Instance.ServerConfig[name4].SetItem("Commands", item.GetCommands());
					BCMPersist.Instance.ServerConfig[name4].SetItem("BaseOptions", item.BaseOptions);
				}
				BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
				break;
			case "command":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("A command and action are required.");
					break;
				}
				BCCommandAbstract bCCommandAbstract = SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommand(BCCommandAbstract.Params[1], _alreadyTokenized: true) as BCCommandAbstract;
				if (bCCommandAbstract == null)
				{
					BCCommandAbstract.SendOutput("Unable to find BCM command " + BCCommandAbstract.Params[1]);
					break;
				}
				switch (BCCommandAbstract.Params[2])
				{
				case "list":
					BCCommandAbstract.SendOutput("Command aliases for " + bCCommandAbstract.GetType().Name + ":");
					BCCommandAbstract.SendOutput(string.Join(", ", bCCommandAbstract.GetCommands()));
					break;
				case "add":
				{
					if (SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommand(BCCommandAbstract.Params[3], _alreadyTokenized: true) != null)
					{
						BCCommandAbstract.SendOutput("Command alias already in use " + BCCommandAbstract.Params[3]);
						break;
					}
					List<string> list5 = bCCommandAbstract.GetCommands().ToList();
					list5.Add(BCCommandAbstract.Params[3]);
					bCCommandAbstract.Commands = list5.ToArray();
					string name3 = bCCommandAbstract.GetType().Name;
					BCCommandAbstract.SendOutput("Adding command alias " + BCCommandAbstract.Params[3] + " to command " + name3 + ". Reboot the server for the changes to take effect.");
					BCMPersist.Instance.ServerConfig[name3].SetItem("Commands", bCCommandAbstract.GetCommands());
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				case "remove":
				{
					List<string> list4 = bCCommandAbstract.GetCommands().ToList();
					list4.Remove(BCCommandAbstract.Params[3]);
					if (list4.Count < 1)
					{
						BCCommandAbstract.SendOutput("Commands must have at least one alias");
						break;
					}
					bCCommandAbstract.Commands = list4.ToArray();
					string name2 = bCCommandAbstract.GetType().Name;
					BCCommandAbstract.SendOutput("Removing command alias " + BCCommandAbstract.Params[3] + " from command " + name2 + ". Reboot the server for the changes to take effect.");
					BCMPersist.Instance.ServerConfig[name2].SetItem("Commands", bCCommandAbstract.GetCommands());
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				case "reset":
				{
					bCCommandAbstract.Commands = bCCommandAbstract.GetDefaults();
					string name = bCCommandAbstract.GetType().Name;
					BCCommandAbstract.SendOutput("Resetting command aliases for " + name + ". Reboot the server for the changes to take effect.");
					BCMPersist.Instance.ServerConfig[name].SetItem("Commands", bCCommandAbstract.GetCommands());
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				default:
					BCCommandAbstract.SendOutput("Unknown action " + BCCommandAbstract.Params[3]);
					break;
				}
				break;
			}
			case "option":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("A command and action are required.");
					break;
				}
				BCCommandAbstract bCCommandAbstract2 = SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommand(BCCommandAbstract.Params[1], _alreadyTokenized: true) as BCCommandAbstract;
				if (bCCommandAbstract2 == null)
				{
					BCCommandAbstract.SendOutput("Unable to find BCM command " + BCCommandAbstract.Params[1]);
					break;
				}
				switch (BCCommandAbstract.Params[2])
				{
				case "list":
					BCCommandAbstract.SendOutput("Command default options for " + bCCommandAbstract2.GetType().Name + ":");
					BCCommandAbstract.SendOutput((bCCommandAbstract2.BaseOptions != null) ? string.Join(" ", bCCommandAbstract2.BaseOptions.Select((KeyValuePair<string, string> kvp) => (kvp.Value == null) ? ("/" + kvp.Key) : ("/" + kvp.Key + "=" + kvp.Value)).ToArray()) : "None");
					break;
				case "add":
				{
					Dictionary<string, string> dictionary2 = bCCommandAbstract2.BaseOptions ?? new Dictionary<string, string>();
					string text4 = ((BCCommandAbstract.Params.Count < 4 && BCCommandAbstract.Options.Count > 0) ? (BCCommandAbstract.Options.First().Key + ((BCCommandAbstract.Options.First().Value != null) ? ("=" + BCCommandAbstract.Options.First().Value) : "")) : BCCommandAbstract.Params[3]);
					text4 = (text4.StartsWith("/") ? text4.Substring(1) : text4);
					string text5 = null;
					if (text4.Contains("="))
					{
						string[] array4 = text4.Split('=');
						text4 = array4[0];
						text5 = array4[1];
					}
					string name7 = bCCommandAbstract2.GetType().Name;
					if (dictionary2.ContainsKey(text4))
					{
						BCCommandAbstract.SendOutput("Updating default option /" + text4 + ((text5 != null) ? ("=" + text5) : "") + " for command " + name7 + ".");
						dictionary2[text4] = text5;
					}
					else
					{
						BCCommandAbstract.SendOutput("Adding default option /" + text4 + ((text5 != null) ? ("=" + text5) : "") + " to command " + name7 + ".");
						dictionary2.Add(text4, text5);
					}
					bCCommandAbstract2.BaseOptions = dictionary2;
					BCMPersist.Instance.ServerConfig[name7].SetItem("BaseOptions", bCCommandAbstract2.BaseOptions);
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				case "remove":
				{
					Dictionary<string, string> dictionary = bCCommandAbstract2.BaseOptions ?? new Dictionary<string, string>();
					string text3 = ((BCCommandAbstract.Params.Count < 4 && BCCommandAbstract.Options.Count > 0) ? BCCommandAbstract.Options.First().Key : BCCommandAbstract.Params[3]);
					text3 = (text3.StartsWith("/") ? text3.Substring(1) : text3);
					if (text3.Contains("="))
					{
						string[] array3 = text3.Split('=');
						dictionary.Remove(array3[0]);
					}
					else
					{
						dictionary.Remove(text3);
					}
					bCCommandAbstract2.BaseOptions = ((dictionary.Count == 0) ? null : dictionary);
					string name6 = bCCommandAbstract2.GetType().Name;
					BCCommandAbstract.SendOutput("Removing default option " + text3 + " from command " + name6 + ".");
					BCMPersist.Instance.ServerConfig[name6].SetItem("BaseOptions", bCCommandAbstract2.BaseOptions);
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				case "reset":
				{
					bCCommandAbstract2.BaseOptions = bCCommandAbstract2.DefaultOptions;
					string name5 = bCCommandAbstract2.GetType().Name;
					BCCommandAbstract.SendOutput("Resetting default options for " + name5 + ".");
					BCMPersist.Instance.ServerConfig[name5].SetItem("BaseOptions", bCCommandAbstract2.BaseOptions);
					BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
					break;
				}
				default:
					BCCommandAbstract.SendOutput("Unknown action " + BCCommandAbstract.Params[2]);
					break;
				}
				break;
			}
			case "event":
				if (BCCommandAbstract.Params.Count < 2)
				{
					BCCommandAbstract.SendOutput("Incorrect arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					break;
				}
				switch (BCCommandAbstract.Params[1])
				{
				case "list":
					foreach (KeyValuePair<string, INeuron> neuron in AIEvents.NeuronList)
					{
						FieldInfo[] fields = neuron.Value.GetType().GetFields();
						foreach (FieldInfo fieldInfo4 in fields)
						{
							foreach (Editable item2 in Attribute.GetCustomAttributes(fieldInfo4).OfType<Editable>())
							{
								BCCommandAbstract.SendOutput(neuron.Key + "." + fieldInfo4.Name + ":" + fieldInfo4.FieldType.Name + " - " + item2.Description);
							}
						}
					}
					break;
				case "values":
					foreach (KeyValuePair<string, INeuron> neuron2 in AIEvents.NeuronList)
					{
						FieldInfo[] fields = neuron2.Value.GetType().GetFields();
						foreach (FieldInfo fieldInfo5 in fields)
						{
							if (!Attribute.GetCustomAttributes(fieldInfo5).OfType<Editable>().Any())
							{
								continue;
							}
							object obj2 = fieldInfo5.GetValue(null);
							if (fieldInfo5.FieldType.Name.StartsWith("List") || fieldInfo5.FieldType.Name.StartsWith("Dictionary"))
							{
								JsonWriter jsonWriter2 = new JsonWriter();
								JsonMapper.RegisterExporter(delegate(float o, JsonWriter w)
								{
									w.Write(Convert.ToDouble(o));
								});
								JsonMapper.ToJson(obj2, jsonWriter2);
								obj2 = jsonWriter2.ToString().TrimStart();
							}
							BCCommandAbstract.SendOutput($"{neuron2.Key}.{fieldInfo5.Name}:{fieldInfo5.FieldType.Name} = {obj2}");
						}
					}
					break;
				default:
					if (BCCommandAbstract.Params.Count == 2)
					{
						if (!BCCommandAbstract.Params[1].Contains("."))
						{
							AIEvents.NeuronList.TryGetValue(BCCommandAbstract.Params[1], out var value);
							if (value == null)
							{
								BCCommandAbstract.SendOutput("Neuron not found " + BCCommandAbstract.Params[1]);
								break;
							}
							FieldInfo[] fields = value.GetType().GetFields();
							foreach (FieldInfo fieldInfo in fields)
							{
								foreach (Editable item3 in Attribute.GetCustomAttributes(fieldInfo).OfType<Editable>())
								{
									BCCommandAbstract.SendOutput(fieldInfo.Name + ":" + fieldInfo.FieldType.Name + " - " + item3.Description);
								}
							}
							break;
						}
						if (BCCommandAbstract.Params[1].Contains("."))
						{
							string[] array = BCCommandAbstract.Params[1].Split('.');
							string text = array[0];
							string fieldName2 = array[1];
							AIEvents.NeuronList.TryGetValue(text, out var value2);
							if (value2 == null)
							{
								BCCommandAbstract.SendOutput("Neuron not found " + text);
								break;
							}
							FieldInfo fieldInfo2 = value2.GetType().GetFields().FirstOrDefault((FieldInfo f) => f.Name == fieldName2);
							if (fieldInfo2 != null && Attribute.GetCustomAttributes(fieldInfo2).OfType<Editable>().Any())
							{
								object obj = fieldInfo2.GetValue(null);
								if (fieldInfo2.FieldType.Name.StartsWith("List") || fieldInfo2.FieldType.Name.StartsWith("Dictionary"))
								{
									JsonWriter jsonWriter = new JsonWriter();
									JsonMapper.RegisterExporter(delegate(float o, JsonWriter w)
									{
										w.Write(Convert.ToDouble(o));
									});
									JsonMapper.ToJson(obj, jsonWriter);
									obj = jsonWriter.ToString().TrimStart();
								}
								BCCommandAbstract.SendOutput($"{fieldInfo2.Name}:{fieldInfo2.FieldType.Name} - {obj}");
							}
							else
							{
								BCCommandAbstract.SendOutput("Editable field " + fieldName2 + " not found");
							}
							break;
						}
					}
					if (BCCommandAbstract.Params.Count > 2 && BCCommandAbstract.Params[1].Contains("."))
					{
						string[] array2 = BCCommandAbstract.Params[1].Split('.');
						string text2 = array2[0];
						string fieldName = array2[1];
						AIEvents.NeuronList.TryGetValue(text2, out var value3);
						if (value3 == null)
						{
							BCCommandAbstract.SendOutput("Neuron not found " + text2);
							break;
						}
						FieldInfo fieldInfo3 = value3.GetType().GetFields().FirstOrDefault((FieldInfo f) => f.Name == fieldName);
						if (fieldInfo3 == null || !Attribute.GetCustomAttributes(fieldInfo3).OfType<Editable>().Any())
						{
							BCCommandAbstract.SendOutput("Editable field " + fieldName + " not found");
							break;
						}
						switch (fieldInfo3.FieldType.Name)
						{
						case "String":
						{
							string value4 = BCCommandAbstract.Params[2];
							fieldInfo3.SetValue(null, value4);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, value4);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Boolean":
						{
							if (!bool.TryParse(BCCommandAbstract.Params[2], out var result9))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a boolean for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result9);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result9);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Byte":
						{
							if (!byte.TryParse(BCCommandAbstract.Params[2], out var result7))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a short integer for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result7);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result7);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Int16":
						{
							if (!short.TryParse(BCCommandAbstract.Params[2], out var result6))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a short integer for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result6);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result6);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Int32":
						{
							if (!int.TryParse(BCCommandAbstract.Params[2], out var result10))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a integer for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result10);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result10);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Int64":
						{
							if (!long.TryParse(BCCommandAbstract.Params[2], out var result5))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a long integer for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result5);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result5);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Single":
						{
							if (!float.TryParse(BCCommandAbstract.Params[2], out var result11))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a float for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result11);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result11);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						case "Double":
						{
							if (!double.TryParse(BCCommandAbstract.Params[2], out var result8))
							{
								BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a double for " + fieldName);
								break;
							}
							fieldInfo3.SetValue(null, result8);
							BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, result8);
							BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
							break;
						}
						default:
							if (fieldInfo3.FieldType.Name.StartsWith("List"))
							{
								if (BCCommandAbstract.Params.Count < 3)
								{
									BCCommandAbstract.SendOutput("Incorrect Params");
									break;
								}
								switch (fieldInfo3.FieldType.GetGenericArguments()[0].Name)
								{
								case "Boolean":
								{
									List<bool> list2 = new List<bool>();
									switch (BCCommandAbstract.Params[2])
									{
									case "add":
									{
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list2 = (fieldInfo3.GetValue(null) as List<bool>) ?? new List<bool>();
										if (!bool.TryParse(BCCommandAbstract.Params[3], out var result2))
										{
											BCCommandAbstract.SendOutput("Couldn't parse " + BCCommandAbstract.Params[3] + " as a boolean");
											return;
										}
										list2.Add(result2);
										BCCommandAbstract.SendOutput("Added item " + BCCommandAbstract.Params[3] + " to the list");
										break;
									}
									case "remove":
									{
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list2 = (fieldInfo3.GetValue(null) as List<bool>) ?? new List<bool>();
										if (!bool.TryParse(BCCommandAbstract.Params[3], out var result))
										{
											BCCommandAbstract.SendOutput("Couldn't parse " + BCCommandAbstract.Params[3] + " as a boolean");
											return;
										}
										if (list2.Contains(result))
										{
											list2.Remove(result);
											BCCommandAbstract.SendOutput("Removed item " + BCCommandAbstract.Params[3] + " from list");
										}
										else
										{
											BCCommandAbstract.SendOutput("Item not found in the list");
										}
										break;
									}
									default:
										BCCommandAbstract.SendOutput("Unknown option for a list " + BCCommandAbstract.Params[2] + ", use clear, add or remove");
										return;
									case "clear":
										break;
									}
									BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, list2);
									BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
									break;
								}
								case "Int32":
								{
									List<int> list3 = new List<int>();
									switch (BCCommandAbstract.Params[2])
									{
									case "add":
									{
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list3 = (fieldInfo3.GetValue(null) as List<int>) ?? new List<int>();
										if (!int.TryParse(BCCommandAbstract.Params[3], out var result4))
										{
											BCCommandAbstract.SendOutput("Couldn't parse " + BCCommandAbstract.Params[3] + " as an integer");
											return;
										}
										list3.Add(result4);
										BCCommandAbstract.SendOutput("Added item " + BCCommandAbstract.Params[3] + " to the list");
										break;
									}
									case "remove":
									{
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list3 = (fieldInfo3.GetValue(null) as List<int>) ?? new List<int>();
										if (!int.TryParse(BCCommandAbstract.Params[3], out var result3))
										{
											BCCommandAbstract.SendOutput("Couldn't parse " + BCCommandAbstract.Params[3] + " as an integer");
											return;
										}
										if (list3.Contains(result3))
										{
											list3.Remove(result3);
											BCCommandAbstract.SendOutput("Removed item " + BCCommandAbstract.Params[3] + " from list");
										}
										else
										{
											BCCommandAbstract.SendOutput("Item not found in the list");
										}
										break;
									}
									default:
										BCCommandAbstract.SendOutput("Unknown option for a list " + BCCommandAbstract.Params[2] + ", use clear, add or remove");
										return;
									case "clear":
										break;
									}
									BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, list3);
									BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
									break;
								}
								case "String":
								{
									List<string> list = new List<string>();
									switch (BCCommandAbstract.Params[2])
									{
									case "add":
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list = (fieldInfo3.GetValue(null) as List<string>) ?? new List<string>();
										list.Add(BCCommandAbstract.Params[3]);
										BCCommandAbstract.SendOutput("Added item " + BCCommandAbstract.Params[3] + " to the list");
										break;
									case "remove":
										if (BCCommandAbstract.Params.Count < 4)
										{
											BCCommandAbstract.SendOutput("Incorrect Params");
											return;
										}
										list = (fieldInfo3.GetValue(null) as List<string>) ?? new List<string>();
										if (list.Contains(BCCommandAbstract.Params[3]))
										{
											list.Remove(BCCommandAbstract.Params[3]);
											BCCommandAbstract.SendOutput("Removed item " + BCCommandAbstract.Params[3] + " from list");
										}
										else
										{
											BCCommandAbstract.SendOutput("Item not found in the list");
										}
										break;
									default:
										BCCommandAbstract.SendOutput("Unknown option for a list " + BCCommandAbstract.Params[2] + ", use clear, add or remove");
										return;
									case "clear":
										break;
									}
									BCMPersist.Instance.ServerConfig[text2].SetItem(fieldInfo3.Name, list);
									BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
									break;
								}
								default:
									BCCommandAbstract.SendOutput("Unknown list member type.");
									break;
								}
							}
							else
							{
								BCCommandAbstract.SendOutput("Unable to determine type of " + fieldName);
							}
							break;
						}
					}
					else
					{
						BCCommandAbstract.SendOutput("Incorrect params.");
						BCCommandAbstract.SendOutput(GetHelp());
					}
					break;
				}
				break;
			default:
				BCCommandAbstract.SendOutput("Unknown option " + BCCommandAbstract.Params[0]);
				break;
			}
		}
	}
}
