using System;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCGiveItemToPlayer : BCCommandAbstract
	{
		public BCGiveItemToPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGiveItemToPlayer"];
			DefaultCommands = new string[1]
			{
				"bc-give"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCGiveItemToPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else
			{
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _, out var _cInfo))
				{
					return;
				}
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				int result = 1;
				int result2 = 6;
				if (BCCommandAbstract.Options.ContainsKey("q"))
				{
					if (int.TryParse(BCCommandAbstract.Options["q"], out var result3))
					{
						result = result3;
						result2 = result3;
					}
				}
				else
				{
					if (BCCommandAbstract.Options.ContainsKey("min") && !int.TryParse(BCCommandAbstract.Options["min"], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse min as a number");
						return;
					}
					if (BCCommandAbstract.Options.ContainsKey("max") && !int.TryParse(BCCommandAbstract.Options["max"], out result2))
					{
						BCCommandAbstract.SendOutput("Unable to parse max as a number");
						return;
					}
				}
				if (result2 > 6)
				{
					BCCommandAbstract.SendOutput($"Warning: max quality is capped at the constants max quality of {6}");
				}
				result = Math.Min(Math.Max(result, 1), 6);
				result2 = Math.Max(Math.Min(result2, 6), 1);
				bool bCreateDefaultModItems = BCCommandAbstract.Options.ContainsKey("mods");
				ItemValue itemValue;
				if (int.TryParse(BCCommandAbstract.Params[1], out var result4))
				{
					itemValue = ((ItemClass.list[result4] == null) ? ItemValue.None : new ItemValue(result4, result, result2, bCreateDefaultModItems));
				}
				else
				{
					ItemValue item = ItemClass.GetItem(BCCommandAbstract.Params[1], _caseInsensitive: true);
					if (item.Equals(ItemValue.None))
					{
						BCCommandAbstract.SendOutput("Unable to find item '" + BCCommandAbstract.Params[1] + "'");
						return;
					}
					itemValue = new ItemValue(item.type, result, result2, bCreateDefaultModItems);
				}
				if (itemValue.Equals(ItemValue.None))
				{
					BCCommandAbstract.SendOutput("Unable to find item '" + BCCommandAbstract.Params[1] + "'");
					return;
				}
				int result5 = 1;
				if (BCCommandAbstract.Options.ContainsKey("c"))
				{
					int.TryParse(BCCommandAbstract.Options["c"], out result5);
				}
				if (BCCommandAbstract.Options.ContainsKey("count"))
				{
					int.TryParse(BCCommandAbstract.Options["count"], out result5);
				}
				if (result5 > 32767)
				{
					BCCommandAbstract.SendOutput($"Count can not be larger than {short.MaxValue}");
					return;
				}
				int entityId = _cInfo.entityId;
				if (!world.Players.dict.ContainsKey(entityId) || !world.Players.dict[entityId].IsSpawned())
				{
					BCCommandAbstract.SendOutput("Player not spawned");
					return;
				}
				EntityItem entityItem = (EntityItem)EntityFactory.CreateEntity(new EntityCreationData
				{
					entityClass = EntityClass.FromString("item"),
					id = EntityFactory.nextEntityID++,
					itemStack = new ItemStack(itemValue, result5),
					pos = world.Players.dict[entityId].position,
					rot = new Vector3(20f, 0f, 20f),
					lifetime = 60f,
					belongsPlayerId = entityId
				});
				world.SpawnEntityInWorld(entityItem);
				_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageEntityCollect>().Setup(entityItem.entityId, entityId));
				world.RemoveEntity(entityItem.entityId, EnumRemoveEntityReason.Killed);
				BCCommandAbstract.SendOutput($"Gave {result5}x {itemValue.ItemClass.GetLocalizedItemName() ?? itemValue.ItemClass.Name} to player: {_cInfo.playerName}");
				if (!BCCommandAbstract.Options.ContainsKey("silent"))
				{
					_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageChat>().Setup(EChatType.Whisper, -1, $"{result5}x {itemValue.ItemClass.GetLocalizedItemName() ?? itemValue.ItemClass.Name} received. If your bag is full check the ground.", BCChat.ServerPM, _localizeMain: false, null));
				}
			}
		}
	}
}
