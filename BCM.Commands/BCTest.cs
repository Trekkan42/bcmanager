using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCTest : BCCommandAbstract
	{
		public BCTest()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCTest"];
			DefaultCommands = new string[1]
			{
				"bc-test"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCTest.ParseHelp(this);
		}

		protected override void Process()
		{
			BCCommandAbstract.SendOutput("Warning: This is not a warning!");
		}
	}
}
