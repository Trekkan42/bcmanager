using System;
using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCUndo : BCCommandAbstract
	{
		public static readonly Dictionary<int, List<BCMPrefabCache>> _undoCache = new Dictionary<int, List<BCMPrefabCache>>();

		public BCUndo()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCUndo"];
			DefaultCommands = new string[1]
			{
				"bc-undo"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCUndo.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld(out var world))
			{
				EntityPlayer sender = null;
				if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
				{
					sender = world.Entities.dict[BCCommandAbstract.SenderInfo.RemoteClientInfo.entityId] as EntityPlayer;
				}
				BCCommandAbstract.SendOutput(UndoSetBlocks(sender) ? "Undoing previous world editing command" : "Undo failed, nothing to undo?");
			}
		}

		public static void CreateUndo(EntityPlayer sender, Vector3i pos, Vector3i size)
		{
			string text = "_server";
			if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
			{
				text = BCCommandAbstract.SenderInfo.RemoteClientInfo.ownerId;
			}
			int key = 0;
			Prefab prefab = BCExport.CopyFromWorld(GameManager.Instance.World, pos, size);
			if (sender != null)
			{
				key = sender.entityId;
			}
			string text2 = $"{text}.{pos.x}.{pos.y}.{pos.z}.{DateTime.UtcNow.ToFileTime()}";
			prefab.Save(text2);
			if (_undoCache.ContainsKey(key))
			{
				_undoCache[key].Add(new BCMPrefabCache(text2, pos));
				return;
			}
			_undoCache.Add(key, new List<BCMPrefabCache>
			{
				new BCMPrefabCache(text2, pos)
			});
		}

		public static bool UndoSetBlocks(EntityPlayer sender)
		{
			int key = 0;
			if (sender != null)
			{
				key = sender.entityId;
			}
			if (!_undoCache.ContainsKey(key))
			{
				return false;
			}
			if (_undoCache[key].Count <= 0)
			{
				return false;
			}
			BCMPrefabCache bCMPrefabCache = _undoCache[key][_undoCache[key].Count - 1];
			if (bCMPrefabCache != null)
			{
				Prefab prefab = new Prefab();
				prefab.Load(bCMPrefabCache.Filename);
				BCImport.InsertPrefab(GameManager.Instance.World, prefab, bCMPrefabCache.Pos, 0);
				string str = BCMPersist.UndoCacheDir + bCMPrefabCache.Filename;
				if (Utils.FileExists(str + ".tts"))
				{
					Utils.FileDelete(str + ".tts");
				}
				if (Utils.FileExists(str + ".xml"))
				{
					Utils.FileDelete(str + ".xml");
				}
			}
			_undoCache[key].RemoveAt(_undoCache[key].Count - 1);
			return true;
		}
	}
}
