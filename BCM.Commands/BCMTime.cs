using System;
using System.Collections.Generic;
using SystemInformation;
using UnityEngine;

namespace BCM.Commands
{
	public class BCMTime
	{
		public readonly Dictionary<string, int> Time = new Dictionary<string, int>
		{
			{
				"D",
				0
			},
			{
				"H",
				0
			},
			{
				"M",
				0
			}
		};

		public double Ticks;

		public double Fps;

		public int Clients;

		public int Entities;

		public int EntityInst;

		public int Players;

		public int Enemies;

		public int Observers;

		public int Chunks;

		public int ChunkInst;

		public int Objects;

		public int Items;

		public double Heap;

		public double Max;

		public double Rss;

		public BCMTime(IDictionary<string, string> options)
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			Time = new Dictionary<string, int>
			{
				{
					"D",
					GameUtils.WorldTimeToDays(world?.worldTime ?? 0)
				},
				{
					"H",
					GameUtils.WorldTimeToHours(world?.worldTime ?? 0)
				},
				{
					"M",
					GameUtils.WorldTimeToMinutes(world?.worldTime ?? 0)
				}
			};
			if (!options.ContainsKey("t") && !options.ContainsKey("s") && world != null)
			{
				Ticks = Math.Round(UnityEngine.Time.timeSinceLevelLoad, 2);
				Fps = Math.Round(GameManager.Instance.fps.Counter, 2);
				Clients = SingletonMonoBehaviour<ConnectionManager>.Instance.ClientCount();
				Entities = world.Entities.Count;
				EntityInst = Entity.InstanceCount;
				Players = world.Players.list.Count;
				Enemies = GameStats.GetInt(EnumGameStats.EnemyCount);
				Observers = world.m_ChunkManager.m_ObservedEntities.Count;
				Chunks = world.ChunkClusters[0].Count();
				ChunkInst = Chunk.InstanceCount;
				Objects = world.m_ChunkManager.GetDisplayedChunkGameObjectsCount();
				Items = EntityItem.ItemInstanceCount;
				if (options.ContainsKey("mem"))
				{
					long totalMemory = GC.GetTotalMemory(forceFullCollection: false);
					Heap = Math.Floor((float)totalMemory / 1048576f);
					Max = Math.Floor((float)GameManager.MaxMemoryConsumption / 1048576f);
					Rss = Math.Floor((float)GetRSS.GetCurrentRSS() / 1048576f);
				}
			}
		}
	}
}
