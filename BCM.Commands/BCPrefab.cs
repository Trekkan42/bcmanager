using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using JetBrains.Annotations;

namespace BCM.Commands
{
	public class BCPrefab : BCCommandAbstract
	{
		private static readonly char _s = Path.DirectorySeparatorChar;

		private static readonly string prefabDir = Utils.GetGameDir($"Data{_s}Prefabs{_s}");

		public BCPrefab()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPrefab"];
			DefaultCommands = new string[1]
			{
				"bc-prefab"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPrefab.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Params.Count < 2 && (BCCommandAbstract.Params.Count == 0 || BCCommandAbstract.Params[0] != "list"))
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "list":
				ListPrefabs();
				break;
			case "create":
				CreatePrefab();
				break;
			case "clone":
				ClonePrefab();
				break;
			case "backup":
				BackupPrefab();
				break;
			case "restore":
				RestorePrefab();
				break;
			case "delete":
				DeletePrefab();
				break;
			case "setprop":
				SetProperty();
				break;
			case "insert":
				InsertLayers();
				break;
			case "cut":
				CutLayers();
				break;
			case "add":
				AddLayers();
				break;
			case "trim":
				TrimLayers();
				break;
			case "setsize":
				SetSize();
				break;
			case "swap":
				SwapBlocks();
				break;
			case "swapmap":
				SwapMap();
				break;
			case "copylayer":
				CopyLayer();
				break;
			case "swaplayer":
				SwapLayer();
				break;
			case "setblock":
				SetBlock();
				break;
			case "repair":
				RepairBlocks();
				break;
			case "lock":
				LockSecureBlocks();
				break;
			case "dim":
				GetDimensions();
				break;
			case "blockcount":
				GetBlockCounts();
				break;
			case "blocks":
				GetBlocks();
				break;
			case "paint":
				GetPaint();
				break;
			case "type":
				GetBlocks("type");
				break;
			case "density":
				GetBlocks("density");
				break;
			case "rotation":
				GetBlocks("rotation");
				break;
			case "damage":
				GetBlocks("damage");
				break;
			case "meta":
				GetBlocks("meta");
				break;
			case "meta2":
				GetBlocks("meta2");
				break;
			case "meta3":
				GetBlocks("meta3");
				break;
			case "stats":
				GetStats();
				break;
			case "xml":
				GetXml();
				break;
			default:
				BCCommandAbstract.SendOutput("Unknown sub command " + BCCommandAbstract.Params[0]);
				break;
			}
		}

		[CanBeNull]
		private static Prefab GetPrefab()
		{
			Prefab prefab = new Prefab();
			if (File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts") && prefab.Load(BCCommandAbstract.Params[1]))
			{
				return prefab;
			}
			BCCommandAbstract.SendOutput("Unable to load prefab " + BCCommandAbstract.Params[1]);
			return null;
		}

		[NotNull]
		private static string GetFaceTex(long n, int f)
		{
			return $"{(n >> f * 8) & 0xFF}";
		}

		[NotNull]
		public static string GetTexStr(long n)
		{
			string[] array = new string[6];
			bool flag = true;
			for (int i = 0; i < 6; i++)
			{
				array[i] = GetFaceTex(n, i);
				if (array[0] != array[i])
				{
					flag = false;
				}
			}
			if (!flag)
			{
				return string.Join(",", array);
			}
			return array[0];
		}

		[NotNull]
		private static string GetFilename(string file)
		{
			int num = file.LastIndexOf(_s);
			if (num >= 0)
			{
				file = file.Substring(num + 1);
			}
			return file.Replace(".tts", "");
		}

		[NotNull]
		private static Dictionary<string, object> ReadBinaryFile(string filepath, bool sizeOnly = false)
		{
			BinaryReader binaryReader = null;
			Dictionary<string, object> result = new Dictionary<string, object>();
			try
			{
				binaryReader = new BinaryReader(File.OpenRead(filepath));
				if (binaryReader.ReadChar() == 't' && binaryReader.ReadChar() == 't' && binaryReader.ReadChar() == 's' && binaryReader.ReadChar() == '\0')
				{
					uint version = binaryReader.ReadUInt32();
					result = ReadPrefab(binaryReader, version, sizeOnly);
				}
			}
			catch (Exception e)
			{
				BCCommandAbstract.SendOutput("Error reading file");
				Log.Exception(e);
			}
			binaryReader?.Close();
			return result;
		}

		[NotNull]
		private static Dictionary<string, object> ReadPrefab(BinaryReader binaryReader, uint version, bool sizeOnly)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			Vector3i vector3i = default(Vector3i);
			vector3i.x = binaryReader.ReadInt16();
			vector3i.y = binaryReader.ReadInt16();
			vector3i.z = binaryReader.ReadInt16();
			Vector3i vector3i2 = vector3i;
			dictionary.Add("Size", vector3i2);
			if (sizeOnly)
			{
				return dictionary;
			}
			if (version >= 2 && version < 7)
			{
				binaryReader.ReadBoolean();
			}
			if (version >= 3 && version < 7)
			{
				binaryReader.ReadBoolean();
			}
			BCMBlockValue[] array = new BCMBlockValue[vector3i2.x * vector3i2.y * vector3i2.z];
			if (version <= 4)
			{
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new BCMBlockValue(binaryReader.ReadUInt32());
				}
			}
			else
			{
				for (int j = 0; j < array.Length; j++)
				{
					array[j] = new BCMBlockValue(binaryReader.ReadUInt32());
				}
			}
			dictionary.Add("Blocks", array);
			byte[] value = ((version <= 4) ? new byte[array.Length] : binaryReader.ReadBytes(array.Length));
			dictionary.Add("Density", value);
			ushort[] array2 = new ushort[array.Length];
			if (version > 8)
			{
				byte[] array3 = binaryReader.ReadBytes(array.Length * 2);
				for (int k = 0; k < array.Length; k++)
				{
					array2[k] = (ushort)(array3[k * 2] | (array3[k * 2 + 1] << 8));
				}
			}
			dictionary.Add("Damage", array2);
			return dictionary;
		}

		private static void CreatePrefab()
		{
			if (BCCommandAbstract.Params.Count != 2 && BCCommandAbstract.Params.Count != 5)
			{
				BCCommandAbstract.SendOutput("Incorrect params for create");
				return;
			}
			if (File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				BCCommandAbstract.SendOutput("A prefab with the name " + BCCommandAbstract.Params[1] + " already exists");
				return;
			}
			int result = 1;
			int result2 = 1;
			int result3 = 1;
			if (BCCommandAbstract.Params.Count == 5 && (!int.TryParse(BCCommandAbstract.Params[2], out result) || !int.TryParse(BCCommandAbstract.Params[3], out result2) || !int.TryParse(BCCommandAbstract.Params[4], out result3)))
			{
				BCCommandAbstract.SendOutput("Unable to parse prefab size");
				return;
			}
			Prefab prefab = new Prefab(new Vector3i(result, result2, result3));
			prefab.bCopyAirBlocks = true;
			prefab.PrefabName = BCCommandAbstract.Params[1];
			prefab.Save(BCCommandAbstract.Params[1]);
			BCCommandAbstract.SendOutput($"Prefab created {BCCommandAbstract.Params[1]}, size={result},{result2},{result3}");
		}

		private static void ClonePrefab()
		{
			if (BCCommandAbstract.Params.Count != 3)
			{
				BCCommandAbstract.SendOutput("Incorrect params for clone");
				return;
			}
			string text = prefabDir + BCCommandAbstract.Params[1];
			if (!File.Exists(text + ".tts"))
			{
				BCCommandAbstract.SendOutput("Source prefab name " + BCCommandAbstract.Params[1] + " doesn't exist");
				return;
			}
			string text2 = prefabDir + BCCommandAbstract.Params[2];
			if (File.Exists(text2 + ".tts"))
			{
				BCCommandAbstract.SendOutput("Target prefab name " + BCCommandAbstract.Params[2] + " already exists");
				return;
			}
			CopyPrefab(text, text2);
			BCCommandAbstract.SendOutput("Prefab cloned from " + BCCommandAbstract.Params[1] + " to " + BCCommandAbstract.Params[2]);
		}

		private static void BackupPrefab(bool suppressErr = false)
		{
			if (BCCommandAbstract.Params.Count != 2 && !suppressErr)
			{
				BCCommandAbstract.SendOutput("Incorrect params for backup");
				return;
			}
			if (!File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				if (!suppressErr)
				{
					BCCommandAbstract.SendOutput("No prefab with the name " + BCCommandAbstract.Params[1] + " found");
				}
				return;
			}
			string text = $"{prefabDir}BCMBackups{_s}{BCCommandAbstract.Params[1]}{_s}";
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			DateTime utcNow = DateTime.UtcNow;
			string text2 = $"{text}{utcNow.Year}_{utcNow.Month + 1}_{utcNow.Day}_{utcNow.Hour}_{utcNow.Minute}_{utcNow.Second}";
			CopyPrefab(prefabDir + BCCommandAbstract.Params[1], text2);
			BCCommandAbstract.SendOutput("Backup saved for prefab " + BCCommandAbstract.Params[1] + " - " + GetFilename(text2));
		}

		private static void CopyPrefab(string sourcePathFile, string targetPathFile)
		{
			File.Copy(sourcePathFile + ".tts", targetPathFile + ".tts", overwrite: true);
			if (File.Exists(sourcePathFile + ".xml"))
			{
				File.Copy(sourcePathFile + ".xml", targetPathFile + ".xml", overwrite: true);
			}
			if (File.Exists(sourcePathFile + ".mesh"))
			{
				File.Copy(sourcePathFile + ".mesh", targetPathFile + ".mesh", overwrite: true);
			}
			if (File.Exists(sourcePathFile + ".blocks.nim"))
			{
				File.Copy(sourcePathFile + ".blocks.nim", targetPathFile + ".blocks.nim", overwrite: true);
			}
			if (File.Exists(sourcePathFile + ".jpg"))
			{
				File.Copy(sourcePathFile + ".jpg", targetPathFile + ".jpg", overwrite: true);
			}
			if (File.Exists(sourcePathFile + ".ins"))
			{
				File.Copy(sourcePathFile + ".ins", targetPathFile + ".ins", overwrite: true);
			}
		}

		private static void DeletePrefab(string sourcePathFile)
		{
			if (File.Exists(sourcePathFile + ".tts"))
			{
				File.Delete(sourcePathFile + ".xml");
			}
			if (File.Exists(sourcePathFile + ".xml"))
			{
				File.Delete(sourcePathFile + ".xml");
			}
			if (File.Exists(sourcePathFile + ".mesh"))
			{
				File.Delete(sourcePathFile + ".mesh");
			}
			if (File.Exists(sourcePathFile + ".blocks.nim"))
			{
				File.Delete(sourcePathFile + ".blocks.nim");
			}
			if (File.Exists(sourcePathFile + ".jpg"))
			{
				File.Delete(sourcePathFile + ".jpg");
			}
			if (File.Exists(sourcePathFile + ".ins"))
			{
				File.Delete(sourcePathFile + ".ins");
			}
		}

		private static void RestorePrefab()
		{
			if (BCCommandAbstract.Params.Count < 2)
			{
				BCCommandAbstract.SendOutput("Incorrect params for restore");
				return;
			}
			string path = $"{prefabDir}BCMBackups{_s}{BCCommandAbstract.Params[1]}{_s}";
			if (!Directory.Exists(path))
			{
				BCCommandAbstract.SendOutput("No prefab backups found for " + BCCommandAbstract.Params[1]);
				return;
			}
			string[] files = Directory.GetFiles(path, "*.tts");
			if (files.Length == 0)
			{
				BCCommandAbstract.SendOutput("No prefab backups found for " + BCCommandAbstract.Params[1]);
				return;
			}
			switch (BCCommandAbstract.Params.Count)
			{
			case 2:
			{
				int num = 0;
				string[] array = files;
				foreach (string file in array)
				{
					BCCommandAbstract.SendOutput($"{num}: {GetFilename(file)}");
					num++;
				}
				break;
			}
			case 3:
			{
				int result2;
				if (BCCommandAbstract.Params[2] == "last")
				{
					result2 = files.Length - 1;
				}
				else if (!int.TryParse(BCCommandAbstract.Params[2], out result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse index");
					break;
				}
				if (result2 < files.Length)
				{
					BackupPrefab(suppressErr: true);
					string text2 = files[result2].Replace(".tts", "");
					CopyPrefab(text2, prefabDir + BCCommandAbstract.Params[1]);
					BCCommandAbstract.SendOutput("Prefab restored from backup " + BCCommandAbstract.Params[1] + " from " + GetFilename(text2));
				}
				break;
			}
			case 4:
			{
				if (BCCommandAbstract.Params[2] != "delete")
				{
					BCCommandAbstract.SendOutput("Incorrect params for restore");
					break;
				}
				int result;
				switch (BCCommandAbstract.Params[3])
				{
				case "all":
					result = -1;
					break;
				case "last":
					result = files.Length - 1;
					break;
				default:
					if (!int.TryParse(BCCommandAbstract.Params[3], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse index");
						return;
					}
					break;
				}
				for (int i = ((result != -1) ? result : 0); i <= ((result == -1) ? (files.Length - 1) : result); i++)
				{
					if (i < files.Length)
					{
						string text = files[i].Replace(".tts", "");
						DeletePrefab(text);
						BCCommandAbstract.SendOutput("Backup " + GetFilename(text) + " deleted for " + BCCommandAbstract.Params[1]);
					}
				}
				if (Directory.GetFiles(path, "*.tts").Length == 0)
				{
					Directory.Delete(path);
				}
				break;
			}
			}
		}

		private static void DeletePrefab()
		{
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput("Incorrect params for delete");
				return;
			}
			if (!File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				BCCommandAbstract.SendOutput("No prefab with the name " + BCCommandAbstract.Params[1] + " found");
				return;
			}
			DeletePrefab(prefabDir + BCCommandAbstract.Params[1]);
			BCCommandAbstract.SendOutput("Prefab deleted " + BCCommandAbstract.Params[1]);
		}

		private static void SetProperty()
		{
			if (BCCommandAbstract.Params.Count < 2)
			{
				BCCommandAbstract.SendOutput("Incorrect params for set");
				return;
			}
			Prefab prefab = GetPrefab();
			if (prefab != null)
			{
				switch (BCCommandAbstract.Params.Count)
				{
				case 2:
					BCCommandAbstract.SendJson(new Dictionary<string, object>
					{
						{
							"CopyAirBlocks",
							prefab.properties.Values.ContainsKey("CopyAirBlocks") ? prefab.properties.GetStringValue("CopyAirBlocks") : null
						},
						{
							"AllowTopSoilDecorations",
							prefab.properties.Values.ContainsKey("AllowTopSoilDecorations") ? prefab.properties.GetStringValue("AllowTopSoilDecorations") : null
						},
						{
							"YOffset",
							prefab.properties.Values.ContainsKey("YOffset") ? prefab.properties.GetStringValue("YOffset") : null
						},
						{
							"RotationToFaceNorth",
							prefab.properties.Values.ContainsKey("RotationToFaceNorth") ? prefab.properties.GetStringValue("RotationToFaceNorth") : null
						},
						{
							"ExcludeDistantPOIMesh",
							prefab.properties.Values.ContainsKey("ExcludeDistantPOIMesh") ? prefab.properties.GetStringValue("ExcludeDistantPOIMesh") : null
						},
						{
							"DistantPOIYOffset",
							prefab.properties.Values.ContainsKey("DistantPOIYOffset") ? prefab.properties.GetStringValue("DistantPOIYOffset") : null
						},
						{
							"DistantPOIOverride",
							prefab.properties.Values.ContainsKey("DistantPOIOverride") ? prefab.properties.GetStringValue("DistantPOIOverride") : null
						},
						{
							"TraderArea",
							prefab.properties.Values.ContainsKey("TraderArea") ? prefab.properties.GetStringValue("TraderArea") : null
						},
						{
							"TraderAreaProtect",
							prefab.properties.Values.ContainsKey("TraderAreaProtect") ? prefab.properties.GetStringValue("TraderAreaProtect") : null
						},
						{
							"TraderAreaTeleportSize",
							prefab.properties.Values.ContainsKey("TraderAreaTeleportSize") ? prefab.properties.GetStringValue("TraderAreaTeleportSize") : null
						},
						{
							"TraderAreaTeleportCenter",
							prefab.properties.Values.ContainsKey("TraderAreaTeleportCenter") ? prefab.properties.GetStringValue("TraderAreaTeleportCenter") : null
						},
						{
							"SleeperVolumeStart",
							prefab.properties.Values.ContainsKey("SleeperVolumeStart") ? prefab.properties.GetStringValue("SleeperVolumeStart") : null
						},
						{
							"SleeperVolumeSize",
							prefab.properties.Values.ContainsKey("SleeperVolumeSize") ? prefab.properties.GetStringValue("SleeperVolumeSize") : null
						},
						{
							"SleeperVolumeGroup",
							prefab.properties.Values.ContainsKey("SleeperVolumeGroup") ? prefab.properties.GetStringValue("SleeperVolumeGroup") : null
						},
						{
							"SleeperIsLootVolume",
							prefab.properties.Values.ContainsKey("SleeperIsLootVolume") ? prefab.properties.GetStringValue("SleeperIsLootVolume") : null
						},
						{
							"SleeperVolumeGameStageAdjust",
							prefab.properties.Values.ContainsKey("SleeperVolumeGameStageAdjust") ? prefab.properties.GetStringValue("SleeperVolumeGameStageAdjust") : null
						},
						{
							"Zoning",
							prefab.properties.Values.ContainsKey("Zoning") ? prefab.properties.GetStringValue("Zoning") : null
						},
						{
							"AllowedBiomes",
							prefab.properties.Values.ContainsKey("AllowedBiomes") ? prefab.properties.GetStringValue("AllowedBiomes") : null
						},
						{
							"AllowedTownships",
							prefab.properties.Values.ContainsKey("AllowedTownships") ? prefab.properties.GetStringValue("AllowedTownships") : null
						},
						{
							"Tags",
							prefab.properties.Values.ContainsKey("Tags") ? prefab.properties.GetStringValue("Tags") : null
						},
						{
							"Condition",
							prefab.properties.Values.ContainsKey("Condition") ? prefab.properties.GetStringValue("Condition") : null
						},
						{
							"Age",
							prefab.properties.Values.ContainsKey("Age") ? prefab.properties.GetStringValue("Age") : null
						},
						{
							"StaticSpawner.Class",
							prefab.properties.Values.ContainsKey("StaticSpawner.Class") ? prefab.properties.GetStringValue("StaticSpawner.Class") : null
						},
						{
							"StaticSpawner.Size",
							prefab.properties.Values.ContainsKey("StaticSpawner.Size") ? prefab.properties.GetStringValue("StaticSpawner.Size") : null
						},
						{
							"StaticSpawner.Trigger",
							prefab.properties.Values.ContainsKey("StaticSpawner.Trigger") ? prefab.properties.GetStringValue("StaticSpawner.Trigger") : null
						}
					});
					break;
				case 3:
					BCCommandAbstract.SendJson(new
					{
						Property = BCCommandAbstract.Params[2],
						Value = (prefab.properties.Values.ContainsKey(BCCommandAbstract.Params[2]) ? prefab.properties.GetStringValue(BCCommandAbstract.Params[2]) : null)
					});
					break;
				case 4:
				{
					string oldValue = (prefab.properties.Values.ContainsKey(BCCommandAbstract.Params[2]) ? prefab.properties.Values[BCCommandAbstract.Params[2]] : null);
					prefab.properties.Values[BCCommandAbstract.Params[2]] = BCCommandAbstract.Params[3];
					prefab.properties.Save("prefab", prefabDir, prefab.PrefabName);
					BCCommandAbstract.SendJson(new
					{
						Property = BCCommandAbstract.Params[2],
						OldValue = oldValue,
						Value = prefab.properties.GetStringValue(BCCommandAbstract.Params[2])
					});
					break;
				}
				}
			}
		}

		private static void SetSize()
		{
			if (BCCommandAbstract.Params.Count != 5)
			{
				BCCommandAbstract.SendOutput("Incorrect params for setsize");
				return;
			}
			Prefab prefab = GetPrefab();
			if (prefab == null)
			{
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[2], out var result) || !int.TryParse(BCCommandAbstract.Params[3], out var result2) || !int.TryParse(BCCommandAbstract.Params[4], out var result3))
			{
				BCCommandAbstract.SendOutput("Unable to parse x,y,z for size");
				return;
			}
			if (result < prefab.size.x || result2 < prefab.size.y || result3 < prefab.size.z)
			{
				BCCommandAbstract.SendOutput("Prefabs can only be made bigger with the size command, use trim to remove layers");
				return;
			}
			Prefab prefab2 = new Prefab(new Vector3i(result, result2, result3));
			BackupPrefab(suppressErr: true);
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.z; j++)
				{
					for (int k = 0; k < prefab.size.y; k++)
					{
						prefab2.SetBlock(i, k, j, prefab.GetBlock(i, k, j));
						prefab2.SetDensity(i, k, j, prefab.GetDensity(i, k, j));
						prefab2.SetTexture(i, k, j, prefab.GetTexture(i, k, j));
					}
				}
			}
			prefab2.PrefabName = BCCommandAbstract.Params[1];
			prefab2.bCopyAirBlocks = true;
			prefab2.Save(BCCommandAbstract.Params[1]);
			prefab2.properties = prefab.properties;
			prefab2.properties.Save("prefab", prefabDir, BCCommandAbstract.Params[1]);
			BCCommandAbstract.SendOutput($"Size set to {result},{result2},{result3} for prefab {BCCommandAbstract.Params[1]}");
		}

		private static void AddLayers()
		{
			if (BCCommandAbstract.Params.Count != 4 || (BCCommandAbstract.Params.Count == 3 && BCCommandAbstract.Params[2] != "all"))
			{
				BCCommandAbstract.SendOutput("Incorrect params for add");
				return;
			}
			Prefab prefab = GetPrefab();
			if (prefab == null)
			{
				return;
			}
			string text = BCCommandAbstract.Params[2];
			int result = 0;
			if (BCCommandAbstract.Params.Count > 3 && !int.TryParse(BCCommandAbstract.Params[3], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse amount: " + BCCommandAbstract.Params[3]);
				return;
			}
			int num = ((text == "all" || text == "left") ? result : 0);
			int num2 = ((text == "all" || text == "right") ? result : 0);
			int num3 = ((text == "all" || text == "front") ? result : 0);
			int num4 = ((text == "all" || text == "back") ? result : 0);
			int num5 = ((text == "all" || text == "bottom") ? result : 0);
			int num6 = ((text == "all" || text == "top") ? result : 0);
			Prefab prefab2 = new Prefab(new Vector3i(prefab.size.x + num + num2, prefab.size.y + num5 + num6, prefab.size.z + num3 + num4));
			BackupPrefab(suppressErr: true);
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.z; j++)
				{
					for (int k = 0; k < prefab.size.y; k++)
					{
						prefab2.SetBlock(i + num, k + num5, j + num3, prefab.GetBlock(i, k, j));
						prefab2.SetDensity(i + num, k + num5, j + num3, prefab.GetDensity(i, k, j));
						prefab2.SetTexture(i + num, k + num5, j + num3, prefab.GetTexture(i, k, j));
					}
				}
			}
			prefab2.PrefabName = BCCommandAbstract.Params[1];
			prefab2.bCopyAirBlocks = true;
			prefab2.Save(BCCommandAbstract.Params[1]);
			prefab2.properties = prefab.properties;
			prefab2.properties.Save("prefab", prefabDir, BCCommandAbstract.Params[1]);
			BCCommandAbstract.SendOutput(string.Format("{0} blocks added to {1} of prefab", result, (text == "all") ? "all sides" : text));
		}

		private static void TrimLayers()
		{
			if (BCCommandAbstract.Params.Count == 3 && BCCommandAbstract.Params[2] != "air")
			{
				BCCommandAbstract.SendOutput("Incorrect params for trim");
				return;
			}
			Prefab prefab = GetPrefab();
			if (prefab == null)
			{
				return;
			}
			string text = BCCommandAbstract.Params[2];
			int result = 0;
			if (BCCommandAbstract.Params.Count > 3 && !int.TryParse(BCCommandAbstract.Params[3], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse amount: " + BCCommandAbstract.Params[3]);
				return;
			}
			int num = ((!(text != "air")) ? GetLeftX(prefab) : ((text == "left") ? result : 0));
			int num2 = ((!(text != "air")) ? GetRightX(prefab) : ((text == "right") ? result : 0));
			int num3 = ((!(text != "air")) ? GetFrontZ(prefab) : ((text == "front") ? result : 0));
			int num4 = ((!(text != "air")) ? GetBackZ(prefab) : ((text == "back") ? result : 0));
			int num5 = ((!(text != "air")) ? GetBottomY(prefab) : ((text == "bottom") ? result : 0));
			int num6 = ((!(text != "air")) ? GetTopY(prefab) : ((text == "top") ? result : 0));
			Prefab prefab2 = new Prefab(new Vector3i(prefab.size.x - num - num2, prefab.size.y - num5 - num6, prefab.size.z - num3 - num4));
			BackupPrefab(suppressErr: true);
			for (int i = num; i < prefab.size.x - num2; i++)
			{
				for (int j = num3; j < prefab.size.z - num4; j++)
				{
					for (int k = num5; k < prefab.size.y - num6; k++)
					{
						prefab2.SetBlock(i - num, k - num5, j - num3, prefab.GetBlock(i, k, j));
						prefab2.SetDensity(i - num, k - num5, j - num3, prefab.GetDensity(i, k, j));
						prefab2.SetTexture(i - num, k - num5, j - num3, prefab.GetTexture(i, k, j));
					}
				}
			}
			prefab2.PrefabName = BCCommandAbstract.Params[1];
			prefab2.bCopyAirBlocks = true;
			prefab2.Save(BCCommandAbstract.Params[1]);
			prefab2.properties = prefab.properties;
			prefab2.properties.Save("prefab", prefabDir, BCCommandAbstract.Params[1]);
			BCCommandAbstract.SendOutput(((text == "air") ? "Air" : $"{result}") + " blocks trimmed from " + ((text == "air") ? "prefab" : (text + " of prefab")));
		}

		private void InsertLayers()
		{
			BCCommandAbstract.SendOutput("Work in Progress");
		}

		private void CutLayers()
		{
			BCCommandAbstract.SendOutput("Work in Progress");
		}

		private void CopyLayer()
		{
			BCCommandAbstract.SendOutput("Work in Progress");
		}

		private void SwapLayer()
		{
			BCCommandAbstract.SendOutput("Work in Progress");
		}

		private void SetBlock()
		{
			BCCommandAbstract.SendOutput("Work in Progress");
		}

		private static void SwapBlocks()
		{
			if (BCCommandAbstract.Params.Count != 4)
			{
				BCCommandAbstract.SendOutput("Incorrect params for swap");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[2], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse source: " + BCCommandAbstract.Params[2]);
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[3], out var result2))
			{
				BCCommandAbstract.SendOutput("Unable to parse target: " + BCCommandAbstract.Params[3]);
				return;
			}
			ItemClass forId = ItemClass.GetForId(result2);
			if (forId == null || !forId.IsBlock())
			{
				BCCommandAbstract.SendOutput("Unable to get target block");
				return;
			}
			Dictionary<string, object> dictionary = ReadBinaryFile(prefabDir + BCCommandAbstract.Params[1] + ".tts");
			object obj = dictionary["Size"];
			if (!(obj is Vector3i))
			{
				return;
			}
			Vector3i vector3i = (Vector3i)obj;
			Prefab prefab = GetPrefab();
			if (prefab == null)
			{
				return;
			}
			BackupPrefab(suppressErr: true);
			int num = 0;
			for (int i = 0; i < vector3i.x; i++)
			{
				for (int j = 0; j < vector3i.z; j++)
				{
					for (int k = 0; k < vector3i.y; k++)
					{
						BCMBlockValue[] array = dictionary["Blocks"] as BCMBlockValue[];
						if (array == null)
						{
							continue;
						}
						int type = array[num].Type;
						if (type == result)
						{
							BlockValue block = prefab.GetBlock(i, k, j);
							if (block.type == 0 && type != 0)
							{
								block.rotation = array[num].Rotation;
								block.meta = array[num].Meta;
							}
							block.type = result2;
							prefab.SetBlock(i, k, j, block);
							num++;
						}
					}
				}
			}
			prefab.Save(BCCommandAbstract.Params[1]);
		}

		private void SwapMap()
		{
			BCCommandAbstract.SendOutput("Work in progress");
		}

		private void RepairBlocks()
		{
			BCCommandAbstract.SendOutput("Work in progress");
		}

		private void LockSecureBlocks()
		{
			BCCommandAbstract.SendOutput("Work in progress");
		}

		private static void ListPrefabs()
		{
			if (!Directory.Exists(prefabDir))
			{
				return;
			}
			IEnumerable<string> enumerable = Directory.GetFiles(prefabDir, "*.tts").Select(GetFilename);
			if (BCCommandAbstract.Params.Count == 2)
			{
				enumerable = enumerable.Where((string p) => p.IndexOf(BCCommandAbstract.Params[1], StringComparison.OrdinalIgnoreCase) > -1);
			}
			if (!BCCommandAbstract.Options.ContainsKey("dim"))
			{
				List<string> list = enumerable.ToList();
				BCCommandAbstract.SendJson(new
				{
					Count = list.Count,
					Filter = ((BCCommandAbstract.Params.Count == 2) ? BCCommandAbstract.Params[1] : null),
					Prefabs = list
				});
				return;
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (string item in enumerable)
			{
				object obj = ReadBinaryFile(prefabDir + item + ".tts", sizeOnly: true)["Size"];
				if (obj is Vector3i)
				{
					Vector3i vector3i = (Vector3i)obj;
					dictionary.Add(item, $"{vector3i.x},{vector3i.y},{vector3i.z}:{vector3i.x * vector3i.y * vector3i.z}");
				}
			}
			BCCommandAbstract.SendJson(new
			{
				Count = dictionary.Count,
				Filter = ((BCCommandAbstract.Params.Count == 2) ? BCCommandAbstract.Params[1] : null),
				Prefabs = dictionary
			});
		}

		private static void GetDimensions()
		{
			if (!File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				BCCommandAbstract.SendOutput("Prefab file not found");
				return;
			}
			object obj = ReadBinaryFile(prefabDir + BCCommandAbstract.Params[1] + ".tts", sizeOnly: true)["Size"];
			if (obj is Vector3i)
			{
				Vector3i vector3i = (Vector3i)obj;
				BCCommandAbstract.SendOutput($"{vector3i.x},{vector3i.y},{vector3i.z}:{vector3i.x * vector3i.y * vector3i.z}");
			}
		}

		private static void GetBlockCounts()
		{
			if (!File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				BCCommandAbstract.SendOutput("Prefab file not found");
				return;
			}
			Dictionary<string, object> dictionary = ReadBinaryFile(prefabDir + BCCommandAbstract.Params[1] + ".tts");
			Dictionary<int, int> dictionary2 = new Dictionary<int, int>();
			object obj = dictionary["Size"];
			if (obj is Vector3i)
			{
				Vector3i vector3i = (Vector3i)obj;
				BCMBlockValue[] array = dictionary["Blocks"] as BCMBlockValue[];
				if (array != null)
				{
					int num = 0;
					for (int i = 0; i < vector3i.x; i++)
					{
						for (int j = 0; j < vector3i.y; j++)
						{
							for (int k = 0; k < vector3i.z; k++)
							{
								int type = array[num].Type;
								if (dictionary2.ContainsKey(type))
								{
									dictionary2[type]++;
								}
								else
								{
									dictionary2.Add(type, 1);
								}
								num++;
							}
						}
					}
					BCCommandAbstract.SendJson(new
					{
						Size = $"{vector3i.x},{vector3i.y},{vector3i.z}",
						Total = $"{vector3i.x * vector3i.y * vector3i.z}",
						Blocks = dictionary2.OrderBy((KeyValuePair<int, int> b) => (!BCCommandAbstract.Options.ContainsKey("bycount")) ? b.Key : (-b.Value)).Select(delegate(KeyValuePair<int, int> b)
						{
							ItemClass forId = ItemClass.GetForId(b.Key);
							return new
							{
								Id = ((forId == null && b.Key != 0) ? "NULL_" : "") + b.Key,
								Name = ((b.Key == 0) ? "air" : ((forId == null) ? "NULL" : forId.Name)),
								Count = b.Value
							};
						}).ToList()
					});
					return;
				}
			}
			BCCommandAbstract.SendOutput("No data to display");
		}

		private static void GetBlocks(string type = "all")
		{
			if (!File.Exists(prefabDir + BCCommandAbstract.Params[1] + ".tts"))
			{
				BCCommandAbstract.SendOutput("Prefab file not found");
				return;
			}
			Dictionary<string, object> dictionary = ReadBinaryFile(prefabDir + BCCommandAbstract.Params[1] + ".tts");
			Dictionary<string, string> dictionary2 = new Dictionary<string, string>();
			object obj = dictionary["Size"];
			if (obj is Vector3i)
			{
				Vector3i vector3i = (Vector3i)obj;
				BCMBlockValue[] array = dictionary["Blocks"] as BCMBlockValue[];
				if (array != null)
				{
					ushort[] array2 = dictionary["Damage"] as ushort[];
					if (array2 != null)
					{
						byte[] array3 = dictionary["Density"] as byte[];
						if (array3 != null)
						{
							int num = 0;
							for (int i = 0; i < vector3i.x; i++)
							{
								for (int j = 0; j < vector3i.y; j++)
								{
									for (int k = 0; k < vector3i.z; k++)
									{
										string key = $"{i},{j},{k}";
										switch (type)
										{
										case "all":
											dictionary2[key] = $"{array[num].Type},{array[num].Rotation},{array2[num]},{array3[num]},{array[num].Meta},{array[num].Meta2}";
											break;
										case "type":
											dictionary2[key] = $"{array[num].Type}";
											break;
										case "rotation":
											dictionary2[key] = $"{array[num].Rotation}";
											break;
										case "damage":
											dictionary2[key] = $"{array2[num]}";
											break;
										case "density":
											dictionary2[key] = $"{array3[num]}";
											break;
										case "meta":
											dictionary2[key] = $"{array[num].Meta}";
											break;
										case "meta2":
											dictionary2[key] = $"{array[num].Meta2}";
											break;
										}
										num++;
									}
								}
							}
							BCCommandAbstract.SendJson(new
							{
								Size = $"{vector3i.x},{vector3i.y},{vector3i.z}",
								Total = $"{vector3i.x * vector3i.y * vector3i.z}",
								Key = ((type == "all") ? "type,rotation,damage,density,meta,meta2,meta3" : type),
								Blocks = dictionary2
							});
							return;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput("No data to display");
		}

		private static void GetPaint()
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			Prefab prefab = GetPrefab();
			if (prefab == null)
			{
				return;
			}
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.y; j++)
				{
					for (int k = 0; k < prefab.size.z; k++)
					{
						dictionary.Add($"{i},{j},{k}", GetTexStr(prefab.GetTexture(i, j, k)));
					}
				}
			}
			BCCommandAbstract.SendJson(dictionary);
		}

		private static void GetStats()
		{
			Prefab prefab = GetPrefab();
			if (prefab != null)
			{
				BCCommandAbstract.SendJson(prefab.GetBlockStatistics());
			}
		}

		private static void GetXml()
		{
			string path = prefabDir + BCCommandAbstract.Params[1] + ".xml";
			if (File.Exists(path))
			{
				BCCommandAbstract.SendOutput(File.ReadAllText(path));
			}
		}

		private static int GetBottomY(Prefab prefab)
		{
			for (int i = 0; i < prefab.size.y; i++)
			{
				for (int j = 0; j < prefab.size.x; j++)
				{
					for (int k = 0; k < prefab.size.z; k++)
					{
						if (!prefab.GetBlock(j, i, k).Equals(BlockValue.Air))
						{
							return i;
						}
					}
				}
			}
			return 0;
		}

		private static int GetTopY(Prefab prefab)
		{
			for (int num = prefab.size.y - 1; num >= 0; num--)
			{
				for (int i = 0; i < prefab.size.x; i++)
				{
					for (int j = 0; j < prefab.size.z; j++)
					{
						if (prefab.GetBlock(i, num, j).type != 0)
						{
							return prefab.size.y - num - 1;
						}
					}
				}
			}
			return 0;
		}

		private static int GetLeftX(Prefab prefab)
		{
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.y; j++)
				{
					for (int k = 0; k < prefab.size.z; k++)
					{
						if (prefab.GetBlock(i, j, k).type != 0)
						{
							return i;
						}
					}
				}
			}
			return 0;
		}

		private static int GetRightX(Prefab prefab)
		{
			for (int num = prefab.size.x - 1; num >= 0; num--)
			{
				for (int i = 0; i < prefab.size.y; i++)
				{
					for (int j = 0; j < prefab.size.z; j++)
					{
						if (prefab.GetBlock(num, i, j).type != 0)
						{
							return prefab.size.x - num - 1;
						}
					}
				}
			}
			return 0;
		}

		private static int GetFrontZ(Prefab prefab)
		{
			for (int i = 0; i < prefab.size.z; i++)
			{
				for (int j = 0; j < prefab.size.x; j++)
				{
					for (int k = 0; k < prefab.size.y; k++)
					{
						if (prefab.GetBlock(j, k, i).type != 0)
						{
							return i;
						}
					}
				}
			}
			return 0;
		}

		private static int GetBackZ(Prefab prefab)
		{
			for (int num = prefab.size.z - 1; num >= 0; num--)
			{
				for (int i = 0; i < prefab.size.x; i++)
				{
					for (int j = 0; j < prefab.size.y; j++)
					{
						if (prefab.GetBlock(i, j, num).type != 0)
						{
							return prefab.size.z - num - 1;
						}
					}
				}
			}
			return 0;
		}
	}
}
