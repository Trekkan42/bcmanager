using System;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCMutePlayer : BCCommandAbstract
	{
		public BCMutePlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCMutePlayer"];
			DefaultCommands = new string[1]
			{
				"bc-mute"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCMutePlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			string _id;
			ClientInfo _cInfo;
			if (1 > BCCommandAbstract.Params.Count || 2 < BCCommandAbstract.Params.Count)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else if (1 == BCCommandAbstract.Params.Count && BCCommandAbstract.Params[0].Equals("list", StringComparison.InvariantCultureIgnoreCase))
			{
				BCCommandAbstract.SendJson((from p in BCMPersist.Instance.ServerPlayersConfig.All()
					where p.Muted
					select p.SteamId + ":" + p.Name).ToArray());
			}
			else if (1 == BCCommandAbstract.Params.Count && !BCCommandAbstract.Params[0].Equals("list", StringComparison.InvariantCultureIgnoreCase))
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out _id, out _cInfo))
			{
				BCMServerPlayerConfig bCMServerPlayerConfig = BCMPersist.Instance.ServerPlayersConfig[_id, true];
				if (!bool.TryParse(BCCommandAbstract.Params[1], out var result))
				{
					BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[1] + " as a boolean");
					return;
				}
				bCMServerPlayerConfig.Muted = result;
				BCMPersist.Instance.Save(BCMSaveType.ServerPlayersConfig);
				BCCommandAbstract.SendOutput((result ? "Muting" : "Un-muting") + " " + bCMServerPlayerConfig.Name);
			}
		}
	}
}
