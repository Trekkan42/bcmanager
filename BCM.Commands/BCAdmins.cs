using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCAdmins : BCCommandAbstract
	{
		private readonly string[] _filterPrefs = new string[6]
		{
			"adminfilename",
			"controlpanel",
			"password",
			"savegamefolder",
			"options",
			"last"
		};

		private readonly string[] _filterStats = new string[1]
		{
			"last"
		};

		public BCAdmins()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCAdmins"];
			DefaultCommands = new string[1]
			{
				"bc-admins"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCAdmins.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Options.ContainsKey("gg"))
			{
				BCCommandAbstract.SendJson(GetGamePrefs());
			}
			else if (BCCommandAbstract.Options.ContainsKey("ggs"))
			{
				BCCommandAbstract.SendJson(GetGameStats());
			}
			else
			{
				BCCommandAbstract.SendJson(GenerateData(new BCMAdmins()));
			}
		}

		private static object GenerateData(BCMAdmins adminData)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (BCCommandAbstract.Options.ContainsKey("admins") || (!BCCommandAbstract.Options.ContainsKey("bans") && !BCCommandAbstract.Options.ContainsKey("whitelist") && !BCCommandAbstract.Options.ContainsKey("permissions")))
			{
				dictionary.Add("Admins", adminData.Admins);
			}
			if (BCCommandAbstract.Options.ContainsKey("bans") || (!BCCommandAbstract.Options.ContainsKey("admins") && !BCCommandAbstract.Options.ContainsKey("whitelist") && !BCCommandAbstract.Options.ContainsKey("permissions")))
			{
				dictionary.Add("Bans", adminData.Bans);
			}
			if (BCCommandAbstract.Options.ContainsKey("whitelist") || (!BCCommandAbstract.Options.ContainsKey("admins") && !BCCommandAbstract.Options.ContainsKey("bans") && !BCCommandAbstract.Options.ContainsKey("permissions")))
			{
				dictionary.Add("Whitelist", adminData.Whitelist);
			}
			if (BCCommandAbstract.Options.ContainsKey("permissions") || (!BCCommandAbstract.Options.ContainsKey("admins") && !BCCommandAbstract.Options.ContainsKey("bans") && !BCCommandAbstract.Options.ContainsKey("whitelist")))
			{
				dictionary.Add("Permissions", adminData.Permissions);
			}
			return dictionary;
		}

		private SortedList<string, object> GetGamePrefs()
		{
			SortedList<string, object> sortedList = new SortedList<string, object>();
			IEnumerator enumerator = Enum.GetValues(typeof(EnumGamePrefs)).GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.Current == null)
				{
					continue;
				}
				EnumGamePrefs enumGamePrefs = (EnumGamePrefs)(int)enumerator.Current;
				if (IsViewablePref(enumGamePrefs))
				{
					try
					{
						sortedList.Add(enumGamePrefs.ToString(), GamePrefs.GetObject(enumGamePrefs));
					}
					catch (Exception)
					{
					}
				}
			}
			(enumerator as IDisposable)?.Dispose();
			sortedList.Add("ServerHostIP", BCUtils.GetIPAddress());
			sortedList.Add("ServerPublicIP", BCUtils.GetPublicIPAddress());
			return sortedList;
		}

		private SortedList<string, object> GetGameStats()
		{
			SortedList<string, object> sortedList = new SortedList<string, object>();
			IEnumerator enumerator = Enum.GetValues(typeof(EnumGameStats)).GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.Current == null)
				{
					continue;
				}
				EnumGameStats enumGameStats = (EnumGameStats)(int)enumerator.Current;
				if (IsViewableStat(enumGameStats))
				{
					object @object = GameStats.GetObject(enumGameStats);
					bool result2;
					double result3;
					if (int.TryParse($"{@object}", out var result))
					{
						sortedList.Add(enumGameStats.ToString(), result);
					}
					else if (bool.TryParse($"{@object}", out result2))
					{
						sortedList.Add(enumGameStats.ToString(), result2);
					}
					else if (double.TryParse($"{@object}", out result3))
					{
						sortedList.Add(enumGameStats.ToString(), result3);
					}
					else
					{
						sortedList.Add(enumGameStats.ToString(), string.IsNullOrEmpty($"{@object}") ? null : $"{@object}");
					}
				}
			}
			return sortedList;
		}

		private bool IsViewablePref(EnumGamePrefs pref)
		{
			return _filterPrefs.All((string value) => !pref.ToString().ToLower().Contains(value));
		}

		private bool IsViewableStat(EnumGameStats stat)
		{
			return _filterStats.All((string value) => !stat.ToString().ToLower().Contains(value));
		}
	}
}
