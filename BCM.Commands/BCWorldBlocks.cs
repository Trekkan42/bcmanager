using System;
using System.Collections.Generic;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCWorldBlocks : BCCommandAbstract
	{
		public BCWorldBlocks()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCWorldBlocks"];
			DefaultCommands = new string[2]
			{
				"bc-block",
				"block"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCWorldBlocks.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("undo"))
			{
				BCCommandAbstract.SendOutput("Please use the bc-undo command to undo changes");
				return;
			}
			Vector3i pos = new Vector3i(int.MinValue, 0, int.MinValue);
			Vector3i pos2 = new Vector3i(int.MinValue, 0, int.MinValue);
			string blockname = null;
			EntityPlayer entityPlayer = null;
			string text = null;
			if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
			{
				text = BCCommandAbstract.SenderInfo.RemoteClientInfo.ownerId;
				entityPlayer = world.Entities.dict[BCCommandAbstract.SenderInfo.RemoteClientInfo.entityId] as EntityPlayer;
				if (!(entityPlayer != null))
				{
					BCCommandAbstract.SendOutput("Error: unable to get player location");
					return;
				}
				pos2 = new Vector3i((int)Math.Floor((float)entityPlayer.serverPos.x / 32f), (int)Math.Floor((float)entityPlayer.serverPos.y / 32f), (int)Math.Floor((float)entityPlayer.serverPos.z / 32f));
			}
			string text2;
			switch (BCCommandAbstract.Params.Count)
			{
			case 2:
			case 3:
				if (text != null)
				{
					pos = BCLocation.GetPos(text);
					if (pos.x == int.MinValue)
					{
						BCCommandAbstract.SendOutput("No location stored. Use bc-loc to store a location.");
						return;
					}
					text2 = BCCommandAbstract.Params[1];
					if (BCCommandAbstract.Params.Count == 3)
					{
						blockname = BCCommandAbstract.Params[2];
					}
					break;
				}
				BCCommandAbstract.SendOutput("Error: unable to get player location");
				return;
			case 8:
			case 9:
				if (!int.TryParse(BCCommandAbstract.Params[1], out pos.x) || !int.TryParse(BCCommandAbstract.Params[2], out pos.y) || !int.TryParse(BCCommandAbstract.Params[3], out pos.z) || !int.TryParse(BCCommandAbstract.Params[4], out pos2.x) || !int.TryParse(BCCommandAbstract.Params[5], out pos2.y) || !int.TryParse(BCCommandAbstract.Params[6], out pos2.z))
				{
					BCCommandAbstract.SendOutput("Error: unable to parse coordinates");
					return;
				}
				text2 = BCCommandAbstract.Params[7];
				if (BCCommandAbstract.Params.Count == 9)
				{
					blockname = BCCommandAbstract.Params[8];
				}
				break;
			default:
				BCCommandAbstract.SendOutput("Error: Incorrect command format.");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			Vector3i size = BCUtils.GetSize(pos, pos2);
			Vector3i vector3i = new Vector3i((pos.x < pos2.x) ? pos.x : pos2.x, (pos.y < pos2.y) ? pos.y : pos2.y, (pos.z < pos2.z) ? pos.z : pos2.z);
			int result;
			BlockValue blockValue = (int.TryParse(text2, out result) ? Block.GetBlockValue(result) : Block.GetBlockValue(text2));
			Dictionary<long, Chunk> affectedChunks = BCMCmdArea.GetAffectedChunks(new BCMCmdArea("Blocks")
			{
				Position = new BCMVector3(vector3i),
				Size = new BCMVector3(size),
				HasPos = true,
				HasSize = true,
				ChunkBounds = new BCMVector4
				{
					x = World.toChunkXZ(vector3i.x),
					y = World.toChunkXZ(vector3i.z),
					z = World.toChunkXZ(vector3i.x + size.x - 1),
					w = World.toChunkXZ(vector3i.z + size.z - 1)
				},
				HasChunkPos = true
			}, world);
			if (!BCCommandAbstract.Options.ContainsKey("noundo"))
			{
				BCUndo.CreateUndo(entityPlayer, vector3i, size);
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "scan":
				ScanBlocks(vector3i, size, blockValue, text2);
				break;
			case "fill":
				FillBlocks(vector3i, size, blockValue, text2, affectedChunks);
				break;
			case "swap":
				SwapBlocks(vector3i, size, blockValue, blockname, affectedChunks);
				break;
			case "repair":
				RepairBlocks(vector3i, size, affectedChunks);
				break;
			case "damage":
				DamageBlocks(vector3i, size, affectedChunks);
				break;
			case "upgrade":
				UpgradeBlocks(vector3i, size, affectedChunks);
				break;
			case "downgrade":
				DowngradeBlocks(vector3i, size, affectedChunks);
				break;
			case "paint":
				SetPaint(vector3i, size, affectedChunks);
				break;
			case "paintface":
				SetPaintFace(vector3i, size, affectedChunks);
				break;
			case "paintstrip":
				RemovePaint(vector3i, size, affectedChunks);
				break;
			case "density":
				SetDensity(vector3i, size, affectedChunks);
				break;
			case "rotate":
				SetRotation(vector3i, size, affectedChunks);
				break;
			case "meta1":
				SetMeta(1, vector3i, size, affectedChunks);
				break;
			case "meta2":
				SetMeta(2, vector3i, size, affectedChunks);
				break;
			case "meta3":
				SetMeta(3, vector3i, size, affectedChunks);
				break;
			default:
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static void SetDensity(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			sbyte result = 1;
			if (BCCommandAbstract.Options.ContainsKey("d") && sbyte.TryParse(BCCommandAbstract.Options["d"], out result))
			{
				BCCommandAbstract.SendOutput($"Using density {result}");
			}
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild)
						{
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, _bChangeBlockValue: false, block, _bChangeDensity: true, result, bNotify: false, updateLight: false, BCCommandAbstract.Options.ContainsKey("force"));
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting density on {num} blocks '{result}' @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void SetMeta(int metaIdx, Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			if (!byte.TryParse(BCCommandAbstract.Options["meta"], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse meta '" + BCCommandAbstract.Options["meta"] + "'");
				return;
			}
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild)
						{
							switch (metaIdx)
							{
							default:
								return;
							case 1:
								block.meta = result;
								break;
							case 2:
								block.meta2 = result;
								break;
							}
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting meta{metaIdx} on '{num}' blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void SetRotation(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("rot") && !byte.TryParse(BCCommandAbstract.Options["rot"], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse rotation '" + BCCommandAbstract.Options["rot"] + "'");
				return;
			}
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild && block.Block.shape.IsRotatable)
						{
							block.rotation = result;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting rotation on '{num}' blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void DowngradeBlocks(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						BlockValue downgradeBlock = block.Block.DowngradeBlock;
						if (!downgradeBlock.Equals(BlockValue.Air) && !block.ischild)
						{
							downgradeBlock.rotation = block.rotation;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, downgradeBlock, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Downgrading {num} blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void UpgradeBlocks(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						BlockValue upgradeBlock = block.Block.UpgradeBlock;
						if (!upgradeBlock.Equals(BlockValue.Air) && !block.ischild)
						{
							upgradeBlock.rotation = block.rotation;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, upgradeBlock, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Upgrading {num} blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void DamageBlocks(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int result = 0;
			int result2 = 0;
			if (BCCommandAbstract.Options.ContainsKey("d"))
			{
				if (BCCommandAbstract.Options["d"].IndexOf(",", StringComparison.InvariantCulture) > -1)
				{
					string[] array = BCCommandAbstract.Options["d"].Split(',');
					if (array.Length != 2)
					{
						BCCommandAbstract.SendOutput("Unable to parse damage values");
						return;
					}
					if (!int.TryParse(array[0], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse damage min value");
						return;
					}
					if (!int.TryParse(array[1], out result2))
					{
						BCCommandAbstract.SendOutput("Unable to parse damage max value");
						return;
					}
				}
				else if (!int.TryParse(BCCommandAbstract.Options["d"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse damage value");
					return;
				}
			}
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue blockValue = GameManager.Instance.World.GetBlock(0, pos);
						if (blockValue.Equals(BlockValue.Air))
						{
							continue;
						}
						int maxDamage = blockValue.Block.blockMaterial.MaxDamage;
						int num2 = ((result2 != 0) ? UnityEngine.Random.Range(result, result2) : result) + blockValue.damage;
						if (BCCommandAbstract.Options.ContainsKey("nobreak"))
						{
							blockValue.damage = Math.Min(num2, maxDamage - 1);
						}
						else if (BCCommandAbstract.Options.ContainsKey("overkill"))
						{
							while (num2 >= maxDamage)
							{
								BlockValue downgradeBlock = blockValue.Block.DowngradeBlock;
								num2 -= maxDamage;
								maxDamage = downgradeBlock.Block.blockMaterial.MaxDamage;
								downgradeBlock.rotation = blockValue.rotation;
								blockValue = downgradeBlock;
							}
							blockValue.damage = num2;
						}
						else if (num2 >= maxDamage)
						{
							BlockValue downgradeBlock2 = blockValue.Block.DowngradeBlock;
							downgradeBlock2.rotation = blockValue.rotation;
							blockValue = downgradeBlock2;
						}
						else
						{
							blockValue.damage = num2;
						}
						GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, blockValue, bNotify: false, updateLight: false);
						num++;
					}
				}
			}
			BCCommandAbstract.SendOutput($"Damaging {num} blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void RepairBlocks(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air))
						{
							block.damage = 0;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Repairing {num} blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void SetPaintFace(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!byte.TryParse(BCCommandAbstract.Options["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return;
				}
				if (BlockTextureData.list[result] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result}");
					return;
				}
			}
			uint result2 = 0u;
			if (BCCommandAbstract.Options.ContainsKey("face") && !uint.TryParse(BCCommandAbstract.Options["face"], out result2))
			{
				BCCommandAbstract.SendOutput("Unable to parse face value");
				return;
			}
			if (result2 > 5)
			{
				BCCommandAbstract.SendOutput("Face must be between 0 and 5");
				return;
			}
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i vector3i = new Vector3i(j + position.x, i + position.y, k + position.z);
						if (!GameManager.Instance.World.GetBlock(0, vector3i).Equals(BlockValue.Air))
						{
							GameManager.Instance.World.ChunkClusters[0].SetBlockFaceTexture(vector3i, (BlockFace)result2, result);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Painting {num} blocks on face '{((BlockFace)result2).ToString()}' with texture '{BlockTextureData.GetDataByTextureID(result)?.Name}' @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void SetPaint(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int result = 0;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!int.TryParse(BCCommandAbstract.Options["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return;
				}
				if (BlockTextureData.list[result] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result}");
					return;
				}
			}
			long num = 0L;
			for (int i = 0; i < 6; i++)
			{
				int num2 = i * 8;
				num &= ~(255L << num2);
				num |= (long)(result & 0xFF) << num2;
			}
			long textureFull = num;
			int num3 = 0;
			for (int j = 0; j < size.y; j++)
			{
				for (int k = 0; k < size.x; k++)
				{
					for (int l = 0; l < size.z; l++)
					{
						Vector3i vector3i = new Vector3i(k + position.x, j + position.y, l + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.Block.shape.IsTerrain() && !block.Equals(BlockValue.Air))
						{
							GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, textureFull);
							num3++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Painting {num3} blocks with texture '{BlockTextureData.GetDataByTextureID(result)?.Name}' @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void RemovePaint(Vector3i position, Vector3i size, Dictionary<long, Chunk> modifiedChunks)
		{
			int num = 0;
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i vector3i = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.Block.shape.IsTerrain() && !block.Equals(BlockValue.Air))
						{
							GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, 0L);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Paint removed from {num} blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void SwapBlocks(Vector3i position, Vector3i size, BlockValue newbv, string blockname, Dictionary<long, Chunk> modifiedChunks)
		{
			int result;
			BlockValue blockValue = (int.TryParse(blockname, out result) ? Block.GetBlockValue(result) : Block.GetBlockValue(blockname));
			Block block = Block.list[blockValue.type];
			if (block == null)
			{
				BCCommandAbstract.SendOutput("Unable to find target block by id or name");
				return;
			}
			Block block2 = Block.list[newbv.type];
			if (block2 == null)
			{
				BCCommandAbstract.SendOutput("Unable to find replacement block by id or name");
				return;
			}
			int num = 0;
			World world = GameManager.Instance.World;
			for (int i = 0; i < size.x; i++)
			{
				for (int j = 0; j < size.y; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						sbyte result2 = 1;
						if (BCCommandAbstract.Options.ContainsKey("d"))
						{
							if (sbyte.TryParse(BCCommandAbstract.Options["d"], out result2))
							{
								BCCommandAbstract.SendOutput($"Using density {result2}");
							}
						}
						else if (newbv.Equals(BlockValue.Air))
						{
							result2 = MarchingCubes.DensityAir;
						}
						else if (newbv.Block.shape.IsTerrain())
						{
							result2 = MarchingCubes.DensityTerrain;
						}
						long textureFull = 0L;
						if (BCCommandAbstract.Options.ContainsKey("t"))
						{
							if (!byte.TryParse(BCCommandAbstract.Options["t"], out var result3))
							{
								BCCommandAbstract.SendOutput("Unable to parse texture index");
								return;
							}
							if (BlockTextureData.list[result3] == null)
							{
								BCCommandAbstract.SendOutput($"Unknown texture index {result3}");
								return;
							}
							long num2 = 0L;
							for (int l = 0; l < 6; l++)
							{
								int num3 = l * 8;
								num2 &= ~(255L << num3);
								num2 |= (long)(result3 & 0xFF) << num3;
							}
							textureFull = num2;
						}
						Vector3i vector3i = new Vector3i(i + position.x, j + position.y, k + position.z);
						if (!(world.GetBlock(vector3i).Block.GetBlockName() != block.GetBlockName()))
						{
							world.ChunkClusters[0].SetBlock(vector3i, _bChangeBlockValue: true, newbv, _bChangeDensity: false, result2, bNotify: false, updateLight: false);
							world.ChunkClusters[0].SetTextureFull(vector3i, textureFull);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Replaced {num} '{block.GetBlockName()}' blocks with '{block2.GetBlockName()}' @ {position} to {BCUtils.GetMaxPos(position, size)}");
			BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			Reload(modifiedChunks);
		}

		private static void FillBlocks(Vector3i position, Vector3i size, BlockValue bv, string search, Dictionary<long, Chunk> modifiedChunks)
		{
			if (Block.list[bv.type] == null)
			{
				BCCommandAbstract.SendOutput("Unable to find block by id or name");
				return;
			}
			SetBlocks(0, position, size, bv, search == "*");
			if (BCCommandAbstract.Options.ContainsKey("delmulti"))
			{
				BCCommandAbstract.SendOutput($"Removed multidim blocks @ {position} to {BCUtils.GetMaxPos(position, size)}");
			}
			else
			{
				BCCommandAbstract.SendOutput($"Inserting block '{Block.list[bv.type].GetBlockName()}' @ {position} to {BCUtils.GetMaxPos(position, size)}");
				BCCommandAbstract.SendOutput("Use bc-undo to revert the changes");
			}
			Reload(modifiedChunks);
		}

		private static void SetBlocks(int clrIdx, Vector3i position, Vector3i size, BlockValue bvNew, bool searchAll)
		{
			World world = GameManager.Instance.World;
			ChunkCluster chunkCluster = world.ChunkClusters[clrIdx];
			sbyte result = 1;
			if (BCCommandAbstract.Options.ContainsKey("d") && sbyte.TryParse(BCCommandAbstract.Options["d"], out result))
			{
				BCCommandAbstract.SendOutput($"Using density {result}");
			}
			long textureFull = 0L;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!byte.TryParse(BCCommandAbstract.Options["t"], out var result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture index");
					return;
				}
				if (BlockTextureData.list[result2] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result2}");
					return;
				}
				long num = 0L;
				for (int i = 0; i < 6; i++)
				{
					int num2 = i * 8;
					num &= ~(255L << num2);
					num |= (long)(result2 & 0xFF) << num2;
				}
				textureFull = num;
			}
			for (int j = 0; j < size.y; j++)
			{
				for (int k = 0; k < size.x; k++)
				{
					for (int l = 0; l < size.z; l++)
					{
						Vector3i vector3i = new Vector3i(position.x + k, position.y + j, position.z + l);
						BlockValue block = world.GetBlock(vector3i);
						if (BCCommandAbstract.Options.ContainsKey("delmulti") && (!searchAll || bvNew.type != block.type))
						{
							continue;
						}
						if (block.Block.isMultiBlock && block.ischild)
						{
							Vector3i parentPos = block.Block.multiBlockPos.GetParentPos(vector3i, block);
							BlockValue block2 = chunkCluster.GetBlock(parentPos);
							if (block2.ischild || block2.type != block.type)
							{
								continue;
							}
							chunkCluster.SetBlock(parentPos, BlockValue.Air, bNotify: false, updateLight: false);
						}
						if (BCCommandAbstract.Options.ContainsKey("delmulti"))
						{
							continue;
						}
						if (block.Block.IndexName == "lpblock")
						{
							GameManager.Instance.persistentPlayers.RemoveLandProtectionBlock(new Vector3i(vector3i.x, vector3i.y, vector3i.z));
						}
						Chunk chunk = world.GetChunkFromWorldPos(vector3i.x, vector3i.y, vector3i.z) as Chunk;
						if (bvNew.Equals(BlockValue.Air))
						{
							result = MarchingCubes.DensityAir;
							if (world.GetTerrainHeight(vector3i.x, vector3i.z) > vector3i.y)
							{
								chunk?.SetTerrainHeight(vector3i.x & 0xF, vector3i.z & 0xF, (byte)vector3i.y);
							}
						}
						else if (bvNew.Block.shape.IsTerrain())
						{
							result = MarchingCubes.DensityTerrain;
							if (world.GetTerrainHeight(vector3i.x, vector3i.z) < vector3i.y)
							{
								chunk?.SetTerrainHeight(vector3i.x & 0xF, vector3i.z & 0xF, (byte)vector3i.y);
							}
						}
						else
						{
							world.ChunkClusters[clrIdx].SetTextureFull(vector3i, textureFull);
						}
						world.ChunkClusters[clrIdx].SetBlock(vector3i, _bChangeBlockValue: true, bvNew, _bChangeDensity: true, result, bNotify: false, updateLight: false);
					}
				}
			}
		}

		private static void ScanBlocks(Vector3i position, Vector3i size, BlockValue bv, string search)
		{
			if (Block.list[bv.type] == null && search != "*")
			{
				BCCommandAbstract.SendOutput("Unable to find block by id or name");
				return;
			}
			SortedDictionary<string, int> sortedDictionary = new SortedDictionary<string, int>();
			for (int i = 0; i < size.y; i++)
			{
				for (int j = 0; j < size.x; j++)
				{
					for (int k = 0; k < size.z; k++)
					{
						Vector3i pos = new Vector3i(j + position.x, i + position.y, k + position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.ischild)
						{
							string text = ItemClass.list[block.type]?.Name;
							if (string.IsNullOrEmpty(text))
							{
								text = "air";
							}
							if (search == "*")
							{
								SetStats(text, block, sortedDictionary);
							}
							else if (!(text != bv.Block.GetBlockName()))
							{
								SetStats(text, block, sortedDictionary);
							}
						}
					}
				}
			}
			BCCommandAbstract.SendJson(sortedDictionary);
		}

		private static void SetStats(string name, BlockValue bv, IDictionary<string, int> stats)
		{
			if (stats.ContainsKey($"{bv.type:D4}:{name}"))
			{
				stats[$"{bv.type:D4}:{name}"]++;
			}
			else
			{
				stats.Add($"{bv.type:D4}:{name}", 1);
			}
		}

		private static void Reload(Dictionary<long, Chunk> modifiedChunks)
		{
			if (!BCCommandAbstract.Options.ContainsKey("noreload") && !BCCommandAbstract.Options.ContainsKey("nr"))
			{
				BCChunks.ReloadForClients(modifiedChunks);
			}
		}
	}
}
