using System;
using System.Linq;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCProtect : BCCommandAbstract
	{
		public BCProtect()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCProtect"];
			DefaultCommands = new string[2]
			{
				"bc-protect",
				"protect"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCProtect.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count < 5)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (!BCUtils.GetXZPair(BCCommandAbstract.Params.Skip(0).Take(4).ToArray(), out var pos, out var pos2, out var error))
			{
				BCCommandAbstract.SendOutput(error);
				return;
			}
			if (BCCommandAbstract.Params[4].Equals("show", StringComparison.OrdinalIgnoreCase))
			{
				if (!BCUtils.GetLoadedChunksInArea(world, pos, pos2, out var chunks, out var error2))
				{
					BCCommandAbstract.SendOutput(error2);
					return;
				}
				int num = 0;
				for (int i = pos.x; i <= pos2.x; i++)
				{
					for (int j = pos.y; j <= pos2.y; j++)
					{
						long key = WorldChunkCache.MakeChunkKey(World.toChunkXZ(i), World.toChunkXZ(j), 0);
						bool flag = chunks[key].IsTraderArea(World.toBlockXZ(i), World.toBlockXZ(j));
						num += (flag ? 1 : 0);
					}
				}
				BCCommandAbstract.SendOutput($"Area contains {num}/{(1 + pos2.x - pos.x) * (1 + pos2.y - pos.y)} - {Math.Round((float)num * 1f / (float)(1 + pos2.x - pos.x) * (float)(1 + pos2.y - pos.y), 1) * 100.0}% protected blocks");
				return;
			}
			if (!bool.TryParse(BCCommandAbstract.Params[4], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse enable setting " + BCCommandAbstract.Params[4] + " as a boolean");
				return;
			}
			if (!BCUtils.GetLoadedChunksInArea(world, pos, pos2, out var chunks2, out var error3))
			{
				BCCommandAbstract.SendOutput(error3);
				return;
			}
			for (int k = pos.x; k <= pos2.x; k++)
			{
				for (int l = pos.y; l <= pos2.y; l++)
				{
					long key2 = WorldChunkCache.MakeChunkKey(World.toChunkXZ(k), World.toChunkXZ(l), 0);
					chunks2[key2].SetTraderArea(World.toBlockXZ(k), World.toBlockXZ(l), result);
				}
			}
			BCChunks.ReloadForClients(chunks2);
			BCCommandAbstract.SendOutput(string.Format("{0} trader area in {1}, {2} - {3}, {4}", result ? "Enabled " : "Disabled", pos.x, pos.y, pos2.x, pos2.y));
		}
	}
}
