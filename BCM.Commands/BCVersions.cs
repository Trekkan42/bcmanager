using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCVersions : BCCommandAbstract
	{
		public BCVersions()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCVersions"];
			DefaultCommands = new string[1]
			{
				"bc-version"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCVersions.ParseHelp(this);
		}

		protected override void Process()
		{
			List<Mod> loadedMods = ModManager.GetLoadedMods();
			List<BCMModInfo> list = new List<BCMModInfo>();
			BCMModInfo item = new BCMModInfo("7 Days To Die (7DTD)", $"{19}.{0} {154}", "http://7daystodie.com", "The Fun Pimps", "The survival horde crafting game");
			list.Add(item);
			list.AddRange(loadedMods.Select((Mod mod) => new BCMModInfo(mod)));
			BCCommandAbstract.SendJson(list);
		}
	}
}
