using System;
using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCChunkInfo : BCCommandAbstract
	{
		public BCChunkInfo()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCChunkInfo"];
			DefaultCommands = new string[1]
			{
				"bc-chunkinfo"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCChunkInfo.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld(out var world))
			{
				BCMCmdArea bCMCmdArea = new BCMCmdArea(new List<string>
				{
					"x"
				}.Concat(BCCommandAbstract.Params).ToList(), BCCommandAbstract.Options, "ChunkInfo");
				EntityPlayer entity;
				if (!BCMCmdArea.ProcessParams(bCMCmdArea, 14))
				{
					BCCommandAbstract.SendOutput(GetHelp());
				}
				else if (!BCMCmdArea.GetIds(world, bCMCmdArea, out entity))
				{
					BCCommandAbstract.SendOutput("Command requires a position when not run by a player.");
				}
				else if (!bCMCmdArea.HasChunkPos && !bCMCmdArea.HasPos && !BCMCmdArea.GetEntPos(bCMCmdArea, entity))
				{
					BCCommandAbstract.SendOutput("Unable to get position.");
				}
				else
				{
					BCMCmdArea.DoProcess(world, bCMCmdArea, this);
				}
			}
		}

		public override void ProcessSwitch(World world, BCMCmdArea command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
			_ = command.Command;
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					string key = $"{i},{j}";
					if (!dictionary.ContainsKey(key))
					{
						int _eventCount;
						int value = (int)Math.Floor(GameManager.Instance.World.aiDirector.GetHeatValueForBlockPos(new Vector3i(i * 16, 0, j * 16), out _eventCount));
						dictionary.Add(key, value);
					}
				}
			}
			BCCommandAbstract.SendJson(dictionary);
		}
	}
}
