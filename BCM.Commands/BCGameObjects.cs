using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGameObjects : BCCommandAbstract
	{
		public BCGameObjects()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGameObjects"];
			DefaultCommands = new string[1]
			{
				"bc-go"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGameObjects.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			switch (BCCommandAbstract.Params.Count)
			{
			case 0:
			{
				Type o = typeof(BCMGameObject.GOTypes);
				BCCommandAbstract.SendJson((from f in o.GetFields().AsQueryable()
					where f.Name != "Players" && f.Name != "Entities"
					select f).ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => field.GetValue(o).ToString()));
				break;
			}
			case 1:
			{
				string text = BCCommandAbstract.Params[0].ToLower();
				if (BCCommandAbstract.Options.ContainsKey("filters"))
				{
					DisplayFilters(text);
					break;
				}
				if (BCCommandAbstract.Options.ContainsKey("index"))
				{
					DisplayIndex(text);
					break;
				}
				GetObjects(text, out var objects);
				ProcessObjects(text, objects, out var data, BCCommandAbstract.GetFilters(text));
				BCCommandAbstract.SendJson(data);
				break;
			}
			case 2:
			{
				string text = BCCommandAbstract.Params[0].ToLower();
				if (GetObject(text, out var obj))
				{
					BCCommandAbstract.SendObject(new BCMGameObject(obj, text, BCCommandAbstract.Options, BCCommandAbstract.GetFilters(text)));
				}
				break;
			}
			default:
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static void DisplayIndex(string type)
		{
			switch (type)
			{
			case "archetypes":
				BCCommandAbstract.SendJson((from kvp in BCMArchetype.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "blocks":
				BCCommandAbstract.SendJson((from kvp in BCMItemClass.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "buffs":
				BCCommandAbstract.SendJson((from kvp in BCMBuff.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "entityclasses":
				BCCommandAbstract.SendJson((from kvp in BCMEntityClass.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "entitygroups":
				BCCommandAbstract.SendJson((from kvp in BCMEntityGroup.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "itemclasses":
				BCCommandAbstract.SendJson((from kvp in BCMItemClass.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "items":
				BCCommandAbstract.SendJson((from kvp in BCMItemClass.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "materials":
				BCCommandAbstract.SendJson((from kvp in BCMMaterial.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "prefabs":
				BCCommandAbstract.SendJson((from kvp in BCMPrefab.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "quests":
				BCCommandAbstract.SendJson((from kvp in BCMQuest.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "recipes":
				BCCommandAbstract.SendJson((from kvp in BCMRecipe.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "skills":
				BCCommandAbstract.SendJson((from kvp in BCMSkill.FilterMap
					group kvp by kvp.Value into @group
					select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
				break;
			case "players":
				BCCommandAbstract.SendOutput("use bc-lp instead for player data");
				break;
			case "entities":
				BCCommandAbstract.SendOutput("use bc-le instead for entity data");
				break;
			default:
				BCCommandAbstract.SendJsonError("Unknown Type");
				break;
			}
		}

		private static void DisplayFilters(string type)
		{
			switch (type)
			{
			case "archetypes":
				BCCommandAbstract.SendJson(typeof(BCMArchetype.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "blocks":
				BCCommandAbstract.SendJson(typeof(BCMItemClass.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "buffs":
				BCCommandAbstract.SendJson(typeof(BCMBuff.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "entityclasses":
				BCCommandAbstract.SendJson(typeof(BCMEntityClass.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "entitygroups":
				BCCommandAbstract.SendJson(typeof(BCMEntityGroup.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "itemclasses":
				BCCommandAbstract.SendJson(typeof(BCMItemClass.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "items":
				BCCommandAbstract.SendJson(typeof(BCMItemClass.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "materials":
				BCCommandAbstract.SendJson(typeof(BCMMaterial.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "prefabs":
				BCCommandAbstract.SendJson(typeof(BCMPrefab.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "quests":
				BCCommandAbstract.SendJson(typeof(BCMQuest.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "recipes":
				BCCommandAbstract.SendJson(typeof(BCMRecipe.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "skills":
				BCCommandAbstract.SendJson(typeof(BCMSkill.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
				break;
			case "players":
				BCCommandAbstract.SendOutput("use bc-lp instead for player data");
				break;
			case "entities":
				BCCommandAbstract.SendOutput("use bc-le instead for entity data");
				break;
			default:
				BCCommandAbstract.SendJsonError("Unknown Type");
				break;
			}
		}

		private static void ProcessObjects(string type, IEnumerable<object> objects, out List<object> data, List<string> filter)
		{
			data = new List<object>();
			foreach (object @object in objects)
			{
				BCMGameObject bCMGameObject = new BCMGameObject(@object, type, BCCommandAbstract.Options, filter);
				if (BCCommandAbstract.Options.ContainsKey("min"))
				{
					data.Add((from d in bCMGameObject.Data()
						select d.Value).ToList());
				}
				else
				{
					data.Add(bCMGameObject.Data());
				}
			}
		}

		private static bool GetObject(string type, out object obj)
		{
			obj = null;
			string name = BCCommandAbstract.Params[1];
			switch (type)
			{
			case "archetypes":
				obj = Archetypes.Instance.GetArchetype(name);
				break;
			default:
				BCCommandAbstract.SendJsonError("Unknown Type");
				break;
			case "blocks":
			case "buffs":
			case "entityclasses":
			case "entitygroups":
			case "itemclasses":
			case "items":
			case "materials":
			case "prefabs":
			case "quests":
			case "recipes":
			case "skills":
				break;
			}
			if (obj != null)
			{
				return true;
			}
			BCCommandAbstract.SendOutput("Object not found.");
			return false;
		}

		private static void GetObjects(string type, out List<object> objects)
		{
			objects = new List<object>();
			switch (type)
			{
			case "archetypes":
				GetArchetypes(objects);
				break;
			case "blocks":
				GetBlocks(objects);
				break;
			case "buffs":
				GetBuffs(objects);
				break;
			case "entityclasses":
				GetEntityClasses(objects);
				break;
			case "entitygroups":
				GetEntityGroups(objects);
				break;
			case "itemclasses":
				GetItemClasses(objects);
				break;
			case "items":
				GetItems(objects);
				break;
			case "materials":
				GetMaterials(objects);
				break;
			case "prefabs":
				GetPrefabs(objects);
				break;
			case "quests":
				GetQuests(objects);
				break;
			case "recipes":
				GetRecipes(objects);
				break;
			case "skills":
				GetSkills(objects);
				break;
			case "players":
				BCCommandAbstract.SendOutput("use bc-lp instead for player data");
				break;
			case "entities":
				BCCommandAbstract.SendOutput("use bc-le instead for entity data");
				break;
			default:
				BCCommandAbstract.SendJsonError("Unknown Type");
				break;
			}
		}

		private static void GetSkills(List<object> objects)
		{
			IEnumerable<ProgressionClass> values = Progression.ProgressionClasses.Values;
			objects.AddRange(values);
		}

		private static void GetRecipes(List<object> objects)
		{
			objects.AddRange(CraftingManager.GetAllRecipes());
		}

		private static void GetQuests(List<object> objects)
		{
			objects.AddRange(QuestClass.s_Quests.Values);
		}

		private static void GetPrefabs(List<object> objects)
		{
			string gameDir = Utils.GetGameDir("Data/Prefabs");
			string[] files = Directory.GetFiles(gameDir, "*");
			for (int num = files.Length - 1; num >= 0; num--)
			{
				string text = files[num];
				if (!(Path.GetExtension(text) != ".tts"))
				{
					int num2 = gameDir.Length + 1;
					int num3 = text.Length - num2 - 4;
					if (num2 + num3 <= text.Length)
					{
						objects.Add(text.Substring(num2, num3));
					}
				}
			}
		}

		private static void GetMaterials(List<object> objects)
		{
			objects.AddRange(MaterialBlock.materials.Values);
		}

		private static void GetItems(List<object> objects)
		{
			int mAX_BLOCKS = Block.MAX_BLOCKS;
			int mAX_ITEMS = ItemClass.MAX_ITEMS;
			for (int i = mAX_BLOCKS; i < mAX_ITEMS; i++)
			{
				if (ItemClass.list[i] != null)
				{
					objects.Add(ItemClass.list[i]);
				}
			}
		}

		private static void GetItemClasses(List<object> objects)
		{
			int mAX_ITEMS = ItemClass.MAX_ITEMS;
			for (int i = 0; i < mAX_ITEMS; i++)
			{
				if (ItemClass.list[i] != null)
				{
					objects.Add(ItemClass.list[i]);
				}
			}
		}

		private static void GetEntityGroups(List<object> objects)
		{
			objects.AddRange(EntityGroups.list.Dict.Select((KeyValuePair<string, List<SEntityClassAndProb>> k) => new KeyValuePair<string, List<SEntityClassAndProb>>(k.Key, EntityGroups.list[k.Key])).Cast<object>());
		}

		private static void GetEntityClasses(List<object> objects)
		{
			objects.AddRange(EntityClass.list.Dict.Keys.Select((int e) => EntityClass.list[e]));
		}

		private static void GetBuffs(List<object> objects)
		{
			objects.AddRange(BuffManager.Buffs.Values);
		}

		private static void GetBlocks(List<object> objects)
		{
			int mAX_BLOCKS = Block.MAX_BLOCKS;
			for (int i = 0; i < mAX_BLOCKS; i++)
			{
				if (ItemClass.list[i] != null)
				{
					objects.Add(ItemClass.list[i]);
				}
			}
		}

		private static void GetArchetypes(List<object> objects)
		{
			objects.AddRange(from n in Archetypes.Instance.GetArchetypeNames()
				select Archetypes.Instance.GetArchetype(n));
		}
	}
}
