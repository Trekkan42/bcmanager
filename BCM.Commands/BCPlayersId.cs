using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCPlayersId : BCPlayers
	{
		public BCPlayersId()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayersId"];
			DefaultCommands = new string[1]
			{
				"bc-id"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPlayersId.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Options.ContainsKey("filter"))
			{
				BCCommandAbstract.SendOutput("Error: Can't set filters on this alias command");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string text = "entityid";
			if (BCCommandAbstract.Options.ContainsKey("n"))
			{
				text += ",name";
			}
			if (BCCommandAbstract.Options.ContainsKey("s"))
			{
				text += ",steamid";
			}
			BCCommandAbstract.Options.Add("filter", text);
			new BCPlayers().Process(BCCommandAbstract.Options, BCCommandAbstract.Params);
		}
	}
}
