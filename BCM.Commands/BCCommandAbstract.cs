using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BCM.Models;
using LitJson;
using UnityEngine;

namespace BCM.Commands
{
	public abstract class BCCommandAbstract : ConsoleCmdAbstract
	{
		public static CommandSenderInfo SenderInfo;

		public static List<string> Params = new List<string>();

		protected static Dictionary<string, string> Options = new Dictionary<string, string>();

		public Dictionary<string, string> DefaultOptions;

		public Dictionary<string, string> BaseOptions;

		public string[] DefaultCommands;

		public string[] Commands;

		public string[] GetDefaults()
		{
			return DefaultCommands;
		}

		public override string[] GetCommands()
		{
			return Commands;
		}

		public override string GetDescription()
		{
			return GetHelp().Split('\r', '\n').FirstOrDefault();
		}

		public override void Execute(List<string> _params, CommandSenderInfo senderInfo)
		{
			SenderInfo = senderInfo;
			Params = new List<string>();
			Options = new Dictionary<string, string>();
			ParseParams(_params);
			try
			{
				Process();
			}
			catch (Exception ex)
			{
				SendOutput("Error while executing command.");
				Log.Out(string.Format("{0} Error in {1}.{2}: {3}", "(BCM)", GetType().Name, MethodBase.GetCurrentMethod().Name, ex));
			}
		}

		public void Process(Dictionary<string, string> options, List<string> _params)
		{
			Options = options;
			Params = _params;
			Process();
		}

		protected virtual void Process()
		{
		}

		public virtual void ProcessSwitch(World world, BCMCmdArea command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
		}

		public virtual void ProcessCmd(BCMCmdProcessor command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
		}

		private void ParseParams(IEnumerable<string> _params)
		{
			foreach (string _param in _params)
			{
				if (_param.IndexOf('/', 0) != 0)
				{
					Params.Add(_param);
					continue;
				}
				if (_param.Length == 1)
				{
					Params.Add("/");
					continue;
				}
				if (_param.IndexOf('=', 1) == -1)
				{
					Options.Add(_param.Substring(1).ToLower(), null);
					continue;
				}
				string[] array = _param.Substring(1).Split('=');
				Options.Add((array[0] == "f") ? "filter" : array[0], array[1]);
			}
			if (BaseOptions == null || BaseOptions.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<string, string> baseOption in BaseOptions)
			{
				string text = baseOption.Key.Trim().ToLower();
				if (!string.IsNullOrEmpty(text) && (text != "online" || (!Options.ContainsKey("offline") && !Options.ContainsKey("all"))) && (text != "offline" || (!Options.ContainsKey("online") && !Options.ContainsKey("all"))) && (text != "nl" || !Options.ContainsKey("nonl")) && (text != "csv" || !Options.ContainsKey("nocsv")) && (text != "loc" || !Options.ContainsKey("r")) && (text != "details" || !Options.ContainsKey("nodetails")) && (text != "strpos" || !Options.ContainsKey("vectors")) && (text != "strpos" || !Options.ContainsKey("csvpos")) && (text != "strpos" || !Options.ContainsKey("worldpos")) && !Options.ContainsKey(text))
				{
					Options.Add(text, baseOption.Value);
				}
			}
		}

		protected static void SendJsonError(string err)
		{
			SendJson(new
			{
				error = err
			});
		}

		public static void SendJson(object data)
		{
			JsonWriter jsonWriter = new JsonWriter();
			if (Options.ContainsKey("pp") && !Options.ContainsKey("1l"))
			{
				jsonWriter.IndentValue = 2;
				jsonWriter.PrettyPrint = true;
			}
			JsonMapper.RegisterExporter(delegate(float o, JsonWriter w)
			{
				w.Write(Convert.ToDouble(o));
			});
			string f = (Options.ContainsKey("strpos") ? "S" : (Options.ContainsKey("worldpos") ? "W" : (Options.ContainsKey("csvpos") ? "C" : "V")));
			JsonMapper.RegisterExporter(delegate(Vector3 v, JsonWriter w)
			{
				BCUtils.WriteVector3(v, w, f);
			});
			JsonMapper.RegisterExporter(delegate(Vector3i v, JsonWriter w)
			{
				BCUtils.WriteVector3i(v, w, f);
			});
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (Options.ContainsKey("tag"))
			{
				dictionary.Add("tag", Options["tag"]);
				dictionary.Add("data", data);
				JsonMapper.ToJson(dictionary, jsonWriter);
			}
			else
			{
				JsonMapper.ToJson(data, jsonWriter);
			}
			SendOutput(jsonWriter.ToString().TrimStart());
		}

		public static void SendOutput(string output)
		{
			if (Options.ContainsKey("log"))
			{
				Log.Out(output);
			}
			else if (Options.ContainsKey("chat"))
			{
				if (Options.ContainsKey("color"))
				{
					output = "[" + Options["color"] + "]" + output + "[-]";
				}
				string[] array = output.Split('\n');
				foreach (string msg in array)
				{
					GameManager.Instance.ChatMessageServer(null, EChatType.Global, -1, msg, BCChat.ServerGlobal, _localizeMain: false, null);
				}
			}
			else if (ThreadManager.IsMainThread())
			{
				string[] array = output.Split('\n');
				foreach (string line in array)
				{
					SingletonMonoBehaviour<SdtdConsole>.Instance.Output(line);
				}
			}
			else if (SenderInfo.RemoteClientInfo != null)
			{
				string[] array = output.Split('\n');
				foreach (string line2 in array)
				{
					SenderInfo.RemoteClientInfo.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup(line2, _bExecute: false));
				}
			}
			else if (SenderInfo.NetworkConnection is TelnetConnection)
			{
				string[] array = output.Split('\n');
				foreach (string text in array)
				{
					SenderInfo.NetworkConnection.SendLine(text);
				}
			}
			else
			{
				Log.Out("(BCM)" + output);
			}
		}

		protected static List<string> GetFilters(string type)
		{
			if (!Options.ContainsKey("filter"))
			{
				return new List<string>();
			}
			List<string> list = new List<string>();
			string[] array = Options["filter"].ToLower().Split(',');
			string[] array2 = array;
			foreach (string cur in array2)
			{
				int result;
				string text = (int.TryParse(cur, out result) ? GetFilter(result, type) : cur);
				if (text == null)
				{
					continue;
				}
				if (list.Contains(text))
				{
					Log.Out("(BCM) Duplicate filter index *" + text + "* in " + array.Aggregate("", (string c, string f) => c + ((f == cur) ? ("*" + f + "*,") : (f + ","))) + " skipping");
				}
				else
				{
					list.Add(text);
				}
			}
			return list;
		}

		protected static void SendObject(BCMAbstract gameobj)
		{
			if (Options.ContainsKey("min"))
			{
				SendJson(new List<List<object>>
				{
					(from d in gameobj.Data()
						select d.Value).ToList()
				});
			}
			else
			{
				SendJson(gameobj.Data());
			}
		}

		private static string GetFilter(int f, string type)
		{
			switch (type)
			{
			case "players":
				if (!BCMPlayer.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMPlayer.FilterMap[f];
			case "entities":
				if (!BCMEntity.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMEntity.FilterMap[f];
			case "archetypes":
				if (!BCMArchetype.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMArchetype.FilterMap[f];
			case "blocks":
				if (!BCMItemClass.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMItemClass.FilterMap[f];
			case "buffs":
				if (!BCMBuff.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMBuff.FilterMap[f];
			case "entityclasses":
				if (!BCMEntityClass.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMEntityClass.FilterMap[f];
			case "entitygroups":
				if (!BCMEntityGroup.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMEntityGroup.FilterMap[f];
			case "itemclasses":
				if (!BCMItemClass.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMItemClass.FilterMap[f];
			case "items":
				if (!BCMItemClass.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMItemClass.FilterMap[f];
			case "materials":
				if (!BCMMaterial.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMMaterial.FilterMap[f];
			case "prefabs":
				if (!BCMPrefab.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMPrefab.FilterMap[f];
			case "quests":
				if (!BCMQuest.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMQuest.FilterMap[f];
			case "recipes":
				if (!BCMRecipe.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMRecipe.FilterMap[f];
			case "skills":
				if (!BCMSkill.FilterMap.ContainsKey(f))
				{
					return null;
				}
				return BCMSkill.FilterMap[f];
			default:
				return null;
			}
		}
	}
}
