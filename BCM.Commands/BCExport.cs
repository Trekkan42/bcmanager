using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCExport : BCCommandAbstract
	{
		public BCExport()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCExport"];
			DefaultCommands = new string[2]
			{
				"bc-export",
				"export"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCExport.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld(out var world))
			{
				BCMCmdArea bCMCmdArea = new BCMCmdArea(BCCommandAbstract.Params, BCCommandAbstract.Options, "Export");
				EntityPlayer entity;
				if (!BCMCmdArea.ProcessParams(bCMCmdArea, 14))
				{
					BCCommandAbstract.SendOutput(GetHelp());
				}
				else if (!BCMCmdArea.GetIds(world, bCMCmdArea, out entity))
				{
					BCCommandAbstract.SendOutput("Command requires a position when not run by a player.");
				}
				else if (!bCMCmdArea.HasChunkPos && !bCMCmdArea.HasPos && !BCMCmdArea.GetEntPos(bCMCmdArea, entity))
				{
					BCCommandAbstract.SendOutput("Unable to get position.");
				}
				else
				{
					BCMCmdArea.DoProcess(world, bCMCmdArea, this);
				}
			}
		}

		public override void ProcessSwitch(World world, BCMCmdArea command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
			ExportPrefab(command, world);
		}

		public static Prefab CopyFromWorld(World world, Vector3i pos, Vector3i size)
		{
			Vector3i maxPos = BCUtils.GetMaxPos(pos, size);
			Prefab prefab = new Prefab(size);
			int num = 0;
			int num2 = pos.y;
			while (num2 <= maxPos.y)
			{
				int num3 = 0;
				int num4 = pos.x;
				while (num4 <= maxPos.x)
				{
					int num5 = 0;
					int num6 = pos.z;
					while (num6 <= maxPos.z)
					{
						prefab.SetBlock(num3, num, num5, world.GetBlock(num4, num2, num6));
						prefab.SetDensity(num3, num, num5, world.GetDensity(0, num4, num2, num6));
						prefab.SetTexture(num3, num, num5, world.GetTexture(num4, num2, num6));
						TileEntity tileEntity = world.GetTileEntity(0, new Vector3i(num4, num2, num6));
						if (tileEntity != null)
						{
							switch (tileEntity.GetTileEntityType())
							{
							case TileEntityType.VendingMachine:
							{
								TileEntityVendingMachine tileEntityVendingMachine = tileEntity as TileEntityVendingMachine;
								if (tileEntityVendingMachine != null && tileEntityVendingMachine.IsLocked())
								{
									BlockValue block3 = prefab.GetBlock(num3, num, num5);
									block3.meta |= 4;
									prefab.SetBlock(num3, num, num5, block3);
								}
								break;
							}
							case TileEntityType.SecureLoot:
							{
								TileEntitySecureLootContainer tileEntitySecureLootContainer = tileEntity as TileEntitySecureLootContainer;
								if (tileEntitySecureLootContainer != null && tileEntitySecureLootContainer.IsLocked())
								{
									BlockValue block2 = prefab.GetBlock(num3, num, num5);
									block2.meta |= 4;
									prefab.SetBlock(num3, num, num5, block2);
								}
								break;
							}
							case TileEntityType.SecureDoor:
							{
								TileEntitySecureDoor tileEntitySecureDoor = tileEntity as TileEntitySecureDoor;
								if (tileEntitySecureDoor != null && tileEntitySecureDoor.IsLocked())
								{
									BlockValue block4 = prefab.GetBlock(num3, num, num5);
									block4.meta |= 4;
									prefab.SetBlock(num3, num, num5, block4);
								}
								break;
							}
							case TileEntityType.Sign:
							{
								TileEntitySign tileEntitySign = tileEntity as TileEntitySign;
								if (tileEntitySign != null && tileEntitySign.IsLocked())
								{
									BlockValue block = prefab.GetBlock(num3, num, num5);
									block.meta |= 4;
									prefab.SetBlock(num3, num, num5, block);
								}
								break;
							}
							}
						}
						num6++;
						num5++;
					}
					num4++;
					num3++;
				}
				num2++;
				num++;
			}
			prefab.addAllChildBlocks();
			return prefab;
		}

		private static void ExportPrefab(BCMCmdArea command, World world)
		{
			if (command.Position == null)
			{
				command.Position = new BCMVector3(command.ChunkBounds.x * 16, 0, command.ChunkBounds.y * 16);
				command.Size = new BCMVector3((command.ChunkBounds.z - command.ChunkBounds.x) * 16 + 15, 255, (command.ChunkBounds.w - command.ChunkBounds.y) * 16 + 15);
			}
			Prefab prefab = CopyFromWorld(world, command.Position.V3i(), command.Size.V3i());
			prefab.PrefabName = command.Pars[0];
			BCCommandAbstract.SendOutput(prefab.Save(prefab.PrefabName) ? $"Prefab {prefab.PrefabName} exported @ {command.Position}, size={command.Size}" : ("Error: Prefab " + prefab.PrefabName + " failed to save."));
		}
	}
}
