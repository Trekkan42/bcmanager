using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCUnlockAll : BCCommandAbstract
	{
		public BCUnlockAll()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCUnlockAll"];
			DefaultCommands = new string[1]
			{
				"bc-unlockall"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCUnlockAll.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCCommandAbstract.Options.ContainsKey("r"))
			{
				BCCommandAbstract.Options.Add("r", "8");
			}
			if (!BCCommandAbstract.Params.Contains("access"))
			{
				List<string> list = new List<string>();
				list.Add("access");
				list.AddRange(BCCommandAbstract.Params);
				BCCommandAbstract.Params = list;
			}
			new BCTileEntity().Process(BCCommandAbstract.Options, BCCommandAbstract.Params);
		}
	}
}
