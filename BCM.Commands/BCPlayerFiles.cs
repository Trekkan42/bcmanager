using System.Collections.Generic;
using System.IO;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using JetBrains.Annotations;

namespace BCM.Commands
{
	public class BCPlayerFiles : BCCommandAbstract
	{
		public BCPlayerFiles()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayerFiles"];
			DefaultCommands = new string[1]
			{
				"bc-pdf"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPlayerFiles.ParseHelp(this);
		}

		protected override void Process()
		{
			List<BCMPlayerDataFile> list = new List<BCMPlayerDataFile>();
			FileSystemInfo[] files = GetFiles(GameUtils.GetPlayerDataDir());
			if (files != null)
			{
				FileSystemInfo[] array = files;
				foreach (FileSystemInfo fileSystemInfo in array)
				{
					if (!(fileSystemInfo.Extension != ".ttp"))
					{
						BCMPlayerDataFile bCMPlayerDataFile = new BCMPlayerDataFile
						{
							SteamId = fileSystemInfo.Name.Substring(0, fileSystemInfo.Name.Length - fileSystemInfo.Extension.Length),
							LastWrite = fileSystemInfo.LastWriteTimeUtc.ToUtcStr()
						};
						BCMWorldPlayerData bCMWorldPlayerData = BCMPersist.Instance.WorldPlayersData[bCMPlayerDataFile.SteamId, false];
						if (bCMWorldPlayerData != null)
						{
							bCMPlayerDataFile.Name = bCMWorldPlayerData.Name;
							bCMPlayerDataFile.LastOnline = bCMWorldPlayerData.LastOnline.ToUtcStr();
							bCMPlayerDataFile.IsOnline = bCMWorldPlayerData.IsOnline;
							bCMPlayerDataFile.LastLogPos = bCMWorldPlayerData.LastLogoutPos?.ToString();
						}
						list.Add(bCMPlayerDataFile);
					}
				}
			}
			if (BCCommandAbstract.Options.ContainsKey("min"))
			{
				BCCommandAbstract.SendJson(list.Select((BCMPlayerDataFile player) => new string[6]
				{
					player.Name,
					player.SteamId,
					player.IsOnline.ToString(),
					player.LastOnline,
					player.LastWrite,
					player.LastLogPos
				}).ToList());
			}
			else
			{
				BCCommandAbstract.SendJson(list);
			}
		}

		[CanBeNull]
		private static FileSystemInfo[] GetFiles(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			if (directoryInfo.Exists)
			{
				return directoryInfo.GetFileSystemInfos();
			}
			return null;
		}
	}
}
