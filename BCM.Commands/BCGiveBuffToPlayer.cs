using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGiveBuffToPlayer : BCCommandAbstract
	{
		public BCGiveBuffToPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGiveBuffToPlayer"];
			DefaultCommands = new string[2]
			{
				"bc-givebuff",
				"bc-gpb"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGiveBuffToPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else
			{
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _, out var _cInfo))
				{
					return;
				}
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				BuffClass buff = BuffManager.GetBuff(BCCommandAbstract.Params[1]);
				if (buff == null)
				{
					BCCommandAbstract.SendOutput("Unable to find buff " + BCCommandAbstract.Params[1]);
					return;
				}
				_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup("buff " + BCCommandAbstract.Params[1], _bExecute: true));
				BCCommandAbstract.SendOutput("Buff " + (buff.LocalizedName ?? buff.Name) + " given to player " + _cInfo.playerName);
			}
		}
	}
}
