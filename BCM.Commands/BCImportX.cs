using System.Collections.Generic;
using BCM.PersistentData;
using UnityEngine;

namespace BCM.Commands
{
	public class BCImportX : BCCommandAbstract
	{
		public BCImportX()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCImportX"];
			DefaultCommands = new string[2]
			{
				"bc-importx",
				"importx"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return "importx <prefabName> <x> <y> <z> [r]Options:  /reset-terrain - reset the terrain in the area to the original state when the world was created  /no-entities - skip the process that adds entities saved in the prefab to the world  /no-enemies - if entities spawns are on this will prevent any enemy entities from spawning but allow others  /no-sleepers - spawn sleeper blocks instead of creating sleeper areas  /no-tf-swap - don't swap terrain filler blocks with the biome dirt";
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Params.Count < 4)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (GameManager.Instance.World == null)
			{
				BCCommandAbstract.SendOutput("World not loaded.");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[1], out var result))
			{
				BCCommandAbstract.SendOutput("Couldn't parse x:'" + BCCommandAbstract.Params[1] + "' as a number.");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[2], out var result2))
			{
				BCCommandAbstract.SendOutput("Couldn't parse y:'" + BCCommandAbstract.Params[2] + "' as a number.");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[3], out var result3))
			{
				BCCommandAbstract.SendOutput("Couldn't parse z:'" + BCCommandAbstract.Params[3] + "' as a number.");
				return;
			}
			byte result4 = 0;
			if (BCCommandAbstract.Params.Count > 4 && !byte.TryParse(BCCommandAbstract.Params[4], out result4))
			{
				BCCommandAbstract.SendOutput("Couldn't parse r:" + BCCommandAbstract.Params[4] + " as a number.");
			}
			else
			{
				ImportPrefab(GameManager.Instance.World, new Vector3i(result, result2, result3), result4);
			}
		}

		private static void ImportPrefab(World world, Vector3i pos, byte rot, QuestTags tags = QuestTags.none)
		{
			DynamicPrefabDecorator dynamicPrefabDecorator = GameManager.Instance.GetDynamicPrefabDecorator();
			Prefab prefab = dynamicPrefabDecorator.GetPrefab(BCCommandAbstract.Params[0]);
			HashSetLong occupiedChunks = GetOccupiedChunks(pos, prefab.size);
			if (BCCommandAbstract.Options.ContainsKey("reset-terrain"))
			{
				BCCommandAbstract.SendOutput("Resetting terrain...");
				world.RebuildTerrain(occupiedChunks, pos, prefab.size, _bStopStabilityUpdate: false, _bRegenerateChunk: false, _bFillEmptyBlocks: false);
			}
			ChunkCluster chunkCluster = world.ChunkClusters[0];
			chunkCluster.ChunkPosNeedsRegeneration_DelayedStart();
			CopyIntoWorld(world, chunkCluster, prefab, pos, !BCCommandAbstract.Options.ContainsKey("no-entities"), bOverwrite: true, tags);
			foreach (long item2 in occupiedChunks)
			{
				chunkCluster.GetChunkSync(item2)?.ResetStability();
			}
			Chunk[] neighbours = new Chunk[8];
			foreach (long item3 in occupiedChunks)
			{
				Chunk chunkSync = chunkCluster.GetChunkSync(item3);
				if (chunkSync != null && chunkCluster.GetNeighborChunks(chunkSync, neighbours))
				{
					chunkCluster.LightChunk(chunkSync, neighbours, null);
				}
			}
			List<TileEntity> list = new List<TileEntity>(10);
			foreach (long item4 in occupiedChunks)
			{
				Chunk chunkSync2 = chunkCluster.GetChunkSync(item4);
				if (chunkSync2 == null)
				{
					continue;
				}
				list.Clear();
				List<TileEntity> list2 = chunkSync2.GetTileEntities().list;
				for (int num = list2.Count - 1; num >= 0; num--)
				{
					if (Block.list[chunkSync2.GetBlock(list2[num].localChunkPos).type].HasTileEntity)
					{
						Vector3i vector3i = list2[num].ToWorldPos();
						if (pos.x <= vector3i.x && pos.y <= vector3i.y && pos.z <= vector3i.z && pos.x + prefab.size.x > vector3i.x && pos.y + prefab.size.y > vector3i.y && pos.z + prefab.size.z > vector3i.z)
						{
							list2[num].Reset();
						}
					}
					else
					{
						list.Add(list2[num]);
					}
				}
				foreach (TileEntity item5 in list)
				{
					chunkSync2.RemoveTileEntity(world, item5);
				}
				chunkSync2.StopStabilityCalculation = false;
			}
			chunkCluster.ChunkPosNeedsRegeneration_DelayedStop();
			foreach (long item6 in occupiedChunks)
			{
				Chunk chunkSync3 = chunkCluster.GetChunkSync(item6);
				if (chunkSync3 == null)
				{
					continue;
				}
				foreach (ChunkManager.ChunkObserver observedEntity in world.m_ChunkManager.m_ObservedEntities)
				{
					if (!observedEntity.bBuildVisualMeshAround && observedEntity.chunksLoaded.Contains(item6))
					{
						SingletonMonoBehaviour<ConnectionManager>.Instance.SendPackage(NetPackageManager.GetPackage<NetPackageChunk>().Setup(chunkSync3, _bOverwriteExisting: true), _onlyClientsAttachedToAnEntity: false, observedEntity.entityIdToSendChunksTo);
					}
				}
			}
			PrefabInstance item = new PrefabInstance(dynamicPrefabDecorator.GetNextId(), prefab.location, pos, rot, prefab, 0)
			{
				bPrefabCopiedIntoWorld = true
			};
			dynamicPrefabDecorator.GetDynamicPrefabs().Add(item);
			dynamicPrefabDecorator.GetPOIPrefabs().Add(item);
		}

		private static void CopyIntoWorld(World world, WorldChunkCache cluster, Prefab prefab, Vector3i pos, bool copyEntities, bool bOverwrite = false, QuestTags tags = QuestTags.none)
		{
			CopyIntoLocal(world, cluster, prefab, pos, bOverwrite, bRegenerate: true, tags);
			if (copyEntities)
			{
				CopyEntitiesIntoWorld(world, prefab, pos);
			}
		}

		private static void CopyIntoLocal(World world, WorldChunkCache cluster, Prefab prefab, Vector3i pos, bool bOverwrite, bool bRegenerate, QuestTags tags = QuestTags.none)
		{
			if (!BCCommandAbstract.Options.ContainsKey("no-sleepers"))
			{
				BCCommandAbstract.SendOutput("Adding sleepers...");
				CopySleeperVolumes(world, prefab, null, pos);
			}
			else
			{
				BCCommandAbstract.SendOutput("Without sleepers...");
			}
			Chunk chunkSync = cluster.GetChunkSync(World.toChunkXZ(pos.x), World.toChunkXZ(pos.z));
			GameRandom gameRandom = ((chunkSync != null) ? Utils.RandomFromSeedOnPos(chunkSync.X, chunkSync.Z, world.Seed) : null);
			GameRandom gameRandom2 = GameRandomManager.Instance.CreateGameRandom((int)world.GetWorldTime());
			BlockValue blockValue = Block.GetBlockValue(Constants.cTerrainFillerBlockName);
			BlockValue blockValue2 = Block.GetBlockValue(Constants.cTerrainFiller2BlockName);
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.z; j++)
				{
					int v = i + pos.x;
					int v2 = j + pos.z;
					int num = World.toChunkXZ(v);
					int num2 = World.toChunkXZ(v2);
					if (chunkSync == null || chunkSync.X != num || chunkSync.Z != num2)
					{
						chunkSync = cluster.GetChunkSync(num, num2);
						GameRandomManager.Instance.FreeGameRandom(gameRandom);
						gameRandom = null;
					}
					if (chunkSync == null)
					{
						continue;
					}
					if (gameRandom == null)
					{
						gameRandom = Utils.RandomFromSeedOnPos(chunkSync.X, chunkSync.Z, world.Seed);
					}
					int num3 = World.toBlockXZ(v);
					int num4 = World.toBlockXZ(v2);
					bool flag = false;
					for (int k = 0; k < prefab.size.y; k++)
					{
						BlockValue blockValue3 = prefab.GetBlock(i, k, j);
						if (!prefab.bCopyAirBlocks && blockValue3.type == 0)
						{
							continue;
						}
						BlockValue blockValue4 = blockValue3;
						bool flag2 = false;
						if (Block.list[blockValue3.type].IsSleeperBlock && !world.IsEditor() && !BCCommandAbstract.Options.ContainsKey("no-sleepers"))
						{
							flag2 = true;
							blockValue3 = BlockValue.Air;
						}
						int num5 = World.toBlockY(k + pos.y);
						if (!world.IsEditor() && !BCCommandAbstract.Options.ContainsKey("no-tf-swap") && blockValue.type != 0 && blockValue3.type == blockValue.type)
						{
							BlockValue block = chunkSync.GetBlock(num3, num5, num4);
							if (block.type == 0 || Block.list[block.type] == null || !Block.list[block.type].shape.IsTerrain())
							{
								int terrainHeight = chunkSync.GetTerrainHeight(num3, num4);
								block = chunkSync.GetBlock(num3, terrainHeight, num4);
							}
							if (block.type == 0 || Block.list[block.type] == null || !Block.list[block.type].shape.IsTerrain())
							{
								continue;
							}
							blockValue3 = block;
							flag = true;
						}
						sbyte b = prefab.GetDensity(i, k, j);
						if (!world.IsEditor() && blockValue2.type != 0 && blockValue3.type == blockValue2.type)
						{
							BlockValue block2 = chunkSync.GetBlock(num3, num5, num4);
							if (block2.type != 0 && Block.list[block2.type] != null && Block.list[block2.type].shape.IsTerrain())
							{
								blockValue3 = block2;
								b = 0;
							}
							else
							{
								blockValue3 = BlockValue.Air;
								b = MarchingCubes.DensityAir;
							}
						}
						if (!world.IsEditor() && blockValue3.type > 0)
						{
							blockValue3 = BlockPlaceholderMap.Instance.Replace(blockValue3, blockValue3.Block.IsReplaceRandom ? gameRandom2 : gameRandom, chunkSync, num3, num4, bOverwrite, tags);
						}
						if (b == 0)
						{
							b = (Block.list[blockValue3.type].shape.IsTerrain() ? MarchingCubes.DensityTerrain : MarchingCubes.DensityAir);
						}
						BlockValue block3 = chunkSync.GetBlock(num3, num5, num4);
						if (block3.ischild || (!bOverwrite && block3.type != 0 && !Block.list[block3.type].shape.IsTerrain()))
						{
							chunkSync.SetDensity(num3, num5, num4, b);
							continue;
						}
						if (!prefab.bAllowTopSoilDecorations)
						{
							int terrainHeight2 = chunkSync.GetTerrainHeight(num3, num4);
							if (num5 >= terrainHeight2 + 1)
							{
								chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigNoSmall);
							}
							else if (num5 == terrainHeight2)
							{
								chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.Nothing);
							}
						}
						else
						{
							chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigOnlySmall);
						}
						if (!flag2)
						{
							chunkSync.SetTextureFull(num3, num5, num4, prefab.GetTexture(i, k, j));
						}
						chunkSync.SetBlock((WorldBase)world, num3, num5, num4, blockValue3);
						Vector3i blockPos = new Vector3i(i, k, j);
						TileEntity tileEntity = prefab.GetTileEntity(blockPos);
						if (tileEntity != null && Block.list[blockValue4.type].IsTileEntitySavedInPrefab())
						{
							TileEntity tileEntity2 = chunkSync.GetTileEntity(new Vector3i(num3, num5, num4));
							if (tileEntity2 == null)
							{
								tileEntity2 = tileEntity.Clone();
								tileEntity2.localChunkPos = new Vector3i(num3, num5, num4);
								tileEntity2.SetChunk(chunkSync);
								chunkSync.AddTileEntity(tileEntity2);
							}
							tileEntity2.CopyFrom(tileEntity);
							tileEntity2.localChunkPos = new Vector3i(num3, num5, num4);
						}
						if (Block.list[blockValue3.type].shape.IsTerrain() && chunkSync.GetTerrainHeight(num3, num4) < num5)
						{
							chunkSync.SetTerrainHeight(num3, num4, (byte)num5);
						}
						chunkSync.SetDensity(num3, num5, num4, b);
					}
					if (!flag)
					{
						chunkSync.SetTopSoilBroken(num3, num4);
					}
					chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigOnlySmall);
					if (bRegenerate)
					{
						chunkSync.NeedsRegeneration = true;
					}
				}
			}
			GameRandomManager.Instance.FreeGameRandom(gameRandom);
			GameRandomManager.Instance.FreeGameRandom(gameRandom2);
		}

		private static void CopyEntitiesIntoWorld(World world, Prefab prefab, Vector3i pos)
		{
			foreach (EntityCreationData entity2 in prefab.GetEntities())
			{
				entity2.id = -1;
				if (!BCCommandAbstract.Options.ContainsKey("no-enemies") && !world.IsEditor() && (GameStats.GetBool(EnumGameStats.IsSpawnEnemies) || !EntityClass.list[entity2.entityClass].bIsEnemyEntity))
				{
					Entity entity = EntityFactory.CreateEntity(entity2);
					entity.SetPosition(pos.ToVector3() + entity.position);
					world.SpawnEntityInWorld(entity);
				}
			}
		}

		private static void CopySleeperVolumes(WorldBase world, Prefab prefab, Chunk chunk, Vector3i offset)
		{
			Vector3i one = Vector3i.zero;
			Vector3i vector3i = Vector3i.zero;
			if (chunk != null)
			{
				one = chunk.GetWorldPos();
				vector3i = one + new Vector3i(16, 256, 16);
			}
			for (int i = 0; i < prefab.SleeperVolumes.Count; i++)
			{
				Prefab.PrefabSleeperVolume prefabSleeperVolume = prefab.SleeperVolumes[i];
				if (!prefabSleeperVolume.used)
				{
					continue;
				}
				Vector3i startPos = prefabSleeperVolume.startPos;
				Vector3i vector3i2 = startPos + prefabSleeperVolume.size;
				Vector3i vector3i3 = startPos + offset;
				Vector3i vector3i4 = vector3i3 + prefabSleeperVolume.size;
				Vector3i vector3i5 = vector3i3 - SleeperVolume.chunkPadding;
				Vector3i vector3i6 = vector3i4 + SleeperVolume.chunkPadding;
				if (chunk == null)
				{
					int num = world.FindSleeperVolume(vector3i3, vector3i4);
					if (num < 0)
					{
						SleeperVolume volume = SleeperVolume.Create(prefabSleeperVolume, vector3i3, vector3i2 + offset);
						num = world.AddSleeperVolume(volume);
						CopySleeperBlocksContainedInVolume(prefab, i, offset, volume, startPos, vector3i2);
					}
					int num2 = World.toChunkXZ(vector3i5.x);
					int num3 = World.toChunkXZ(vector3i6.x - 1);
					int num4 = World.toChunkXZ(vector3i5.z);
					int num5 = World.toChunkXZ(vector3i6.z - 1);
					for (int j = num2; j <= num3; j++)
					{
						for (int k = num4; k <= num5; k++)
						{
							((Chunk)world.GetChunkSync(j, 0, k))?.GetSleeperVolumes().Add(num);
						}
					}
				}
				else if (vector3i5.x < vector3i.x && (vector3i6.x > one.x || vector3i5.y < vector3i.y) && (vector3i6.y > one.y || vector3i5.z < vector3i.z) && vector3i6.z > one.z)
				{
					int num6 = world.FindSleeperVolume(vector3i3, vector3i4);
					if (num6 < 0)
					{
						SleeperVolume volume2 = SleeperVolume.Create(prefabSleeperVolume, vector3i3, vector3i2 + offset);
						num6 = world.AddSleeperVolume(volume2);
						CopySleeperBlocksContainedInVolume(prefab, i, offset, volume2, startPos, vector3i2);
					}
					chunk.GetSleeperVolumes().Add(num6);
				}
			}
		}

		private static void CopySleeperBlocksContainedInVolume(Prefab prefab, int skipIndex, Vector3i offset, SleeperVolume volume, Vector3i mins, Vector3i maxs)
		{
			int num = Mathf.Max(mins.x, 0);
			int num2 = Mathf.Max(mins.y, 0);
			int num3 = Mathf.Max(mins.z, 0);
			int num4 = Mathf.Min(prefab.size.x, maxs.x);
			int num5 = Mathf.Min(prefab.size.y, maxs.y);
			int num6 = Mathf.Min(prefab.size.z, maxs.z);
			for (int i = num; i < num4; i++)
			{
				for (int j = num3; j < num6; j++)
				{
					for (int k = num2; k < num5; k++)
					{
						if (k <= 0 || !Block.list[prefab.GetBlockNoDamage(prefab.GetLocalRotation(), i, k - 1, j).type].IsSleeperBlock)
						{
							BlockValue block = prefab.GetBlock(i, k, j);
							if (block.Block.IsSleeperBlock && !IsPosInSleeperPriorityVolume(prefab, new Vector3i(i, k, j), skipIndex))
							{
								volume.AddSpawnPoint(i + offset.x, k + offset.y, j + offset.z, (BlockSleeper)block.Block, block);
							}
						}
					}
				}
			}
		}

		private static bool IsPosInSleeperPriorityVolume(Prefab prefab, Vector3i pos, int skipIndex)
		{
			for (int i = 0; i < prefab.SleeperVolumes.Count; i++)
			{
				if (i != skipIndex)
				{
					Prefab.PrefabSleeperVolume prefabSleeperVolume = prefab.SleeperVolumes[i];
					if (prefabSleeperVolume.used && prefabSleeperVolume.isPriority && IsPosInSleeperVolume(prefabSleeperVolume.startPos, prefabSleeperVolume.size, pos))
					{
						return true;
					}
				}
			}
			return false;
		}

		private static bool IsPosInSleeperVolume(Vector3i startPos, Vector3i volumeSize, Vector3i pos)
		{
			Vector3i vector3i = startPos + volumeSize;
			if (pos.x >= startPos.x && pos.x < vector3i.x && pos.y >= startPos.y && pos.y < vector3i.y && pos.z >= startPos.z)
			{
				return pos.z < vector3i.z;
			}
			return false;
		}

		private static HashSetLong GetOccupiedChunks(Vector3i pos, Vector3i size)
		{
			int num = World.toChunkXZ(pos.x);
			int num2 = World.toChunkXZ(pos.x + size.x);
			int num3 = World.toChunkXZ(pos.z);
			int num4 = World.toChunkXZ(pos.z + size.z);
			HashSetLong hashSetLong = new HashSetLong();
			for (int i = num; i <= num2; i++)
			{
				for (int j = num3; j <= num4; j++)
				{
					hashSetLong.Add(WorldChunkCache.MakeChunkKey(i, j));
				}
			}
			return hashSetLong;
		}
	}
}
