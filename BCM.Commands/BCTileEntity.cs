using System;
using System.Collections.Generic;
using BCM.Models;
using BCM.Models.TileEntities;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCTileEntity : BCCommandAbstract
	{
		public BCTileEntity()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCTileEntity"];
			DefaultCommands = new string[2]
			{
				"bc-tile",
				"tile"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCTileEntity.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld(out var world))
			{
				BCMCmdArea bCMCmdArea = new BCMCmdArea(BCCommandAbstract.Params, BCCommandAbstract.Options, "TileEntity");
				EntityPlayer entity;
				if (!BCMCmdArea.ProcessParams(bCMCmdArea, 14))
				{
					BCCommandAbstract.SendOutput(GetHelp());
				}
				else if (!BCMCmdArea.GetIds(world, bCMCmdArea, out entity))
				{
					BCCommandAbstract.SendOutput("Command requires a position when not run by a player.");
				}
				else if (!bCMCmdArea.HasChunkPos && !bCMCmdArea.HasPos && !BCMCmdArea.GetEntPos(bCMCmdArea, entity))
				{
					BCCommandAbstract.SendOutput("Unable to get position.");
				}
				else
				{
					BCMCmdArea.DoProcess(world, bCMCmdArea, this);
				}
			}
		}

		public override void ProcessSwitch(World world, BCMCmdArea command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
			switch (command.Command)
			{
			case "owner":
				SetOwner(command, world);
				reload = ReloadMode.Target;
				break;
			case "access":
				GrantAccess(command, world);
				reload = ReloadMode.Target;
				break;
			case "revoke":
				RevokeAccess(command, world);
				reload = ReloadMode.Target;
				break;
			case "lock":
			{
				command.Opts.TryGetValue("pwd", out var value);
				SetLocked(command, locked: true, command.Opts.ContainsKey("pwd"), value ?? string.Empty, world);
				reload = ReloadMode.All;
				break;
			}
			case "unlock":
				SetLocked(command, locked: false, setPwd: false, "", world);
				reload = ReloadMode.All;
				break;
			case "empty":
				EmptyContainers(command, world);
				break;
			case "reset":
				ResetTouched(command, world);
				break;
			case "remove":
				if (!command.Opts.ContainsKey("confirm"))
				{
					BCCommandAbstract.SendOutput("Please use /confirm to confirm that you understand the impact of this command (removes all targeted tile entities)");
					break;
				}
				RemoveTiles(command, world);
				reload = ReloadMode.All;
				break;
			case "scan":
				ScanTiles(command, world);
				break;
			case "additem":
				AddLoot(command, world);
				break;
			case "settext":
				SetText(command, world);
				break;
			default:
				BCCommandAbstract.SendOutput("Unknown param " + command.Command);
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			case "removeitem":
				break;
			}
		}

		private static void SetText(BCMCmdArea command, World world)
		{
			command.Opts.TryGetValue("text", out var value);
			string text = "";
			if (value != null)
			{
				string oldValue = "_";
				if (BCCommandAbstract.Options.ContainsKey("p"))
				{
					oldValue = BCCommandAbstract.Options["p"].Substring(0, 1);
				}
				text = value.Replace(oldValue, " ");
			}
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.Sign:
						{
							TileEntitySign tileEntitySign = item.Value as TileEntitySign;
							if (tileEntitySign != null)
							{
								tileEntitySign.SetText(text);
								num++;
							}
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						case TileEntityType.None:
						case TileEntityType.Loot:
						case TileEntityType.Trader:
						case TileEntityType.VendingMachine:
						case TileEntityType.Forge:
						case TileEntityType.Campfire:
						case TileEntityType.SecureLoot:
						case TileEntityType.SecureDoor:
						case TileEntityType.Workstation:
						case TileEntityType.GoreBlock:
						case TileEntityType.Powered:
						case TileEntityType.PowerSource:
						case TileEntityType.PowerRangeTrap:
						case TileEntityType.Trigger:
							break;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Set {text} on {num} secure blocks");
		}

		private static void AddLoot(BCMCmdArea command, World world)
		{
			if (!command.HasPos || command.ItemStack == null)
			{
				BCCommandAbstract.SendOutput("AddLoot option requires a specific x y z and a value /item value");
				return;
			}
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						return;
					}
					Vector3i key = new Vector3i(World.toBlockXZ(command.Position.x), command.Position.y, World.toBlockXZ(command.Position.z));
					if (!dictionaryList.dict.ContainsKey(key))
					{
						BCCommandAbstract.SendOutput($"Tile Entity not found at given location {command.Position}");
						return;
					}
					TileEntity tileEntity = dictionaryList.dict[key];
					if (tileEntity.IsUserAccessing())
					{
						BCCommandAbstract.SendOutput("Tile Entity is currently being accessed");
						return;
					}
					if (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(tileEntity.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase))
					{
						return;
					}
					switch (tileEntity.GetTileEntityType())
					{
					case TileEntityType.Loot:
					{
						TileEntityLootContainer tileEntityLootContainer = tileEntity as TileEntityLootContainer;
						if (tileEntityLootContainer != null && !tileEntityLootContainer.AddItem(command.ItemStack))
						{
							BCCommandAbstract.SendOutput("Couldn't add item, container is full?");
						}
						break;
					}
					case TileEntityType.SecureLoot:
					{
						TileEntitySecureLootContainer tileEntitySecureLootContainer = tileEntity as TileEntitySecureLootContainer;
						if (tileEntitySecureLootContainer != null && !tileEntitySecureLootContainer.AddItem(command.ItemStack))
						{
							BCCommandAbstract.SendOutput("Couldn't add item, container is full?");
						}
						break;
					}
					case TileEntityType.GoreBlock:
					{
						TileEntityGoreBlock tileEntityGoreBlock = tileEntity as TileEntityGoreBlock;
						if (tileEntityGoreBlock != null && !tileEntityGoreBlock.AddItem(command.ItemStack))
						{
							BCCommandAbstract.SendOutput("Couldn't add item, container is full?");
						}
						break;
					}
					default:
						BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {command.Position}");
						break;
					case TileEntityType.None:
					case TileEntityType.Trader:
					case TileEntityType.VendingMachine:
					case TileEntityType.Forge:
					case TileEntityType.Campfire:
					case TileEntityType.SecureDoor:
					case TileEntityType.Workstation:
					case TileEntityType.Sign:
					case TileEntityType.Powered:
					case TileEntityType.PowerSource:
					case TileEntityType.PowerRangeTrap:
					case TileEntityType.Trigger:
						break;
					}
				}
			}
			BCCommandAbstract.SendOutput($"Added to loot container: {command.ItemStack.itemValue.ItemClass.Name} x{command.ItemStack.count} at {command.Position}");
		}

		private static void ScanTiles(BCMCmdArea command, World world)
		{
			int num = 0;
			Dictionary<string, List<BCMTileEntity>> dictionary = new Dictionary<string, List<BCMTileEntity>>();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					Chunk chunkSync = world.ChunkCache.GetChunkSync(i, j);
					DictionaryList<Vector3i, TileEntity> dictionaryList = chunkSync?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.None:
						{
							TileEntity tileEntity = item.Value;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("None"))
								{
									dictionary.Add("None", new List<BCMTileEntity>());
								}
								dictionary["None"].Add(new BCMTileEntity(vector3i, tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Loot:
						{
							TileEntity tileEntity = item.Value as TileEntityLootContainer;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Loot"))
								{
									dictionary.Add("Loot", new List<BCMTileEntity>());
								}
								dictionary["Loot"].Add(new BCMTileEntityLootContainer(vector3i, (TileEntityLootContainer)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Trader:
						{
							TileEntity tileEntity = item.Value as TileEntityTrader;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Trader"))
								{
									dictionary.Add("Trader", new List<BCMTileEntity>());
								}
								dictionary["Trader"].Add(new BCMTileEntityTrader(vector3i, (TileEntityTrader)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.VendingMachine:
						{
							TileEntity tileEntity = item.Value as TileEntityVendingMachine;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("VendingMachine"))
								{
									dictionary.Add("VendingMachine", new List<BCMTileEntity>());
								}
								dictionary["VendingMachine"].Add(new BCMTileEntityVendingMachine(vector3i, (TileEntityVendingMachine)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Forge:
						{
							TileEntity tileEntity = item.Value as TileEntityForge;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Forge"))
								{
									dictionary.Add("Forge", new List<BCMTileEntity>());
								}
								dictionary["Forge"].Add(new BCMTileEntityForge(vector3i, (TileEntityForge)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Campfire:
						{
							TileEntity tileEntity = item.Value as TileEntityWorkstation;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Campfire"))
								{
									dictionary.Add("Campfire", new List<BCMTileEntity>());
								}
								dictionary["Campfire"].Add(new BCMTileEntityCampfire(vector3i, (TileEntityWorkstation)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.SecureLoot:
						{
							TileEntity tileEntity = item.Value as TileEntitySecureLootContainer;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("SecureLoot"))
								{
									dictionary.Add("SecureLoot", new List<BCMTileEntity>());
								}
								dictionary["SecureLoot"].Add(new BCMTileEntitySecureLootContainer(vector3i, (TileEntitySecureLootContainer)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.SecureDoor:
						{
							TileEntity tileEntity = item.Value as TileEntitySecureDoor;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("SecureDoor"))
								{
									dictionary.Add("SecureDoor", new List<BCMTileEntity>());
								}
								dictionary["SecureDoor"].Add(new BCMTileEntitySecureDoor(vector3i, (TileEntitySecureDoor)tileEntity, chunkSync));
							}
							num++;
							break;
						}
						case TileEntityType.Workstation:
						{
							TileEntity tileEntity = item.Value as TileEntityWorkstation;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Workstation"))
								{
									dictionary.Add("Workstation", new List<BCMTileEntity>());
								}
								dictionary["Workstation"].Add(new BCMTileEntityWorkstation(vector3i, (TileEntityWorkstation)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Sign:
						{
							TileEntity tileEntity = item.Value as TileEntitySign;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Sign"))
								{
									dictionary.Add("Sign", new List<BCMTileEntity>());
								}
								dictionary["Sign"].Add(new BCMTileEntitySign(vector3i, (TileEntitySign)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.GoreBlock:
						{
							TileEntity tileEntity = item.Value as TileEntityGoreBlock;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("GoreBlock"))
								{
									dictionary.Add("GoreBlock", new List<BCMTileEntity>());
								}
								dictionary["GoreBlock"].Add(new BCMTileEntityGoreBlock(vector3i, (TileEntityGoreBlock)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Powered:
						{
							TileEntity tileEntity = item.Value as TileEntityPowered;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Powered"))
								{
									dictionary.Add("Powered", new List<BCMTileEntity>());
								}
								dictionary["Powered"].Add(new BCMTileEntityPowered(vector3i, (TileEntityPowered)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.PowerSource:
						{
							TileEntity tileEntity = item.Value as TileEntityPowerSource;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("PowerSource"))
								{
									dictionary.Add("PowerSource", new List<BCMTileEntity>());
								}
								dictionary["PowerSource"].Add(new BCMTileEntityPowerSource(vector3i, (TileEntityPowerSource)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.PowerRangeTrap:
						{
							TileEntity tileEntity = item.Value as TileEntityPoweredRangedTrap;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("PowerRangeTrap"))
								{
									dictionary.Add("PowerRangeTrap", new List<BCMTileEntity>());
								}
								dictionary["PowerRangeTrap"].Add(new BCMTileEntityPoweredRangeTrap(vector3i, (TileEntityPoweredRangedTrap)tileEntity));
							}
							num++;
							break;
						}
						case TileEntityType.Trigger:
						{
							TileEntity tileEntity = item.Value as TileEntityPoweredTrigger;
							if (tileEntity != null)
							{
								if (!dictionary.ContainsKey("Trigger"))
								{
									dictionary.Add("Trigger", new List<BCMTileEntity>());
								}
								dictionary["Trigger"].Add(new BCMTileEntityPoweredTrigger(vector3i, (TileEntityPoweredTrigger)tileEntity));
							}
							num++;
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						}
					}
				}
			}
			if (dictionary.Count > 0)
			{
				BCCommandAbstract.SendJson(new
				{
					Count = num,
					Tiles = dictionary
				});
			}
			else
			{
				BCCommandAbstract.SendOutput("No tile entities found in area");
			}
		}

		private static void RemoveTiles(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					Chunk chunkSync = world.ChunkCache.GetChunkSync(i, j);
					DictionaryList<Vector3i, TileEntity> dictionaryList = chunkSync?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					List<Vector3i> list = new List<Vector3i>();
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i pos = item.Key + other;
						if ((!command.HasPos || command.IsWithinBounds(pos)) && (command.Filter == null || command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) || command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							(item.Value as TileEntityLootContainer)?.SetEmpty();
							list.Add(item.Key);
						}
					}
					foreach (Vector3i item2 in list)
					{
						chunkSync.SetBlock((WorldBase)world, item2.x, item2.y, item2.z, BlockValue.Air);
						num++;
					}
				}
			}
			BCCommandAbstract.SendOutput($"Removed {num} blocks");
		}

		private static void ResetTouched(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (item.Value.IsUserAccessing())
						{
							continue;
						}
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.Loot:
						{
							TileEntityLootContainer tileEntityLootContainer = item.Value as TileEntityLootContainer;
							if (tileEntityLootContainer != null)
							{
								tileEntityLootContainer.SetEmpty();
								tileEntityLootContainer.bWasTouched = false;
								tileEntityLootContainer.bTouched = false;
								tileEntityLootContainer.SetModified();
							}
							num++;
							break;
						}
						case TileEntityType.SecureLoot:
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = item.Value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer != null)
							{
								tileEntitySecureLootContainer.SetEmpty();
								tileEntitySecureLootContainer.bWasTouched = false;
								tileEntitySecureLootContainer.bTouched = false;
								tileEntitySecureLootContainer.SetModified();
							}
							num++;
							break;
						}
						case TileEntityType.GoreBlock:
						{
							TileEntityGoreBlock tileEntityGoreBlock = item.Value as TileEntityGoreBlock;
							if (tileEntityGoreBlock != null)
							{
								tileEntityGoreBlock.SetEmpty();
								tileEntityGoreBlock.bWasTouched = false;
								tileEntityGoreBlock.bTouched = false;
								tileEntityGoreBlock.SetModified();
							}
							num++;
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						case TileEntityType.None:
						case TileEntityType.Trader:
						case TileEntityType.VendingMachine:
						case TileEntityType.Forge:
						case TileEntityType.Campfire:
						case TileEntityType.SecureDoor:
						case TileEntityType.Workstation:
						case TileEntityType.Sign:
						case TileEntityType.Powered:
						case TileEntityType.PowerSource:
						case TileEntityType.PowerRangeTrap:
						case TileEntityType.Trigger:
							break;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Reset {num} loot containers");
		}

		private static void EmptyContainers(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (item.Value.IsUserAccessing())
						{
							continue;
						}
						Vector3i vector3i = item.Key + other;
						if ((!command.HasPos || command.IsWithinBounds(vector3i)) && (command.Filter == null || command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) || command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							switch (item.Value.GetTileEntityType())
							{
							case TileEntityType.Loot:
								(item.Value as TileEntityLootContainer)?.SetEmpty();
								num++;
								break;
							case TileEntityType.SecureLoot:
								(item.Value as TileEntitySecureLootContainer)?.SetEmpty();
								num++;
								break;
							case TileEntityType.GoreBlock:
								(item.Value as TileEntityGoreBlock)?.SetEmpty();
								num++;
								break;
							default:
								BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
								break;
							case TileEntityType.None:
							case TileEntityType.Trader:
							case TileEntityType.VendingMachine:
							case TileEntityType.Forge:
							case TileEntityType.Campfire:
							case TileEntityType.SecureDoor:
							case TileEntityType.Workstation:
							case TileEntityType.Sign:
							case TileEntityType.Powered:
							case TileEntityType.PowerSource:
							case TileEntityType.PowerRangeTrap:
							case TileEntityType.Trigger:
								break;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Emptied {num} loot containers");
		}

		private static void SetLocked(BCMCmdArea command, bool locked, bool setPwd, string pwd, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						bool changed;
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.VendingMachine:
						{
							TileEntityVendingMachine tileEntityVendingMachine = item.Value as TileEntityVendingMachine;
							if (tileEntityVendingMachine != null && (tileEntityVendingMachine.IsLocked() != locked || setPwd))
							{
								tileEntityVendingMachine.SetLocked(locked);
								if (locked && setPwd)
								{
									tileEntityVendingMachine.CheckPassword(pwd, tileEntityVendingMachine.GetOwner(), out changed);
								}
								num++;
							}
							break;
						}
						case TileEntityType.SecureLoot:
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = item.Value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer != null && (tileEntitySecureLootContainer.IsLocked() != locked || setPwd))
							{
								tileEntitySecureLootContainer.SetLocked(locked);
								if (locked && setPwd)
								{
									tileEntitySecureLootContainer.CheckPassword(pwd, tileEntitySecureLootContainer.GetOwner(), out changed);
								}
								num++;
							}
							break;
						}
						case TileEntityType.SecureDoor:
						{
							TileEntitySecureDoor tileEntitySecureDoor = item.Value as TileEntitySecureDoor;
							if (tileEntitySecureDoor != null && (tileEntitySecureDoor.IsLocked() != locked || setPwd))
							{
								tileEntitySecureDoor.SetLocked(locked);
								if (locked && setPwd)
								{
									tileEntitySecureDoor.CheckPassword(pwd, tileEntitySecureDoor.GetOwner(), out changed);
								}
								num++;
							}
							break;
						}
						case TileEntityType.Sign:
						{
							TileEntitySign tileEntitySign = item.Value as TileEntitySign;
							if (tileEntitySign != null && (tileEntitySign.IsLocked() != locked || setPwd))
							{
								tileEntitySign.SetLocked(locked);
								if (locked && setPwd)
								{
									tileEntitySign.CheckPassword(pwd, tileEntitySign.GetOwner(), out changed);
								}
								num++;
							}
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						case TileEntityType.None:
						case TileEntityType.Loot:
						case TileEntityType.Trader:
						case TileEntityType.Forge:
						case TileEntityType.Campfire:
						case TileEntityType.Workstation:
						case TileEntityType.GoreBlock:
						case TileEntityType.Powered:
						case TileEntityType.PowerSource:
						case TileEntityType.PowerRangeTrap:
						case TileEntityType.Trigger:
							break;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Set {0} on {1} secure blocks {2}", locked ? "locked" : "unlocked", num, setPwd ? " with new password" : ""));
		}

		private static void GrantAccess(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.VendingMachine:
						{
							TileEntityVendingMachine tileEntityVendingMachine = item.Value as TileEntityVendingMachine;
							if (tileEntityVendingMachine != null)
							{
								List<string> users3 = tileEntityVendingMachine.GetUsers();
								if (users3.Contains(command.SteamId))
								{
									break;
								}
								users3.Add(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.SecureLoot:
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = item.Value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer != null)
							{
								List<string> users2 = tileEntitySecureLootContainer.GetUsers();
								if (users2.Contains(command.SteamId))
								{
									break;
								}
								users2.Add(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.SecureDoor:
						{
							TileEntitySecureDoor tileEntitySecureDoor = item.Value as TileEntitySecureDoor;
							if (tileEntitySecureDoor != null)
							{
								List<string> users4 = tileEntitySecureDoor.GetUsers();
								if (users4.Contains(command.SteamId))
								{
									break;
								}
								users4.Add(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.Sign:
						{
							TileEntitySign tileEntitySign = item.Value as TileEntitySign;
							if (tileEntitySign != null)
							{
								List<string> users = tileEntitySign.GetUsers();
								if (users.Contains(command.SteamId))
								{
									break;
								}
								users.Add(command.SteamId);
							}
							num++;
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						case TileEntityType.None:
						case TileEntityType.Loot:
						case TileEntityType.Trader:
						case TileEntityType.Forge:
						case TileEntityType.Campfire:
						case TileEntityType.Workstation:
						case TileEntityType.GoreBlock:
						case TileEntityType.Powered:
						case TileEntityType.PowerSource:
						case TileEntityType.PowerRangeTrap:
						case TileEntityType.Trigger:
							break;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Granted access to {num} secure blocks");
		}

		private static void RevokeAccess(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((command.HasPos && !command.IsWithinBounds(vector3i)) || (command.Filter != null && !command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) && !command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							continue;
						}
						switch (item.Value.GetTileEntityType())
						{
						case TileEntityType.VendingMachine:
						{
							TileEntityVendingMachine tileEntityVendingMachine = item.Value as TileEntityVendingMachine;
							if (tileEntityVendingMachine != null)
							{
								List<string> users3 = tileEntityVendingMachine.GetUsers();
								if (!users3.Contains(command.SteamId))
								{
									break;
								}
								users3.Remove(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.SecureLoot:
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = item.Value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer != null)
							{
								List<string> users2 = tileEntitySecureLootContainer.GetUsers();
								if (!users2.Contains(command.SteamId))
								{
									break;
								}
								users2.Remove(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.SecureDoor:
						{
							TileEntitySecureDoor tileEntitySecureDoor = item.Value as TileEntitySecureDoor;
							if (tileEntitySecureDoor != null)
							{
								List<string> users4 = tileEntitySecureDoor.GetUsers();
								if (!users4.Contains(command.SteamId))
								{
									break;
								}
								users4.Remove(command.SteamId);
							}
							num++;
							break;
						}
						case TileEntityType.Sign:
						{
							TileEntitySign tileEntitySign = item.Value as TileEntitySign;
							if (tileEntitySign != null)
							{
								List<string> users = tileEntitySign.GetUsers();
								if (!users.Contains(command.SteamId))
								{
									break;
								}
								users.Remove(command.SteamId);
							}
							num++;
							break;
						}
						default:
							BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
							break;
						case TileEntityType.None:
						case TileEntityType.Loot:
						case TileEntityType.Trader:
						case TileEntityType.Forge:
						case TileEntityType.Campfire:
						case TileEntityType.Workstation:
						case TileEntityType.GoreBlock:
						case TileEntityType.Powered:
						case TileEntityType.PowerSource:
						case TileEntityType.PowerRangeTrap:
						case TileEntityType.Trigger:
							break;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Revoked access for {num} secure blocks");
		}

		private static void SetOwner(BCMCmdArea command, World world)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = world.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i other = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						Vector3i vector3i = item.Key + other;
						if ((!command.HasPos || command.IsWithinBounds(vector3i)) && (command.Filter == null || command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase) || command.Filter.Equals(item.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase)))
						{
							switch (item.Value.GetTileEntityType())
							{
							case TileEntityType.VendingMachine:
								(item.Value as TileEntityVendingMachine)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.SecureLoot:
								(item.Value as TileEntitySecureLootContainer)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.SecureDoor:
								(item.Value as TileEntitySecureDoor)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.Sign:
								(item.Value as TileEntitySign)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.PowerSource:
								(item.Value as TileEntityPowerSource)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.PowerRangeTrap:
								(item.Value as TileEntityPoweredRangedTrap)?.SetOwner(command.SteamId);
								num++;
								break;
							case TileEntityType.Trigger:
								(item.Value as TileEntityPoweredTrigger)?.SetOwner(command.SteamId);
								num++;
								break;
							default:
								BCCommandAbstract.SendOutput($"Error finding TileEntity Type at {vector3i}");
								break;
							case TileEntityType.None:
							case TileEntityType.Loot:
							case TileEntityType.Trader:
							case TileEntityType.Forge:
							case TileEntityType.Campfire:
							case TileEntityType.Workstation:
							case TileEntityType.GoreBlock:
							case TileEntityType.Powered:
								break;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Set owner on {num} secure blocks");
		}
	}
}
