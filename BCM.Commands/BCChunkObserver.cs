using System;
using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCChunkObserver : BCCommandAbstract
	{
		public BCChunkObserver()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCChunkObserver"];
			DefaultCommands = new string[1]
			{
				"bc-co"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCChunkObserver.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "list":
				GetList(world.m_ChunkManager.m_ObservedEntities);
				break;
			case "spawn":
			{
				int num = SpawnChunkObserver(world);
				BCCommandAbstract.SendOutput((num == -1) ? "Spawn CO failed" : $"Spawned CO: {num}");
				break;
			}
			case "remove":
				DoRemove(world.m_ChunkManager.m_ObservedEntities, world);
				break;
			case "move":
				DoMove(world.m_ChunkManager.m_ObservedEntities);
				break;
			case "reload":
				DoReload();
				break;
			default:
				BCCommandAbstract.SendOutput("Unknown param " + BCCommandAbstract.Params[0]);
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private int SpawnChunkObserver(World world)
		{
			if (BCCommandAbstract.Params.Count > 3 && int.TryParse(BCCommandAbstract.Params[1], out var result) && int.TryParse(BCCommandAbstract.Params[2], out var result2) && int.TryParse(BCCommandAbstract.Params[3], out var result3))
			{
				return world.m_ChunkManager.AddChunkObserver(new Vector3(result, 0f, result2), _bBuildVisualMeshAround: false, result3, -1)?.id ?? (-1);
			}
			BCCommandAbstract.SendOutput("Unable to parse x or z for spawn");
			BCCommandAbstract.SendOutput(GetHelp());
			return -1;
		}

		private void DoMove(IEnumerable<ChunkManager.ChunkObserver> observedEntities)
		{
			if (BCCommandAbstract.Params.Count <= 3 || !int.TryParse(BCCommandAbstract.Params[1], out var result))
			{
				BCCommandAbstract.SendOutput("Incorrect params count for move");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[2], out var result2) || !int.TryParse(BCCommandAbstract.Params[3], out var result3))
			{
				BCCommandAbstract.SendOutput("Unable to parse x or z for move");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			foreach (ChunkManager.ChunkObserver observedEntity in observedEntities)
			{
				if (observedEntity.id == result)
				{
					if (!IsPlayer(observedEntity))
					{
						observedEntity.SetPosition(new Vector3(result2, 0f, result3));
						BCCommandAbstract.SendOutput($"Moved chunk observer {observedEntity.id} to {result2} {result3}");
					}
					return;
				}
			}
			BCCommandAbstract.SendOutput($"Unable to find observer with id: {result}");
		}

		private static bool IsPlayer(ChunkManager.ChunkObserver oe)
		{
			return GameManager.Instance.World.Players.dict.ContainsKey(oe.entityIdToSendChunksTo);
		}

		private void DoRemove(IEnumerable<ChunkManager.ChunkObserver> observedEntities, World world)
		{
			if (BCCommandAbstract.Params.Count <= 1 || !int.TryParse(BCCommandAbstract.Params[1], out var result))
			{
				BCCommandAbstract.SendOutput("Incorrect params count for remove");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			foreach (ChunkManager.ChunkObserver observedEntity in observedEntities)
			{
				if (observedEntity.id == result)
				{
					if (!IsPlayer(observedEntity))
					{
						world.m_ChunkManager.RemoveChunkObserver(observedEntity);
						BCCommandAbstract.SendOutput($"Removed chunk observer {observedEntity.id}");
					}
					return;
				}
			}
			BCCommandAbstract.SendOutput($"Unable to find observer with id: {result}");
		}

		private void DoReload()
		{
			if (BCCommandAbstract.Params.Count != 5 && BCCommandAbstract.Params.Count != 3)
			{
				BCCommandAbstract.SendOutput("Incorrect params count for reload");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			int result;
			int result2;
			int result3;
			int result4;
			if (BCCommandAbstract.Params.Count == 3)
			{
				if (!int.TryParse(BCCommandAbstract.Params[1], out result) || !int.TryParse(BCCommandAbstract.Params[2], out result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse chunk co-ords from params");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				result3 = result;
				result4 = result2;
			}
			else if (!int.TryParse(BCCommandAbstract.Params[1], out result) || !int.TryParse(BCCommandAbstract.Params[2], out result2) || !int.TryParse(BCCommandAbstract.Params[3], out result3) || !int.TryParse(BCCommandAbstract.Params[4], out result4))
			{
				BCCommandAbstract.SendOutput("Unable to parse chunk co-ords from params");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			int num = Math.Min(result, result3);
			int num2 = Math.Max(result, result3);
			int num3 = Math.Min(result2, result4);
			int num4 = Math.Max(result2, result4);
			Dictionary<long, Chunk> dictionary = new Dictionary<long, Chunk>();
			for (int i = num; i <= num2; i++)
			{
				for (int j = num3; j <= num4; j++)
				{
					long key = WorldChunkCache.MakeChunkKey(i, j);
					dictionary[key] = null;
				}
			}
			int num5 = BCChunks.ReloadForClients(dictionary);
			BCCommandAbstract.SendOutput($"Chunks reloaded for {num5} clients in area: {num},{num3} to {num2},{num4}");
		}

		private static void GetList(ICollection<ChunkManager.ChunkObserver> observedEntities)
		{
			BCCommandAbstract.SendOutput($"Count:{observedEntities.Count}");
			foreach (ChunkManager.ChunkObserver observedEntity in observedEntities)
			{
				BCCommandAbstract.SendOutput($"id:{observedEntity.id}, pos:{Math.Round(observedEntity.position.x, 0)} {Math.Round(observedEntity.position.z, 0)}, dim:{observedEntity.viewDim}, chunks:" + ((observedEntity.entityIdToSendChunksTo > 0) ? $"{observedEntity.chunksLoaded.Count}/{observedEntity.chunksLoaded.Count + observedEntity.chunksToLoad.list.Count}" : $"{observedEntity.chunksToLoad.list.Count}") + $", entity:{observedEntity.entityIdToSendChunksTo}");
			}
		}
	}
}
