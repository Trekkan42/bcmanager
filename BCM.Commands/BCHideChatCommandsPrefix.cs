using System;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCHideChatCommandsPrefix : BCCommandAbstract
	{
		public static string ChatCommandPrefix = "";

		public BCHideChatCommandsPrefix()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCHideChatCommandsPrefix"];
			DefaultCommands = new string[2]
			{
				"bc-chatprefix",
				"bc-prefix"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
			ChatCommandPrefix = configValue.GetItem("ChatCommandPrefix", "");
		}

		public override string GetHelp()
		{
			return Resources.BCHideChatCommandsPrefix.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Options.Count == 1)
			{
				string text = BCCommandAbstract.Options.Values.FirstOrDefault();
				BCCommandAbstract.Params.Add("/" + BCCommandAbstract.Options.Keys.First() + ((text != null) ? ("=" + text) : ""));
			}
			if (1 != BCCommandAbstract.Params.Count)
			{
				BCCommandAbstract.SendOutput((ChatCommandPrefix == "") ? "Chat command prefix is not set" : ("Current chat command prefix " + ChatCommandPrefix));
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (BCCommandAbstract.Params[0].Equals("show", StringComparison.OrdinalIgnoreCase))
			{
				BCCommandAbstract.SendOutput((ChatCommandPrefix == "") ? "Chat command prefix is not set" : ("Current chat command prefix " + ChatCommandPrefix));
				return;
			}
			if (BCCommandAbstract.Params[0].IndexOf(" ", StringComparison.Ordinal) > -1)
			{
				BCCommandAbstract.SendOutput("The hidden commands prefix must not contain spaces");
				return;
			}
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCHideChatCommandsPrefix"];
			ChatCommandPrefix = BCCommandAbstract.Params[0];
			configValue.SetItem("ChatCommandPrefix", (BCCommandAbstract.Params.Count == 0) ? "" : ChatCommandPrefix);
			BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
			BCCommandAbstract.SendOutput("Hidden chat commands prefix " + (string.IsNullOrEmpty(ChatCommandPrefix) ? "cleared" : ("set to " + ChatCommandPrefix)));
		}
	}
}
