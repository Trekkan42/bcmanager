using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCChatColor : BCCommandAbstract
	{
		public BCChatColor()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCChatColor"];
			DefaultCommands = new string[1]
			{
				"bc-chatcolor"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCChatColor.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Params.Count < 2 || BCCommandAbstract.Params.Count > 3)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else
			{
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _id, out var _))
				{
					return;
				}
				BCMServerPlayerConfig bCMServerPlayerConfig = BCMPersist.Instance.ServerPlayersConfig[_id, true];
				string color = "";
				bool result = false;
				if (BCCommandAbstract.Params[1] != "clear")
				{
					if (BCCommandAbstract.Params.Count != 3)
					{
						BCCommandAbstract.SendOutput("Wrong number of arguments");
						BCCommandAbstract.SendOutput(GetHelp());
						return;
					}
					if (!BCUtils.ParseColor(BCCommandAbstract.Params[1], out color))
					{
						BCCommandAbstract.SendOutput("Unable to parse color " + BCCommandAbstract.Params[1]);
						return;
					}
					if (!bool.TryParse(BCCommandAbstract.Params[2], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse " + BCCommandAbstract.Params[2] + " as a boolean");
						return;
					}
				}
				bCMServerPlayerConfig.ChatColor = color;
				bCMServerPlayerConfig.ChatColorFullText = result;
				BCMPersist.Instance.Save(BCMSaveType.ServerPlayersConfig);
				BCCommandAbstract.SendOutput(string.IsNullOrEmpty(color) ? "Clearing custom player chat color" : ("Setting player chat color to " + color + ", " + (result ? "full text" : "name only")));
			}
		}
	}
}
