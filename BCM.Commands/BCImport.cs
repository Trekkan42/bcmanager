using System;
using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCImport : BCCommandAbstract
	{
		public BCImport()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCImport"];
			DefaultCommands = new string[2]
			{
				"bc-import",
				"import"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCImport.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("undo"))
			{
				BCCommandAbstract.SendOutput("Please use the bc-undo command to undo changes");
				return;
			}
			EntityPlayer entityPlayer = null;
			if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
			{
				entityPlayer = world.Entities.dict[BCCommandAbstract.SenderInfo.RemoteClientInfo.entityId] as EntityPlayer;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				return;
			}
			Prefab prefab = new Prefab();
			int r;
			int x;
			int y;
			int z;
			if (!prefab.Load(BCCommandAbstract.Params[0]))
			{
				BCCommandAbstract.SendOutput("Unable to load prefab " + BCCommandAbstract.Params[0]);
			}
			else if (GetRxyz(prefab, entityPlayer, out r, out x, out y, out z))
			{
				Vector3i vector3i = (BCCommandAbstract.Options.ContainsKey("nooffset") ? new Vector3i(x, y, z) : new Vector3i(x, y + prefab.yOffset, z));
				if (BCCommandAbstract.Options.ContainsKey("air"))
				{
					prefab.bCopyAirBlocks = true;
				}
				if (BCCommandAbstract.Options.ContainsKey("noair"))
				{
					prefab.bCopyAirBlocks = false;
				}
				BlockTranslations(world, prefab, vector3i);
				if (!BCCommandAbstract.Options.ContainsKey("noundo"))
				{
					BCUndo.CreateUndo(entityPlayer, vector3i, prefab.size);
				}
				Log.Out(string.Format("{0}Spawning prefab {1} @ {2}, size={3}, rot={4}", "(BCM)", prefab.PrefabName, vector3i, prefab.size, r));
				BCCommandAbstract.SendOutput($"Spawning prefab {prefab.PrefabName} @ {vector3i}, size={prefab.size}, rot={r}");
				if (BCCommandAbstract.Options.ContainsKey("sblock") || BCCommandAbstract.Options.ContainsKey("editmode") || GameManager.Instance.IsEditMode())
				{
					BCCommandAbstract.SendOutput("* with Sleeper Blocks option set");
				}
				else
				{
					BCCommandAbstract.SendOutput("* with Sleeper Spawning option set");
				}
				if (BCCommandAbstract.Options.ContainsKey("tfill") || BCCommandAbstract.Options.ContainsKey("editmode") || GameManager.Instance.IsEditMode())
				{
					BCCommandAbstract.SendOutput("* with Terrain Filler option set");
				}
				if (BCCommandAbstract.Options.ContainsKey("notrans") || BCCommandAbstract.Options.ContainsKey("editmode") || GameManager.Instance.IsEditMode())
				{
					BCCommandAbstract.SendOutput("* with No Placeholder Translations option set");
				}
				BCCommandAbstract.SendOutput("use bc-undo to revert the changes");
				InsertPrefab(world, prefab, vector3i, r);
			}
		}

		public static void InsertPrefab(World world, Prefab prefab, Vector3i pos, int rot)
		{
			if (prefab == null)
			{
				BCCommandAbstract.SendOutput("No Prefab loaded.");
				return;
			}
			Dictionary<long, Chunk> dictionary = new Dictionary<long, Chunk>();
			for (int i = -1; i <= prefab.size.x + 16; i += 16)
			{
				for (int j = -1; j <= prefab.size.z + 16; j += 16)
				{
					Chunk chunk = world.GetChunkFromWorldPos(pos.x + i, 0, pos.z + j) as Chunk;
					if (chunk != null)
					{
						if (!dictionary.ContainsKey(chunk.Key))
						{
							dictionary.Add(chunk.Key, chunk);
						}
					}
					else
					{
						BCCommandAbstract.SendOutput($"Unable to load chunk for prefab @ {pos.x + i},{pos.z + j}");
					}
				}
			}
			CopyIntoLocal(prefab, world, pos, rot);
			if (!BCCommandAbstract.Options.ContainsKey("noreload") && !BCCommandAbstract.Options.ContainsKey("nr"))
			{
				BCChunks.ReloadForClients(dictionary);
			}
		}

		private bool GetRxyz(Prefab prefab, Entity entity, out int r, out int x, out int y, out int z)
		{
			r = 0;
			x = 0;
			y = 0;
			z = 0;
			if (entity == null && BCCommandAbstract.Params.Count < 4)
			{
				BCCommandAbstract.SendOutput("Command requires <name> <x> <y> <z> params if not sent by an online player");
			}
			else if (entity != null)
			{
				Vector3i vector3i = new Vector3i((int)Math.Floor((double)entity.serverPos.x / 32.0), (int)Math.Floor((double)entity.serverPos.y / 32.0), (int)Math.Floor((double)entity.serverPos.z / 32.0));
				x = vector3i.x - prefab.size.x / 2;
				y = vector3i.y;
				z = vector3i.z - prefab.size.z / 2;
				if (BCCommandAbstract.Options.ContainsKey("csw") || BCCommandAbstract.Options.ContainsKey("ne"))
				{
					x = vector3i.x;
					z = vector3i.z;
				}
				else if (BCCommandAbstract.Options.ContainsKey("cse") || BCCommandAbstract.Options.ContainsKey("nw"))
				{
					x = vector3i.x - prefab.size.x;
					z = vector3i.z;
				}
				else if (BCCommandAbstract.Options.ContainsKey("cnw") || BCCommandAbstract.Options.ContainsKey("se"))
				{
					x = vector3i.x;
					z = vector3i.z - prefab.size.z;
				}
				else if (BCCommandAbstract.Options.ContainsKey("cne") || BCCommandAbstract.Options.ContainsKey("sw"))
				{
					x = vector3i.x - prefab.size.x;
					z = vector3i.z - prefab.size.z;
				}
			}
			if (BCCommandAbstract.Params.Count > 1)
			{
				switch (BCCommandAbstract.Params.Count)
				{
				case 2:
					if (!int.TryParse(BCCommandAbstract.Params[1], out r))
					{
						BCCommandAbstract.SendOutput("<rot> param could not be parsed as a number.");
						return false;
					}
					break;
				case 4:
					if (!int.TryParse(BCCommandAbstract.Params[1], out x) || !int.TryParse(BCCommandAbstract.Params[2], out y) || !int.TryParse(BCCommandAbstract.Params[3], out z))
					{
						BCCommandAbstract.SendOutput("One of <x> <y> <z> params could not be parsed as a number.");
						return false;
					}
					r = 0;
					break;
				case 5:
					if (!int.TryParse(BCCommandAbstract.Params[1], out x) || !int.TryParse(BCCommandAbstract.Params[2], out y) || !int.TryParse(BCCommandAbstract.Params[3], out z) || !int.TryParse(BCCommandAbstract.Params[4], out r))
					{
						BCCommandAbstract.SendOutput("One of <x> <y> <z> <rot> params could not be parsed as a number.");
						return false;
					}
					break;
				default:
					if (BCCommandAbstract.Params.Count != 6)
					{
						BCCommandAbstract.SendOutput("Error: Incorrect command format.");
						BCCommandAbstract.SendOutput(GetHelp());
						return false;
					}
					break;
				}
				prefab.RotateY(_bLeft: false, r % 4);
			}
			if (y < 0)
			{
				BCCommandAbstract.SendOutput($"Y position is too low by {y * -1} blocks");
				return false;
			}
			if (y + prefab.size.y > 255)
			{
				BCCommandAbstract.SendOutput($"Y position is too high by {y + prefab.size.y - 255} blocks");
				return false;
			}
			return true;
		}

		private static void BlockTranslations(World world, Prefab prefab, Vector3i pos)
		{
			List<int> entityIds = new List<int>();
			prefab.CopyEntitiesIntoWorld(world, pos, entityIds, !BCCommandAbstract.Options.ContainsKey("noent") && !BCCommandAbstract.Options.ContainsKey("editmode") && !GameManager.Instance.IsEditMode());
			if (BCCommandAbstract.Options.ContainsKey("notrans") || BCCommandAbstract.Options.ContainsKey("editmode") || GameManager.Instance.IsEditMode())
			{
				return;
			}
			GameRandom gameRandom = GameManager.Instance.World.GetGameRandom();
			BlockPlaceholderMap instance = BlockPlaceholderMap.Instance;
			for (int i = 0; i < prefab.size.x; i++)
			{
				for (int j = 0; j < prefab.size.y; j++)
				{
					for (int k = 0; k < prefab.size.z; k++)
					{
						BlockValue block = prefab.GetBlock(i, j, k);
						if (block.type != 0)
						{
							BlockValue bv = new BlockValue(instance.Replace(block, gameRandom, pos.x + i, pos.z + k).rawData);
							if (block.type != bv.type)
							{
								prefab.SetBlock(i, j, k, bv);
							}
						}
					}
				}
			}
		}

		private static bool IsPointInVolume(Prefab.PrefabSleeperVolume _volume, Vector3i _pos)
		{
			if (_volume.used && _pos.x >= _volume.startPos.x && _pos.x < (_volume.startPos + _volume.size).x && _pos.y >= _volume.startPos.y && _pos.y < (_volume.startPos + _volume.size).y && _pos.z >= _volume.startPos.z)
			{
				return _pos.z < (_volume.startPos + _volume.size).z;
			}
			return false;
		}

		private static bool HasOverlappingVolume(Prefab _prefab, Vector3i _pos, int _index)
		{
			for (int i = 0; i < _prefab.SleeperVolumes.Count; i++)
			{
				if (i != _index)
				{
					Prefab.PrefabSleeperVolume prefabSleeperVolume = _prefab.SleeperVolumes[i];
					if (prefabSleeperVolume.used && prefabSleeperVolume.isPriority && IsPointInVolume(prefabSleeperVolume, _pos))
					{
						return true;
					}
				}
			}
			return false;
		}

		private static void AddSleeperSpawns(Prefab _prefab, int _index, Vector3i _pos, SleeperVolume _volume, Vector3i _startPos, Vector3i _endPos, int _rot)
		{
			for (int i = Mathf.Max(_startPos.x, 0); i < Mathf.Min(_prefab.size.x, _endPos.x); i++)
			{
				int x = i + _pos.x;
				for (int j = Mathf.Max(_startPos.z, 0); j < Mathf.Min(_prefab.size.z, _endPos.z); j++)
				{
					int z = j + _pos.z;
					for (int k = Mathf.Max(_startPos.y, 0); k < Mathf.Min(_prefab.size.y, _endPos.y); k++)
					{
						if (k > 0)
						{
							BlockValue blockNoDamage = _prefab.GetBlockNoDamage(_rot, i, k - 1, j);
							if (Block.list[blockNoDamage.type].IsSleeperBlock)
							{
								continue;
							}
						}
						BlockValue block = _prefab.GetBlock(i, k, j);
						Block block2 = Block.list[block.type];
						if (block2.IsSleeperBlock)
						{
							int y = k + _pos.y;
							if (!HasOverlappingVolume(_prefab, new Vector3i(i, k, j), _index))
							{
								_volume.AddSpawnPoint(x, y, z, (BlockSleeper)block2, block);
							}
						}
					}
				}
			}
		}

		private static void ProcessSleepers(Prefab _prefab, WorldBase _world, Vector3i _pos, int _rot)
		{
			for (int i = 0; i < _prefab.SleeperVolumes.Count; i++)
			{
				Prefab.PrefabSleeperVolume prefabSleeperVolume = _prefab.SleeperVolumes[i];
				if (!prefabSleeperVolume.used)
				{
					continue;
				}
				Vector3i startPos = prefabSleeperVolume.startPos;
				Vector3i vector3i = startPos + prefabSleeperVolume.size;
				Vector3i vector3i2 = startPos + _pos;
				Vector3i vector3i3 = vector3i2 + prefabSleeperVolume.size;
				Vector3i vector3i4 = vector3i2 - SleeperVolume.chunkPadding;
				Vector3i vector3i5 = vector3i3 + SleeperVolume.chunkPadding;
				int num = _world.FindSleeperVolume(vector3i2, vector3i3);
				if (num < 0)
				{
					SleeperVolume volume = SleeperVolume.Create(prefabSleeperVolume, vector3i2, vector3i + _pos);
					num = _world.AddSleeperVolume(volume);
					AddSleeperSpawns(_prefab, i, _pos, volume, startPos, vector3i, _rot);
				}
				for (int j = World.toChunkXZ(vector3i4.x); j <= World.toChunkXZ(vector3i5.x - 1); j++)
				{
					for (int k = World.toChunkXZ(vector3i4.z); k <= World.toChunkXZ(vector3i5.z - 1); k++)
					{
						((Chunk)_world.GetChunkSync(j, 0, k))?.GetSleeperVolumes().Add(num);
					}
				}
			}
		}

		public static void CopyIntoLocal(Prefab _prefab, World _world, Vector3i _pos, int _rot)
		{
			ChunkCluster chunkCache = _world.ChunkCache;
			if (!_world.IsEditor())
			{
				ProcessSleepers(_prefab, _world, _pos, _rot);
			}
			Chunk chunkSync = chunkCache.GetChunkSync(World.toChunkXZ(_pos.x), World.toChunkXZ(_pos.z));
			GameRandom gameRandom = GameManager.Instance.World.GetGameRandom();
			int type = Block.GetBlockValue(Constants.cTerrainFillerBlockName).type;
			for (int i = 0; i < _prefab.size.x; i++)
			{
				for (int j = 0; j < _prefab.size.z; j++)
				{
					int v = i + _pos.x;
					int v2 = j + _pos.z;
					int num = World.toChunkXZ(v);
					int num2 = World.toChunkXZ(v2);
					int num3 = World.toBlockXZ(v);
					int num4 = World.toBlockXZ(v2);
					if (chunkSync == null || chunkSync.X != num || chunkSync.Z != num2)
					{
						chunkSync = chunkCache.GetChunkSync(num, num2);
					}
					if (chunkSync == null)
					{
						continue;
					}
					int terrainHeight = chunkSync.GetTerrainHeight(num3, num4);
					for (int k = 0; k < _prefab.size.y; k++)
					{
						BlockValue blockValue = _prefab.GetBlock(i, k, j);
						if (!_prefab.bCopyAirBlocks && blockValue.type == 0)
						{
							continue;
						}
						int type2 = blockValue.type;
						if (!_world.IsEditor() && Block.list[blockValue.type].IsSleeperBlock)
						{
							blockValue = BlockValue.Air;
						}
						int num5 = World.toBlockY(k + _pos.y);
						if (!_world.IsEditor() && type != 0 && blockValue.type == type)
						{
							BlockValue block = chunkSync.GetBlock(num3, num5, num4);
							if (block.type == 0 || Block.list[block.type] == null || !Block.list[block.type].shape.IsTerrain())
							{
								block = chunkSync.GetBlock(num3, chunkSync.GetTerrainHeight(num3, num4), num4);
							}
							if (block.type == 0 || Block.list[block.type] == null || !Block.list[block.type].shape.IsTerrain())
							{
								continue;
							}
							blockValue = block;
						}
						if (!_world.IsEditor() && blockValue.type > 0)
						{
							blockValue = BlockPlaceholderMap.Instance.Replace(blockValue, gameRandom, num3, num4);
						}
						sbyte b = _prefab.GetDensity(i, k, j);
						if (b == 0)
						{
							b = (Block.list[blockValue.type].shape.IsTerrain() ? MarchingCubes.DensityTerrain : MarchingCubes.DensityAir);
						}
						if (!chunkSync.GetBlock(num3, num5, num4).ischild)
						{
							if (_prefab.bAllowTopSoilDecorations)
							{
								chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigOnlySmall);
							}
							else if (num5 >= terrainHeight + 1)
							{
								chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigNoSmall);
							}
							else if (num5 == terrainHeight)
							{
								chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.Nothing);
							}
							chunkSync.SetTextureFull(num3, num5, num4, _prefab.GetTexture(i, k, j));
							chunkSync.SetBlock((WorldBase)_world, num3, num5, num4, blockValue);
							if (Block.list[blockValue.type] == null)
							{
								continue;
							}
							TileEntity tileEntity;
							if (Block.list[type2].IsTileEntitySavedInPrefab() && (tileEntity = _prefab.GetTileEntity(new Vector3i(i, k, j))) != null)
							{
								TileEntity tileEntity2 = chunkSync.GetTileEntity(new Vector3i(num3, num5, num4));
								if (tileEntity2 == null)
								{
									tileEntity2 = tileEntity.Clone();
									tileEntity2.localChunkPos = new Vector3i(num3, num5, num4);
									tileEntity2.SetChunk(chunkSync);
									chunkSync.AddTileEntity(tileEntity2);
								}
								tileEntity2.CopyFrom(tileEntity);
								tileEntity2.localChunkPos = new Vector3i(num3, num5, num4);
							}
							if (Block.list[blockValue.type].shape.IsTerrain() && chunkSync.GetTerrainHeight(num3, num4) < num5)
							{
								chunkSync.SetTerrainHeight(num3, num4, (byte)num5);
							}
						}
						chunkSync.SetDensity(num3, num5, num4, b);
					}
					chunkSync.SetDecoAllowedAt(num3, num4, EnumDecoAllowed.NoBigOnlySmall);
					chunkSync.NeedsRegeneration = true;
				}
			}
		}
	}
}
