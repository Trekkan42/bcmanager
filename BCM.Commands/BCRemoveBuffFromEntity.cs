using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCRemoveBuffFromEntity : BCCommandAbstract
	{
		public BCRemoveBuffFromEntity()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCRemoveBuffFromEntity"];
			DefaultCommands = new string[1]
			{
				"bc-debuff"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCRemoveBuffFromEntity.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[0], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse entity id " + BCCommandAbstract.Params[0]);
				return;
			}
			EntityAlive entityAlive = world.GetEntity(result) as EntityAlive;
			if ((object)entityAlive == null)
			{
				BCCommandAbstract.SendOutput("Unable to find entity");
			}
			else if (BuffManager.Buffs.ContainsKey(BCCommandAbstract.Params[1]))
			{
				entityAlive.Buffs.RemoveBuff(BCCommandAbstract.Params[1]);
				BCCommandAbstract.SendOutput($"Buff {BCCommandAbstract.Params[1]} removed from entity ({entityAlive.entityId}) {entityAlive.EntityName}");
			}
			else
			{
				BCCommandAbstract.SendOutput("Unable to find buff " + BCCommandAbstract.Params[1]);
			}
		}
	}
}
