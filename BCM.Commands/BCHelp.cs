using System.Collections.Generic;
using System.Linq;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
    public class BCHelp : BCCommandAbstract
    {
        public override int DefaultPermissionLevel => 1000;

        public BCHelp()
        {
            ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCHelp"];
            DefaultCommands = new string[1]
            {
                "bc-help"
            };
            Commands = configValue.GetItem("Commands", DefaultCommands);
            BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
        }

        public override string GetHelp()
        {
            return Resources.BCHelp.ParseHelp(this);
        }

        protected override void Process()
        {
            if (BCCommandAbstract.Params.Count > 0)
            {
                IConsoleCommand command = SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommand(BCCommandAbstract.Params[0]);
                if (command == null)
                {
                    BCCommandAbstract.SendJsonError("Command not found: " + BCCommandAbstract.Params[0]);
                    return;
                }
                string help = command.GetHelp();
                if (!BCCommandAbstract.Options.ContainsKey("sm"))
                {
                    BCCommandAbstract.SendOutput(help);
                    return;
                }
                string[] array = help.Split('\n');
                int num = 0;
                string[] array2 = array;
                foreach (string text in array2)
                {
                    array[num] = text.TrimEnd('\r');
                    num++;
                }
                BCCommandAbstract.SendJson(array);
                return;
            }
            if (BCCommandAbstract.Options.ContainsKey("sm"))
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                foreach (IConsoleCommand command2 in SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommands())
                {
                    string text2 = command2.GetType().ToString();
                    string commands = string.Join(" ", command2.GetCommands());
                    int j = 0;
                    if (dictionary.ContainsKey(text2))
                    {
                        for (; dictionary.ContainsKey($"{text2}_{j}"); j++)
                        {
                        }
                        text2 = $"{text2}_{j}";
                    }
                    dictionary.Add(text2, new
                    {
                        Commands = commands,
                        Description = command2.GetDescription(),
                        Permission = command2.DefaultPermissionLevel
                    });
                }
                BCCommandAbstract.SendJson(dictionary);
                return;
            }
            List<string> list = new List<string>
            {
                "***Bad Company Commands***"
            };
            list.AddRange(from c in SingletonMonoBehaviour<SdtdConsole>.Instance.GetCommands()
                          where c.GetCommands()[0].StartsWith("bc-")
                          select string.Join(", ", c.GetCommands()) + " => " + c.GetDescription());
            list.Add("");
            list.Add("***Options***");
            list.Add("/log => Send the command output to the log file");
            list.Add("/chat => Send the command output to chat");
            list.Add("/console => Override command default settings for /log or /chat");
            list.Add("/color=FFFFFF => Specify a color for text sent to chat");
            list.Add("/details => For commands that support it, will give more details on items returned");
            list.Add("/nodetails => Override command default settings for /details");
            list.Add("/online => For ListPlayers commands it will display only online players (default shows all players)");
            list.Add("/offline => For ListPlayers commands it will display only offline players");
            list.Add("/all => Override command default settings for /online or /offline");
            list.Add("***Output Format Options***");
            list.Add("/1l => Returns json output on a single line (for server managers)");
            list.Add("/pp => Returns json output with print pretty enabled (default: on)");
            list.Add("/vectors => Returns all BCM Vectors as x y z objects rather than single string");
            list.Add("/csvpos =>  Converts all Vector3 co-ords to csv separated (default is space separated)");
            list.Add("/worldpos => Converts all Vector3 co-ords to Map co-ords");
            list.Add("/strpos => Override command default settings for /csvpos or /worldpos");
            BCCommandAbstract.SendOutput(string.Join("\n", list.ToArray()));
        }
    }
}
