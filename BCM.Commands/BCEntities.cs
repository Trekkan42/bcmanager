using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCEntities : BCCommandAbstract
	{
		public BCEntities()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCEntities"];
			DefaultCommands = new string[2]
			{
				"bc-entities",
				"bc-le"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCEntities.ParseHelp(this);
		}

		private static void Filters()
		{
			BCCommandAbstract.SendJson(typeof(BCMEntity.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMEntity.StrFilters))}"));
		}

		private static void Indexed()
		{
			BCCommandAbstract.SendJson((from kvp in BCMEntity.FilterMap
				group kvp by kvp.Value into @group
				select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("filters"))
			{
				Filters();
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("index"))
			{
				Indexed();
				return;
			}
			if (BCCommandAbstract.Params.Count > 1)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (BCCommandAbstract.Params.Count == 1)
			{
				Entity entity = null;
				if (int.TryParse(BCCommandAbstract.Params[0], out var result) && world.Entities.dict.ContainsKey(result))
				{
					entity = world.Entities.dict[result];
				}
				if (entity == null)
				{
					BCCommandAbstract.SendOutput("Entity id not found.");
					return;
				}
				BCMEntity bCMEntity = new BCMEntity(entity, BCCommandAbstract.Options, BCCommandAbstract.GetFilters("entities"));
				if (BCCommandAbstract.Options.ContainsKey("min"))
				{
					BCCommandAbstract.SendJson(new List<List<object>>
					{
						(from d in bCMEntity.Data()
							select d.Value).ToList()
					});
				}
				else
				{
					BCCommandAbstract.SendJson(bCMEntity.Data());
				}
				return;
			}
			List<object> list = new List<object>();
			foreach (BCMEntity item in BCMCmdProcessor.FilterEntities(world.Entities.dict, BCCommandAbstract.Options).Values.Select((Entity en) => new BCMEntity(en, BCCommandAbstract.Options, BCCommandAbstract.GetFilters("entities"))))
			{
				if (BCCommandAbstract.Options.ContainsKey("min"))
				{
					list.Add((from d in item.Data()
						select d.Value).ToList());
				}
				else
				{
					list.Add(item.Data());
				}
			}
			BCCommandAbstract.SendJson(list);
		}
	}
}
