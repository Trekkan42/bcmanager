using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BCM.Models;
using BCM.Models.TileEntities;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCBlock : BCCommandAbstract
	{
		private static class Sub
		{
			public const string Scan = "scan";

			public const string TileScan = "tilescan";

			public const string Fill = "fill";

			public const string Swap = "swap";

			public const string Repair = "repair";

			public const string Damage = "damage";

			public const string Upgrade = "upgrade";

			public const string Downgrade = "downgrade";

			public const string Density = "density";

			public const string Rotate = "rotate";

			public const string Owner = "owner";

			public const string Access = "access";

			public const string Revoke = "revoke";

			public const string Lock = "lock";

			public const string Unlock = "unlock";

			public const string Empty = "empty";

			public const string Reset = "reset";

			public const string Remove = "remove";

			public const string AddItem = "additem";

			public const string RemoveItem = "removeitem";

			public const string SortLoot = "sortitems";

			public const string SetText = "settext";

			public const string Paint = "paint";

			public const string PaintFace = "paintface";

			public const string PaintStrip = "paintstrip";

			public const string Open = "open";

			public const string Close = "close";

			public const string LightOn = "lighton";

			public const string LightOff = "lightoff";

			public const string Meta1 = "meta1";

			public const string Meta2 = "meta2";

			public const string LooterScan = "looterscan";
		}

		private class SubDefinition
		{
			public readonly Func<BCMCmdProcessor, bool> Action;

			public readonly ReloadMode Reload;

			public readonly string Desc;

			public readonly bool HasUndo;

			public SubDefinition(Func<BCMCmdProcessor, bool> action, ReloadMode reload, bool hasUndo, string desc)
			{
				Action = action;
				Reload = reload;
				HasUndo = hasUndo;
				Desc = desc;
			}
		}

		private const string _lockedTileEntitiesFunction = "lockedTileEntities";

		private static FieldInfo LockedTileEntities;

		private static readonly Dictionary<string, SubDefinition> SubDefs = new Dictionary<string, SubDefinition>
		{
			{
				"scan",
				new SubDefinition(ScanBlocks, ReloadMode.None, hasUndo: false, "Scan blocks and report counts by type." + Nl(8) + "Use scan <co-ords> !partialBlockName to provide a block type filter. If the filter is omitted then all block types are scanned.")
			},
			{
				"tilescan",
				new SubDefinition(ScanTiles, ReloadMode.None, hasUndo: false, "Scans tile entity block details." + Nl(8) + "Use tilescan. Use /type=<TileEntityType> to provide a filter for the type of tile entity to scan.")
			},
			{
				"fill",
				new SubDefinition(FillBlocks, ReloadMode.All, hasUndo: true, "Fill the area with the given block." + Nl(8) + "Use fill <co-ords> !blockName to specify the block to fill with, blockName can be a partial name if it matches only one block type." + Nl(8) + "Use /t=# to specify a texture to be applied to the blocks if they are paint-able." + Nl(8) + "e.g. fill !air or fill !terrStone")
			},
			{
				"swap",
				new SubDefinition(SwapBlocks, ReloadMode.All, hasUndo: true, "Swap blocks of type1 with type2." + Nl(8) + "Use swap <co-ords> !partialBlockName !blockName to specify target and replacement blocks, blockName can be a partial name if it matches only one block type." + Nl(8) + "e.g. swap !terr !terrStone - change all terr blocks to terrStone. swap !ore !steelBlock - change all ore blocks to a steelBlock.")
			},
			{
				"repair",
				new SubDefinition(RepairBlocks, ReloadMode.All, hasUndo: true, "Repair damaged blocks." + Nl(8) + "Use repair <co-ords> !# to specify an amount, or !#,# to give a random range, or leave out the !# to fully repair.")
			},
			{
				"damage",
				new SubDefinition(DamageBlocks, ReloadMode.All, hasUndo: true, "Cause damage to blocks." + Nl(8) + "Use damage <co-ords> !# to specify the damage amount, or !#,# to give a random range." + Nl(8) + "Option /nobreak will prevent any blocks from breaking. Option /overkill to pass damage to downgrade blocks.")
			},
			{
				"upgrade",
				new SubDefinition(UpgradeBlocks, ReloadMode.All, hasUndo: true, "Upgrade blocks 1 step. Upgrading blocks also fully repairs them.")
			},
			{
				"downgrade",
				new SubDefinition(DowngradeBlocks, ReloadMode.All, hasUndo: true, "Downgrade blocks 1 step. Downgrading blocks also fully repairs them.")
			},
			{
				"density",
				new SubDefinition(SetDensity, ReloadMode.All, hasUndo: true, "Set the density of blocks." + Nl(8) + "Use density <co-ords> !# to specify the density value. Option /force will set the density outside of standard ranges for the block types.")
			},
			{
				"rotate",
				new SubDefinition(SetRotation, ReloadMode.All, hasUndo: true, "Rotate blocks to the given value." + Nl(8) + "Use rotate !# to specify the rotation index to set the blocks to.")
			},
			{
				"owner",
				new SubDefinition(SetOwner, ReloadMode.Target, hasUndo: false, "Set the owner of tile entity blocks." + Nl(8) + "Use owner <co-ords> /id=## to grant ownership to another player using their steam id." + Nl(8) + "Use /claim to override owner on land claims as well. Use /type=<TileEntityType> to provide a filter for the type of tile entity.")
			},
			{
				"access",
				new SubDefinition(GrantAccess, ReloadMode.Target, hasUndo: false, "Grant access to secure blocks." + Nl(8) + "Use access <co-ords> /id=## to grant access to another player using their steam id. Use /type=<TileEntityType> to provide a filter for the type of tile entity.")
			},
			{
				"revoke",
				new SubDefinition(RevokeAccess, ReloadMode.Target, hasUndo: false, "Revoke access to secure blocks." + Nl(8) + "Use revoke <co-ords> /id=## to revoke access for another player using their steam id. Use /type=<TileEntityType> to provide a filter for the type of tile entity.")
			},
			{
				"lock",
				new SubDefinition(SetLocked, ReloadMode.All, hasUndo: false, "Lock secure blocks." + Nl(8) + "Use lock <co-ords> to lock a secure container or door. Use /pwd=<password> to set a password as well.")
			},
			{
				"unlock",
				new SubDefinition(SetUnLocked, ReloadMode.All, hasUndo: false, "Unlock secure blocks." + Nl(8) + "Use unlock <co-ords> to unlock a secure container or door.")
			},
			{
				"empty",
				new SubDefinition(EmptyContainers, ReloadMode.None, hasUndo: false, "Remove the contents of containers." + Nl(8) + "Use empty <co-ords> to empty all containers in the area. Containers in use will not be affected. Use /type=<TileEntityType> to provide a filter for the type of tile entity.")
			},
			{
				"reset",
				new SubDefinition(ResetTouched, ReloadMode.None, hasUndo: false, "Reset the touched status of containers." + Nl(8) + "Use reset to reset the loot containers in the area. Use /type=<TileEntityType> to provide a filter for the type of tile entity.")
			},
			{
				"remove",
				new SubDefinition(RemoveTiles, ReloadMode.All, hasUndo: true, "Removes blocks for tile entities." + Nl(8) + "Use remove /confirm to remove tile entity blocks. Use /type=<TileEntityType> to provide a filter for the type of tile entity to remove.")
			},
			{
				"additem",
				new SubDefinition(AddLoot, ReloadMode.None, hasUndo: false, "Add an item stack to a container" + Nl(8) + "Use additem")
			},
			{
				"removeitem",
				new SubDefinition(RemoveLoot, ReloadMode.None, hasUndo: false, "Remove an item stack from a container" + Nl(8) + "Use removeitem")
			},
			{
				"sortitems",
				new SubDefinition(SortItems, ReloadMode.None, hasUndo: false, "Organize items in a container." + Nl(8) + "Use sortitems to sort items in all containers in the area. Only works on containers not currently being accessed.")
			},
			{
				"settext",
				new SubDefinition(SetSignText, ReloadMode.None, hasUndo: false, "Set the text of signs." + Nl(8) + "Use settext !text_with_underscore_for_spaces, or settext \"!text with spaces wrapped in quotes\". Use /p=_ to change the character to use for spaces, default is underscore.")
			},
			{
				"paint",
				new SubDefinition(SetPaint, ReloadMode.All, hasUndo: true, "Paint all sides of paint-able blocks." + Nl(8) + "Use paint !# where # is a texture id as seen in the paintbrush select paint menu.")
			},
			{
				"paintface",
				new SubDefinition(SetPaintFace, ReloadMode.All, hasUndo: true, "Paint 1 side of paint-able blocks." + Nl(8) + "Use paintface !# /face=0-5 where # is a texture id as seen in the paintbrush select paint menu and face is the face to paint. The face is relative to the block rotation.")
			},
			{
				"paintstrip",
				new SubDefinition(RemovePaint, ReloadMode.All, hasUndo: true, "Remove all paint from blocks." + Nl(8) + "Use paintstrip /face=0-5 to remove paint from a single face. Omit the face option to remove from all sides. The face is relative to the block rotation.")
			},
			{
				"open",
				new SubDefinition(OpenDoors, ReloadMode.None, hasUndo: false, "Opens door blocks.")
			},
			{
				"close",
				new SubDefinition(CloseDoors, ReloadMode.None, hasUndo: false, "Closes door blocks.")
			},
			{
				"lighton",
				new SubDefinition(TurnLightsOn, ReloadMode.None, hasUndo: false, "Turns lights blocks on.")
			},
			{
				"lightoff",
				new SubDefinition(TurnLightsOff, ReloadMode.None, hasUndo: false, "Turns lights blocks off.")
			},
			{
				"meta1",
				new SubDefinition(SetMeta1, ReloadMode.All, hasUndo: false, "Set the meta1 to a specific value." + Nl(8) + "Use meta1 !# to specify the value to set for meta1.")
			},
			{
				"meta2",
				new SubDefinition(SetMeta2, ReloadMode.All, hasUndo: false, "Set the meta2 to a specific value." + Nl(8) + "Use meta2 !# to specify the value to set for meta2.")
			},
			{
				"looterscan",
				new SubDefinition(ScanLooters, ReloadMode.None, hasUndo: false, "Scan and report players that are currently accessing loot containers.")
			}
		};

		public BCBlock()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCBlock"];
			DefaultCommands = new string[2]
			{
				"bc-block2",
				"block2"
			};
			DefaultOptions = new Dictionary<string, string>
			{
				{
					"loc",
					null
				}
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
			LockedTileEntities = typeof(GameManager).GetField("lockedTileEntities", BindingFlags.Instance | BindingFlags.NonPublic);
			if (LockedTileEntities == null)
			{
				throw new Exception("BCBlock: Couldn't access method for lockedTileEntities in GameManager");
			}
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCBlock.ParseHelp(this) + string.Join("", SubDefs.Select((KeyValuePair<string, SubDefinition> s) => Nl(6) + s.Key + " - " + s.Value.Desc).ToArray());
		}

		private static string Nl(int l)
		{
			return "\n" + new string(' ', l);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld())
			{
				BCMCmdProcessor command = new BCMCmdProcessor(BCCommandAbstract.Params, BCCommandAbstract.Options, "BCBlock");
				if (BCMCmdProcessor.ProcessParams(command))
				{
					BCMCmdProcessor.ProcessTask(command, this);
				}
			}
		}

		public override void ProcessCmd(BCMCmdProcessor command, out ReloadMode reload)
		{
			reload = ReloadMode.None;
			if (!SubDefs.ContainsKey(command.Command))
			{
				BCCommandAbstract.SendOutput("Unknown sub-command " + command.Command);
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else if (SubDefs[command.Command].Action(command))
			{
				reload = SubDefs[command.Command].Reload;
			}
		}

		private static bool ScanBlocks(BCMCmdProcessor command)
		{
			string text = ((command.BPars.Count > 0) ? command.BPars[0] : "");
			List<int> list = new List<int>();
			if (!string.IsNullOrEmpty(text))
			{
				if (int.TryParse(text, out var result))
				{
					list.Add(result);
				}
				else
				{
					list = GetBlockIds(text).ToList();
					if (!list.Any())
					{
						BCCommandAbstract.SendOutput("Unable to find block(s) containing " + text + " in their name");
						return false;
					}
				}
			}
			SortedDictionary<string, int> sortedDictionary = new SortedDictionary<string, int>();
			for (int i = command.Position.y; i <= command.MaxPos.y; i++)
			{
				for (int j = command.Position.x; j <= command.MaxPos.x; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(j, i, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.ischild && (!(text != "") || list.Contains(block.type)))
						{
							SetStats(block.Block.GetBlockName(), block, sortedDictionary);
						}
					}
				}
			}
			BCCommandAbstract.SendJson(sortedDictionary);
			return true;
		}

		private static bool FillBlocks(BCMCmdProcessor command)
		{
			if (command.BPars.Count == 0 || string.IsNullOrEmpty(command.BPars[0]))
			{
				BCCommandAbstract.SendOutput("A block name must be provided in the form !blockName.");
				return false;
			}
			string text = command.BPars[0];
			List<int> list = new List<int>();
			if (int.TryParse(text, out var result))
			{
				list.Add(result);
			}
			else
			{
				list = GetBlockIds(text).ToList();
				if (!list.Any())
				{
					BCCommandAbstract.SendOutput("Unable to find block containing " + text + " in their name.");
					return false;
				}
				if (list.Count > 1)
				{
					BCCommandAbstract.SendOutput($"{list.Count} results found for {text}, please be more specific:");
					BCCommandAbstract.SendOutput(string.Join(", ", list.Select((int i) => Block.list[i]?.GetBlockName()).ToArray()));
					return false;
				}
			}
			if (Block.list[list[0]] == null)
			{
				BCCommandAbstract.SendOutput("Unable to find block by id or name.");
				return false;
			}
			BlockValue blockValue = Block.GetBlockValue(list[0]);
			if (blockValue.Equals(BlockValue.Air) && list[0] != 0)
			{
				BCCommandAbstract.SendOutput("Unable to find block type.");
				return false;
			}
			SetBlocks(command, blockValue, searchAll: true);
			BCCommandAbstract.SendOutput((!command.Opts.ContainsKey("delmulti")) ? ("Filling area with " + Block.list[list[0]].GetBlockName() + " blocks.") : "Removed multidim blocks.");
			return true;
		}

		private static bool SwapBlocks(BCMCmdProcessor command)
		{
			if (command.BPars.Count < 2)
			{
				BCCommandAbstract.SendOutput("A source and target block param is required in the form of !sourceBlockFilter !targetBlockName");
				return false;
			}
			string text = command.BPars[0];
			string text2 = command.BPars[1];
			List<int> list = new List<int>();
			if (int.TryParse(text, out var result))
			{
				list.Add(result);
			}
			else
			{
				list = GetBlockIds(text).ToList();
				if (!list.Any())
				{
					BCCommandAbstract.SendOutput("Unable to find block(s) containing " + text + " in their name.");
					return false;
				}
			}
			List<int> list2 = new List<int>();
			if (int.TryParse(text2, out var result2))
			{
				list2.Add(result2);
			}
			else
			{
				list2 = GetBlockIds(text2).ToList();
				if (!list2.Any())
				{
					BCCommandAbstract.SendOutput("Unable to find block(s) containing " + text2 + " in their name.");
					return false;
				}
				if (list2.Count > 1)
				{
					BCCommandAbstract.SendOutput($"{list2.Count} results found for replacement block {text2}, please be more specific:");
					BCCommandAbstract.SendOutput(string.Join(", ", list2.Select((int i) => Block.list[i]?.GetBlockName()).ToArray()));
					return false;
				}
			}
			BlockValue blockValue = Block.GetBlockValue(list2[0]);
			if (blockValue.Equals(BlockValue.Air) && list2[0] != 0)
			{
				BCCommandAbstract.SendOutput("Unable to find replacement block type.");
				return false;
			}
			long textureFull = 0L;
			if (command.Opts.ContainsKey("t"))
			{
				if (!byte.TryParse(command.Opts["t"], out var result3))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture index.");
					return false;
				}
				if (BlockTextureData.list[result3] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result3}.");
					return false;
				}
				long num = 0L;
				for (int j = 0; j < 6; j++)
				{
					int num2 = j * 8;
					num &= ~(255L << num2);
					num |= (long)(result3 & 0xFF) << num2;
				}
				textureFull = num;
			}
			sbyte result4 = 1;
			if (command.Opts.ContainsKey("d"))
			{
				if (sbyte.TryParse(command.Opts["d"], out result4))
				{
					BCCommandAbstract.SendOutput($"Using density {result4}.");
				}
			}
			else if (blockValue.Equals(BlockValue.Air))
			{
				result4 = MarchingCubes.DensityAir;
			}
			else if (blockValue.Block.shape.IsTerrain())
			{
				result4 = MarchingCubes.DensityTerrain;
			}
			int num3 = 0;
			for (int k = command.Position.x; k <= command.MaxPos.x; k++)
			{
				for (int l = command.Position.y; l <= command.MaxPos.y; l++)
				{
					for (int m = command.Position.z; m <= command.MaxPos.z; m++)
					{
						Vector3i vector3i = new Vector3i(k, l, m);
						if (list.Contains(GameManager.Instance.World.GetBlock(vector3i).type))
						{
							GameManager.Instance.World.ChunkClusters[0].SetBlock(vector3i, _bChangeBlockValue: true, blockValue, _bChangeDensity: false, result4, bNotify: false, updateLight: false);
							GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, textureFull);
							num3++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Replaced {num3} blocks with '{blockValue.Block.GetBlockName()}'.");
			return true;
		}

		private static bool RepairBlocks(BCMCmdProcessor command)
		{
			int result = 0;
			int result2 = 0;
			if (command.BPars.Count != 0)
			{
				if (command.BPars[0].IndexOf(",", StringComparison.InvariantCulture) > -1)
				{
					string[] array = command.BPars[0].Split(',');
					if (array.Length != 2)
					{
						BCCommandAbstract.SendOutput("Unable to parse repair values.");
						return false;
					}
					if (!int.TryParse(array[0], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse repair min value.");
						return false;
					}
					if (!int.TryParse(array[1], out result2))
					{
						BCCommandAbstract.SendOutput("Unable to parse repair max value.");
						return false;
					}
				}
				else if (!int.TryParse(command.BPars[0], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse repair value.");
					return false;
				}
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air))
						{
							if (result == 0 && result2 == 0)
							{
								block.damage = 0;
							}
							else
							{
								block.damage = Math.Max(0, block.damage - ((result2 != 0) ? UnityEngine.Random.Range(result, result2) : result));
							}
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Repairing {0} for {1} blocks.", (result == 0 && result2 == 0) ? "all damage" : string.Format("{0} {1}", result, (result2 != 0) ? $" to {result2}" : ""), num));
			return true;
		}

		private static bool DamageBlocks(BCMCmdProcessor command)
		{
			int result = 0;
			int result2 = 0;
			if (command.BPars.Count == 0)
			{
				BCCommandAbstract.SendOutput("A damage amount is required in the form !#.");
				return false;
			}
			if (command.BPars[0].IndexOf(",", StringComparison.InvariantCulture) > -1)
			{
				string[] array = command.BPars[0].Split(',');
				if (array.Length != 2)
				{
					BCCommandAbstract.SendOutput("Unable to parse damage values.");
					return false;
				}
				if (!int.TryParse(array[0], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse damage min value.");
					return false;
				}
				if (!int.TryParse(array[1], out result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse damage max value.");
					return false;
				}
			}
			else if (!int.TryParse(command.BPars[0], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse damage value.");
				return false;
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue blockValue = GameManager.Instance.World.GetBlock(0, pos);
						if (blockValue.Equals(BlockValue.Air))
						{
							continue;
						}
						int maxDamage = blockValue.Block.blockMaterial.MaxDamage;
						int num2 = ((result2 != 0) ? UnityEngine.Random.Range(result, result2) : result) + blockValue.damage;
						if (command.Opts.ContainsKey("nobreak"))
						{
							blockValue.damage = Math.Min(num2, maxDamage - 1);
						}
						else if (command.Opts.ContainsKey("overkill"))
						{
							while (num2 >= maxDamage)
							{
								BlockValue downgradeBlock = blockValue.Block.DowngradeBlock;
								num2 -= maxDamage;
								maxDamage = downgradeBlock.Block.blockMaterial.MaxDamage;
								downgradeBlock.rotation = blockValue.rotation;
								blockValue = downgradeBlock;
							}
							blockValue.damage = num2;
						}
						else if (num2 >= maxDamage)
						{
							BlockValue downgradeBlock2 = blockValue.Block.DowngradeBlock;
							downgradeBlock2.rotation = blockValue.rotation;
							blockValue = downgradeBlock2;
						}
						else
						{
							blockValue.damage = num2;
						}
						GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, blockValue, bNotify: false, updateLight: false);
						num++;
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Dealing {0} {1} damage to {2} blocks.", result, (result2 != 0) ? $" to {result2}" : "", num));
			return true;
		}

		private static bool UpgradeBlocks(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						BlockValue upgradeBlock = block.Block.UpgradeBlock;
						if (!upgradeBlock.Equals(BlockValue.Air) && !block.ischild)
						{
							upgradeBlock.rotation = block.rotation;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, upgradeBlock, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Upgrading {num} blocks.");
			return true;
		}

		private static bool DowngradeBlocks(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						BlockValue downgradeBlock = block.Block.DowngradeBlock;
						if (!downgradeBlock.Equals(BlockValue.Air) && !block.ischild)
						{
							downgradeBlock.rotation = block.rotation;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, downgradeBlock, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Downgrading {num} blocks.");
			return true;
		}

		private static bool SetDensity(BCMCmdProcessor command)
		{
			if (command.BPars.Count == 0)
			{
				BCCommandAbstract.SendOutput("A density amount is required in the form !#.");
				return false;
			}
			if (!sbyte.TryParse(command.BPars[0], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse density value.");
				return false;
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild)
						{
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, _bChangeBlockValue: false, block, _bChangeDensity: true, result, bNotify: false, updateLight: false, command.Opts.ContainsKey("force"));
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting density {result} on {num} blocks.");
			return true;
		}

		private static bool SetRotation(BCMCmdProcessor command)
		{
			if (command.BPars.Count == 0)
			{
				BCCommandAbstract.SendOutput("A rotation is required in the form !#.");
				return false;
			}
			if (!byte.TryParse(command.BPars[0], out var result))
			{
				BCCommandAbstract.SendOutput("Unable to parse rotation value.");
				return false;
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i pos = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild && block.Block.shape.IsRotatable)
						{
							block.rotation = result;
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting rotation {result} on {num} blocks.");
			return true;
		}

		private static bool SetOwner(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (NotIncluded(command, worldChunkPos, item))
						{
							continue;
						}
						TileEntity value = item.Value;
						TileEntityVendingMachine tileEntityVendingMachine = value as TileEntityVendingMachine;
						if (tileEntityVendingMachine == null)
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer == null)
							{
								TileEntitySecureDoor tileEntitySecureDoor = value as TileEntitySecureDoor;
								if (tileEntitySecureDoor == null)
								{
									TileEntitySign tileEntitySign = value as TileEntitySign;
									if (tileEntitySign == null)
									{
										TileEntityPowerSource tileEntityPowerSource = value as TileEntityPowerSource;
										if (tileEntityPowerSource == null)
										{
											TileEntityPoweredRangedTrap tileEntityPoweredRangedTrap = value as TileEntityPoweredRangedTrap;
											if (tileEntityPoweredRangedTrap == null)
											{
												TileEntityPoweredTrigger tileEntityPoweredTrigger = value as TileEntityPoweredTrigger;
												if (tileEntityPoweredTrigger == null)
												{
													TileEntityLandClaim tileEntityLandClaim = value as TileEntityLandClaim;
													if (tileEntityLandClaim != null && command.Opts.ContainsKey("claim"))
													{
														tileEntityLandClaim.SetOwner(command.SteamId);
														num++;
													}
												}
												else
												{
													tileEntityPoweredTrigger.SetOwner(command.SteamId);
													num++;
												}
											}
											else
											{
												tileEntityPoweredRangedTrap.SetOwner(command.SteamId);
												num++;
											}
										}
										else
										{
											tileEntityPowerSource.SetOwner(command.SteamId);
											num++;
										}
									}
									else
									{
										tileEntitySign.SetOwner(command.SteamId);
										num++;
									}
								}
								else
								{
									tileEntitySecureDoor.SetOwner(command.SteamId);
									num++;
								}
							}
							else
							{
								tileEntitySecureLootContainer.SetOwner(command.SteamId);
								num++;
							}
						}
						else
						{
							tileEntityVendingMachine.SetOwner(command.SteamId);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Set owner on {num} secure blocks.");
			return true;
		}

		private static bool GrantAccess(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (NotIncluded(command, worldChunkPos, item))
						{
							continue;
						}
						TileEntity value = item.Value;
						TileEntityVendingMachine tileEntityVendingMachine = value as TileEntityVendingMachine;
						if (tileEntityVendingMachine == null)
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer == null)
							{
								TileEntitySecureDoor tileEntitySecureDoor = value as TileEntitySecureDoor;
								if (tileEntitySecureDoor == null)
								{
									TileEntitySign tileEntitySign = value as TileEntitySign;
									if (tileEntitySign != null)
									{
										List<string> users = tileEntitySign.GetUsers();
										if (!users.Contains(command.SteamId))
										{
											users.Add(command.SteamId);
											num++;
										}
									}
								}
								else
								{
									List<string> users2 = tileEntitySecureDoor.GetUsers();
									if (!users2.Contains(command.SteamId))
									{
										users2.Add(command.SteamId);
										num++;
									}
								}
							}
							else
							{
								List<string> users3 = tileEntitySecureLootContainer.GetUsers();
								if (!users3.Contains(command.SteamId))
								{
									users3.Add(command.SteamId);
									num++;
								}
							}
						}
						else
						{
							List<string> users4 = tileEntityVendingMachine.GetUsers();
							if (!users4.Contains(command.SteamId))
							{
								users4.Add(command.SteamId);
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Granted access to {num} secure blocks");
			return true;
		}

		private static bool RevokeAccess(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (NotIncluded(command, worldChunkPos, item))
						{
							continue;
						}
						TileEntity value = item.Value;
						TileEntityVendingMachine tileEntityVendingMachine = value as TileEntityVendingMachine;
						if (tileEntityVendingMachine == null)
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer == null)
							{
								TileEntitySecureDoor tileEntitySecureDoor = value as TileEntitySecureDoor;
								if (tileEntitySecureDoor == null)
								{
									TileEntitySign tileEntitySign = value as TileEntitySign;
									if (tileEntitySign != null)
									{
										List<string> users = tileEntitySign.GetUsers();
										if (users.Contains(command.SteamId))
										{
											users.Remove(command.SteamId);
											num++;
										}
									}
								}
								else
								{
									List<string> users2 = tileEntitySecureDoor.GetUsers();
									if (users2.Contains(command.SteamId))
									{
										users2.Remove(command.SteamId);
										num++;
									}
								}
							}
							else
							{
								List<string> users3 = tileEntitySecureLootContainer.GetUsers();
								if (users3.Contains(command.SteamId))
								{
									users3.Remove(command.SteamId);
									num++;
								}
							}
						}
						else
						{
							List<string> users4 = tileEntityVendingMachine.GetUsers();
							if (users4.Contains(command.SteamId))
							{
								users4.Remove(command.SteamId);
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Revoked access for {num} secure blocks.");
			return true;
		}

		private static bool SetLocked(BCMCmdProcessor command)
		{
			string value;
			return SetLock(command, command.Opts.ContainsKey("pwd"), command.Opts.TryGetValue("pwd", out value) ? value : string.Empty, locked: true);
		}

		private static bool SetUnLocked(BCMCmdProcessor command)
		{
			return SetLock(command, setPwd: false, "", locked: false);
		}

		private static bool EmptyContainers(BCMCmdProcessor command)
		{
			int num = 0;
			int num2 = 0;
			Dictionary<TileEntity, int> dictionary = TileLocks();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (dictionary.ContainsKey(item.Value))
						{
							num2++;
						}
						else if (!NotIncluded(command, worldChunkPos, item))
						{
							TileEntityLootContainer tileEntityLootContainer = item.Value as TileEntityLootContainer;
							if (tileEntityLootContainer != null)
							{
								tileEntityLootContainer.SetEmpty();
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Emptied {0} loot containers. {1}", num, (num2 > 0) ? $"{num2} loot container(s) were excluded because they were in use." : ""));
			return true;
		}

		private static bool ResetTouched(BCMCmdProcessor command)
		{
			int num = 0;
			int num2 = 0;
			Dictionary<TileEntity, int> dictionary = TileLocks();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (dictionary.ContainsKey(item.Value))
						{
							num2++;
						}
						else if (!NotIncluded(command, worldChunkPos, item))
						{
							TileEntityLootContainer tileEntityLootContainer = item.Value as TileEntityLootContainer;
							if (tileEntityLootContainer != null)
							{
								tileEntityLootContainer.SetEmpty();
								tileEntityLootContainer.bWasTouched = false;
								tileEntityLootContainer.bTouched = false;
								tileEntityLootContainer.SetModified();
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Reset {0} loot containers. {1}", num, (num2 > 0) ? $"{num2} loot container(s) were excluded because they were in use." : ""));
			return true;
		}

		private static bool RemoveTiles(BCMCmdProcessor command)
		{
			if (!command.Opts.ContainsKey("confirm"))
			{
				BCCommandAbstract.SendOutput("Please use /confirm to confirm that you understand the impact of this command (removes all targeted tile entities)");
				return false;
			}
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					Chunk chunkSync = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j);
					DictionaryList<Vector3i, TileEntity> dictionaryList = chunkSync?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					List<Vector3i> list = new List<Vector3i>();
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (!NotIncluded(command, worldChunkPos, item))
						{
							(item.Value as TileEntityLootContainer)?.SetEmpty();
							list.Add(item.Key);
						}
					}
					foreach (Vector3i item2 in list)
					{
						chunkSync.SetBlock((WorldBase)GameManager.Instance.World, item2.x, item2.y, item2.z, BlockValue.Air);
						num++;
					}
				}
			}
			BCCommandAbstract.SendOutput($"Removed {num} blocks");
			return true;
		}

		private static bool ScanTiles(BCMCmdProcessor command)
		{
			int num = 0;
			Dictionary<string, List<BCMTileEntity>> dictionary = new Dictionary<string, List<BCMTileEntity>>();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					Chunk chunkSync = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j);
					DictionaryList<Vector3i, TileEntity> dictionaryList = chunkSync?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i vector3i = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (NotIncluded(command, vector3i, item))
						{
							continue;
						}
						TileEntity value = item.Value;
						TileEntityForge tileEntityForge = value as TileEntityForge;
						if (tileEntityForge == null)
						{
							if (value is TileEntityLandClaim || value is TileEntityLight)
							{
								continue;
							}
							TileEntityGoreBlock tileEntityGoreBlock = value as TileEntityGoreBlock;
							if (tileEntityGoreBlock == null)
							{
								TileEntitySecureLootContainer tileEntitySecureLootContainer = value as TileEntitySecureLootContainer;
								if (tileEntitySecureLootContainer == null)
								{
									TileEntitySecureDoor tileEntitySecureDoor = value as TileEntitySecureDoor;
									if (tileEntitySecureDoor == null)
									{
										if (value is TileEntitySecure)
										{
											continue;
										}
										TileEntityLootContainer tileEntityLootContainer = value as TileEntityLootContainer;
										if (tileEntityLootContainer == null)
										{
											TileEntityPoweredRangedTrap tileEntityPoweredRangedTrap = value as TileEntityPoweredRangedTrap;
											if (tileEntityPoweredRangedTrap == null)
											{
												if (value is TileEntityPoweredBlock)
												{
													continue;
												}
												TileEntityPoweredTrigger tileEntityPoweredTrigger = value as TileEntityPoweredTrigger;
												if (tileEntityPoweredTrigger == null)
												{
													TileEntityPowerSource tileEntityPowerSource = value as TileEntityPowerSource;
													if (tileEntityPowerSource == null)
													{
														TileEntityPowered tileEntityPowered = value as TileEntityPowered;
														if (tileEntityPowered == null)
														{
															TileEntitySign tileEntitySign = value as TileEntitySign;
															if (tileEntitySign == null)
															{
																if (value is TileEntitySleeper)
																{
																	continue;
																}
																TileEntityVendingMachine tileEntityVendingMachine = value as TileEntityVendingMachine;
																if (tileEntityVendingMachine == null)
																{
																	TileEntityTrader tileEntityTrader = value as TileEntityTrader;
																	if (tileEntityTrader == null)
																	{
																		TileEntityWorkstation tileEntityWorkstation = value as TileEntityWorkstation;
																		if (tileEntityWorkstation != null)
																		{
																			if (!dictionary.ContainsKey(tileEntityWorkstation.GetTileEntityType().ToString()))
																			{
																				dictionary.Add(tileEntityWorkstation.GetTileEntityType().ToString(), new List<BCMTileEntity>());
																			}
																			dictionary[tileEntityWorkstation.GetTileEntityType().ToString()].Add(new BCMTileEntityWorkstation(item.Key + vector3i, tileEntityWorkstation));
																			num++;
																		}
																	}
																	else
																	{
																		if (!dictionary.ContainsKey(tileEntityTrader.GetTileEntityType().ToString()))
																		{
																			dictionary.Add(tileEntityTrader.GetTileEntityType().ToString(), new List<BCMTileEntity>());
																		}
																		dictionary[tileEntityTrader.GetTileEntityType().ToString()].Add(new BCMTileEntityTrader(item.Key + vector3i, tileEntityTrader));
																		num++;
																	}
																}
																else
																{
																	if (!dictionary.ContainsKey(tileEntityVendingMachine.GetTileEntityType().ToString()))
																	{
																		dictionary.Add(tileEntityVendingMachine.GetTileEntityType().ToString(), new List<BCMTileEntity>());
																	}
																	dictionary[tileEntityVendingMachine.GetTileEntityType().ToString()].Add(new BCMTileEntityVendingMachine(item.Key + vector3i, tileEntityVendingMachine));
																	num++;
																}
															}
															else
															{
																if (!dictionary.ContainsKey(tileEntitySign.GetTileEntityType().ToString()))
																{
																	dictionary.Add(tileEntitySign.GetTileEntityType().ToString(), new List<BCMTileEntity>());
																}
																dictionary[tileEntitySign.GetTileEntityType().ToString()].Add(new BCMTileEntitySign(item.Key + vector3i, tileEntitySign));
																num++;
															}
														}
														else
														{
															if (!dictionary.ContainsKey(tileEntityPowered.GetTileEntityType().ToString()))
															{
																dictionary.Add(tileEntityPowered.GetTileEntityType().ToString(), new List<BCMTileEntity>());
															}
															dictionary[tileEntityPowered.GetTileEntityType().ToString()].Add(new BCMTileEntityPowered(item.Key + vector3i, tileEntityPowered));
															num++;
														}
													}
													else
													{
														if (!dictionary.ContainsKey(tileEntityPowerSource.GetTileEntityType().ToString()))
														{
															dictionary.Add(tileEntityPowerSource.GetTileEntityType().ToString(), new List<BCMTileEntity>());
														}
														dictionary[tileEntityPowerSource.GetTileEntityType().ToString()].Add(new BCMTileEntityPowerSource(item.Key + vector3i, tileEntityPowerSource));
														num++;
													}
												}
												else
												{
													if (!dictionary.ContainsKey(tileEntityPoweredTrigger.GetTileEntityType().ToString()))
													{
														dictionary.Add(tileEntityPoweredTrigger.GetTileEntityType().ToString(), new List<BCMTileEntity>());
													}
													dictionary[tileEntityPoweredTrigger.GetTileEntityType().ToString()].Add(new BCMTileEntityPoweredTrigger(item.Key + vector3i, tileEntityPoweredTrigger));
													num++;
												}
											}
											else
											{
												if (!dictionary.ContainsKey(tileEntityPoweredRangedTrap.GetTileEntityType().ToString()))
												{
													dictionary.Add(tileEntityPoweredRangedTrap.GetTileEntityType().ToString(), new List<BCMTileEntity>());
												}
												dictionary[tileEntityPoweredRangedTrap.GetTileEntityType().ToString()].Add(new BCMTileEntityPoweredRangeTrap(item.Key + vector3i, tileEntityPoweredRangedTrap));
												num++;
											}
										}
										else
										{
											if (!dictionary.ContainsKey(tileEntityLootContainer.GetTileEntityType().ToString()))
											{
												dictionary.Add(tileEntityLootContainer.GetTileEntityType().ToString(), new List<BCMTileEntity>());
											}
											dictionary[tileEntityLootContainer.GetTileEntityType().ToString()].Add(new BCMTileEntityLootContainer(item.Key + vector3i, tileEntityLootContainer));
											num++;
										}
									}
									else
									{
										if (!dictionary.ContainsKey(tileEntitySecureDoor.GetTileEntityType().ToString()))
										{
											dictionary.Add(tileEntitySecureDoor.GetTileEntityType().ToString(), new List<BCMTileEntity>());
										}
										dictionary[tileEntitySecureDoor.GetTileEntityType().ToString()].Add(new BCMTileEntitySecureDoor(item.Key + vector3i, tileEntitySecureDoor, chunkSync));
										num++;
									}
								}
								else
								{
									if (!dictionary.ContainsKey(tileEntitySecureLootContainer.GetTileEntityType().ToString()))
									{
										dictionary.Add(tileEntitySecureLootContainer.GetTileEntityType().ToString(), new List<BCMTileEntity>());
									}
									dictionary[tileEntitySecureLootContainer.GetTileEntityType().ToString()].Add(new BCMTileEntitySecureLootContainer(item.Key + vector3i, tileEntitySecureLootContainer));
									num++;
								}
							}
							else
							{
								if (!dictionary.ContainsKey(tileEntityGoreBlock.GetTileEntityType().ToString()))
								{
									dictionary.Add(tileEntityGoreBlock.GetTileEntityType().ToString(), new List<BCMTileEntity>());
								}
								dictionary[tileEntityGoreBlock.GetTileEntityType().ToString()].Add(new BCMTileEntityGoreBlock(item.Key + vector3i, tileEntityGoreBlock));
								num++;
							}
						}
						else
						{
							if (!dictionary.ContainsKey(tileEntityForge.GetTileEntityType().ToString()))
							{
								dictionary.Add(tileEntityForge.GetTileEntityType().ToString(), new List<BCMTileEntity>());
							}
							dictionary[tileEntityForge.GetTileEntityType().ToString()].Add(new BCMTileEntityForge(item.Key + vector3i, tileEntityForge));
							num++;
						}
					}
				}
			}
			if (dictionary.Count > 0)
			{
				BCCommandAbstract.SendJson(new
				{
					Count = num,
					Tiles = dictionary
				});
			}
			else
			{
				BCCommandAbstract.SendOutput("No tile entities found in area");
			}
			return true;
		}

		private static bool AddLoot(BCMCmdProcessor command)
		{
			BCCommandAbstract.SendOutput("Work in progress");
			return false;
		}

		private static bool RemoveLoot(BCMCmdProcessor command)
		{
			BCCommandAbstract.SendOutput("Work in progress");
			return false;
		}

		private static bool SortItems(BCMCmdProcessor command)
		{
			int num = 0;
			int num2 = 0;
			Dictionary<TileEntity, int> dictionary = TileLocks();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (dictionary.ContainsKey(item.Value))
						{
							num2++;
						}
						else
						{
							if (NotIncluded(command, worldChunkPos, item))
							{
								continue;
							}
							TileEntityLootContainer tileEntityLootContainer = item.Value as TileEntityLootContainer;
							if (tileEntityLootContainer != null)
							{
								ItemStack[] array = CoalesceTEItems(tileEntityLootContainer);
								ItemStack[] array2 = StackSortUtil.SortStacks(array);
								for (int k = 0; k < array.Length; k++)
								{
									tileEntityLootContainer.UpdateSlot(k, array2[k]);
								}
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Sorted {0} loot containers. {1}", num, (num2 > 0) ? $"{num2} loot container(s) were excluded because they were in use." : ""));
			return true;
		}

		private static bool SetSignText(BCMCmdProcessor command)
		{
			if (!command.Opts.TryGetValue("text", out var value) && command.BPars.Count > 0)
			{
				value = command.BPars[0];
			}
			string text = "";
			if (value != null)
			{
				string oldValue = "_";
				if (command.Opts.ContainsKey("p"))
				{
					oldValue = command.Opts["p"].Substring(0, 1);
				}
				text = value.Replace(oldValue, " ");
			}
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (!NotIncluded(command, worldChunkPos, item))
						{
							TileEntitySign tileEntitySign = item.Value as TileEntitySign;
							if (tileEntitySign != null)
							{
								tileEntitySign.SetText(text);
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Set text '{text}' on {num} signs");
			return true;
		}

		private static bool SetPaint(BCMCmdProcessor command)
		{
			int result = 0;
			if (command.Opts.ContainsKey("t"))
			{
				if (!int.TryParse(command.Opts["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return false;
				}
			}
			else if (command.BPars.Count > 0 && !int.TryParse(command.BPars[0], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse texture value");
				return false;
			}
			if (result == 0)
			{
				BCCommandAbstract.SendOutput("Sub-command paint requires a texture id other than the remove texture, use paintstrip to remove paint.");
				return false;
			}
			if (BlockTextureData.list[result] == null)
			{
				BCCommandAbstract.SendOutput($"Unknown texture index {result}");
				return false;
			}
			long num = 0L;
			for (int i = 0; i < 6; i++)
			{
				int num2 = i * 8;
				num &= ~(255L << num2);
				num |= (long)(result & 0xFF) << num2;
			}
			long textureFull = num;
			int num3 = 0;
			for (int j = command.Position.x; j <= command.MaxPos.x; j++)
			{
				for (int k = command.Position.y; k <= command.MaxPos.y; k++)
				{
					for (int l = command.Position.z; l <= command.MaxPos.z; l++)
					{
						Vector3i vector3i = new Vector3i(j, k, l);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.Block.shape.IsTerrain() && !block.Equals(BlockValue.Air))
						{
							GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, textureFull);
							num3++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Painting {num3} blocks with texture '{BlockTextureData.list[result]?.LocalizedName ?? BlockTextureData.list[result]?.Name}'");
			return true;
		}

		private static bool SetPaintFace(BCMCmdProcessor command)
		{
			int result = 0;
			if (command.Opts.ContainsKey("t"))
			{
				if (!int.TryParse(command.Opts["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return false;
				}
			}
			else if (command.BPars.Count > 0 && !int.TryParse(command.BPars[0], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse texture value");
				return false;
			}
			if (result == 0)
			{
				BCCommandAbstract.SendOutput("Sub-command paintface requires a texture id other than the remove texture, use paintstrip to remove paint.");
				return false;
			}
			if (BlockTextureData.list[result] == null)
			{
				BCCommandAbstract.SendOutput($"Unknown texture index {result}");
				return false;
			}
			uint result2 = 6u;
			if (command.Opts.ContainsKey("face") && !uint.TryParse(command.Opts["face"], out result2))
			{
				BCCommandAbstract.SendOutput("Unable to parse face value");
				return false;
			}
			if (result2 > 5)
			{
				BCCommandAbstract.SendOutput("Face must be between 0 and 5. Use /face=# to provide a face to paint.");
				return false;
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						if (!GameManager.Instance.World.GetBlock(0, vector3i).Equals(BlockValue.Air))
						{
							GameManager.Instance.World.ChunkClusters[0].SetBlockFaceTexture(vector3i, (BlockFace)result2, result);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Painting {num} blocks on face '{((BlockFace)result2).ToString()}' with texture '{BlockTextureData.list[result]?.LocalizedName ?? BlockTextureData.list[result]?.Name}'");
			return true;
		}

		private static bool RemovePaint(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.Block.shape.IsTerrain() && !block.Equals(BlockValue.Air))
						{
							uint result = 6u;
							if (command.Opts.ContainsKey("face") && !uint.TryParse(command.Opts["face"], out result))
							{
								BCCommandAbstract.SendOutput("Unable to parse face value");
								return false;
							}
							if (result > 5)
							{
								GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, 0L);
							}
							else
							{
								GameManager.Instance.World.ChunkClusters[0].SetBlockFaceTexture(vector3i, (BlockFace)result, 0);
							}
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Paint removed from {num} blocks.");
			return true;
		}

		private static bool OpenDoors(BCMCmdProcessor command)
		{
			EntityAlive player = null;
			if (command.EntityId.HasValue && GameManager.Instance.World.Players.dict.ContainsKey(command.EntityId.Value))
			{
				player = GameManager.Instance.World.Players.dict[command.EntityId.Value];
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.ischild && !block.Equals(BlockValue.Air))
						{
							BlockDoor blockDoor = block.Block as BlockDoor;
							if (blockDoor != null)
							{
								block.meta = (byte)(block.meta & 0xFFFFFFFEu);
								blockDoor.OnBlockActivated(GameManager.Instance.World, 0, vector3i, block, player);
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Opening {num} doors.");
			return true;
		}

		private static bool CloseDoors(BCMCmdProcessor command)
		{
			EntityAlive player = null;
			if (command.EntityId.HasValue && GameManager.Instance.World.Players.dict.ContainsKey(command.EntityId.Value))
			{
				player = GameManager.Instance.World.Players.dict[command.EntityId.Value];
			}
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.ischild && !block.Equals(BlockValue.Air))
						{
							BlockDoor blockDoor = block.Block as BlockDoor;
							if (blockDoor != null)
							{
								block.meta |= 1;
								blockDoor.OnBlockActivated(GameManager.Instance.World, 0, vector3i, block, player);
								num++;
							}
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Closing {num} doors.");
			return true;
		}

		private static bool TurnLightsOn(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.ischild && !block.Equals(BlockValue.Air) && block.Block is BlockLight)
						{
							SetLightState(lightOn: true, vector3i, block);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Switching {num} lights on.");
			return true;
		}

		private static bool TurnLightsOff(BCMCmdProcessor command)
		{
			int num = 0;
			for (int i = command.Position.x; i <= command.MaxPos.x; i++)
			{
				for (int j = command.Position.y; j <= command.MaxPos.y; j++)
				{
					for (int k = command.Position.z; k <= command.MaxPos.z; k++)
					{
						Vector3i vector3i = new Vector3i(i, j, k);
						BlockValue block = GameManager.Instance.World.GetBlock(0, vector3i);
						if (!block.ischild && !block.Equals(BlockValue.Air) && block.Block is BlockLight)
						{
							SetLightState(lightOn: false, vector3i, block);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Switching {num} lights off.");
			return true;
		}

		private static bool SetMeta1(BCMCmdProcessor command)
		{
			SetMeta(1, command);
			return true;
		}

		private static bool SetMeta2(BCMCmdProcessor command)
		{
			SetMeta(2, command);
			return true;
		}

		private static bool ScanLooters(BCMCmdProcessor command)
		{
			int num = 0;
			List<string> list = new List<string>();
			Dictionary<TileEntity, int> dictionary = TileLocks();
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (!NotIncluded(command, worldChunkPos, item))
						{
							num++;
							if (dictionary.ContainsKey(item.Value))
							{
								list.Add($"Position: {new BCMVector3(item.Key)}, Type: {item.Value.GetTileEntityType()}, EntityId: {dictionary[item.Value]}");
							}
						}
					}
				}
			}
			if (list.Count > 0)
			{
				BCCommandAbstract.SendJson(new
				{
					TotalContainers = num,
					AccessedContainers = list.Count,
					AccessedInfo = list
				});
			}
			else
			{
				BCCommandAbstract.SendOutput((num == 0) ? "No containers found in area." : $"None of the {num} containers in the area are currently being accessed.");
			}
			return true;
		}

		private static IEnumerable<int> GetBlockIds(string blockParam)
		{
			if (!Block.list.Any((Block b) => b?.GetBlockName().Equals(blockParam, StringComparison.InvariantCultureIgnoreCase) ?? false))
			{
				return from b in Block.list
					where b != null && b.GetBlockName().IndexOf(blockParam, StringComparison.InvariantCultureIgnoreCase) >= 0
					select b.blockID;
			}
			return new int[1]
			{
				Block.GetBlockByName(blockParam, _caseInsensitive: true).blockID
			};
		}

		private static bool SetLock(BCMCmdProcessor command, bool setPwd, string pwd, bool locked)
		{
			int num = 0;
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					DictionaryList<Vector3i, TileEntity> dictionaryList = GameManager.Instance.World.ChunkCache.GetChunkSync(i, j)?.GetTileEntities();
					if (dictionaryList == null)
					{
						continue;
					}
					Vector3i worldChunkPos = new Vector3i(i << 4, 0, j << 4);
					foreach (KeyValuePair<Vector3i, TileEntity> item in dictionaryList.dict)
					{
						if (NotIncluded(command, worldChunkPos, item))
						{
							continue;
						}
						TileEntity value = item.Value;
						TileEntityVendingMachine tileEntityVendingMachine = value as TileEntityVendingMachine;
						bool changed;
						if (tileEntityVendingMachine == null)
						{
							TileEntitySecureLootContainer tileEntitySecureLootContainer = value as TileEntitySecureLootContainer;
							if (tileEntitySecureLootContainer == null)
							{
								TileEntitySecureDoor tileEntitySecureDoor = value as TileEntitySecureDoor;
								if (tileEntitySecureDoor == null)
								{
									TileEntitySign tileEntitySign = value as TileEntitySign;
									if (tileEntitySign != null && (tileEntitySign.IsLocked() != locked || setPwd))
									{
										tileEntitySign.SetLocked(locked);
										if (locked && setPwd)
										{
											tileEntitySign.CheckPassword(pwd, tileEntitySign.GetOwner(), out changed);
										}
										num++;
									}
								}
								else if (tileEntitySecureDoor.IsLocked() != locked || setPwd)
								{
									tileEntitySecureDoor.SetLocked(locked);
									if (locked && setPwd)
									{
										tileEntitySecureDoor.CheckPassword(pwd, tileEntitySecureDoor.GetOwner(), out changed);
									}
									num++;
								}
							}
							else if (tileEntitySecureLootContainer.IsLocked() != locked || setPwd)
							{
								tileEntitySecureLootContainer.SetLocked(locked);
								if (locked && setPwd)
								{
									tileEntitySecureLootContainer.CheckPassword(pwd, tileEntitySecureLootContainer.GetOwner(), out changed);
								}
								num++;
							}
						}
						else if (tileEntityVendingMachine.IsLocked() != locked || setPwd)
						{
							tileEntityVendingMachine.SetLocked(locked);
							if (locked && setPwd)
							{
								tileEntityVendingMachine.CheckPassword(pwd, tileEntityVendingMachine.GetOwner(), out changed);
							}
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput(string.Format("Set {0} on {1} secure blocks{2}", locked ? "locked" : "unlocked", num, setPwd ? " with new password" : ""));
			return true;
		}

		private static void SetMeta(int metaIdx, BCMCmdProcessor command)
		{
			byte result = 0;
			if (command.Opts.ContainsKey("meta"))
			{
				if (!byte.TryParse(command.Opts["meta"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse meta '" + command.Opts["meta"] + "'");
					return;
				}
			}
			else if (command.BPars.Count > 0 && !byte.TryParse(command.BPars[0], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse meta '" + command.Opts["meta"] + "'");
				return;
			}
			int num = 0;
			for (int i = 0; i < command.Size.y; i++)
			{
				for (int j = 0; j < command.Size.x; j++)
				{
					for (int k = 0; k < command.Size.z; k++)
					{
						Vector3i pos = new Vector3i(j + command.Position.x, i + command.Position.y, k + command.Position.z);
						BlockValue block = GameManager.Instance.World.GetBlock(0, pos);
						if (!block.Equals(BlockValue.Air) && !block.ischild)
						{
							switch (metaIdx)
							{
							case 1:
								block.meta = result;
								break;
							case 2:
								block.meta2 = result;
								break;
							default:
								BCCommandAbstract.SendOutput($"Meta value {metaIdx} not supported");
								return;
							}
							GameManager.Instance.World.ChunkClusters[0].SetBlock(pos, block, bNotify: false, updateLight: false);
							num++;
						}
					}
				}
			}
			BCCommandAbstract.SendOutput($"Setting meta{metaIdx} on '{num}' blocks");
		}

		private static bool NotIncluded(BCMCmdProcessor command, Vector3i worldChunkPos, KeyValuePair<Vector3i, TileEntity> posAndTE)
		{
			if (!command.IsBlockArea || command.IsWithinBounds(posAndTE.Key + worldChunkPos))
			{
				if (command.Filter != null)
				{
					if (!command.Filter.Equals("all", StringComparison.OrdinalIgnoreCase))
					{
						return !command.Filter.Equals(posAndTE.Value.GetTileEntityType().ToString(), StringComparison.OrdinalIgnoreCase);
					}
					return false;
				}
				return false;
			}
			return true;
		}

		private static void SetBlocks(BCMCmdProcessor command, BlockValue bvNew, bool searchAll)
		{
			sbyte result = 1;
			if (command.Opts.ContainsKey("d") && sbyte.TryParse(command.Opts["d"], out result))
			{
				BCCommandAbstract.SendOutput($"Using density {result}");
			}
			long textureFull = 0L;
			if (command.Opts.ContainsKey("t"))
			{
				if (!byte.TryParse(command.Opts["t"], out var result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture index");
					return;
				}
				if (BlockTextureData.list[result2] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result2}");
					return;
				}
				long num = 0L;
				for (int i = 0; i < 6; i++)
				{
					int num2 = i * 8;
					num &= ~(255L << num2);
					num |= (long)(result2 & 0xFF) << num2;
				}
				textureFull = num;
			}
			for (int j = command.Position.y; j <= command.MaxPos.y; j++)
			{
				for (int k = command.Position.x; k <= command.MaxPos.x; k++)
				{
					for (int l = command.Position.z; l <= command.MaxPos.z; l++)
					{
						Vector3i vector3i = new Vector3i(k, j, l);
						BlockValue block = GameManager.Instance.World.GetBlock(vector3i);
						if (command.Opts.ContainsKey("delmulti") && (!searchAll || bvNew.type != block.type))
						{
							continue;
						}
						if (block.Block.isMultiBlock && block.ischild)
						{
							Vector3i parentPos = block.Block.multiBlockPos.GetParentPos(vector3i, block);
							BlockValue block2 = GameManager.Instance.World.ChunkClusters[0].GetBlock(parentPos);
							if (block2.ischild || block2.type != block.type)
							{
								continue;
							}
							GameManager.Instance.World.ChunkClusters[0].SetBlock(parentPos, BlockValue.Air, bNotify: false, updateLight: false);
						}
						if (command.Opts.ContainsKey("delmulti"))
						{
							continue;
						}
						if (block.Block.IndexName == "lpblock")
						{
							GameManager.Instance.persistentPlayers.RemoveLandProtectionBlock(new Vector3i(vector3i.x, vector3i.y, vector3i.z));
						}
						Chunk chunk = GameManager.Instance.World.GetChunkFromWorldPos(vector3i.x, vector3i.y, vector3i.z) as Chunk;
						if (bvNew.Equals(BlockValue.Air))
						{
							result = MarchingCubes.DensityAir;
							if (GameManager.Instance.World.GetTerrainHeight(vector3i.x, vector3i.z) > vector3i.y)
							{
								chunk?.SetTerrainHeight(vector3i.x & 0xF, vector3i.z & 0xF, (byte)vector3i.y);
							}
						}
						else if (bvNew.Block.shape.IsTerrain())
						{
							result = MarchingCubes.DensityTerrain;
							if (GameManager.Instance.World.GetTerrainHeight(vector3i.x, vector3i.z) < vector3i.y)
							{
								chunk?.SetTerrainHeight(vector3i.x & 0xF, vector3i.z & 0xF, (byte)vector3i.y);
							}
						}
						else
						{
							GameManager.Instance.World.ChunkClusters[0].SetTextureFull(vector3i, textureFull);
						}
						GameManager.Instance.World.ChunkClusters[0].SetBlock(vector3i, _bChangeBlockValue: true, bvNew, _bChangeDensity: true, result, bNotify: false, updateLight: false);
					}
				}
			}
		}

		private static void SetStats(string name, BlockValue bv, IDictionary<string, int> stats)
		{
			if (stats.ContainsKey($"{bv.type:D4}:{name}"))
			{
				stats[$"{bv.type:D4}:{name}"]++;
			}
			else
			{
				stats.Add($"{bv.type:D4}:{name}", 1);
			}
		}

		private static ItemStack[] CoalesceTEItems(TileEntityLootContainer te)
		{
			ItemStack[] items = te.items;
			for (int i = 0; i < items.Length - 1; i++)
			{
				if (items[i].IsEmpty())
				{
					for (int j = i + 1; j < items.Length; j++)
					{
						if (!items[j].IsEmpty())
						{
							items[i] = items[j];
							items[j] = ItemStack.Empty.Clone();
							break;
						}
					}
				}
				if (!items[i].IsEmpty())
				{
					ItemClass itemClass = items[i].itemValue.ItemClass;
					int num = itemClass.Stacknumber.Value - items[i].count;
					if (itemClass.HasQuality || num == 0)
					{
						continue;
					}
					for (int k = i + 1; k < items.Length; k++)
					{
						if (items[i].itemValue.type == items[k].itemValue.type)
						{
							int num2 = Utils.FastMin(items[k].count, num);
							items[i].count += num2;
							items[k].count -= num2;
							num -= num2;
							if (items[k].count == 0)
							{
								items[k] = ItemStack.Empty.Clone();
							}
						}
						if (num == 0)
						{
							break;
						}
					}
				}
				te.UpdateSlot(i, items[i]);
			}
			return items;
		}

		private static void SetLightState(bool lightOn, Vector3i worldPos, BlockValue worldBlock)
		{
			BlockEntityData blockEntity = GameManager.Instance.World.ChunkCache.GetChunkSync(World.toChunkXZ(worldPos.x), World.toChunkXZ(worldPos.z)).GetBlockEntity(worldPos);
			if (blockEntity == null || !blockEntity.bHasTransform)
			{
				return;
			}
			worldBlock.meta = (byte)((worldBlock.meta & 0xFFFFFFFDu) | (lightOn ? 2u : 0u));
			GameManager.Instance.World.SetBlockRPC(0, worldPos, worldBlock);
			TileEntityLight tileEntityLight = (TileEntityLight)GameManager.Instance.World.GetTileEntity(0, worldPos);
			Transform transform = blockEntity.transform.Find("MainLight");
			if (transform != null)
			{
				LightLOD component = transform.GetComponent<LightLOD>();
				if (component != null)
				{
					component.SwitchOnOff(lightOn);
					if (tileEntityLight != null)
					{
						Light light = component.GetLight();
						light.type = tileEntityLight.LightType;
						component.MaxIntensity = tileEntityLight.LightIntensity;
						light.color = tileEntityLight.LightColor;
						light.shadows = tileEntityLight.LightShadows;
						light.spotAngle = tileEntityLight.LightAngle;
						component.LightStateType = tileEntityLight.LightState;
						component.StateRate = tileEntityLight.Rate;
						component.FluxDelay = tileEntityLight.Delay;
						component.SetRange(tileEntityLight.LightRange);
						component.EmissiveColor = tileEntityLight.LightColor;
						int num = 0;
						while (component.RefIlluminatedMaterials != null && num < component.RefIlluminatedMaterials.Length)
						{
							if (component.RefIlluminatedMaterials[num] != null)
							{
								Renderer component2 = component.RefIlluminatedMaterials[num].GetComponent<Renderer>();
								if (component2 != null)
								{
									Material material = component2.material;
									if (material != null)
									{
										material.SetColor("_EmissionColor", tileEntityLight.LightColor);
									}
								}
							}
							num++;
						}
					}
					else
					{
						GameObject gameObject = DataLoader.LoadAsset<GameObject>(worldBlock.Block.Properties.Values["Model"]);
						if (gameObject != null)
						{
							Transform transform2 = gameObject.transform.Find("MainLight");
							if (transform2 != null)
							{
								LightLOD component3 = transform2.GetComponent<LightLOD>();
								Light light2 = component3.GetLight();
								Light light3 = component.GetLight();
								if (light3 != null && light2 != null)
								{
									light3.type = light2.type;
									component.MaxIntensity = light2.intensity;
									light3.color = light2.color;
									light3.shadows = light2.shadows;
									light3.spotAngle = light2.spotAngle;
									component.LightStateType = component3.LightStateType;
									component.StateRate = component3.StateRate;
									component.FluxDelay = component3.FluxDelay;
									component.SetRange(light2.range);
									component.EmissiveColor = component3.EmissiveColor;
									int num2 = 0;
									while (component.RefIlluminatedMaterials != null && num2 < component.RefIlluminatedMaterials.Length)
									{
										if (component.RefIlluminatedMaterials[num2] != null)
										{
											Renderer component4 = component.RefIlluminatedMaterials[num2].GetComponent<Renderer>();
											if (component4 != null)
											{
												Material material2 = component4.material;
												if (material2 != null)
												{
													material2.SetColor("_EmissionColor", light2.color);
												}
											}
										}
										num2++;
									}
								}
							}
						}
					}
				}
			}
			transform = blockEntity.transform.Find("SeparatedLensFlare");
			if (transform != null)
			{
				LightLOD component5 = transform.GetComponent<LightLOD>();
				if (component5 != null)
				{
					component5.SwitchOnOff(lightOn);
				}
			}
			transform = blockEntity.transform.Find("BulbGlow");
			if (transform != null)
			{
				MeshRenderer component6 = transform.GetComponent<MeshRenderer>();
				if (component6 != null)
				{
					component6.enabled = lightOn;
				}
			}
			transform = blockEntity.transform.Find("ExtraPointLight");
			if (transform != null)
			{
				LightLOD component7 = transform.GetComponent<LightLOD>();
				if (component7 != null)
				{
					component7.SwitchOnOff(lightOn);
				}
			}
			transform = blockEntity.transform.Find("Point light");
			if (transform != null)
			{
				LightLOD component8 = transform.GetComponent<LightLOD>();
				if (component8 != null)
				{
					component8.SwitchOnOff(lightOn);
				}
			}
		}

		private static Dictionary<TileEntity, int> TileLocks()
		{
			return LockedTileEntities.GetValue(GameManager.Instance) as Dictionary<TileEntity, int>;
		}
	}
}
