using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;
using UMA;
using UnityEngine;

namespace BCM.Commands
{
	public class BCAssets : BCCommandAbstract
	{
		public BCAssets()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCAssets"];
			DefaultCommands = new string[1]
			{
				"bc-assets"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCAssets.ParseHelp(this);
		}

		protected override void Process()
		{
			List<object> list = new List<object>();
			switch (BCCommandAbstract.Params.Count)
			{
			case 0:
				BCCommandAbstract.SendJson(new string[7]
				{
					"overlays",
					"slots",
					"particles",
					"textures",
					"meshes",
					"itemicons",
					"resources"
				});
				break;
			case 1:
			case 2:
				switch (BCCommandAbstract.Params[0])
				{
				case "overlays":
				{
					OverlayDataAsset[] allOverlayAssets = OverlayLibrary.Instance.GetAllOverlayAssets();
					list.AddRange(allOverlayAssets.Select((OverlayDataAsset oda) => oda.name));
					break;
				}
				case "slots":
				{
					SlotDataAsset[] allSlotAssets = SlotLibrary.Instance.GetAllSlotAssets();
					list.AddRange(allSlotAssets.Select((SlotDataAsset sda) => sda.slotName));
					break;
				}
				case "particles":
				{
					Dictionary<string, Transform> dictionary = LoadParticleEffects();
					list.AddRange(dictionary.Keys.ToArray());
					break;
				}
				case "textures":
				{
					IEnumerable<BCMTexture> textures = GetTextures();
					list.AddRange(textures);
					break;
				}
				case "meshes":
					if (BCCommandAbstract.Options.ContainsKey("full"))
					{
						IEnumerable<BCMMesh> meshes = GetMeshes();
						list.AddRange(meshes);
					}
					else
					{
						IEnumerable<BCMMeshShort> meshesShort = GetMeshesShort();
						list.AddRange(meshesShort);
					}
					break;
				case "itemicons":
				{
					int bakedCount;
					List<string> sprites = GetSprites(out bakedCount);
					if (sprites == null)
					{
						return;
					}
					list.Add(new
					{
						Count = sprites.Count,
						BakedCount = bakedCount,
						Icons = sprites
					});
					break;
				}
				case "resources":
				{
					int count;
					Dictionary<string, List<string>> resources = GetResources(out count);
					list.Add(new
					{
						Count = count.ToString(),
						Resources = resources
					});
					break;
				}
				}
				BCCommandAbstract.SendJson(list);
				break;
			default:
				BCCommandAbstract.SendOutput("Incorrect params");
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static List<string> GetSprites(out int bakedCount)
		{
			bakedCount = 0;
			GameObject gameObject = GameObject.Find("/NGUI Root (2D)/ItemIconAtlas");
			if (gameObject == null)
			{
				BCCommandAbstract.SendOutput("Atlas object not found");
				return null;
			}
			DynamicUIAtlas component = gameObject.GetComponent<DynamicUIAtlas>();
			if (component == null)
			{
				BCCommandAbstract.SendOutput("Atlas component not found");
				return null;
			}
			if (!DynamicUIAtlasTools.ReadPrebakedAtlasDescriptor(component.PrebakedAtlas, out var _sprites, out var _elementWidth, out var _elementHeight, out var _))
			{
				BCCommandAbstract.SendOutput("Could not read dynamic atlas descriptor");
				return null;
			}
			List<string> list = _sprites.Select((UISpriteData s) => s.name).ToList();
			bakedCount = list.Count;
			foreach (Mod loadedMod in ModManager.GetLoadedMods())
			{
				string path = loadedMod.Path + "/ItemIcons";
				if (!Directory.Exists(path))
				{
					continue;
				}
				string[] files = Directory.GetFiles(path);
				foreach (string text in files)
				{
					if (!text.ToLower().EndsWith(".png"))
					{
						continue;
					}
					string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(text);
					if (!list.Contains(fileNameWithoutExtension))
					{
						Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
						if (texture2D.width == _elementWidth && texture2D.height == _elementHeight)
						{
							list.Add(fileNameWithoutExtension);
						}
						Object.Destroy(texture2D);
					}
				}
			}
			return list;
		}

		private static Dictionary<string, List<string>> GetResources(out int count)
		{
			Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
			Object[] array = UnityEngine.Resources.LoadAll((BCCommandAbstract.Params.Count == 2) ? BCCommandAbstract.Params[1] : "");
			count = array.Length;
			Object[] array2 = array;
			foreach (Object @object in array2)
			{
				if (dictionary.ContainsKey(@object.GetType().ToString()))
				{
					dictionary[@object.GetType().ToString()].Add(@object.name);
					continue;
				}
				dictionary.Add(@object.GetType().ToString(), new List<string>
				{
					@object.name
				});
			}
			UnityEngine.Resources.UnloadUnusedAssets();
			return dictionary;
		}

		private static IEnumerable<BCMMeshShort> GetMeshesShort()
		{
			List<BCMMeshShort> list = new List<BCMMeshShort>();
			MeshDescription[] meshes = MeshDescription.meshes;
			foreach (MeshDescription meshDescription in meshes)
			{
				if (meshDescription == null)
				{
					continue;
				}
				XmlDocument xmlDocument = new XmlDocument();
				List<BCMMeshDataShort> list2 = new List<BCMMeshDataShort>();
				if (meshDescription.MetaData != null && !string.IsNullOrEmpty(meshDescription.MetaData.text))
				{
					xmlDocument.LoadXml(meshDescription.MetaData.text);
					XmlNodeList xmlNodeList = xmlDocument.DocumentElement?.ChildNodes;
					if (xmlNodeList != null)
					{
						foreach (XmlElement item in xmlNodeList)
						{
							list2.Add(new BCMMeshDataShort(item));
						}
					}
				}
				list.Add(new BCMMeshShort(meshDescription, list2));
			}
			return list;
		}

		private static IEnumerable<BCMMesh> GetMeshes()
		{
			List<BCMMesh> list = new List<BCMMesh>();
			MeshDescription[] meshes = MeshDescription.meshes;
			foreach (MeshDescription meshDescription in meshes)
			{
				if (meshDescription == null)
				{
					continue;
				}
				XmlDocument xmlDocument = new XmlDocument();
				List<BCMMeshData> list2 = new List<BCMMeshData>();
				if (meshDescription.MetaData != null && !string.IsNullOrEmpty(meshDescription.MetaData.text))
				{
					xmlDocument.LoadXml(meshDescription.MetaData.text);
					XmlNodeList xmlNodeList = xmlDocument.DocumentElement?.ChildNodes;
					if (xmlNodeList != null)
					{
						foreach (XmlElement item2 in xmlNodeList)
						{
							list2.Add(new BCMMeshData(item2));
						}
					}
				}
				BCMMesh item = new BCMMesh(meshDescription, list2);
				list.Add(item);
			}
			return list;
		}

		private static IEnumerable<BCMTexture> GetTextures()
		{
			List<BCMTexture> list = new List<BCMTexture>();
			BlockTextureData[] list2 = BlockTextureData.list;
			foreach (BlockTextureData blockTextureData in list2)
			{
				if (blockTextureData != null)
				{
					list.Add(new BCMTexture(blockTextureData));
				}
			}
			return list;
		}

		private static Dictionary<string, Transform> LoadParticleEffects()
		{
			Dictionary<string, Transform> dictionary = new Dictionary<string, Transform>();
			Object[] array = UnityEngine.Resources.LoadAll("ParticleEffects", typeof(Transform));
			foreach (Object @object in array)
			{
				string name = ((Transform)@object).gameObject.name;
				if (name.StartsWith("p_"))
				{
					name = name.Substring("p_".Length);
					if (dictionary.ContainsKey(name))
					{
						Log.Error("Particle Effect " + name + " already exists! Skipping it!");
					}
					else
					{
						dictionary.Add(name, (Transform)@object);
					}
				}
			}
			return dictionary;
		}
	}
}
