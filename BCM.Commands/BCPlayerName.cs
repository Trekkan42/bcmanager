using System;
using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCPlayerName : BCCommandAbstract
	{
		[Serializable]
		public class PlayerNameGroups
		{
			public readonly string Name;

			public string Prefix;

			public string Color;

			public readonly List<string> Members = new List<string>();

			public PlayerNameGroups(string name, string prefix, string color)
			{
				Name = name;
				Prefix = prefix;
				Color = color;
			}
		}

		public static Dictionary<string, PlayerNameGroups> Groups = new Dictionary<string, PlayerNameGroups>();

		public static List<string> MemberLookup = new List<string>();

		public BCPlayerName()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayerName"];
			DefaultCommands = new string[2]
			{
				"bc-playername",
				"bc-pn"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
			Groups = (Dictionary<string, PlayerNameGroups>)configValue.GetItem("Groups", new Dictionary<string, PlayerNameGroups>());
			MemberLookup = configValue.GetItem("MemberLookup", new List<string>());
		}

		public override string GetHelp()
		{
			return Resources.BCPlayerName.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Params.Count < 1)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "list":
				if (BCCommandAbstract.Params.Count < 2)
				{
					BCCommandAbstract.SendJson(Groups.Keys.ToList());
					return;
				}
				switch (BCCommandAbstract.Params[1])
				{
				case "groups":
					BCCommandAbstract.SendJson(Groups.Values.ToList());
					break;
				case "overrides":
					BCCommandAbstract.SendJson((from p in BCMPersist.Instance.ServerPlayersConfig.All()
						where p.HasCustomChatName
						select p.SteamId + ":" + p.Name + ":" + p.ChatName).ToList());
					break;
				default:
					BCCommandAbstract.SendOutput("Unknown option " + BCCommandAbstract.Params[1]);
					break;
				}
				return;
			case "override":
			{
				string _id2;
				ClientInfo _cInfo3;
				if (BCCommandAbstract.Params.Count < 2)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
				}
				else if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[1], out _id2, out _cInfo3))
				{
					BCMServerPlayerConfig bCMServerPlayerConfig = BCMPersist.Instance.ServerPlayersConfig[_id2, true];
					if (BCCommandAbstract.Params.Count > 2 && !BCCommandAbstract.Params[2].Equals(string.Empty))
					{
						bCMServerPlayerConfig.ChatName = BCCommandAbstract.Params[2];
						BCCommandAbstract.SendOutput("Chat name for player " + bCMServerPlayerConfig.Name + " changed to " + bCMServerPlayerConfig.ChatName);
					}
					else
					{
						bCMServerPlayerConfig.ChatName = null;
						BCCommandAbstract.SendOutput("Chat name for player " + bCMServerPlayerConfig.Name + " reset to default");
					}
					BCMPersist.Instance.Save(BCMSaveType.ServerPlayersConfig);
				}
				return;
			}
			case "create":
			case "creategroup":
			{
				if (BCCommandAbstract.Params.Count < 2)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Group with name " + BCCommandAbstract.Params[1] + " already exists");
					return;
				}
				string color2 = "";
				if (BCCommandAbstract.Options.ContainsKey("color") && !BCUtils.ParseColor(BCCommandAbstract.Options["color"], out color2))
				{
					BCCommandAbstract.SendOutput("Unable to prase color " + BCCommandAbstract.Options["color"]);
					return;
				}
				PlayerNameGroups playerNameGroups3 = new PlayerNameGroups(BCCommandAbstract.Params[1], (BCCommandAbstract.Params.Count > 2) ? BCCommandAbstract.Params[2] : BCCommandAbstract.Params[1], color2);
				Groups.Add(BCCommandAbstract.Params[1], playerNameGroups3);
				BCCommandAbstract.SendOutput("Chat prefix group " + playerNameGroups3.Name + " created. Prefix = " + playerNameGroups3.Prefix + ", Color = " + playerNameGroups3.Color);
				break;
			}
			case "delete":
			case "deletegroup":
				if (BCCommandAbstract.Params.Count < 2)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (!Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Unable to find group " + BCCommandAbstract.Params[1]);
					return;
				}
				Groups.Remove(BCCommandAbstract.Params[1]);
				BCCommandAbstract.SendOutput("Group " + BCCommandAbstract.Params[1] + " deleted");
				break;
			case "setprefix":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (!Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Group " + BCCommandAbstract.Params[1] + " not found.");
					return;
				}
				if (!Groups.TryGetValue(BCCommandAbstract.Params[1], out var value2))
				{
					BCCommandAbstract.SendOutput("Unable to find group " + BCCommandAbstract.Params[1]);
					return;
				}
				value2.Prefix = BCCommandAbstract.Params[2];
				BCCommandAbstract.SendOutput("Prefix set to " + BCCommandAbstract.Params[2] + " for group " + BCCommandAbstract.Params[1]);
				break;
			}
			case "setcolor":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (!Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Group " + BCCommandAbstract.Params[1] + " not found.");
					return;
				}
				if (!Groups.TryGetValue(BCCommandAbstract.Params[1], out var value))
				{
					BCCommandAbstract.SendOutput("Unable to find group " + BCCommandAbstract.Params[1]);
					return;
				}
				string color = "";
				if (BCCommandAbstract.Params[2] != "clear" && !BCUtils.ParseColor(BCCommandAbstract.Params[2], out color))
				{
					BCCommandAbstract.SendOutput("Unable to parse color " + BCCommandAbstract.Params[2]);
					return;
				}
				value.Color = color;
				BCCommandAbstract.SendOutput(string.IsNullOrEmpty(color) ? "Clearing custom color for group" : ("Color set to " + color + " for group " + BCCommandAbstract.Params[1]));
				break;
			}
			case "add":
			case "addplayer":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (!Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Group " + BCCommandAbstract.Params[1] + " not found, add it before trying to add players");
					return;
				}
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[2], out var steamId, out var _cInfo2))
				{
					return;
				}
				bool flag = MemberLookup.Contains(steamId);
				if (flag && !BCCommandAbstract.Options.ContainsKey("force"))
				{
					BCCommandAbstract.SendOutput("Player already in a group, use /force to move them to the new group");
					return;
				}
				if (flag)
				{
					MemberLookup.Remove(steamId);
					(from g in Groups.Values
						where g.Members.Contains(steamId)
						select g.Members).FirstOrDefault()?.Remove(steamId);
				}
				MemberLookup.Add(steamId);
				PlayerNameGroups playerNameGroups2 = Groups[BCCommandAbstract.Params[1]];
				playerNameGroups2.Members.Add(steamId);
				BCCommandAbstract.SendOutput("Player '" + ((_cInfo2 != null) ? _cInfo2.playerName : steamId) + "' added to group " + playerNameGroups2.Name);
				break;
			}
			case "remove":
			case "removeplayer":
			{
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("Wrong number of arguments");
					BCCommandAbstract.SendOutput(GetHelp());
					return;
				}
				if (!Groups.ContainsKey(BCCommandAbstract.Params[1]))
				{
					BCCommandAbstract.SendOutput("Group " + BCCommandAbstract.Params[1] + " not found");
					return;
				}
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[2], out var _id, out var _cInfo))
				{
					return;
				}
				PlayerNameGroups playerNameGroups = Groups[BCCommandAbstract.Params[1]];
				if (!playerNameGroups.Members.Contains(_id))
				{
					BCCommandAbstract.SendOutput("Player not found in group " + playerNameGroups.Name);
					return;
				}
				MemberLookup.Remove(_id);
				playerNameGroups.Members.Remove(_id);
				BCCommandAbstract.SendOutput("Player " + ((_cInfo != null) ? _cInfo.playerName : _id) + " removed from group " + playerNameGroups.Name);
				break;
			}
			default:
				BCCommandAbstract.SendOutput("Unknown sub-command " + BCCommandAbstract.Params[0]);
				return;
			}
			BCMPersist.Instance.ServerConfig["BCPlayerName"].SetItem("Groups", Groups);
			BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
		}
	}
}
