using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCTeleport : BCCommandAbstract
	{
		public BCTeleport()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCTeleport"];
			DefaultCommands = new string[3]
			{
				"bc-teleport",
				"bc-move",
				"move"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCTeleport.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string text = BCCommandAbstract.Params[0];
			if (text != null && text == "entity")
			{
				TeleportEntity(world, BCCommandAbstract.Params[1]);
			}
			else
			{
				TeleportEntity(world, BCCommandAbstract.Params[0]);
			}
		}

		private static bool GetPos(World world, out Vector3 position)
		{
			position = new Vector3(0f, 0f, 0f);
			switch (BCCommandAbstract.Params.Count)
			{
			case 5:
			{
				if (!int.TryParse(BCCommandAbstract.Params[2], out var result10) || !int.TryParse(BCCommandAbstract.Params[3], out var result11) || !int.TryParse(BCCommandAbstract.Params[4], out var result12))
				{
					BCCommandAbstract.SendOutput("Unable to parse x y z for numbers");
					return false;
				}
				position = new Vector3(result10, result11, result12);
				return true;
			}
			case 4:
			{
				if (!int.TryParse(BCCommandAbstract.Params[1], out var result7) || !int.TryParse(BCCommandAbstract.Params[2], out var result8) || !int.TryParse(BCCommandAbstract.Params[3], out var result9))
				{
					BCCommandAbstract.SendOutput("Unable to parse x y z for numbers");
					return false;
				}
				position = new Vector3(result7, result8, result9);
				return true;
			}
			default:
				if (BCCommandAbstract.Options.ContainsKey("player"))
				{
					ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Options["player"], out var _, out var _cInfo);
					if (_cInfo == null)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from remote client");
						return false;
					}
					Vector3? vector = world.Players.dict[_cInfo.entityId]?.position;
					if (!vector.HasValue)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from client entity");
						return false;
					}
					position = vector.Value;
					return true;
				}
				if (BCCommandAbstract.Options.ContainsKey("p"))
				{
					string[] array = BCCommandAbstract.Options["p"].Split(',');
					if (array.Length != 3)
					{
						BCCommandAbstract.SendOutput("Unable to get position from " + BCCommandAbstract.Options["p"] + ", incorrect number of co-ords: /p=x,y,z");
						return false;
					}
					if (!int.TryParse(array[0], out var result) || !int.TryParse(array[1], out var result2) || !int.TryParse(array[2], out var result3))
					{
						BCCommandAbstract.SendOutput("Unable to get x y z for " + BCCommandAbstract.Options["p"]);
						return false;
					}
					position = new Vector3(result, result2, result3);
					return true;
				}
				if (BCCommandAbstract.Options.ContainsKey("position"))
				{
					string[] array2 = BCCommandAbstract.Options["position"].Split(',');
					if (array2.Length != 3)
					{
						BCCommandAbstract.SendOutput("Unable to get position from '" + BCCommandAbstract.Options["position"] + "', incorrect number of co-ords: /position=x,y,z");
						return false;
					}
					if (!int.TryParse(array2[0], out var result4) || !int.TryParse(array2[1], out var result5) || !int.TryParse(array2[2], out var result6))
					{
						BCCommandAbstract.SendOutput("Unable to get x y z from '" + BCCommandAbstract.Options["position"] + "'");
						return false;
					}
					position = new Vector3(result4, result5, result6);
					return true;
				}
				if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
				{
					ClientInfo remoteClientInfo = BCCommandAbstract.SenderInfo.RemoteClientInfo;
					if (remoteClientInfo == null)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from remote client");
						return false;
					}
					Vector3? vector2 = world.Players.dict[remoteClientInfo.entityId]?.position;
					if (!vector2.HasValue)
					{
						BCCommandAbstract.SendOutput("Unable to get player position from client entity");
						return false;
					}
					position = vector2.Value;
					return true;
				}
				return false;
			}
		}

		private static bool GetEntity(World world, string eid, out Entity entity)
		{
			entity = null;
			if (!int.TryParse(eid, out var result))
			{
				return false;
			}
			entity = (world.Entities.dict.ContainsKey(result) ? world.Entities.dict[result] : null);
			return true;
		}

		private static bool GetClientInfo(string eid, out ClientInfo ci)
		{
			return (ci = ConsoleHelper.ParseParamIdOrName(eid)) != null;
		}

		private static void TeleportEntity(World world, string param)
		{
			if (!GetPos(world, out var position))
			{
				return;
			}
			Entity entity;
			if (GetClientInfo(param, out var ci))
			{
				ci.SendPackage(NetPackageManager.GetPackage<NetPackageTeleportPlayer>().Setup(position));
				BCCommandAbstract.SendOutput($"Teleporting {ci.playerName} to {position.x} {position.y} {position.z}");
			}
			else if (GetEntity(world, param, out entity))
			{
				if (entity == null)
				{
					BCCommandAbstract.SendOutput("Entity not found'");
					return;
				}
				entity.SetPosition(position);
				object[] array = new object[4];
				EntityAlive entityAlive = entity as EntityAlive;
				array[0] = (((object)entityAlive != null) ? entityAlive.EntityName : entity.entityType.ToString());
				array[1] = position.x;
				array[2] = position.y;
				array[3] = position.z;
				BCCommandAbstract.SendOutput(string.Format("Teleporting Entity: {0} to {1} {2} {3}", array));
			}
		}
	}
}
