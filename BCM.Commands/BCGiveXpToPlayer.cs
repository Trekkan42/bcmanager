using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGiveXpToPlayer : BCCommandAbstract
	{
		public BCGiveXpToPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGiveXpToPlayer"];
			DefaultCommands = new string[1]
			{
				"bc-givexp"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGiveXpToPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			string _id;
			ClientInfo _cInfo;
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out _id, out _cInfo))
			{
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup("giveselfxp " + BCCommandAbstract.Params[1], _bExecute: true));
				BCCommandAbstract.SendOutput(BCCommandAbstract.Params[1] + " experience given to player " + _cInfo.playerName);
			}
		}
	}
}
