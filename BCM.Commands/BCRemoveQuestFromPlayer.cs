using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCRemoveQuestFromPlayer : BCCommandAbstract
	{
		public BCRemoveQuestFromPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCRemoveQuestFromPlayer"];
			DefaultCommands = new string[1]
			{
				"bc-removequest"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCRemoveQuestFromPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string _id;
			ClientInfo _cInfo;
			int num = ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out _id, out _cInfo);
			if (num == 1)
			{
				if (_cInfo != null)
				{
					if (QuestClass.s_Quests.ContainsKey(BCCommandAbstract.Params[1].ToLower()))
					{
						_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup("removequest " + BCCommandAbstract.Params[1], _bExecute: true));
						BCCommandAbstract.SendOutput("Quest " + BCCommandAbstract.Params[1] + " removed from player " + _cInfo.playerName);
					}
					else
					{
						BCCommandAbstract.SendOutput("Unable to find quest " + BCCommandAbstract.Params[1]);
					}
				}
			}
			else if (num > 1)
			{
				BCCommandAbstract.SendOutput($"{num} matches found, please refine your search text.");
			}
			else
			{
				BCCommandAbstract.SendOutput("Unable to find player.");
			}
		}
	}
}
