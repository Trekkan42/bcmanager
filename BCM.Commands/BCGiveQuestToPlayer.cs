using System.Collections.Generic;
using System.Collections.ObjectModel;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGiveQuestToPlayer : BCCommandAbstract
	{
		public BCGiveQuestToPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGiveQuestToPlayer"];
			DefaultCommands = new string[1]
			{
				"bc-givequest"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGiveQuestToPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			List<ClientInfo> list = new List<ClientInfo>();
			int count = BCCommandAbstract.Params.Count;
			string text;
			if (count != 1)
			{
				if (count != 2)
				{
					goto IL_00e7;
				}
				text = BCCommandAbstract.Params[1];
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _, out var _cInfo))
				{
					return;
				}
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				list.Add(_cInfo);
			}
			else
			{
				if (!BCCommandAbstract.Options.ContainsKey("all"))
				{
					goto IL_00e7;
				}
				text = BCCommandAbstract.Params[0];
				if (SingletonMonoBehaviour<ConnectionManager>.Instance.ClientCount() == 0)
				{
					return;
				}
				ReadOnlyCollection<ClientInfo> list2 = SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.List;
				if (list2 == null)
				{
					return;
				}
				foreach (ClientInfo item in list2)
				{
					if (item != null && item.loginDone)
					{
						list.Add(item);
					}
				}
			}
			if (QuestClass.s_Quests.ContainsKey(text))
			{
				if (list.Count == 0)
				{
					BCCommandAbstract.SendOutput("No clients found to give quest to");
					return;
				}
				foreach (ClientInfo item2 in list)
				{
					item2.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup("givequest " + text, _bExecute: true));
					BCCommandAbstract.SendOutput("Quest " + text + " given to player " + item2.playerName);
				}
			}
			else
			{
				BCCommandAbstract.SendOutput("Unable to find quest " + text);
			}
			return;
			IL_00e7:
			BCCommandAbstract.SendOutput("Wrong number of arguments");
			BCCommandAbstract.SendOutput(GetHelp());
		}
	}
}
