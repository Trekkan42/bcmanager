using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCPlayersGamestage : BCPlayers
	{
		public BCPlayersGamestage()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayersGamestage"];
			DefaultCommands = new string[1]
			{
				"bc-gs"
			};
			DefaultOptions = new Dictionary<string, string>
			{
				{
					"n",
					null
				}
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPlayersGamestage.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Options.ContainsKey("filter"))
			{
				BCCommandAbstract.SendOutput("Error: Can't set filters on this alias command");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string text = "gamestage";
			if (BCCommandAbstract.Options.ContainsKey("n"))
			{
				text += ",name";
			}
			if (BCCommandAbstract.Options.ContainsKey("e"))
			{
				text += ",entityid";
			}
			if (BCCommandAbstract.Options.ContainsKey("s"))
			{
				text += ",steamid";
			}
			BCCommandAbstract.Options.Add("filter", text);
			new BCPlayers().Process(BCCommandAbstract.Options, BCCommandAbstract.Params);
		}
	}
}
