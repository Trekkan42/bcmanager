using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCTime : BCCommandAbstract
	{
		public BCTime()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCTime"];
			DefaultCommands = new string[1]
			{
				"bc-time"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCTime.ParseHelp(this);
		}

		protected override void Process()
		{
			BCMTime bCMTime = new BCMTime(BCCommandAbstract.Options);
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				BCCommandAbstract.SendJson(bCMTime.Time);
			}
			else if (BCCommandAbstract.Options.ContainsKey("s"))
			{
				BCCommandAbstract.SendJson(new int[3]
				{
					bCMTime.Time["D"],
					bCMTime.Time["H"],
					bCMTime.Time["M"]
				});
			}
			else
			{
				BCCommandAbstract.SendJson(bCMTime);
			}
		}
	}
}
