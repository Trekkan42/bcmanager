using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCPlayersPosition : BCPlayers
	{
		public BCPlayersPosition()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayersPosition"];
			DefaultCommands = new string[2]
			{
				"bc-pos",
				"bc-lppos"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPlayersPosition.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCCommandAbstract.Options.ContainsKey("filter"))
			{
				BCCommandAbstract.SendOutput("Error: Can't set filters on this alias command");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string text = "position";
			if (BCCommandAbstract.Options.ContainsKey("n"))
			{
				text += ",name";
			}
			if (BCCommandAbstract.Options.ContainsKey("e"))
			{
				text += ",entityid";
			}
			if (BCCommandAbstract.Options.ContainsKey("r"))
			{
				text += ",rotation";
			}
			if (BCCommandAbstract.Options.ContainsKey("u"))
			{
				text += ",underground";
			}
			if (BCCommandAbstract.Options.ContainsKey("g"))
			{
				text += ",onground";
			}
			BCCommandAbstract.Options.Add("filter", text);
			new BCPlayers().Process(BCCommandAbstract.Options, BCCommandAbstract.Params);
		}
	}
}
