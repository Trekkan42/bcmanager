using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCSetSkillOnPlayer : BCCommandAbstract
	{
		public BCSetSkillOnPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCSetSkillOnPlayer"];
			DefaultCommands = new string[2]
			{
				"bc-setskill",
				"setskill"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCSetSkillOnPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 3)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else
			{
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _, out var _cInfo))
				{
					return;
				}
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				EntityPlayer entityPlayer = world.Entities.dict[_cInfo.entityId] as EntityPlayer;
				if (entityPlayer == null)
				{
					BCCommandAbstract.SendOutput("Unable to find player entity");
					return;
				}
				ProgressionValue progressionValue = entityPlayer.Progression.GetProgressionValue(BCCommandAbstract.Params[1]);
				if (progressionValue == null)
				{
					BCCommandAbstract.SendOutput("Skill " + BCCommandAbstract.Params[1] + " not found");
					return;
				}
				if (!int.TryParse(BCCommandAbstract.Params[2], out var result))
				{
					BCCommandAbstract.SendOutput("Level must be a number");
					return;
				}
				ProgressionClass progressionClass = progressionValue.ProgressionClass;
				if (result > progressionClass.MaxLevel)
				{
					result = progressionClass.MaxLevel;
				}
				if (result < progressionClass.MinLevel)
				{
					result = progressionClass.MinLevel;
				}
				_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelClient>().Setup(_cInfo.entityId, progressionValue.Name, result));
				BCCommandAbstract.SendOutput(string.Format("Setting {0} '{1}' to level {2} for player {3}", progressionClass.IsAttribute ? "attribute" : (progressionClass.IsPerk ? "perk" : "skill"), progressionClass.Name, result, _cInfo.playerName));
			}
		}
	}
}
