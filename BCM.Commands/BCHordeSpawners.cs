using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCHordeSpawners : BCCommandAbstract
	{
		public BCHordeSpawners()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCHordeSpawners"];
			DefaultCommands = new string[1]
			{
				"bc-manager"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCHordeSpawners.ParseHelp(this);
		}

		protected override void Process()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			lock (EntitySpawner.SpawnQueue)
			{
				dictionary.Add("QueueCount", EntitySpawner.SpawnQueue.Count);
				dictionary.Add("Queue", EntitySpawner.SpawnQueue);
			}
			lock (EntitySpawner.HordeSpawners)
			{
				dictionary.Add("HordeSpawners", EntitySpawner.HordeSpawners);
			}
			BCCommandAbstract.SendJson(dictionary);
		}
	}
}
