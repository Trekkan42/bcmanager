using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCResetSkills : BCCommandAbstract
	{
		public BCResetSkills()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCResetSkills"];
			DefaultCommands = new string[1]
			{
				"bc-resetskills"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCResetSkills.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 1)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else
			{
				if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out var _, out var _cInfo))
				{
					return;
				}
				if (_cInfo == null)
				{
					BCCommandAbstract.SendOutput("Unable to locate player.");
					return;
				}
				EntityPlayer entityPlayer = world.Entities.dict[_cInfo.entityId] as EntityPlayer;
				if (entityPlayer == null)
				{
					BCCommandAbstract.SendOutput("Unable to find player entity");
					return;
				}
				foreach (ProgressionValue value in entityPlayer.Progression.ProgressionValues.Values)
				{
					ProgressionClass progressionClass = value.ProgressionClass;
					_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageEntitySetSkillLevelClient>().Setup(_cInfo.entityId, value.Name, progressionClass.MinLevel));
				}
				BCCommandAbstract.SendOutput("Reset progression for player " + _cInfo.playerName);
			}
		}
	}
}
