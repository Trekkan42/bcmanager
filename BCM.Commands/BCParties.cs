using System.Collections.Generic;
using System.Linq;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCParties : BCCommandAbstract
	{
		public static class SubCommand
		{
			public const string List = "list";

			public const string Add = "add";

			public const string Remove = "remove";

			public const string Leader = "leader";

			public const string AutoParty = "auto";
		}

		public BCParties()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCParties"];
			DefaultCommands = new string[1]
			{
				"bc-party"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCParties.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				Dictionary<string, List<string>> parties = GetParties(world);
				if (parties.Count == 0)
				{
					BCCommandAbstract.SendOutput("No parties found.");
					return;
				}
				foreach (KeyValuePair<string, List<string>> item in parties)
				{
					BCCommandAbstract.SendOutput("Leader: " + item.Key);
					BCCommandAbstract.SendOutput("Members: " + string.Join(", ", item.Value.ToArray()));
				}
				return;
			}
			string _id;
			switch (BCCommandAbstract.Params[0])
			{
			case "list":
				BCCommandAbstract.SendJson(GetParties(world));
				break;
			case "add":
				if (BCCommandAbstract.Params.Count < 3)
				{
					BCCommandAbstract.SendOutput("add requires a host and target player");
				}
				else
				{
					if (1 != ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[1], out _id, out var _cInfo2))
					{
						break;
					}
					EntityPlayer entityPlayer2 = world.Entities.dict[_cInfo2.entityId] as EntityPlayer;
					ClientInfo _cInfo3;
					if (entityPlayer2 == null)
					{
						BCCommandAbstract.SendOutput("Unable to find host player.");
					}
					else if (entityPlayer2.Party == null)
					{
						BCCommandAbstract.SendOutput(_cInfo2.playerName + " is not in a party.");
					}
					else if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[2], out _id, out _cInfo3))
					{
						EntityPlayer entityPlayer3 = world.Entities.dict[_cInfo3.entityId] as EntityPlayer;
						if (entityPlayer3 == null)
						{
							BCCommandAbstract.SendOutput("Unable to find target player.");
							break;
						}
						entityPlayer2.Party.AddPlayer(entityPlayer3);
						BCCommandAbstract.SendOutput("Added player " + _cInfo3.playerName + " to party of " + _cInfo2.playerName);
					}
				}
				break;
			case "remove":
			{
				if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[2], out _id, out var _cInfo4))
				{
					EntityPlayer entityPlayer4 = world.Entities.dict[_cInfo4.entityId] as EntityPlayer;
					if (entityPlayer4 == null)
					{
						BCCommandAbstract.SendOutput("Unable to find target player.");
						break;
					}
					if (entityPlayer4.Party == null)
					{
						BCCommandAbstract.SendOutput(_cInfo4.playerName + " is not in a party.");
						break;
					}
					entityPlayer4.Party.RemovePlayer(entityPlayer4);
					BCCommandAbstract.SendOutput("Removed player " + _cInfo4.playerName + " from party.");
				}
				break;
			}
			case "leader":
			{
				if (1 == ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[2], out _id, out var _cInfo))
				{
					EntityPlayer entityPlayer = world.Entities.dict[_cInfo.entityId] as EntityPlayer;
					if (entityPlayer == null)
					{
						BCCommandAbstract.SendOutput("Unable to find target player.");
						break;
					}
					if (entityPlayer.Party == null)
					{
						BCCommandAbstract.SendOutput(_cInfo.playerName + " is not in a party.");
						break;
					}
					Party.ServerHandleChangeLead(entityPlayer);
					BCCommandAbstract.SendOutput("Player " + _cInfo.playerName + " set as leader.");
				}
				break;
			}
			case "auto":
				BCCommandAbstract.SendOutput("Not Implemented.");
				break;
			default:
				BCCommandAbstract.SendOutput("Unknown sub-command " + BCCommandAbstract.Params[0] + ".");
				break;
			}
		}

		private static Dictionary<string, List<string>> GetParties(World world)
		{
			Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
			foreach (EntityPlayer item in world.Players.dict.Values.Where((EntityPlayer ep) => ep.IsPartyLead()))
			{
				if (!dictionary.ContainsKey(item.EntityName))
				{
					dictionary.Add(item.EntityName, item.Party.MemberList.Select((EntityPlayer ep) => ep.EntityName).ToList());
				}
			}
			return dictionary;
		}
	}
}
