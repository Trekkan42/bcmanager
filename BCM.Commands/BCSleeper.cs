using System;
using System.Collections.Generic;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCSleeper : BCCommandAbstract
	{
		private const string _volumesFunction = "HB";

		public BCSleeper()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCSleeper"];
			DefaultCommands = new string[1]
			{
				"bc-sleeper"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCSleeper.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count == 0)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "chunk":
			{
				if (GetChunkKey(world, out var chunkKey2))
				{
					ListVolumes(world, chunkKey2);
				}
				break;
			}
			case "clear":
			{
				if (GetChunkKey(world, out var chunkKey))
				{
					ClearChunkVolume(world, chunkKey);
				}
				break;
			}
			case "list":
				GetWorldVolumes(world);
				break;
			case "volume":
				if (BCCommandAbstract.Params.Count != 2)
				{
					BCCommandAbstract.SendOutput(GetHelp());
				}
				else
				{
					GetWorldVolume(world);
				}
				break;
			default:
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static void GetWorldVolumes(World world)
		{
			FieldInfo field = typeof(World).GetField("HB", BindingFlags.Instance | BindingFlags.NonPublic);
			if (field != null)
			{
				List<SleeperVolume> list = field.GetValue(world) as List<SleeperVolume>;
				if (list == null)
				{
					return;
				}
				int num = 0;
				List<BCMSleeperVolume> list2 = new List<BCMSleeperVolume>();
				foreach (SleeperVolume item in list)
				{
					list2.Add(new BCMSleeperVolume(num, item, world));
					num++;
				}
				BCCommandAbstract.SendJson(new
				{
					Total = list2.Count,
					Volumes = list2
				});
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't get sleeper volumes from world");
			}
		}

		private static void GetWorldVolume(World world)
		{
			FieldInfo field = typeof(World).GetField("HB", BindingFlags.Instance | BindingFlags.NonPublic);
			if (field != null)
			{
				List<SleeperVolume> list = field.GetValue(world) as List<SleeperVolume>;
				if (list != null)
				{
					if (int.TryParse(BCCommandAbstract.Params[1], out var result))
					{
						BCCommandAbstract.SendJson(new BCMSleeperVolumeDetialed(result, list[result], world));
					}
					else
					{
						BCCommandAbstract.SendOutput("Unable to find given index");
					}
				}
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't get sleeper volumes from world");
			}
		}

		private static void ClearChunkVolume(World world, long chunkKey)
		{
			Chunk chunk = world.GetChunkSync(chunkKey) as Chunk;
			if (chunk == null)
			{
				BCCommandAbstract.SendOutput("Unable to retrieve chunk");
				return;
			}
			chunk.GetSleeperVolumes().Clear();
			BCCommandAbstract.SendOutput($"Sleeper volumes cleared from {chunk.X},{chunk.Z}");
		}

		private static void ListVolumes(World world, long chunkKey)
		{
			Chunk chunk = world.GetChunkSync(chunkKey) as Chunk;
			if (chunk == null)
			{
				BCCommandAbstract.SendOutput("Unable to retrieve chunk");
			}
			else
			{
				BCCommandAbstract.SendJson(new
				{
					msg = $"Sleeper volume array indexes for chunk {chunk.X},{chunk.Z}",
					data = chunk.GetSleeperVolumes()
				});
			}
		}

		private static bool GetChunkKey(World world, out long chunkKey)
		{
			chunkKey = long.MinValue;
			if (BCCommandAbstract.Params.Count == 3)
			{
				if (!int.TryParse(BCCommandAbstract.Params[1], out var result) || !int.TryParse(BCCommandAbstract.Params[2], out var result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse x z for numbers");
					return false;
				}
				chunkKey = WorldChunkCache.MakeChunkKey(result, result2);
				return true;
			}
			if (BCCommandAbstract.SenderInfo.RemoteClientInfo == null)
			{
				BCCommandAbstract.SendOutput("Unable to get location from player position");
				return false;
			}
			int entityId = BCCommandAbstract.SenderInfo.RemoteClientInfo.entityId;
			if (!world.Players.dict.ContainsKey(entityId))
			{
				BCCommandAbstract.SendOutput("Unable to get location from player position");
				return false;
			}
			EntityPlayer entityPlayer = world.Players.dict[entityId];
			chunkKey = WorldChunkCache.MakeChunkKey((int)Math.Floor(entityPlayer.position.x) >> 4, (int)Math.Floor(entityPlayer.position.z) >> 4);
			return true;
		}
	}
}
