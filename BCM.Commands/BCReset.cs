using System;
using System.Collections.Generic;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCReset : BCCommandAbstract
	{
		private const string _decorateFunction = "tryToDecorate";

		private static MethodInfo TryToDecorate;

		public BCReset()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCReset"];
			DefaultCommands = new string[1]
			{
				"bc-reset"
			};
			DefaultOptions = new Dictionary<string, string>
			{
				{
					"r",
					"0"
				}
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
			TryToDecorate = typeof(ChunkProviderGenerateWorld).GetMethod("tryToDecorate", BindingFlags.Instance | BindingFlags.NonPublic);
			if (TryToDecorate == null)
			{
				throw new Exception("BCReset: Couldn't access method for tryToDecorate in ChunkProviderGenerateWorld");
			}
		}

		public override string GetHelp()
		{
			return Resources.BCReset.ParseHelp(this);
		}

		protected override void Process()
		{
			if (BCUtils.CheckWorld())
			{
				BCMCmdProcessor command = new BCMCmdProcessor(BCCommandAbstract.Params, BCCommandAbstract.Options, "BCReset");
				if (BCMCmdProcessor.ProcessParams(command, hasSub: false))
				{
					BCMCmdProcessor.ProcessTask(command, this);
				}
			}
		}

		public override void ProcessCmd(BCMCmdProcessor command, out ReloadMode reload)
		{
			reload = ReloadMode.All;
			ResetChunks(command);
			BCCommandAbstract.SendOutput("Chunks reset");
		}

		private static void ResetChunks(BCMCmdProcessor command)
		{
			ChunkCluster chunkCache = GameManager.Instance.World.ChunkCache;
			ChunkProviderGenerateWorld chunkProviderGenerateWorld = chunkCache.ChunkProvider as ChunkProviderGenerateWorld;
			if (chunkProviderGenerateWorld == null)
			{
				BCCommandAbstract.SendOutput("Unable to load chunk provider");
				return;
			}
			TerrainGeneratorWithBiomeResource terrainGeneratorWithBiomeResource = chunkProviderGenerateWorld.GetTerrainGenerator() as TerrainGeneratorWithBiomeResource;
			if (terrainGeneratorWithBiomeResource == null)
			{
				BCCommandAbstract.SendOutput("Couldn't load terrain generator");
				return;
			}
			for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
			{
				for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
				{
					Chunk chunk = MemoryPools.PoolChunks.AllocSync(_bReset: true);
					if (chunk == null)
					{
						BCCommandAbstract.SendOutput("Couldn't allocate chunk from memory pool");
						return;
					}
					chunk.X = i;
					chunk.Z = j;
					GameRandom random = Utils.RandomFromSeedOnPos(chunk.X, chunk.Z, GameManager.Instance.World.Seed);
					terrainGeneratorWithBiomeResource.GenerateTerrain(GameManager.Instance.World, chunk, random);
					chunk.NeedsDecoration = true;
					chunk.NeedsLightCalculation = true;
					DoPrefabDecorations(GameManager.Instance.World, chunkProviderGenerateWorld, chunk);
					DoEntityDecoration(GameManager.Instance.World, chunkProviderGenerateWorld, chunk);
					DoSpawnerDecorations(GameManager.Instance.World, chunkProviderGenerateWorld, chunk);
					lock (chunkCache.GetSyncRoot())
					{
						if (chunkCache.ContainsChunkSync(chunk.Key))
						{
							chunkCache.RemoveChunkSync(chunk.Key);
						}
						if (chunkCache.ContainsChunkSync(chunk.Key))
						{
							BCCommandAbstract.SendOutput("Reset chunk still exists in chunk cache");
							return;
						}
						if (!chunkCache.AddChunkSync(chunk))
						{
							MemoryPools.PoolChunks.FreeSync(chunk);
							BCCommandAbstract.SendOutput("Unable to add new chunk to cache");
							return;
						}
					}
					TryToDecorate.Invoke(chunkProviderGenerateWorld, new object[1]
					{
						chunk
					});
					chunk.InProgressRegeneration = false;
					chunk.NeedsCopying = true;
					chunk.isModified = true;
				}
			}
		}

		private static void DoSpawnerDecorations(World world, IChunkProvider chunkProvider, Chunk chunk)
		{
			if (chunkProvider.GetDynamicEntitySpawnerDecorator() != null)
			{
				chunkProvider.GetDynamicEntitySpawnerDecorator().DecorateChunk(world, chunk);
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't load entity spawner decorator");
			}
		}

		private static void DoEntityDecoration(World world, IChunkProvider chunkProvider, Chunk chunk)
		{
			if (chunkProvider.GetDynamicEntityDecorator() != null)
			{
				chunkProvider.GetDynamicEntityDecorator().DecorateChunk(world, chunk);
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't load entity decorator");
			}
		}

		private static void DoPrefabDecorations(World world, IChunkProvider chunkProvider, Chunk chunk)
		{
			if (chunkProvider.GetDynamicPrefabDecorator() != null)
			{
				chunkProvider.GetDynamicPrefabDecorator().DecorateChunk(world, chunk);
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't load prefab decorator");
			}
		}
	}
}
