using System.Collections.Generic;
using System.Threading;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGetSpawn : BCCommandAbstract
	{
		public BCGetSpawn()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGetSpawn"];
			DefaultCommands = new string[1]
			{
				"bc-getspawn"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGetSpawn.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[0], out var result))
			{
				BCCommandAbstract.SendOutput("x was not a number");
				return;
			}
			if (!int.TryParse(BCCommandAbstract.Params[1], out var result2))
			{
				BCCommandAbstract.SendOutput("z was not a number");
				return;
			}
			int x = World.toChunkXZ(result);
			int y = World.toChunkXZ(result2);
			int i = 0;
			Chunk chunk = null;
			world.ChunkCache.ChunkProvider.RequestChunk(x, y);
			long key = WorldChunkCache.MakeChunkKey(x, y);
			for (; i < 100; i++)
			{
				chunk = world.GetChunkSync(key) as Chunk;
				if (chunk != null)
				{
					break;
				}
				Thread.Sleep(10);
			}
			if (chunk == null)
			{
				BCCommandAbstract.SendOutput("Unable to get chunk");
				return;
			}
			List<string> list = new List<string>();
			if (BCCommandAbstract.Options.ContainsKey("ch"))
			{
				for (int j = 0; j < 16; j++)
				{
					for (int k = 0; k < 16; k++)
					{
						list.Add(chunk.GetHeight(j, k).ToString());
					}
				}
				BCCommandAbstract.SendOutput("ChunkHeights:" + string.Join(",", list.ToArray()));
				return;
			}
			int x2 = result & 0xF;
			int z = result2 & 0xF;
			int y2;
			if (BCCommandAbstract.Options.ContainsKey("ph"))
			{
				BCCommandAbstract.SendOutput("PointHeight:" + chunk.GetHeight(x2, z));
			}
			else if (chunk.FindSpawnPointAtXZ(x2, z, out y2, 15, 0, 3, 251, _bIgnoreCanMobsSpawnOn: true))
			{
				BCCommandAbstract.SendOutput("SpawnPoint:" + result + " " + y2 + " " + result2);
			}
			else
			{
				BCCommandAbstract.SendOutput("Couldn't find valid spawn point at " + result + " " + result2);
			}
		}
	}
}
