using System;
using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;
using UnityEngine;

namespace BCM.Commands
{
	public class BCWorldBlocksRPC : BCCommandAbstract
	{
		public BCWorldBlocksRPC()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCWorldBlocksRPC"];
			DefaultCommands = new string[2]
			{
				"bc-blockrpc",
				"blockrpc"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return BCM.Properties.Resources.BCWorldBlocksRPC.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world) || !ProcessParams(out var position, out var blockname))
			{
				return;
			}
			int result;
			BlockValue targetbv = (int.TryParse(blockname, out result) ? Block.GetBlockValue(result) : Block.GetBlockValue(blockname));
			if (targetbv.type == 0)
			{
				switch (blockname)
				{
				default:
					BCCommandAbstract.SendOutput("Unable to find block by name or id '" + blockname + "'");
					return;
				case "*":
				case "air":
				case "0":
					break;
				}
			}
			if (world.GetChunkSync(World.toChunkXZ(position.x), World.toChunkXZ(position.z)) == null)
			{
				BCCommandAbstract.SendOutput("Chunk not loaded at given location");
				return;
			}
			switch (BCCommandAbstract.Params[0])
			{
			case "scan":
				ScanBlock(world, position);
				break;
			case "fill":
				FillBlock(world, position, targetbv);
				break;
			case "repair":
				RepairBlock(world, position);
				break;
			case "damage":
				DamageBlock(world, position);
				break;
			case "upgrade":
				UpgradeBlock(world, position);
				break;
			case "downgrade":
				DowngradeBlock(world, position);
				break;
			case "paint":
				SetPaint(world, position);
				break;
			case "paintface":
				SetPaintFace(world, position);
				break;
			case "strippaint":
			case "paintstrip":
				RemovePaint(world, position);
				break;
			case "density":
				SetDensity(world, position);
				break;
			case "rotate":
				SetRotation(world, position);
				break;
			case "meta1":
				SetMeta(1, world, position);
				break;
			case "meta2":
				SetMeta(2, world, position);
				break;
			case "meta3":
				SetMeta(3, world, position);
				break;
			default:
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private bool ProcessParams(out Vector3i position, out string blockname1)
		{
			position = default(Vector3i);
			blockname1 = null;
			if (BCCommandAbstract.Params.Count == 4 || BCCommandAbstract.Params.Count == 5)
			{
				if (!int.TryParse(BCCommandAbstract.Params[1], out position.x) || !int.TryParse(BCCommandAbstract.Params[2], out position.y) || !int.TryParse(BCCommandAbstract.Params[3], out position.z))
				{
					BCCommandAbstract.SendOutput("Unable to parse coordinates");
					return false;
				}
				blockname1 = ((BCCommandAbstract.Params.Count == 5) ? BCCommandAbstract.Params[4] : "*");
				return true;
			}
			BCCommandAbstract.SendOutput(GetHelp());
			return false;
		}

		private static void SetBlock(World world, Vector3i pos, BlockValue targetbv)
		{
			sbyte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("d") && sbyte.TryParse(BCCommandAbstract.Options["d"], out result))
			{
				BCCommandAbstract.SendOutput($"Using density {result}");
			}
			byte result2 = 0;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!byte.TryParse(BCCommandAbstract.Options["t"], out result2))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture index");
					return;
				}
				if (BlockTextureData.list[result2] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result2}");
					return;
				}
				BCCommandAbstract.SendOutput("Using texture '" + BlockTextureData.GetDataByTextureID(result2)?.Name + "'");
			}
			if (BCCommandAbstract.Options.ContainsKey("delmulti"))
			{
				return;
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Block.isMultiBlock && block.ischild)
			{
				Vector3i parentPos = block.Block.multiBlockPos.GetParentPos(pos, block);
				BlockValue block2 = world.GetBlock(parentPos);
				if (block2.ischild || block2.type != block.type)
				{
					return;
				}
				world.ChunkClusters[0].SetBlock(parentPos, BlockValue.Air, bNotify: false, updateLight: false);
			}
			if (BCCommandAbstract.Options.ContainsKey("delmulti"))
			{
				return;
			}
			if (block.Block.IndexName == "lpblock")
			{
				GameManager.Instance.persistentPlayers.RemoveLandProtectionBlock(pos);
			}
			Chunk chunk = world.GetChunkFromWorldPos(pos.x, pos.y, pos.z) as Chunk;
			if (targetbv.Equals(BlockValue.Air))
			{
				result = MarchingCubes.DensityAir;
				if (world.GetTerrainHeight(pos.x, pos.z) > pos.y)
				{
					chunk?.SetTerrainHeight(pos.x & 0xF, pos.z & 0xF, (byte)pos.y);
				}
			}
			else if (targetbv.Block.shape.IsTerrain())
			{
				result = MarchingCubes.DensityTerrain;
				if (world.GetTerrainHeight(pos.x, pos.z) < pos.y)
				{
					chunk?.SetTerrainHeight(pos.x & 0xF, pos.z & 0xF, (byte)pos.y);
				}
			}
			else
			{
				GameManager.Instance.SetBlockTextureServer(pos, BlockFace.None, result2, -1);
			}
			world.SetBlockRPC(pos, targetbv, result);
		}

		private static void ScanBlock(World world, Vector3i pos)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			BlockValue block = world.GetBlock(pos.x, pos.y, pos.z);
			string value = ItemClass.list[block.type]?.Name;
			if (string.IsNullOrEmpty(value))
			{
				value = "air";
			}
			dictionary.Add("Name", value);
			dictionary.Add("Type", $"{block.type}");
			dictionary.Add("Density", $"{world.GetDensity(0, pos.x, pos.y, pos.z)}");
			dictionary.Add("Rotation", $"{block.rotation}");
			dictionary.Add("Damage", $"{block.damage}");
			dictionary.Add("Paint", BCPrefab.GetTexStr(world.GetTexture(pos.x, pos.y, pos.z)) ?? "");
			dictionary.Add("Meta", $"{block.meta}");
			dictionary.Add("Meta2", $"{block.meta2}");
			if (block.ischild)
			{
				dictionary.Add("IsChild", $"{block.ischild}");
				dictionary.Add("ParentXYZ", $"{block.parentx}, {block.parenty}, {block.parentz}");
			}
			BCCommandAbstract.SendJson(dictionary);
		}

		private static void FillBlock(World world, Vector3i pos, BlockValue targetbv)
		{
			BCCommandAbstract.SendOutput(BCCommandAbstract.Options.ContainsKey("delmulti") ? $"Removed multidim blocks @ {pos}" : $"Inserting block '{Block.list[targetbv.type].GetBlockName()}' @ {pos}");
			SetBlock(world, pos, targetbv);
		}

		private static void SetDensity(WorldBase world, Vector3i pos)
		{
			sbyte result = 1;
			if (BCCommandAbstract.Options.ContainsKey("d") && sbyte.TryParse(BCCommandAbstract.Options["d"], out result))
			{
				BCCommandAbstract.SendOutput($"Using density {result}");
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
			}
			else if (block.ischild)
			{
				BCCommandAbstract.SendOutput($"Target block is a child block @ {pos} - Parent@ {block.parentx},{block.parenty},{block.parentz}");
			}
			else
			{
				sbyte density = world.GetDensity(0, pos);
				if (density == result)
				{
					BCCommandAbstract.SendOutput($"No change in density @ {pos}");
					return;
				}
				world.SetBlockRPC(pos, block, result);
				BCCommandAbstract.SendOutput($"Changing density on block from '{density}' to '{result}' @ {pos}");
			}
		}

		private static void SetRotation(WorldBase world, Vector3i pos)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("rot") && !byte.TryParse(BCCommandAbstract.Options["rot"], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse rotation '" + BCCommandAbstract.Options["rot"] + "'");
				return;
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
			}
			else if (block.ischild)
			{
				BCCommandAbstract.SendOutput($"Target child block can't be rotated @ {pos} - Parent@ {block.parentx},{block.parenty},{block.parentz}");
			}
			else if (!block.Block.shape.IsRotatable)
			{
				BCCommandAbstract.SendOutput($"Target block can't be rotated @ {pos}");
			}
			else
			{
				byte rotation = block.rotation;
				block.rotation = result;
				world.SetBlockRPC(pos, block);
				BCCommandAbstract.SendOutput($"Changing rotation on block from '{rotation}' to '{result}' @ {pos}");
			}
		}

		private static void DowngradeBlock(WorldBase world, Vector3i pos)
		{
			BlockValue block = world.GetBlock(pos);
			BlockValue downgradeBlock = block.Block.DowngradeBlock;
			if (downgradeBlock.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block has no downgrade @ {pos}");
			}
			else if (block.ischild)
			{
				BCCommandAbstract.SendOutput($"Can't downgrade a child block @ {pos} - Parent@ {block.parentx},{block.parenty},{block.parentz}");
			}
			else
			{
				downgradeBlock.rotation = block.rotation;
				world.SetBlockRPC(pos, downgradeBlock);
				BCCommandAbstract.SendOutput($"Downgrading block from '{block.Block.GetBlockName()}' to '{downgradeBlock.Block.GetBlockName()}' @ {pos}");
			}
		}

		private static void UpgradeBlock(WorldBase world, Vector3i pos)
		{
			BlockValue block = world.GetBlock(pos);
			BlockValue upgradeBlock = block.Block.UpgradeBlock;
			if (upgradeBlock.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block has no upgrade @ {pos}");
			}
			else if (block.ischild)
			{
				BCCommandAbstract.SendOutput($"Can't upgrade a child block @ {pos} - Parent@ {block.parentx},{block.parenty},{block.parentz}");
			}
			else
			{
				upgradeBlock.rotation = block.rotation;
				world.SetBlockRPC(pos, upgradeBlock);
				BCCommandAbstract.SendOutput($"Upgrading block from '{block.Block.GetBlockName()}' to '{upgradeBlock.Block.GetBlockName()}' @ {pos}");
			}
		}

		private static void DamageBlock(WorldBase world, Vector3i pos)
		{
			int result = 0;
			int result2 = 0;
			if (BCCommandAbstract.Options.ContainsKey("d"))
			{
				if (BCCommandAbstract.Options["d"].IndexOf(",", StringComparison.InvariantCulture) > -1)
				{
					string[] array = BCCommandAbstract.Options["d"].Split(',');
					if (array.Length != 2)
					{
						BCCommandAbstract.SendOutput("Unable to parse damage values");
						return;
					}
					if (!int.TryParse(array[0], out result))
					{
						BCCommandAbstract.SendOutput("Unable to parse damage min value");
						return;
					}
					if (!int.TryParse(array[1], out result2))
					{
						BCCommandAbstract.SendOutput("Unable to parse damage max value");
						return;
					}
				}
				else if (!int.TryParse(BCCommandAbstract.Options["d"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse damage value");
					return;
				}
			}
			BlockValue blockValue = world.GetBlock(pos);
			if (blockValue.Equals(BlockValue.Air))
			{
				return;
			}
			int maxDamage = blockValue.Block.blockMaterial.MaxDamage;
			int num = ((result2 != 0) ? UnityEngine.Random.Range(result, result2) : result);
			int num2 = num + blockValue.damage;
			if (BCCommandAbstract.Options.ContainsKey("nobreak"))
			{
				blockValue.damage = Math.Min(num2, maxDamage - 1);
			}
			else if (BCCommandAbstract.Options.ContainsKey("overkill"))
			{
				if (num2 >= maxDamage)
				{
					BlockValue downgradeBlock = blockValue.Block.DowngradeBlock;
					while (num2 >= maxDamage)
					{
						downgradeBlock = blockValue.Block.DowngradeBlock;
						num2 -= maxDamage;
						maxDamage = downgradeBlock.Block.blockMaterial.MaxDamage;
						downgradeBlock.rotation = blockValue.rotation;
						blockValue = downgradeBlock;
					}
					blockValue.damage = num2;
					BCCommandAbstract.SendOutput($"Damaging block for {-num} caused downgrade to '{downgradeBlock.Block.GetBlockName()}' @ {pos}");
				}
				else
				{
					blockValue.damage = num2;
				}
			}
			else if (num2 >= maxDamage)
			{
				BlockValue downgradeBlock2 = blockValue.Block.DowngradeBlock;
				BCCommandAbstract.SendOutput($"Damaging block for {-num} caused downgrade to '{downgradeBlock2.Block.GetBlockName()}' @ {pos}");
				downgradeBlock2.rotation = blockValue.rotation;
				blockValue = downgradeBlock2;
			}
			else
			{
				blockValue.damage = num2;
			}
			world.SetBlockRPC(pos, blockValue);
			if (!blockValue.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Damaging block for '{-num}' leaving {blockValue.Block.blockMaterial.MaxDamage - blockValue.damage}/{blockValue.Block.blockMaterial.MaxDamage} @ {pos}");
			}
		}

		private static void RepairBlock(WorldBase world, Vector3i pos)
		{
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
				return;
			}
			int damage = block.damage;
			if (damage > 0)
			{
				BCCommandAbstract.SendOutput($"Target block not damaged @ {pos}");
				return;
			}
			block.damage = 0;
			world.SetBlockRPC(pos, block);
			BCCommandAbstract.SendOutput($"Repairing block for '{damage}' damage @ {pos}");
		}

		private static void SetPaintFace(WorldBase world, Vector3i pos)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!byte.TryParse(BCCommandAbstract.Options["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return;
				}
				if (BlockTextureData.list[result] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result}");
					return;
				}
			}
			uint result2 = 0u;
			if (BCCommandAbstract.Options.ContainsKey("face") && !uint.TryParse(BCCommandAbstract.Options["face"], out result2))
			{
				BCCommandAbstract.SendOutput("Unable to parse face value");
				return;
			}
			if (result2 > 5)
			{
				BCCommandAbstract.SendOutput("Face must be between 0 and 5");
				return;
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
				return;
			}
			if (block.Block.shape.IsTerrain())
			{
				BCCommandAbstract.SendOutput($"Target block is terrain @ {pos}");
				return;
			}
			GameManager.Instance.SetBlockTextureServer(pos, (BlockFace)result2, result, -1);
			BCCommandAbstract.SendOutput($"Painting block on face '{((BlockFace)result2).ToString()}' with texture '{BlockTextureData.GetDataByTextureID(result)?.Name}' @ {pos}");
		}

		private static void SetPaint(WorldBase world, Vector3i pos)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("t"))
			{
				if (!byte.TryParse(BCCommandAbstract.Options["t"], out result))
				{
					BCCommandAbstract.SendOutput("Unable to parse texture value");
					return;
				}
				if (BlockTextureData.list[result] == null)
				{
					BCCommandAbstract.SendOutput($"Unknown texture index {result}");
					return;
				}
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
				return;
			}
			if (block.Block.shape.IsTerrain())
			{
				BCCommandAbstract.SendOutput($"Target block is terrain @ {pos}");
				return;
			}
			GameManager.Instance.SetBlockTextureServer(pos, BlockFace.None, result, -1);
			BCCommandAbstract.SendOutput($"Painting block with texture '{BlockTextureData.GetDataByTextureID(result)?.Name}' @ {pos}");
		}

		private static void RemovePaint(WorldBase world, Vector3i pos)
		{
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
				return;
			}
			if (block.Block.shape.IsTerrain())
			{
				BCCommandAbstract.SendOutput($"Target block is terrain @ {pos}");
				return;
			}
			GameManager.Instance.SetBlockTextureServer(pos, BlockFace.None, 0, -1);
			BCCommandAbstract.SendOutput($"Paint removed from block @ {pos}");
		}

		private static void SetMeta(int metaIdx, WorldBase world, Vector3i pos)
		{
			byte result = 0;
			if (BCCommandAbstract.Options.ContainsKey("meta") && !byte.TryParse(BCCommandAbstract.Options["meta"], out result))
			{
				BCCommandAbstract.SendOutput("Unable to parse meta '" + BCCommandAbstract.Options["meta"] + "'");
				return;
			}
			BlockValue block = world.GetBlock(pos);
			if (block.Equals(BlockValue.Air))
			{
				BCCommandAbstract.SendOutput($"Target block is air @ {pos}");
				return;
			}
			if (block.ischild)
			{
				BCCommandAbstract.SendOutput($"Target child block can't be set @ {pos} - Parent@ {block.parentx},{block.parenty},{block.parentz}");
				return;
			}
			byte meta = block.meta;
			switch (metaIdx)
			{
			default:
				return;
			case 1:
				block.meta = result;
				break;
			case 2:
				block.meta2 = result;
				break;
			}
			world.SetBlockRPC(pos, block);
			BCCommandAbstract.SendOutput($"Changing meta{metaIdx} on block from '{meta}' to '{result}' @ {pos}");
		}
	}
}
