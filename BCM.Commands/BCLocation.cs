using System;
using System.Collections.Generic;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCLocation : BCCommandAbstract
	{
		private static readonly Dictionary<string, Vector3i> Cache = new Dictionary<string, Vector3i>();

		public override int DefaultPermissionLevel => 1000;

		public BCLocation()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCLocation"];
			DefaultCommands = new string[2]
			{
				"bc-loc",
				"loc"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCLocation.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			Vector3i pos = new Vector3i(int.MinValue, 0, int.MinValue);
			EntityPlayer entityPlayer = null;
			string text = null;
			if (BCCommandAbstract.SenderInfo.RemoteClientInfo != null)
			{
				text = BCCommandAbstract.SenderInfo.RemoteClientInfo.ownerId;
				entityPlayer = world.Entities.dict[BCCommandAbstract.SenderInfo.RemoteClientInfo.entityId] as EntityPlayer;
				if (entityPlayer != null && BCCommandAbstract.Params.Count == 0)
				{
					pos = new Vector3i((int)Math.Floor((float)entityPlayer.serverPos.x / 32f), (int)Math.Floor((float)entityPlayer.serverPos.y / 32f), (int)Math.Floor((float)entityPlayer.serverPos.z / 32f));
				}
			}
			switch (BCCommandAbstract.Params.Count)
			{
			case 0:
				if (entityPlayer == null || text == null)
				{
					BCCommandAbstract.SendOutput("Error getting location of command sender.");
					return;
				}
				SetPos(text, pos);
				BCCommandAbstract.SendOutput("Location: " + pos.x + " " + pos.y + " " + pos.z);
				BCCommandAbstract.SendOutput("ChunkXZ: " + (pos.x >> 4) + "," + (pos.z >> 4) + " + ChunkBlockXZ: " + (pos.x & 0xF) + "," + (pos.z & 0xF));
				BCCommandAbstract.SendOutput("RegionXZ: " + (pos.x >> 9) + "," + (pos.z >> 9) + " + RegionBlockXZ: " + (pos.x & 0x1FF) + "," + (pos.z & 0x1FF));
				BCCommandAbstract.SendOutput("Distance To Ground Height: " + (pos.y - world.GetHeight(pos.x, pos.z) - 1));
				BCCommandAbstract.SendOutput("Distance To Terrain Height: " + (pos.y - world.GetTerrainHeight(pos.x, pos.z) - 1));
				return;
			case 3:
				if (int.TryParse(BCCommandAbstract.Params[0], out pos.x) && int.TryParse(BCCommandAbstract.Params[1], out pos.y) && int.TryParse(BCCommandAbstract.Params[2], out pos.z) && entityPlayer != null && text != null)
				{
					SetPos(text, pos);
					BCCommandAbstract.SendOutput("Location set to: " + pos.x + " " + pos.y + " " + pos.z);
					BCCommandAbstract.SendOutput("ChunkXZ: " + (pos.x >> 4) + "," + (pos.z >> 4) + " + ChunkBlockXZ: " + (pos.x & 0xF) + "," + (pos.z & 0xF));
					BCCommandAbstract.SendOutput("RegionXZ: " + (pos.x >> 9) + "," + (pos.z >> 9) + " + RegionBlockXZ: " + (pos.x & 0x1FF) + "," + (pos.z & 0x1FF));
					BCCommandAbstract.SendOutput("Distance To Ground Height: " + (pos.y - world.GetHeight(pos.x, pos.z) - 1));
					BCCommandAbstract.SendOutput("Distance To Terrain Height: " + (pos.y - world.GetTerrainHeight(pos.x, pos.z) - 1));
					return;
				}
				break;
			}
			BCCommandAbstract.SendOutput(GetHelp());
		}

		public static Vector3i GetPos(string steamId)
		{
			if (steamId != null && Cache.ContainsKey(steamId))
			{
				return Cache[steamId];
			}
			return new Vector3i(int.MinValue, 0, int.MinValue);
		}

		private static void SetPos(string steamId, Vector3i pos)
		{
			Cache[steamId] = pos;
		}
	}
}
