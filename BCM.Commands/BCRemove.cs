using System.Collections.Generic;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCRemove : BCCommandAbstract
	{
		public BCRemove()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCRemove"];
			DefaultCommands = new string[1]
			{
				"bc-remove"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCRemove.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("all") || BCCommandAbstract.Options.ContainsKey("istype") || BCCommandAbstract.Options.ContainsKey("type") || BCCommandAbstract.Options.ContainsKey("vehicle") || BCCommandAbstract.Options.ContainsKey("ecname"))
			{
				Dictionary<string, int> dictionary = new Dictionary<string, int>();
				foreach (int key in BCMCmdProcessor.FilterEntities(world.Entities.dict, BCCommandAbstract.Options).Keys)
				{
					RemoveEntity(world, key, dictionary);
				}
				BCCommandAbstract.SendJson(dictionary);
			}
			else if (BCCommandAbstract.Params.Count == 1)
			{
				if (!int.TryParse(BCCommandAbstract.Params[0], out var result))
				{
					BCCommandAbstract.SendOutput("Unable to parse entity id");
				}
				else
				{
					RemoveEntity(world, result);
				}
			}
			else
			{
				BCCommandAbstract.SendOutput(GetHelp());
			}
		}

		private static void RemoveEntity(World world, int entityId, IDictionary<string, int> count = null)
		{
			if (!world.Entities.dict.ContainsKey(entityId))
			{
				if (!BCCommandAbstract.Options.ContainsKey("all"))
				{
					BCCommandAbstract.SendOutput($"Invalid entityId, entity not found: {entityId}");
				}
				return;
			}
			Entity entity = world.Entities.dict[entityId];
			if ((object)entity != null)
			{
				if (!(entity is EntityPlayer))
				{
					if (entity is EntityVehicle && !BCCommandAbstract.Options.ContainsKey("vehicle"))
					{
						BCCommandAbstract.SendOutput("Vehicle not removed, use /vehicle to remove vehicles");
						return;
					}
					EntityClass entityClass = EntityClass.list[entity.entityClass];
					world.RemoveEntity(entityId, EnumRemoveEntityReason.Despawned);
					if (BCCommandAbstract.Options.ContainsKey("all") || BCCommandAbstract.Options.ContainsKey("istype") || BCCommandAbstract.Options.ContainsKey("type") || BCCommandAbstract.Options.ContainsKey("vehicle") || BCCommandAbstract.Options.ContainsKey("ecname"))
					{
						if (count != null)
						{
							if (count.ContainsKey($"{entity.GetType()}:{entityClass.entityClassName}"))
							{
								count[$"{entity.GetType()}:{entityClass.entityClassName}"]++;
							}
							else
							{
								count.Add($"{entity.GetType()}:{entityClass.entityClassName}", 1);
							}
						}
					}
					else
					{
						BCMVector3 arg = new BCMVector3(entity.position);
						BCCommandAbstract.SendOutput(string.Format("Entity Removed: {0}:{1} @{2}", entity.GetType(), (entityClass != null) ? entityClass.entityClassName : "", arg));
					}
				}
				else if (!BCCommandAbstract.Options.ContainsKey("all"))
				{
					BCCommandAbstract.SendOutput("You can't remove a player!");
				}
			}
			else if (!BCCommandAbstract.Options.ContainsKey("all"))
			{
				BCCommandAbstract.SendOutput($"Invalid entity, entity not found: {entityId}");
			}
		}
	}
}
