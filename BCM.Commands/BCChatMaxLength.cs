using System;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCChatMaxLength : BCCommandAbstract
	{
		public static int ChatMaxLength;

		public BCChatMaxLength()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCChatMaxLength"];
			DefaultCommands = new string[1]
			{
				"bc-chatmax"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
			ChatMaxLength = configValue.GetItem("ChatMaxLength", 0);
		}

		public override string GetHelp()
		{
			return Resources.BCChatMaxLength.ParseHelp(this);
		}

		protected override void Process()
		{
			if (1 != BCCommandAbstract.Params.Count)
			{
				BCCommandAbstract.SendOutput("Current chat max length is: " + ((ChatMaxLength == 0) ? "unset" : $"{ChatMaxLength}"));
				BCCommandAbstract.SendOutput(GetHelp());
			}
			else if (BCCommandAbstract.Params[0].Equals("show", StringComparison.OrdinalIgnoreCase))
			{
				BCCommandAbstract.SendOutput("Current chat max length is: " + ((ChatMaxLength == 0) ? "unset" : $"{ChatMaxLength}"));
			}
			else if (!int.TryParse(BCCommandAbstract.Params[0], out ChatMaxLength))
			{
				BCCommandAbstract.SendOutput($"Unable to parse {ChatMaxLength} as a number");
			}
			else
			{
				BCMPersist.Instance.ServerConfig["BCChatMaxLength"].SetItem("ChatMaxLength", ChatMaxLength);
				BCMPersist.Instance.Save(BCMSaveType.ServerConfig);
				BCCommandAbstract.SendOutput($"Chat text maximum length set to {ChatMaxLength}");
			}
		}
	}
}
