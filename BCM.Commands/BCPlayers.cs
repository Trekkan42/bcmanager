using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCPlayers : BCCommandAbstract
	{
		public BCPlayers()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCPlayers"];
			DefaultCommands = new string[1]
			{
				"bc-lp"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCPlayers.ParseHelp(this);
		}

		private static void Filters()
		{
			BCCommandAbstract.SendJson(typeof(BCMPlayer.StrFilters).GetFields().ToDictionary((FieldInfo field) => field.Name, (FieldInfo field) => $"{field.GetValue(typeof(BCMPlayer.StrFilters))}"));
		}

		private static void Indexed()
		{
			BCCommandAbstract.SendJson((from kvp in BCMPlayer.FilterMap
				group kvp by kvp.Value into @group
				select @group.First()).ToDictionary((KeyValuePair<int, string> kvp) => kvp.Value, (KeyValuePair<int, string> kvp) => kvp.Key));
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("filters"))
			{
				Filters();
				return;
			}
			if (BCCommandAbstract.Options.ContainsKey("index"))
			{
				Indexed();
				return;
			}
			if (BCCommandAbstract.Params.Count > 1)
			{
				BCCommandAbstract.SendOutput("Wrong number of arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			if (BCCommandAbstract.Params.Count == 1)
			{
				if (!PlayerStore.GetId(BCCommandAbstract.Params[0], out var steamId, "CON"))
				{
					return;
				}
				BCMPlayer bCMPlayer = new BCMPlayer(PlayerData.PlayerInfo(steamId), BCCommandAbstract.Options, BCCommandAbstract.GetFilters("players"));
				if (BCCommandAbstract.Options.ContainsKey("min"))
				{
					BCCommandAbstract.SendJson(new List<List<object>>
					{
						(from d in bCMPlayer.Data()
							select d.Value).ToList()
					});
				}
				else
				{
					BCCommandAbstract.SendJson(bCMPlayer.Data());
				}
				return;
			}
			List<object> list = new List<object>();
			foreach (BCMPlayer item in from s in PlayerStore.GetAll(BCCommandAbstract.Options)
				select new BCMPlayer(PlayerData.PlayerInfo(s), BCCommandAbstract.Options, BCCommandAbstract.GetFilters("players")))
			{
				if (BCCommandAbstract.Options.ContainsKey("min"))
				{
					list.Add((from d in item.Data()
						select d.Value).ToList());
				}
				else
				{
					list.Add(item.Data());
				}
			}
			BCCommandAbstract.SendJson(list);
		}
	}
}
