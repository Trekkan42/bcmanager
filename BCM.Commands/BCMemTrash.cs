using System;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCMemTrash : BCCommandAbstract
	{
		public BCMemTrash()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCMemTrash"];
			DefaultCommands = new string[1]
			{
				"bc-trash"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCMemTrash.ParseHelp(this);
		}

		protected override void Process()
		{
			GC.Collect();
			GC.WaitForPendingFinalizers();
			BCCommandAbstract.SendOutput("Trash Disposed");
		}
	}
}
