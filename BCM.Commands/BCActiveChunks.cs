using System.Collections.Generic;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCActiveChunks : BCCommandAbstract
	{
		public BCActiveChunks()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCActiveChunks"];
			DefaultCommands = new string[2]
			{
				"bc-chunks",
				"bc-cc"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCActiveChunks.ParseHelp(this);
		}

		protected override void Process()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			ChunkCluster chunkCluster = GameManager.Instance.World.ChunkClusters[0];
			lock (chunkCluster.GetSyncRoot())
			{
				dictionary.Add("Count", chunkCluster.Count());
				using LinkedList<Chunk>.Enumerator enumerator = chunkCluster.GetChunkArray().GetEnumerator();
				List<object> list = new List<object>();
				while (enumerator.MoveNext())
				{
					if (enumerator.Current != null)
					{
						list.Add(new BCMChunkInfo(enumerator.Current));
					}
				}
				dictionary.Add("Chunks", list);
			}
			BCCommandAbstract.SendJson(dictionary);
		}
	}
}
