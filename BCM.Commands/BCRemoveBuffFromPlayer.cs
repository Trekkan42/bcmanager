using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCRemoveBuffFromPlayer : BCCommandAbstract
	{
		public BCRemoveBuffFromPlayer()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCRemoveBuffFromPlayer"];
			DefaultCommands = new string[1]
			{
				"bc-debuffplayer"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCRemoveBuffFromPlayer.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld())
			{
				return;
			}
			if (BCCommandAbstract.Params.Count != 2)
			{
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				return;
			}
			string _id;
			ClientInfo _cInfo;
			int num = ConsoleHelper.ParseParamPartialNameOrId(BCCommandAbstract.Params[0], out _id, out _cInfo);
			if (num == 1)
			{
				if (_cInfo != null)
				{
					if (BuffManager.Buffs.ContainsKey(BCCommandAbstract.Params[1]))
					{
						_cInfo.SendPackage(NetPackageManager.GetPackage<NetPackageConsoleCmdClient>().Setup("debuff " + BCCommandAbstract.Params[1], _bExecute: true));
						BCCommandAbstract.SendOutput("Buff " + BCCommandAbstract.Params[1] + " removed from player " + _cInfo.playerName);
					}
					else
					{
						BCCommandAbstract.SendOutput("Unable to find buff " + BCCommandAbstract.Params[1]);
					}
				}
			}
			else if (num > 1)
			{
				BCCommandAbstract.SendOutput($"{num} matches found, please refine your search text.");
			}
			else
			{
				BCCommandAbstract.SendOutput("Player name or entity ID not found.");
			}
		}
	}
}
