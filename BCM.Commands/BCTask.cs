using System;
using System.Collections.Generic;
using System.Threading;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCTask : BCCommandAbstract
	{
		private static readonly Dictionary<string, BCMTask> Tasks = new Dictionary<string, BCMTask>();

		public BCTask()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCTask"];
			DefaultCommands = new string[1]
			{
				"bc-task"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCTask.ParseHelp(this);
		}

		protected override void Process()
		{
			if (Tasks.Count == 0)
			{
				BCCommandAbstract.SendOutput("No tracked tasks currently running");
				return;
			}
			foreach (BCMTask value in Tasks.Values)
			{
				BCCommandAbstract.SendJson(new
				{
					TypeHash = value.Type + "_" + value.Hash,
					Timestamp = value.Timestamp.ToUtcStr(),
					Completed = value.Completion.ToUtcStr(),
					Duration = value.Duration.TotalSeconds,
					Status = value.Status.ToString(),
					Command = ((value.Command == null) ? "" : value.Command.Command),
					Output = value.Output
				});
			}
		}

		public static void AddTask(string cmdName, int hash)
		{
			if (Tasks.ContainsKey($"{cmdName}_{hash}"))
			{
				Log.Out(string.Format("{0} Unable to add tracked task to list, already in tracked list - {1}_{2}", "(BCM)", cmdName, hash));
				return;
			}
			Tasks.Add($"{cmdName}_{hash}", new BCMTask
			{
				Type = cmdName,
				Hash = hash,
				Output = null,
				Timestamp = DateTime.UtcNow
			});
		}

		public static void DelTask(string cmdName, int hash, int delay = 60)
		{
			ThreadManager.AddSingleTask(delegate
			{
				if (Tasks.ContainsKey($"{cmdName}_{hash}"))
				{
					BCMTask bCMTask = Tasks[$"{cmdName}_{hash}"];
					bCMTask.Status = BCMTaskStatus.Complete;
					bCMTask.Completion = DateTime.UtcNow;
					bCMTask.Duration = bCMTask.Completion - bCMTask.Timestamp;
					Thread.Sleep(delay * 1000);
					if (Tasks.ContainsKey($"{cmdName}_{hash}"))
					{
						Tasks.Remove($"{cmdName}_{hash}");
						BCCommandAbstract.SendOutput($"Task complete {hash}");
					}
				}
			});
		}

		public static BCMTask GetTask(string cmdName, int? hash)
		{
			if (hash.HasValue && Tasks.ContainsKey($"{cmdName}_{hash}"))
			{
				return Tasks[$"{cmdName}_{hash}"];
			}
			return null;
		}
	}
}
