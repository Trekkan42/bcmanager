using System.Collections.Generic;
using System.Linq;
using BCM.Models;
using BCM.PersistentData;
using BCM.Properties;

namespace BCM.Commands
{
	public class BCGiveBuffToEntity : BCCommandAbstract
	{
		public BCGiveBuffToEntity()
		{
			ConfigValue configValue = BCMPersist.Instance.ServerConfig["BCGiveBuffToEntity"];
			DefaultCommands = new string[2]
			{
				"bc-geb",
				"buffentity"
			};
			Commands = configValue.GetItem("Commands", DefaultCommands);
			BaseOptions = configValue.GetItem("BaseOptions", DefaultOptions);
		}

		public override string GetHelp()
		{
			return Resources.BCGiveBuffToEntity.ParseHelp(this);
		}

		protected override void Process()
		{
			if (!BCUtils.CheckWorld(out var world))
			{
				return;
			}
			switch (BCCommandAbstract.Params.Count)
			{
			case 0:
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			case 1:
			{
				List<object> data = new List<object>();
				if (BCCommandAbstract.Params[0] != "list")
				{
					if (!int.TryParse(BCCommandAbstract.Params[0], out var result2))
					{
						BCCommandAbstract.SendOutput("Couldn't parse param '" + BCCommandAbstract.Params[0] + "' for entityId");
						break;
					}
					EntityAlive entityAlive2 = world.Entities.dict[result2] as EntityAlive;
					if (entityAlive2 == null)
					{
						BCCommandAbstract.SendOutput("Unable to find entity by id " + BCCommandAbstract.Params[0]);
						break;
					}
					GetEntityBuffs(data, entityAlive2);
				}
				else
				{
					for (int num2 = world.Entities.list.Count - 1; num2 >= 0; num2--)
					{
						EntityAlive entityAlive3 = world.Entities.list[num2] as EntityAlive;
						if (!(entityAlive3 == null))
						{
							GetEntityBuffs(data, entityAlive3);
						}
					}
				}
				BCCommandAbstract.SendJson(data);
				break;
			}
			case 2:
			{
				if (BCCommandAbstract.Options.ContainsKey("type"))
				{
					string b = BCCommandAbstract.Params[0];
					string text = BCCommandAbstract.Params[1];
					if (!BuffManager.Buffs.ContainsKey(text))
					{
						BCCommandAbstract.SendOutput("Unknown Buff " + text);
					}
					else
					{
						if (BuffManager.GetBuff(text) == null)
						{
							break;
						}
						int num = 0;
						foreach (EntityAlive item in world.Entities.list.OfType<EntityAlive>())
						{
							if (!(item.GetType().ToString() != b))
							{
								item.Buffs.AddBuff(text, -1, true, false);
								num++;
							}
						}
						BCCommandAbstract.SendOutput(string.Format("{0} {1} buffed with {2}", num, (num == 1) ? "entity" : "entities", text));
					}
					break;
				}
				if (!int.TryParse(BCCommandAbstract.Params[0], out var result))
				{
					BCCommandAbstract.SendOutput("Error parsing entity id");
					break;
				}
				string text2 = BCCommandAbstract.Params[1];
				if (!BuffManager.Buffs.ContainsKey(text2))
				{
					BCCommandAbstract.SendOutput("Unknown buff " + text2);
					break;
				}
				if (!world.Entities.dict.ContainsKey(result))
				{
					BCCommandAbstract.SendOutput("Entity not found");
					break;
				}
				BuffClass buff = BuffManager.GetBuff(text2);
				EntityAlive entityAlive = world.Entities.dict[result] as EntityAlive;
				if (buff != null && entityAlive != null)
				{
					entityAlive.Buffs.AddBuff(text2, -1, true, false);
					BCCommandAbstract.SendOutput($"Buffed entity {result} with {text2}");
				}
				break;
			}
			default:
				BCCommandAbstract.SendOutput("Invalid arguments");
				BCCommandAbstract.SendOutput(GetHelp());
				break;
			}
		}

		private static void GetEntityBuffs(ICollection<object> data, EntityAlive entity)
		{
			string name = "";
			if (entity is EntityPlayer)
			{
				name = entity.EntityName;
			}
			else if (EntityClass.list.ContainsKey(entity.entityClass))
			{
				name = EntityClass.list[entity.entityClass].entityClassName;
			}
			BCMBuffEntity bCMBuffEntity = new BCMBuffEntity(entity.entityId, name);
			foreach (BuffValue activeBuff in entity.Buffs.ActiveBuffs)
			{
				if (activeBuff != null)
				{
					bCMBuffEntity.Buffs.Add(new BCMBuffInfo(activeBuff));
				}
			}
			data.Add(bCMBuffEntity);
		}
	}
}
