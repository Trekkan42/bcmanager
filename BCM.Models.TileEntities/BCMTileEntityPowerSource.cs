using System.Collections.Generic;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityPowerSource : BCMTileEntity
	{
		public string Owner;

		public bool IsPlayerPlaced;

		public bool IsOn;

		public int LastOutput;

		public int MaxOutput;

		public int CurrentFuel;

		public int MaxFuel;

		public string PowerItemType;

		public List<BCMItemStack> Slots = new List<BCMItemStack>();

		public BCMTileEntityPowerSource(Vector3i pos, TileEntityPowerSource te)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			IsPlayerPlaced = te.IsPlayerPlaced;
			IsOn = te.IsOn;
			LastOutput = te.LastOutput;
			MaxOutput = te.MaxOutput;
			CurrentFuel = te.CurrentFuel;
			MaxFuel = te.MaxFuel;
			PowerItemType = te.GetPowerItem().PowerItemType.ToString();
			ItemStack[] itemSlots = te.ItemSlots;
			foreach (ItemStack item in itemSlots)
			{
				Slots.Add(new BCMItemStack(item));
			}
		}
	}
}
