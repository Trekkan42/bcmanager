using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntitySecureDoor : BCMTileEntity
	{
		public string Owner;

		public List<string> Users;

		public bool HasPassword;

		public bool IsLocked;

		public bool IsOpen;

		public BCMTileEntitySecureDoor(Vector3i pos, [NotNull] TileEntitySecureDoor te, Chunk chunk)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			Users = te.GetUsers();
			HasPassword = te.HasPassword();
			IsLocked = te.IsLocked();
			IsOpen = chunk.GetBlock(pos.x & 0xF, pos.y & 0xF, pos.z & 0xF).meta == 1;
		}
	}
}
