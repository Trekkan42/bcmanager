using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityTrader : BCMTileEntity
	{
		public int Money;

		public ulong ResetTime;

		public int TraderId;

		public bool IsOpen;

		public bool PlayerOwned;

		[NotNull]
		[UsedImplicitly]
		public List<BCMItemStack> Inventory = new List<BCMItemStack>();

		[NotNull]
		[UsedImplicitly]
		public List<List<BCMItemStack>> TierGroups = new List<List<BCMItemStack>>();

		public BCMTileEntityTrader(Vector3i pos, TileEntityTrader te)
			: base(pos, te)
		{
			Money = te.TraderData.AvailableMoney;
			ResetTime = te.TraderData.NextResetTime;
			TraderId = te.TraderData.TraderID;
			IsOpen = te.TraderData.TraderInfo.IsOpen;
			PlayerOwned = te.TraderData.TraderInfo.PlayerOwned;
			foreach (ItemStack item in te.TraderData.PrimaryInventory)
			{
				Inventory.Add(new BCMItemStack(item));
			}
			foreach (ItemStack[] tierItemGroup in te.TraderData.TierItemGroups)
			{
				TierGroups.Add(tierItemGroup.Select((ItemStack itemStack) => new BCMItemStack(itemStack)).ToList());
			}
		}
	}
}
