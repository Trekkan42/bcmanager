using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityVendingMachine : BCMTileEntityTrader
	{
		public string Owner;

		public List<string> Users;

		public bool HasPassword;

		public bool IsLocked;

		public BCMTileEntityVendingMachine(Vector3i pos, [NotNull] TileEntityVendingMachine te)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			Users = te.GetUsers();
			HasPassword = te.HasPassword();
			IsLocked = te.IsLocked();
		}
	}
}
