using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntitySecureLootContainer : BCMTileEntityLootContainer
	{
		public string Owner;

		public List<string> Users;

		public bool HasPassword;

		public bool IsLocked;

		public BCMTileEntitySecureLootContainer(Vector3i pos, [NotNull] TileEntitySecureLootContainer te)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			Users = te.GetUsers();
			HasPassword = te.HasPassword();
			IsLocked = te.IsLocked();
		}
	}
}
