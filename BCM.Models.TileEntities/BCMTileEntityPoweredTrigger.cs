namespace BCM.Models.TileEntities
{
	public class BCMTileEntityPoweredTrigger : BCMTileEntity
	{
		public string Owner;

		public bool IsTriggered;

		public bool TargetAllies;

		public bool TargetSelf;

		public bool TargetStrangers;

		public bool TargetZombies;

		public BCMTileEntityPoweredTrigger(Vector3i pos, TileEntityPoweredTrigger te)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			IsTriggered = te.IsTriggered;
			TargetAllies = te.TargetAllies;
			TargetSelf = te.TargetSelf;
			TargetStrangers = te.TargetStrangers;
			TargetZombies = te.TargetZombies;
		}
	}
}
