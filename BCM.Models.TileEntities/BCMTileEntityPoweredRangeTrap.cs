using System.Collections.Generic;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityPoweredRangeTrap : BCMTileEntity
	{
		public string Owner;

		public bool IsLocked;

		public bool TargetAllies;

		public bool TargetSelf;

		public bool TargetStrangers;

		public bool TargetZombies;

		public string AmmoItem;

		public List<BCMItemStack> Slots = new List<BCMItemStack>();

		public BCMTileEntityPoweredRangeTrap(Vector3i pos, TileEntityPoweredRangedTrap te)
			: base(pos, te)
		{
			Owner = te.GetOwner();
			IsLocked = te.IsLocked;
			TargetAllies = te.TargetAllies;
			TargetSelf = te.TargetSelf;
			TargetStrangers = te.TargetStrangers;
			TargetZombies = te.TargetZombies;
			AmmoItem = te.AmmoItem.GetItemName();
			ItemStack[] itemSlots = te.ItemSlots;
			foreach (ItemStack item in itemSlots)
			{
				Slots.Add(new BCMItemStack(item));
			}
		}
	}
}
