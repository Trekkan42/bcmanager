using System.Collections.Generic;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityWorkstation : BCMTileEntity
	{
		public bool IsActive;

		public bool IsBurning;

		public bool IsPlayerPlaced;

		public bool IsBesideWater;

		public readonly List<BCMItemStack> Tools = new List<BCMItemStack>();

		public readonly List<BCMItemStack> Fuel = new List<BCMItemStack>();

		public readonly List<BCMItemStack> Input = new List<BCMItemStack>();

		public readonly List<BCMItemStack> Output = new List<BCMItemStack>();

		public readonly List<BCMCraftingQueue> CraftingQueue = new List<BCMCraftingQueue>();

		public BCMTileEntityWorkstation(Vector3i pos, TileEntityWorkstation te)
			: base(pos, te)
		{
			IsActive = te.IsActive(GameManager.Instance.World);
			IsBurning = te.IsBurning;
			IsPlayerPlaced = te.IsPlayerPlaced;
			IsBesideWater = te.IsBesideWater;
			ItemStack[] tools = te.Tools;
			foreach (ItemStack item in tools)
			{
				Tools.Add(new BCMItemStack(item));
			}
			RecipeQueueItem[] queue = te.Queue;
			foreach (RecipeQueueItem item2 in queue)
			{
				CraftingQueue.Add(new BCMCraftingQueue(item2));
			}
			tools = te.Fuel;
			foreach (ItemStack item3 in tools)
			{
				Fuel.Add(new BCMItemStack(item3));
			}
			tools = te.Input;
			foreach (ItemStack item4 in tools)
			{
				Input.Add(new BCMItemStack(item4));
			}
			tools = te.Output;
			foreach (ItemStack item5 in tools)
			{
				Output.Add(new BCMItemStack(item5));
			}
		}
	}
}
