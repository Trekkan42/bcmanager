namespace BCM.Models.TileEntities
{
	public class BCMTileEntityPowered : BCMTileEntity
	{
		public int ChildCount;

		public bool IsPlayerPlaced;

		public bool IsPowered;

		public int PowerUsed;

		public int RequiredPower;

		public BCMTileEntityPowered(Vector3i pos, TileEntityPowered te)
			: base(pos, te)
		{
			ChildCount = te.ChildCount;
			IsPlayerPlaced = te.IsPlayerPlaced;
			IsPowered = te.IsPowered;
			PowerUsed = te.PowerUsed;
			RequiredPower = te.RequiredPower;
		}
	}
}
