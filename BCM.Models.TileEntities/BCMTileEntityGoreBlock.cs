using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models.TileEntities
{
	public class BCMTileEntityGoreBlock : BCMTileEntity
	{
		public int LootList;

		public bool Touched;

		public ulong TimeTouched;

		public BCMVector2 Size;

		public double OpenTime;

		[NotNull]
		[UsedImplicitly]
		public List<BCMItemStack> Items = new List<BCMItemStack>();

		public BCMTileEntityGoreBlock(Vector3i pos, [NotNull] TileEntityGoreBlock te)
			: base(pos, te)
		{
			LootList = te.lootListIndex;
			Touched = te.bWasTouched;
			TimeTouched = te.worldTimeTouched;
			Size = new BCMVector2(te.GetContainerSize());
			OpenTime = te.GetOpenTime();
			ItemStack[] items = te.GetItems();
			foreach (ItemStack itemStack in items)
			{
				if (itemStack.itemValue.type != 0)
				{
					Items.Add(new BCMItemStack(itemStack));
				}
			}
		}
	}
}
