namespace BCM.Models
{
    public class BCMPrefabSleeperVolume
    {
        public bool Used;

        public BCMVector3 Start;

        public BCMVector3 Size;

        public string Group;

        public bool IsBoss;

        public BCMPrefabSleeperVolume(Prefab prefab, int x)
        {
            Used = prefab.SleeperVolumes[x].used;
            Start = new BCMVector3(prefab.SleeperVolumes[x].startPos);
            Size = new BCMVector3(prefab.SleeperVolumes[x].size);
            Group = prefab.SleeperVolumes[x].groupName;
            IsBoss = false; // prefab.SleeperVolumes[x].isBoss;
        }
    }
}
