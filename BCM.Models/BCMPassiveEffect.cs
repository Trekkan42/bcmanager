using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMPassiveEffect
    {
        public bool OrCompare;

        public string ModifierTypes;

        public string Effects;

        public bool MatchAnyTags;

        public List<string> CVars = new List<string>();

        public List<double> Levels = new List<double>();

        public List<double> Values = new List<double>();

        public List<BCMRequirement> Requirements = new List<BCMRequirement>();

        public BCMPassiveEffect(PassiveEffect effect)
        {
            OrCompare = effect.OrCompare;
            float[] values = effect.Values;
            foreach (float num in values)
            {
                Values.Add(num);
            }
            values = effect.Levels;
            foreach (float num2 in values)
            {
                Levels.Add(num2);
            }
            string[] cVarValues = effect.CVarValues;
            foreach (string item in cVarValues)
            {
                CVars.Add(item);
            }
            MatchAnyTags = effect.MatchAnyTags;
            ModifierTypes = effect.Modifier.ToString();
            foreach (IRequirement requirement in effect.Requirements)
            {
                Requirements.Add(new BCMRequirement(requirement));
            }
            Effects = effect.Type.ToString();
        }
    }
}
