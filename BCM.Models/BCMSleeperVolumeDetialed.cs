using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BCM.Models
{
    public class BCMSleeperVolumeDetialed : BCMSleeperVolume
    {
        private const string SpawnsFieldName = "VH";

        public List<BCMSpawnPoint> SpawnPoints;

        public BCMSleeperVolumeDetialed(int index, SleeperVolume volume, World world)
            : base(index, volume, world)
        {
            FieldInfo field = typeof(SleeperVolume).GetField("VH", BindingFlags.Instance | BindingFlags.NonPublic);
            if (field == null)
            {
                return;
            }
            List<SleeperVolume.SpawnPoint> list = field.GetValue(volume) as List<SleeperVolume.SpawnPoint>;
            if (list != null)
            {
                SpawnPoints = list.Select((SleeperVolume.SpawnPoint sp) => new BCMSpawnPoint(sp)).ToList();
            }
        }
    }
}
