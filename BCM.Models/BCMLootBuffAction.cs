namespace BCM.Models
{
    public class BCMLootBuffAction
    {
        public string BuffId;

        public string Name;

        public double Chance;

        public BCMLootBuffAction(string buffId, double chance)
        {
            BuffId = buffId;
            Name = BuffManager.GetBuff(buffId).Name;
            Chance = chance;
        }
    }
}
