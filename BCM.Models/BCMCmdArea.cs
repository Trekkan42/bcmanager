using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using BCM.Commands;
using JetBrains.Annotations;
using UnityEngine;

namespace BCM.Models
{
    public class BCMCmdArea : BCCmd
    {
        public string Filter;

        public ushort Radius;

        public bool HasPos;

        public BCMVector3 Position;

        public bool HasSize;

        public BCMVector3 Size;

        public bool HasChunkPos;

        public BCMVector4 ChunkBounds;

        public ItemStack ItemStack;

        [NotNull]
        public readonly Dictionary<string, string> Opts;

        [NotNull]
        public readonly List<string> Pars;

        [NotNull]
        public readonly string CmdType;

        public BCMVector3 MaxPos => new BCMVector3(Position.x + Size.x - 1, Position.y + Size.y - 1, Position.z + Size.z - 1);

        public BCMCmdArea(string cmdType)
        {
            Opts = new Dictionary<string, string>();
            Pars = new List<string>();
            CmdType = cmdType;
        }

        public BCMCmdArea(List<string> pars, Dictionary<string, string> options, string cmdType)
        {
            Opts = options;
            Pars = pars;
            CmdType = cmdType;
        }

        public bool IsWithinBounds(Vector3i pos)
        {
            if (!HasPos)
            {
                return false;
            }
            BCMVector3 bCMVector = (HasSize ? Size : new BCMVector3(1, 1, 1));
            if (Position.x <= pos.x && pos.x < Position.x + bCMVector.x && Position.y <= pos.y && pos.y < Position.y + bCMVector.y)
            {
                if (Position.z <= pos.z)
                {
                    return pos.z < Position.z + bCMVector.z;
                }
                return false;
            }
            return false;
        }

        public static bool ProcessParams(BCMCmdArea command, ushort maxRadius)
        {
            if (command.Opts.ContainsKey("type"))
            {
                command.Filter = command.Opts["type"];
            }
            switch (command.Pars.Count)
            {
                case 1:
                    GetRadius(command, maxRadius);
                    command.Command = command.Pars[0];
                    return true;
                case 3:
                    command.Command = command.Pars[0];
                    GetRadius(command, maxRadius);
                    return GetChunkPosXz(command);
                case 4:
                    command.Command = command.Pars[0];
                    return GetPositionXyz(command);
                case 5:
                    command.Command = command.Pars[0];
                    return GetChunkSizeXyzw(command);
                case 7:
                    command.Command = command.Pars[0];
                    return GetPosSizeXyz(command);
                default:
                    return false;
            }
        }

        public static bool GetIds(World world, BCMCmdArea command, out EntityPlayer entity)
        {
            entity = null;
            int? num = null;
            if (command.Opts.ContainsKey("id"))
            {
                if (!PlayerStore.GetId(command.Opts["id"], out command.SteamId, "CON"))
                {
                    return false;
                }
                num = ConsoleHelper.ParseParamSteamIdOnline(command.SteamId)?.entityId;
            }
            if (command.SteamId == null)
            {
                num = BCCommandAbstract.SenderInfo.RemoteClientInfo?.entityId;
                command.SteamId = BCCommandAbstract.SenderInfo.RemoteClientInfo?.playerId;
            }
            if (num.HasValue)
            {
                entity = world.Players.dict[num.Value];
            }
            if (!(entity != null))
            {
                return BCCommandAbstract.Params.Count >= 3;
            }
            return true;
        }

        public static bool GetEntPos(BCMCmdArea command, EntityPlayer entity)
        {
            if (!(entity != null))
            {
                BCCommandAbstract.SendOutput("Unable to get a position");
                return false;
            }
            Vector3i vector3i = new Vector3i(int.MinValue, 0, int.MinValue);
            bool flag = false;
            if (command.Opts.ContainsKey("loc"))
            {
                vector3i = BCLocation.GetPos(BCCommandAbstract.SenderInfo.RemoteClientInfo?.playerId);
                if (vector3i.x == int.MinValue)
                {
                    BCCommandAbstract.SendOutput("No location stored or player not found. Use bc-loc to store a location.");
                    return false;
                }
                flag = true;
                command.Position = new BCMVector3(Math.Min(vector3i.x, (int)entity.position.x), Math.Min(vector3i.y, (int)entity.position.y), Math.Min(vector3i.z, (int)entity.position.z));
                command.HasPos = true;
                command.Size = new BCMVector3(Math.Abs(vector3i.x - Utils.Fastfloor(entity.position.x)) + 1, Math.Abs(vector3i.y - Utils.Fastfloor(entity.position.y)) + 1, Math.Abs(vector3i.z - Utils.Fastfloor(entity.position.z)) + 1);
                command.HasSize = true;
            }
            command.ChunkBounds = new BCMVector4
            {
                x = World.toChunkXZ(flag ? Math.Min(vector3i.x, (int)entity.position.x) : ((int)entity.position.x - command.Radius)),
                y = World.toChunkXZ(flag ? Math.Min(vector3i.z, (int)entity.position.z) : ((int)entity.position.z - command.Radius)),
                z = World.toChunkXZ(flag ? Math.Max(vector3i.x, (int)entity.position.x) : ((int)entity.position.x + command.Radius)),
                w = World.toChunkXZ(flag ? Math.Max(vector3i.z, (int)entity.position.z) : ((int)entity.position.z + command.Radius))
            };
            command.HasChunkPos = true;
            return true;
        }

        public static void DoProcess(World world, BCMCmdArea command, BCCommandAbstract cmdRef)
        {
            if (command.Opts.ContainsKey("forcesync"))
            {
                BCCommandAbstract.SendOutput("Processing Command synchronously...");
                ProcessCommand(world, command, cmdRef);
                return;
            }
            if (BCCommandAbstract.SenderInfo.NetworkConnection != null && !(BCCommandAbstract.SenderInfo.NetworkConnection is TelnetConnection))
            {
                BCCommandAbstract.SendOutput("Processing Async Command... Sending output to log");
            }
            else
            {
                BCCommandAbstract.SendOutput("Processing Async Command...");
            }
            BCTask.AddTask(command.CmdType, ThreadManager.AddSingleTask(delegate
            {
                ProcessCommand(world, command, cmdRef);
            }, null, delegate (ThreadManager.TaskInfo info, Exception e)
            {
                BCTask.DelTask(command.CmdType, info.GetHashCode());
            }).GetHashCode());
        }

        public static Dictionary<int, Entity> FilterEntities(Dictionary<int, Entity> entities, Dictionary<string, string> options)
        {
            Dictionary<int, Entity> dictionary = new Dictionary<int, Entity>();
            foreach (KeyValuePair<int, Entity> entity in entities)
            {
                if (options.ContainsKey("all"))
                {
                    if (options.ContainsKey("minibike") || !(entity.Value is EntityMinibike))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("type"))
                {
                    if (!(entity.Value == null) && !(entity.Value.GetType().ToString() != options["type"]))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("istype"))
                {
                    if (entity.Value == null)
                    {
                        continue;
                    }
                    string assemblyQualifiedName = entity.Value.GetType().AssemblyQualifiedName;
                    if (assemblyQualifiedName != null)
                    {
                        Type type = Type.GetType(assemblyQualifiedName.Replace(entity.Value.GetType().ToString(), options["istype"]));
                        if (!(type == null) && type.IsInstanceOfType(entity.Value))
                        {
                            dictionary.Add(entity.Key, entity.Value);
                        }
                    }
                }
                else if (options.ContainsKey("minibike"))
                {
                    if (entity.Value is EntityMinibike)
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("ecname"))
                {
                    EntityClass entityClass = EntityClass.list[entity.Value.entityClass];
                    if (entityClass != null && !(options["ecname"] != entityClass.entityClassName))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (entity.Value is EntityEnemy || entity.Value is EntityAnimal)
                {
                    dictionary.Add(entity.Key, entity.Value);
                }
            }
            return dictionary;
        }

        public static void GetRadius(BCMCmdArea command, ushort max)
        {
            if (command.Opts.ContainsKey("r"))
            {
                ushort.TryParse(command.Opts["r"], out command.Radius);
                if (command.Radius > max)
                {
                    command.Radius = max;
                }
                BCCommandAbstract.SendOutput($"Setting radius to +{command.Radius}");
            }
            else if (!command.Opts.ContainsKey("loc"))
            {
                BCCommandAbstract.SendOutput("Setting radius to default of +0");
            }
            else
            {
                BCCommandAbstract.SendOutput("Using stored location for bounding box");
            }
        }

        public static bool GetChunkSizeXyzw(BCMCmdArea command)
        {
            if (!int.TryParse(command.Pars[1], out var result) || !int.TryParse(command.Pars[2], out var result2) || !int.TryParse(command.Pars[3], out var result3) || !int.TryParse(command.Pars[4], out var result4))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,z x2,z2 into ints");
                return false;
            }
            command.ChunkBounds = new BCMVector4(Math.Min(result, result3), Math.Min(result2, result4), Math.Max(result, result3), Math.Max(result2, result4));
            command.HasChunkPos = true;
            return true;
        }

        public static bool GetChunkPosXz(BCMCmdArea command)
        {
            if (!int.TryParse(command.Pars[1], out var result) || !int.TryParse(command.Pars[2], out var result2))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,z into ints");
                return false;
            }
            command.ChunkBounds = new BCMVector4(result - command.Radius, result2 - command.Radius, result + command.Radius, result2 + command.Radius);
            command.HasChunkPos = true;
            return true;
        }

        public static bool GetPositionXyz(BCMCmdArea command)
        {
            if (!int.TryParse(command.Pars[1], out var result) || !int.TryParse(command.Pars[2], out var result2) || !int.TryParse(command.Pars[3], out var result3))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,y,z into ints");
                return false;
            }
            command.Position = new BCMVector3(result, result2, result3);
            command.HasPos = true;
            command.ChunkBounds = new BCMVector4(World.toChunkXZ(result), World.toChunkXZ(result3), World.toChunkXZ(result), World.toChunkXZ(result3));
            command.HasChunkPos = true;
            if (command.Opts.ContainsKey("item") && !(command.Command != "additem"))
            {
                return GetItemStack(command);
            }
            return true;
        }

        public static bool GetItemStack(BCMCmdArea command)
        {
            int result = -1;
            int result2 = 1;
            if (command.Opts.ContainsKey("q") && !int.TryParse(command.Opts["q"], out result))
            {
                BCCommandAbstract.SendOutput("Unable to parse quality, using random value");
            }
            if (command.Opts.ContainsKey("c") && !int.TryParse(command.Opts["c"], out result2))
            {
                BCCommandAbstract.SendOutput($"Unable to parse count, using default value of {result2}");
            }
            int result3;
            ItemClass itemClass = (int.TryParse(command.Opts["item"], out result3) ? ItemClass.GetForId(result3) : ItemClass.GetItemClass(command.Opts["item"], _caseInsensitive: true));
            if (itemClass == null)
            {
                BCCommandAbstract.SendOutput("Unable to get item or block from given value '" + command.Opts["item"] + "'");
                return false;
            }
            command.ItemStack = new ItemStack
            {
                itemValue = new ItemValue(itemClass.Id, _bCreateDefaultParts: true),
                count = ((result2 <= itemClass.Stacknumber.Value) ? result2 : itemClass.Stacknumber.Value)
            };
            if (command.ItemStack.count < result2)
            {
                BCCommandAbstract.SendOutput("Using max stack size for " + itemClass.Name + " of " + command.ItemStack.count);
            }
            if (command.ItemStack.itemValue.HasQuality && result > 0)
            {
                command.ItemStack.itemValue.Quality = result;
            }
            return true;
        }

        public static bool GetPosSizeXyz(BCMCmdArea command)
        {
            if (!int.TryParse(command.Pars[1], out var result) || !int.TryParse(command.Pars[2], out var result2) || !int.TryParse(command.Pars[3], out var result3))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,y,z into ints");
                return false;
            }
            if (!int.TryParse(command.Pars[4], out var result4) || !int.TryParse(command.Pars[5], out var result5) || !int.TryParse(command.Pars[6], out var result6))
            {
                BCCommandAbstract.SendOutput("Unable to parse x2,y2,z2 into ints");
                return false;
            }
            command.Position = new BCMVector3(Math.Min(result, result4), Math.Min(result2, result5), Math.Min(result3, result6));
            command.HasPos = true;
            command.Size = new BCMVector3(Math.Abs(result - result4) + 1, Math.Abs(result2 - result5) + 1, Math.Abs(result3 - result6) + 1);
            command.HasSize = true;
            command.ChunkBounds = new BCMVector4(World.toChunkXZ(Math.Min(result, result4)), World.toChunkXZ(Math.Min(result3, result6)), World.toChunkXZ(Math.Max(result, result4)), World.toChunkXZ(Math.Max(result3, result6)));
            command.HasChunkPos = true;
            return true;
        }

        public static Dictionary<long, Chunk> GetAffectedChunks(BCMCmdArea command, World world)
        {
            Dictionary<long, Chunk> dictionary = new Dictionary<long, Chunk>();
            int result = 2000;
            if (command.Opts.ContainsKey("timeout") && command.Opts["timeout"] != null)
            {
                int.TryParse(command.Opts["timeout"], out result);
            }
            ChunkObserver(command, world, result / 2000);
            for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
            {
                for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
                {
                    long key = WorldChunkCache.MakeChunkKey(i, j);
                    dictionary.Add(key, null);
                }
            }
            int num = (command.ChunkBounds.z - command.ChunkBounds.x + 1) * (command.ChunkBounds.w - command.ChunkBounds.y + 1);
            int num2 = 0;
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (num2 < num && stopwatch.ElapsedMilliseconds < result)
            {
                for (int k = command.ChunkBounds.x; k <= command.ChunkBounds.z; k++)
                {
                    for (int l = command.ChunkBounds.y; l <= command.ChunkBounds.w; l++)
                    {
                        long key2 = WorldChunkCache.MakeChunkKey(k, l);
                        if ((!dictionary.ContainsKey(key2) || dictionary[key2] == null) && world.ChunkCache.ContainsChunkSync(key2))
                        {
                            dictionary[key2] = world.GetChunkSync(key2) as Chunk;
                            if (dictionary[key2] != null)
                            {
                                num2++;
                            }
                        }
                    }
                }
            }
            stopwatch.Stop();
            if (num2 < num)
            {
                BCCommandAbstract.SendOutput($"Unable to load {num - num2}/{num} chunks");
                return null;
            }
            BCCommandAbstract.SendOutput($"Loading {num} chunks took {Math.Round((float)stopwatch.ElapsedMilliseconds / 1000f, 2)} seconds");
            return dictionary;
        }

        public static void ChunkObserver(BCMCmdArea command, World world, int timeoutSec)
        {
            Vector3 initialPosition = (command.HasPos ? command.Position.V3() : command.ChunkBounds.ToV3());
            int viewDim = ((!command.Opts.ContainsKey("r")) ? command.Radius : command.ChunkBounds.Radius());
            ChunkManager.ChunkObserver chunkObserver = world.m_ChunkManager.AddChunkObserver(initialPosition, _bBuildVisualMeshAround: false, viewDim, -1);
            int timerSec = 60;
            if (command.Opts.ContainsKey("ts") && command.Opts["ts"] != null)
            {
                int.TryParse(command.Opts["ts"], out timerSec);
            }
            timerSec += timeoutSec;
            BCTask.AddTask(command.CmdType, ThreadManager.AddSingleTask(delegate (ThreadManager.TaskInfo info)
            {
                DoCleanup(world, chunkObserver, command.CmdType, timerSec, info);
            }, null, delegate (ThreadManager.TaskInfo info, Exception e)
            {
                BCTask.DelTask(command.CmdType, info.GetHashCode(), 120);
            }).GetHashCode());
        }

        public static void DoCleanup(World world, ChunkManager.ChunkObserver co, string commandType, int ts = 0, ThreadManager.TaskInfo taskInfo = null)
        {
            BCMTask task = BCTask.GetTask(commandType, taskInfo?.GetHashCode());
            for (int i = 0; i < ts; i++)
            {
                if (task != null)
                {
                    task.Output = new
                    {
                        timer = i,
                        total = ts
                    };
                }
                Thread.Sleep(1000);
            }
            world.m_ChunkManager.RemoveChunkObserver(co);
        }

        public static void ProcessCommand(World world, BCMCmdArea command, BCCommandAbstract cmdRef)
        {
            Dictionary<long, Chunk> affectedChunks = GetAffectedChunks(command, world);
            if (affectedChunks == null)
            {
                BCCommandAbstract.SendOutput("Aborting, unable to load all chunks in area before timeout.");
                BCCommandAbstract.SendOutput("Use /timeout=#### to override the default 2000 millisecond limit, or wait for the requested chunks to finish loading and try again.");
                return;
            }
            string text = "";
            if (command.HasPos)
            {
                text += $"Pos {command.Position.x} {command.Position.y} {command.Position.z} ";
                if (command.HasSize)
                {
                    BCMVector3 maxPos = command.MaxPos;
                    text += $"to {maxPos.x} {maxPos.y} {maxPos.z} ";
                }
            }
            if (command.HasChunkPos)
            {
                text += $"Chunks {command.ChunkBounds.x} {command.ChunkBounds.y} to {command.ChunkBounds.z} {command.ChunkBounds.w}";
            }
            if (!string.IsNullOrEmpty(text))
            {
                BCCommandAbstract.SendOutput(text);
            }
            cmdRef.ProcessSwitch(world, command, out var reload);
            if (reload != 0 && !command.Opts.ContainsKey("noreload") && !command.Opts.ContainsKey("nr"))
            {
                BCChunks.ReloadForClients(affectedChunks, (reload == ReloadMode.Target) ? command.SteamId : string.Empty);
            }
        }
    }
}
