using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMTexture
    {
        public int Id;

        public string Name;

        public string Local;

        public ushort TextureId;

        public BCMTexture([NotNull] BlockTextureData texture)
        {
            Id = texture.ID;
            Name = texture.Name;
            Local = texture.LocalizedName;
            TextureId = texture.TextureID;
        }
    }
}
