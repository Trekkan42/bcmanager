using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMAdmin
    {
        [UsedImplicitly]
        public string SteamId;

        [UsedImplicitly]
        public int PermissionLevel;

        [UsedImplicitly]
        public string PlayerName;

        public BCMAdmin(AdminToolsClientInfo atci, string playerName)
        {
            SteamId = atci.SteamId;
            PermissionLevel = atci.PermissionLevel;
            PlayerName = playerName;
        }
    }
}
