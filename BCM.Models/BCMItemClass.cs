using System;
using System.Collections.Generic;

namespace BCM.Models
{
    [Serializable]
    public class BCMItemClass : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Id = "id";

            public const string Name = "name";

            public const string Local = "local";

            public const string Material = "material";

            public const string StackNumber = "stack";

            public const string Icon = "icon";

            public const string IconTint = "icontint";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "id"
            },
            {
                1,
                "name"
            },
            {
                2,
                "local"
            },
            {
                3,
                "material"
            },
            {
                4,
                "stack"
            },
            {
                5,
                "icon"
            },
            {
                6,
                "icontint"
            }
        };

        public int Id;

        public string Name;

        public string Local;

        public string Material;

        public int StackNumber;

        public string Icon;

        public string IconTint;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMItemClass(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            ItemClass itemClass = obj as ItemClass;
            if (itemClass == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "id":
                            GetId(itemClass);
                            break;
                        case "name":
                            GetName(itemClass);
                            break;
                        case "local":
                            GetLocal(itemClass);
                            break;
                        case "material":
                            GetMaterial(itemClass);
                            break;
                        case "stack":
                            GetStackNumber(itemClass);
                            break;
                        case "icon":
                            GetIcon(itemClass);
                            break;
                        case "icontint":
                            GetIconTint(itemClass);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetId(itemClass);
                GetName(itemClass);
                GetLocal(itemClass);
                GetMaterial(itemClass);
                GetStackNumber(itemClass);
                GetIcon(itemClass);
                GetIconTint(itemClass);
            }
        }

        private void GetIconTint(ItemClass item)
        {
            base.Bin.Add("IconTint", IconTint = item.GetIconTint().ToStringRgbHex(hash: false));
        }

        private void GetIcon(ItemClass item)
        {
            base.Bin.Add("Icon", Icon = item.GetIconName());
        }

        private void GetStackNumber(ItemClass item)
        {
            base.Bin.Add("StackNumber", StackNumber = item.Stacknumber.Value);
        }

        private void GetMaterial(ItemClass item)
        {
            base.Bin.Add("Material", Material = item.MadeOfMaterial?.id);
        }

        private void GetLocal(ItemClass item)
        {
            Dictionary<string, object> bin = base.Bin;
            string obj = item.GetLocalizedItemName() ?? item.GetItemName();
            string value = obj;
            Local = obj;
            bin.Add("Local", value);
        }

        private void GetName(ItemClass itemClass)
        {
            base.Bin.Add("Name", Name = itemClass.Name);
        }

        private void GetId(ItemClass itemClass)
        {
            base.Bin.Add("Id", Id = itemClass.Id);
        }
    }
}
