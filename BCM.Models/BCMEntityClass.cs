using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace BCM.Models
{
    [Serializable]
    public class BCMEntityClass : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Id = "id";

            public const string Name = "name";

            public const string CanSpawn = "canspawn";

            public const string Class = "class";

            public const string Model = "model";

            public const string Archetype = "archetype";

            public const string Experience = "experience";

            public const string MaxHealth = "maxhp";

            public const string DeadHP = "deadhp";

            public const string HandItem = "handitem";

            public const string LootListOnDeath = "lootdead";

            public const string LootListAlive = "lootalive";

            public const string IsEnemy = "isenemy";

            public const string WalkType = "walktype";

            public const string RotateToGround = "toground";

            public const string MoveSpeed = "speed";

            public const string MoveSpeedAggro = "speedaggro";

            public const string MoveSpeedNight = "speednight";

            public const string MoveSpeedPanic = "speedpanic";

            public const string CanClimbVertical = "climbwall";

            public const string CanClimbLadders = "climbladder";

            public const string AttackTimeoutDay = "attackday";

            public const string AttackTimeoutNight = "attacknight";

            public const string AITasks = "tasks";

            public const string AITargetTasks = "targets";

            public const string ExplosionData = "explosion";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "id"
            },
            {
                1,
                "name"
            },
            {
                2,
                "canspawn"
            },
            {
                3,
                "class"
            },
            {
                4,
                "model"
            },
            {
                5,
                "archetype"
            },
            {
                6,
                "experience"
            },
            {
                7,
                "maxhp"
            },
            {
                8,
                "deadhp"
            },
            {
                9,
                "handitem"
            },
            {
                10,
                "lootdead"
            },
            {
                11,
                "lootalive"
            },
            {
                12,
                "isenemy"
            },
            {
                13,
                "walktype"
            },
            {
                14,
                "toground"
            },
            {
                15,
                "speed"
            },
            {
                16,
                "speedaggro"
            },
            {
                17,
                "speednight"
            },
            {
                18,
                "speedpanic"
            },
            {
                19,
                "climbwall"
            },
            {
                20,
                "climbladder"
            },
            {
                21,
                "attackday"
            },
            {
                22,
                "attacknight"
            },
            {
                23,
                "tasks"
            },
            {
                24,
                "targets"
            },
            {
                25,
                "explosion"
            }
        };

        public int Id;

        public string Name;

        public bool CanSpawn;

        public string Class;

        public string Model;

        public string Archetype;

        public int Experience;

        public int MaxHealth;

        public int DeadHP;

        public string HandItem;

        public int LootListOnDeath;

        public int LootListAlive;

        public bool IsEnemy;

        public int WalkType;

        public bool RotateToGround;

        public double MoveSpeed;

        public BCMVector2 MoveSpeedAggro;

        public double MoveSpeedNight;

        public double MoveSpeedPanic;

        public bool CanClimbVertical;

        public bool CanClimbLadders;

        public double AttackTimeoutDay;

        public double AttackTimeoutNight;

        [NotNull]
        [UsedImplicitly]
        public List<BCMDynamicProp> AITasks = new List<BCMDynamicProp>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMDynamicProp> AITargetTasks = new List<BCMDynamicProp>();

        public BCMExplosionData Explosion;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMEntityClass(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            EntityClass entityClass = obj as EntityClass;
            if (entityClass == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "id":
                            GetId(entityClass);
                            break;
                        case "name":
                            GetName(entityClass);
                            break;
                        case "canspawn":
                            GetUserCanSpawn(entityClass);
                            break;
                        case "class":
                            GetClass(entityClass);
                            break;
                        case "model":
                            GetModel(entityClass);
                            break;
                        case "archetype":
                            GetArchetype(entityClass);
                            break;
                        case "experience":
                            GetExperience(entityClass);
                            break;
                        case "maxhp":
                            GetMaxHealth(entityClass);
                            break;
                        case "deadhp":
                            GetDeadHP(entityClass);
                            break;
                        case "handitem":
                            GetHandItem(entityClass);
                            break;
                        case "lootdead":
                            GetLootListOnDeath(entityClass);
                            break;
                        case "lootalive":
                            GetLootListAlive(entityClass);
                            break;
                        case "isenemy":
                            GetIsEnemy(entityClass);
                            break;
                        case "walktype":
                            GetWalkType(entityClass);
                            break;
                        case "toground":
                            GetRotateToGround(entityClass);
                            break;
                        case "speed":
                            GetMoveSpeed(entityClass);
                            break;
                        case "speedaggro":
                            GetMoveSpeedAggro(entityClass);
                            break;
                        case "speednight":
                            GetMoveSpeedNight(entityClass);
                            break;
                        case "speedpanic":
                            GetMoveSpeedPanic(entityClass);
                            break;
                        case "climbwall":
                            GetCanClimbVertical(entityClass);
                            break;
                        case "climbladder":
                            GetCanClimbLadders(entityClass);
                            break;
                        case "attackday":
                            GetAttackTimeoutDay(entityClass);
                            break;
                        case "attacknight":
                            GetAttackTimeoutNight(entityClass);
                            break;
                        case "tasks":
                            GetAITasks(entityClass);
                            break;
                        case "targets":
                            GetAITargetTasks(entityClass);
                            break;
                        case "explosion":
                            GetExplosionData(entityClass);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
                return;
            }
            GetId(entityClass);
            GetName(entityClass);
            GetUserCanSpawn(entityClass);
            GetClass(entityClass);
            GetModel(entityClass);
            GetArchetype(entityClass);
            GetExperience(entityClass);
            GetMaxHealth(entityClass);
            GetDeadHP(entityClass);
            GetHandItem(entityClass);
            GetLootListOnDeath(entityClass);
            GetLootListAlive(entityClass);
            GetIsEnemy(entityClass);
            GetWalkType(entityClass);
            GetRotateToGround(entityClass);
            GetMoveSpeed(entityClass);
            GetMoveSpeedAggro(entityClass);
            GetMoveSpeedNight(entityClass);
            GetMoveSpeedPanic(entityClass);
            GetCanClimbVertical(entityClass);
            GetCanClimbLadders(entityClass);
            GetAttackTimeoutDay(entityClass);
            GetAttackTimeoutNight(entityClass);
            GetAITasks(entityClass);
            GetAITargetTasks(entityClass);
            GetExplosionData(entityClass);
            if (base.Options.ContainsKey("full") || base.Options.ContainsKey("extra"))
            {
                GetProperty(entityClass, EntityClass.PropEntityType);
                GetProperty(entityClass, EntityClass.PropMesh);
                GetProperty(entityClass, EntityClass.PropAvatarController);
                GetProperty(entityClass, EntityClass.PropMaterialSwap0);
                GetProperty(entityClass, EntityClass.PropMaterialSwap1);
                GetProperty(entityClass, EntityClass.PropMaterialSwap2);
                GetProperty(entityClass, EntityClass.PropMaterialSwap3);
                GetProperty(entityClass, EntityClass.PropMaterialSwap4);
                GetProperty(entityClass, EntityClass.PropRightHandJointName);
                GetProperty(entityClass, EntityClass.PropMaxViewAngle);
                GetProperty(entityClass, EntityClass.PropTimeStayAfterDeath);
                GetProperty(entityClass, EntityClass.PropImmunity);
                GetProperty(entityClass, EntityClass.PropPhysicsBody);
                GetProperty(entityClass, EntityClass.PropCorpseBlock);
                GetProperty(entityClass, EntityClass.PropCorpseBlockChance);
                GetProperty(entityClass, EntityClass.PropParticleOnSpawn);
                GetProperty(entityClass, EntityClass.PropParticleOnDeath);
                GetProperty(entityClass, EntityClass.PropNPCID);
                GetProperty(entityClass, EntityClass.PropAIPackages);
                GetProperty(entityClass, EntityClass.PropBuffs);
                if (base.Options.ContainsKey("full"))
                {
                    GetProperty(entityClass, EntityClass.PropPrefab);
                    GetProperty(entityClass, EntityClass.PropParent);
                    GetProperty(entityClass, EntityClass.PropWeight);
                    GetProperty(entityClass, EntityClass.PropPushFactor);
                    GetProperty(entityClass, EntityClass.PropMapIcon);
                    GetProperty(entityClass, EntityClass.PropRootMotion);
                    GetProperty(entityClass, EntityClass.PropHasDeathAnim);
                    GetProperty(entityClass, EntityClass.PropSoundRandomTime);
                    GetProperty(entityClass, EntityClass.PropSoundAlertTime);
                    GetProperty(entityClass, EntityClass.PropSoundRandom);
                    GetProperty(entityClass, EntityClass.PropSoundHurt);
                    GetProperty(entityClass, EntityClass.PropSoundJump);
                    GetProperty(entityClass, EntityClass.PropSoundHurtSmall);
                    GetProperty(entityClass, EntityClass.PropSoundDrownPain);
                    GetProperty(entityClass, EntityClass.PropSoundDrownDeath);
                    GetProperty(entityClass, EntityClass.PropSoundWaterSurface);
                    GetProperty(entityClass, EntityClass.PropSoundDeath);
                    GetProperty(entityClass, EntityClass.PropSoundAttack);
                    GetProperty(entityClass, EntityClass.PropSoundAlert);
                    GetProperty(entityClass, EntityClass.PropSoundSense);
                    GetProperty(entityClass, EntityClass.PropSoundStamina);
                    GetProperty(entityClass, EntityClass.PropSoundLiving);
                    GetProperty(entityClass, EntityClass.PropSoundSpawn);
                    GetProperty(entityClass, EntityClass.PropSoundLand);
                    GetProperty(entityClass, EntityClass.PropSoundFootstepModifier);
                    GetProperty(entityClass, EntityClass.PropSoundGiveUp);
                    GetProperty(entityClass, EntityClass.PropLegCrawlerThreshold);
                    GetProperty(entityClass, EntityClass.PropKnockdownKneelDamageThreshold);
                    GetProperty(entityClass, EntityClass.PropKnockdownKneelStunDuration);
                    GetProperty(entityClass, EntityClass.PropKnockdownProneDamageThreshold);
                    GetProperty(entityClass, EntityClass.PropKnockdownProneStunDuration);
                    GetProperty(entityClass, EntityClass.PropKnockdownProneRefillRate);
                    GetProperty(entityClass, EntityClass.PropKnockdownKneelRefillRate);
                    GetProperty(entityClass, EntityClass.PropArmsExplosionDamageMultiplier);
                    GetProperty(entityClass, EntityClass.PropLegsExplosionDamageMultiplier);
                    GetProperty(entityClass, EntityClass.PropChestExplosionDamageMultiplier);
                    GetProperty(entityClass, EntityClass.PropHeadExplosionDamageMultiplier);
                    GetProperty(entityClass, EntityClass.PropSightRange);
                    GetProperty(entityClass, EntityClass.PropSleeperWakeupSightDetectionMin);
                    GetProperty(entityClass, EntityClass.PropSleeperWakeupSightDetectionMax);
                    GetProperty(entityClass, EntityClass.PropSleeperGroanSightDetectionMin);
                    GetProperty(entityClass, EntityClass.PropSleeperGroanSightDetectionMax);
                    GetProperty(entityClass, EntityClass.PropSoundSleeperGroan);
                    GetProperty(entityClass, EntityClass.PropSoundSleeperSnore);
                    GetProperty(entityClass, EntityClass.PropSightRange);
                    GetProperty(entityClass, EntityClass.PropNoiseAlertThreshold);
                    GetProperty(entityClass, EntityClass.PropSleeperNoiseWakeThreshold);
                    GetProperty(entityClass, EntityClass.PropSleeperNoiseGroanThreshold);
                    GetProperty(entityClass, EntityClass.PropSmellAlertThreshold);
                    GetProperty(entityClass, EntityClass.PropSleeperSmellWakeThreshold);
                    GetProperty(entityClass, EntityClass.PropSleeperSmellGroanThreshold);
                    GetProperty(entityClass, EntityClass.PropSoundSleeperGroanChance);
                }
            }
        }

        private void GetProperty(EntityClass ec, string prop)
        {
            base.Bin.Add(prop, ec.Properties.Values.ContainsKey(prop) ? ec.Properties.Values[prop] : null);
        }

        private void GetAITargetTasks(EntityClass ec)
        {
            int num = 1;
            while (ec.Properties.Values.ContainsKey(EntityClass.PropAITargetTask + num))
            {
                AITargetTasks.Add(new BCMDynamicProp(new string[4]
                {
                    EntityClass.PropAITargetTask + num,
                    ec.Properties.Values[EntityClass.PropAITargetTask + num],
                    ec.Properties.Params1.ContainsKey(EntityClass.PropAITargetTask + num) ? ec.Properties.Params1[EntityClass.PropAITargetTask + num] : "",
                    ec.Properties.Params2.ContainsKey(EntityClass.PropAITargetTask + num) ? ec.Properties.Params2[EntityClass.PropAITargetTask + num] : ""
                }));
                num++;
            }
            base.Bin.Add("AITargetTasks", AITargetTasks);
        }

        private void GetAITasks(EntityClass ec)
        {
            int num = 1;
            while (ec.Properties.Values.ContainsKey(EntityClass.PropAITask + num))
            {
                AITasks.Add(new BCMDynamicProp(new string[4]
                {
                    EntityClass.PropAITask + num,
                    ec.Properties.Values[EntityClass.PropAITask + num],
                    ec.Properties.Params1.ContainsKey(EntityClass.PropAITask + num) ? ec.Properties.Params1[EntityClass.PropAITask + num] : "",
                    ec.Properties.Params2.ContainsKey(EntityClass.PropAITask + num) ? ec.Properties.Params2[EntityClass.PropAITask + num] : ""
                }));
                num++;
            }
            base.Bin.Add("AITasks", AITasks);
        }

        private void GetExplosionData(EntityClass ec)
        {
            base.Bin.Add("Explosion", Explosion = ((!ec.Properties.Values.ContainsKey("Explosion.ParticleIndex")) ? null : ((int.TryParse(ec.Properties.Values["Explosion.ParticleIndex"], out var result) && result <= 0) ? null : new BCMExplosionData(ec.Properties))));
        }

        private void GetWalkType(EntityClass ec)
        {
            base.Bin.Add("WalkType", WalkType = (ec.Properties.Values.ContainsKey(EntityClass.PropWalkType) ? int.Parse(ec.Properties.Values[EntityClass.PropWalkType]) : 0));
        }

        private void GetMoveSpeed(EntityClass ec)
        {
            base.Bin.Add("MoveSpeed", MoveSpeed = (ec.Properties.Values.ContainsKey(EntityClass.PropMoveSpeed) ? double.Parse(ec.Properties.Values[EntityClass.PropMoveSpeed]) : 0.0));
        }

        private void GetMoveSpeedAggro(EntityClass ec)
        {
            Vector2 optionalValue = default(Vector2);
            if (ec.Properties.Values.ContainsKey(EntityClass.PropMoveSpeedAggro))
            {
                ec.Properties.ParseVec(EntityClass.PropMoveSpeedAggro, ref optionalValue);
            }
            base.Bin.Add("MoveSpeedAggro", MoveSpeedAggro = new BCMVector2(optionalValue));
        }

        private void GetMoveSpeedNight(EntityClass ec)
        {
            base.Bin.Add("MoveSpeedNight", MoveSpeedNight = (ec.Properties.Values.ContainsKey(EntityClass.PropMoveSpeedNight) ? double.Parse(ec.Properties.Values[EntityClass.PropMoveSpeedNight]) : 0.0));
        }

        private void GetMoveSpeedPanic(EntityClass ec)
        {
            base.Bin.Add("MoveSpeedPanic", MoveSpeedPanic = (ec.Properties.Values.ContainsKey(EntityClass.PropMoveSpeedPanic) ? double.Parse(ec.Properties.Values[EntityClass.PropMoveSpeedPanic]) : 0.0));
        }

        private void GetAttackTimeoutDay(EntityClass ec)
        {
            base.Bin.Add("AttackTimeoutDay", AttackTimeoutDay = (ec.Properties.Values.ContainsKey(EntityClass.PropAttackTimeoutDay) ? double.Parse(ec.Properties.Values[EntityClass.PropAttackTimeoutDay]) : 0.0));
        }

        private void GetAttackTimeoutNight(EntityClass ec)
        {
            base.Bin.Add("AttackTimeoutNight", AttackTimeoutNight = (ec.Properties.Values.ContainsKey(EntityClass.PropAttackTimeoutNight) ? double.Parse(ec.Properties.Values[EntityClass.PropAttackTimeoutNight]) : 0.0));
        }

        private void GetRotateToGround(EntityClass ec)
        {
            base.Bin.Add("RotateToGround", RotateToGround = ec.Properties.Values.ContainsKey(EntityClass.PropRotateToGround) && bool.Parse(ec.Properties.Values[EntityClass.PropRotateToGround]));
        }

        private void GetCanClimbVertical(EntityClass ec)
        {
            base.Bin.Add("CanClimbVertical", CanClimbVertical = ec.Properties.Values.ContainsKey(EntityClass.PropCanClimbVertical) && bool.Parse(ec.Properties.Values[EntityClass.PropCanClimbVertical]));
        }

        private void GetCanClimbLadders(EntityClass ec)
        {
            base.Bin.Add("CanClimbLadders", CanClimbLadders = ec.Properties.Values.ContainsKey(EntityClass.PropCanClimbLadders) && bool.Parse(ec.Properties.Values[EntityClass.PropCanClimbLadders]));
        }

        private void GetHandItem(EntityClass ec)
        {
            base.Bin.Add("HandItem", HandItem = (ec.Properties.Values.ContainsKey(EntityClass.PropHandItem) ? ec.Properties.Values[EntityClass.PropHandItem] : null));
        }

        private void GetIsEnemy(EntityClass ec)
        {
            base.Bin.Add("IsEnemy", IsEnemy = ec.bIsEnemyEntity);
        }

        private void GetLootListAlive(EntityClass ec)
        {
            base.Bin.Add("LootListAlive", LootListAlive = (ec.Properties.Values.ContainsKey(EntityClass.PropLootListAlive) ? int.Parse(ec.Properties.Values[EntityClass.PropLootListAlive]) : 0));
        }

        private void GetLootListOnDeath(EntityClass ec)
        {
            base.Bin.Add("LootListOnDeath", LootListOnDeath = (ec.Properties.Values.ContainsKey(EntityClass.PropLootListOnDeath) ? int.Parse(ec.Properties.Values[EntityClass.PropLootListOnDeath]) : 0));
        }

        private void GetDeadHP(EntityClass ec)
        {
            base.Bin.Add("DeadHP", DeadHP = ec.DeadBodyHitPoints);
        }

        private void GetMaxHealth(EntityClass ec)
        {
            base.Bin.Add("MaxHealth", MaxHealth = (ec.Properties.Values.ContainsKey(EntityClass.PropMaxHealth) ? int.Parse(ec.Properties.Values[EntityClass.PropMaxHealth]) : 100));
        }

        private void GetExperience(EntityClass ec)
        {
            base.Bin.Add("Experience", Experience = ec.ExperienceValue);
        }

        private void GetArchetype(EntityClass ec)
        {
            base.Bin.Add("Archetype", Archetype = ec.ArchetypeName);
        }

        private void GetModel(EntityClass ec)
        {
            base.Bin.Add("Model", Model = ec.modelType);
        }

        private void GetClass(EntityClass ec)
        {
            base.Bin.Add("Class", Class = ec.classname?.ToString());
        }

        private void GetUserCanSpawn(EntityClass ec)
        {
            base.Bin.Add("CanSpawn", CanSpawn = ec.bAllowUserInstantiate);
        }

        private void GetName(EntityClass entityClass)
        {
            base.Bin.Add("Name", Name = entityClass.entityClassName);
        }

        private void GetId(EntityClass entityClass)
        {
            base.Bin.Add("Id", Id = entityClass.entityClassName.GetHashCode());
        }
    }
}
