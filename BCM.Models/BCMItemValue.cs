using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMItemValue
    {
        public string Name;

        public string Class;

        public int Type;

        public int Quality;

        public int Meta;

        public double UseTimes;

        public int MaxUse;

        public byte Activated;

        public byte AmmoIndex;

        public List<BCMItemMod> Mods = new List<BCMItemMod>();

        public BCMItemValue(ItemValue item)
        {
            if (item == null || item.type == 0)
            {
                return;
            }
            Name = item.ItemClass.Name;
            Class = item.ItemClass.GetType().ToString();
            Type = item.type;
            Quality = item.Quality;
            Meta = item.Meta;
            UseTimes = item.UseTimes;
            MaxUse = item.MaxUseTimes;
            Activated = item.Activated;
            AmmoIndex = item.SelectedAmmoTypeIndex;
            ItemValue[] cosmeticMods;
            if (item.CosmeticMods != null && item.CosmeticMods.Length != 0)
            {
                cosmeticMods = item.CosmeticMods;
                foreach (ItemValue itemValue in cosmeticMods)
                {
                    if (itemValue != null && itemValue.type != 0)
                    {
                        Mods.Add(new BCMItemMod(itemValue));
                    }
                }
            }
            if (item.Modifications == null || item.Modifications.Length == 0)
            {
                return;
            }
            cosmeticMods = item.Modifications;
            foreach (ItemValue itemValue2 in cosmeticMods)
            {
                if (itemValue2 != null && itemValue2.type != 0)
                {
                    Mods.Add(new BCMItemMod(itemValue2));
                }
            }
        }
    }
}
