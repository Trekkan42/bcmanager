namespace BCM.Models
{
    public class BCMSpawnPoint
    {
        public BCMVector3 Pos;

        public BCMVector3 Look;

        public double Rot;

        public int Pose;

        public int BlockType;

        public BCMSpawnPoint(SleeperVolume.SpawnPoint spawn)
        {
            Pos = new BCMVector3(spawn.pos);
            Look = new BCMVector3(spawn.look);
            Rot = spawn.rot;
            Pose = spawn.pose;
            BlockType = spawn.blockType;
        }
    }
}
