using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMAdmins
    {
        public readonly List<BCMAdmin> Admins = new List<BCMAdmin>();

        public readonly List<BCMBan> Bans = new List<BCMBan>();

        public readonly List<BCMWhitelist> Whitelist = new List<BCMWhitelist>();

        public readonly List<BCMPermission> Permissions = new List<BCMPermission>();

        public BCMAdmins()
        {
            foreach (KeyValuePair<string, AdminToolsClientInfo> admin in GameManager.Instance.adminTools.GetAdmins())
            {
                Admins.Add(new BCMAdmin(admin.Value, SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.ForPlayerId(admin.Key)?.playerName ?? ""));
            }
            foreach (AdminToolsClientInfo item in GameManager.Instance.adminTools.GetBanned())
            {
                Bans.Add(new BCMBan(item));
            }
            foreach (KeyValuePair<string, AdminToolsClientInfo> whitelistedUser in GameManager.Instance.adminTools.GetWhitelistedUsers())
            {
                Whitelist.Add(new BCMWhitelist(whitelistedUser.Key, SingletonMonoBehaviour<ConnectionManager>.Instance.Clients.ForPlayerId(whitelistedUser.Key)?.playerName ?? ""));
            }
            foreach (KeyValuePair<string, AdminToolsCommandPermissions> command in GameManager.Instance.adminTools.GetCommands())
            {
                Permissions.Add(new BCMPermission(command.Value.Command, command.Value.PermissionLevel));
            }
        }
    }
}
