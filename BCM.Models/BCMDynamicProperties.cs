using System.Collections.Generic;
using System.Linq;

namespace BCM.Models
{
    public class BCMDynamicProperties
    {
        public List<string> Keys;

        public List<string> Values = new List<string>();

        public Dictionary<string, string> Params1 = new Dictionary<string, string>();

        public Dictionary<string, string> Params2 = new Dictionary<string, string>();

        public BCMDynamicProperties(DynamicProperties props)
        {
            Keys = props.Values.Dict.Dict.Keys.ToList();
            foreach (string key in Keys)
            {
                Values.Add(props.Values[key]);
            }
            foreach (string key2 in props.Params1.Dict.Keys)
            {
                Params1.Add(key2, props.Params1[key2]);
            }
            foreach (string key3 in props.Params2.Dict.Keys)
            {
                Params2.Add(key3, props.Params2[key3]);
            }
        }
    }
}
