using System.Collections.Generic;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMRuleset
    {
        public string Name;

        public string TerrainGen;

        public string BiomeGen;

        public int CellCache;

        public int CellSize;

        public double CellOffset;

        [NotNull]
        [UsedImplicitly]
        public Dictionary<string, BCMFilterData> CellRules = new Dictionary<string, BCMFilterData>();

        public BCMRuleset(Ruleset ruleset)
        {
            Name = ruleset.Name;
            TerrainGen = ruleset.TerrainGenerator;
            BiomeGen = ruleset.BiomeGenerator;
            CellCache = ruleset.CellCacheSize;
            CellSize = ruleset.CellSize;
            CellOffset = ruleset.CellOffset;
            foreach (KeyValuePair<string, FilterData> cellRule in ruleset.CellRules)
            {
                CellRules.Add(cellRule.Key, new BCMFilterData(cellRule.Value));
            }
        }
    }
}
