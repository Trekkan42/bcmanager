using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMBiomeSpawnRule
    {
        [UsedImplicitly]
        public string Name;

        [NotNull]
        [UsedImplicitly]
        public readonly List<BCMVector2d> BiomeList = new List<BCMVector2d>();

        [NotNull]
        [UsedImplicitly]
        public readonly List<BCMVector2> DistList = new List<BCMVector2>();

        [NotNull]
        [UsedImplicitly]
        public readonly List<BCMVector2> TerrainList = new List<BCMVector2>();

        public BCMBiomeSpawnRule([NotNull] BiomeSpawnRule spawnRule)
        {
            Name = spawnRule.Name;
            if (spawnRule.BiomeGenRanges != null)
            {
                foreach (Vector2 biomeGenRange in spawnRule.BiomeGenRanges)
                {
                    BiomeList.Add(new BCMVector2d(biomeGenRange));
                }
            }
            if (spawnRule.DistanceFromCenterRanges != null)
            {
                foreach (Vector2 distanceFromCenterRange in spawnRule.DistanceFromCenterRanges)
                {
                    DistList.Add(new BCMVector2(distanceFromCenterRange));
                }
            }
            if (spawnRule.TerrainGenRanges == null)
            {
                return;
            }
            foreach (Vector2 terrainGenRange in spawnRule.TerrainGenRanges)
            {
                TerrainList.Add(new BCMVector2(terrainGenRange));
            }
        }
    }
}
