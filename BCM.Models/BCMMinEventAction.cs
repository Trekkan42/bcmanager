using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMMinEventAction
    {
        public bool OrCompare;

        public string EventType;

        public List<BCMRequirement> Requirements = new List<BCMRequirement>();

        public BCMMinEventAction(MinEventActionBase action)
        {
            OrCompare = action.OrCompare;
            EventType = action.EventType.ToString();
            foreach (IRequirement requirement in action.Requirements)
            {
                Requirements.Add(new BCMRequirement(requirement));
            }
        }
    }
}
