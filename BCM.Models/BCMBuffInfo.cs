namespace BCM.Models
{
    public class BCMBuffInfo
    {
        public string Name;

        public string Duration;

        public string Percent;

        public BCMBuffInfo(BuffValue buff)
        {
            Name = buff.BuffClass.Name;
            Duration = $"{buff.DurationInSeconds:0}/{buff.BuffClass.DurationMax}(s)";
            Percent = $"{buff.DurationInSeconds / buff.BuffClass.DurationMax * 100f:0.0}%";
        }
    }
}
