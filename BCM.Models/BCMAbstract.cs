using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMAbstract
    {
        [NonSerialized]
        private Dictionary<string, string> _options;

        [NonSerialized]
        private Dictionary<string, object> _bin;

        [NonSerialized]
        private string _typeStr;

        [NonSerialized]
        private List<string> _strFilter;

        [NotNull]
        protected Dictionary<string, string> Options
        {
            get
            {
                return _options;
            }
            private set
            {
                _options = value;
            }
        }

        protected Dictionary<string, object> Bin
        {
            get
            {
                return _bin ?? (_bin = new Dictionary<string, object>());
            }
            set
            {
                _bin = value;
            }
        }

        protected string TypeStr
        {
            get
            {
                return _typeStr;
            }
            private set
            {
                _typeStr = value;
            }
        }

        [NotNull]
        protected List<string> StrFilter
        {
            get
            {
                return _strFilter ?? (_strFilter = new List<string>());
            }
            private set
            {
                _strFilter = value;
            }
        }

        protected BCMAbstract(object obj, string typeStr, [NotNull] Dictionary<string, string> options, [CanBeNull] List<string> filters)
        {
            TypeStr = typeStr;
            Options = options;
            StrFilter = filters ?? new List<string>();
            GetData(obj);
        }

        public Dictionary<string, object> Data()
        {
            return Bin;
        }

        protected virtual void GetData(object obj)
        {
        }

        internal bool IsOption(string key, bool isOr = true)
        {
            string[] array = key.Split(' ');
            if (array.Length <= 1)
            {
                return Options.ContainsKey(key);
            }
            bool flag = !isOr;
            string[] array2 = array;
            foreach (string key2 in array2)
            {
                flag = ((!isOr) ? (flag & Options.ContainsKey(key2)) : (flag | Options.ContainsKey(key2)));
            }
            return flag;
        }
    }
}
