using System;
using System.Collections.Generic;

namespace BCM.Models
{
    [Serializable]
    public class BCMMaterial : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Id = "id";

            public const string Experience = "exp";

            public const string MaxDamage = "maxdam";

            public const string MovementFactor = "movement";

            public const string FertileLevel = "fert";

            public const string Hardness = "hardness";

            public const string LightOpacity = "light";

            public const string Mass = "mass";

            public const string StabilityGlue = "stabglue";

            public const string StabilitySupport = "stabsupport";

            public const string DamageCategory = "damcat";

            public const string ForgeCategory = "forgecat";

            public const string ParticleCategory = "particle";

            public const string DestroyCategory = "destroycat";

            public const string SurfaceCategory = "surfacecat";

            public const string StepSound = "sound";

            public const string Resistance = "resistance";

            public const string IsCollidable = "collide";

            public const string IsGroundCover = "cover";

            public const string IsLiquid = "liquid";

            public const string IsPlant = "plant";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "id"
            },
            {
                1,
                "exp"
            },
            {
                2,
                "maxdam"
            },
            {
                3,
                "movement"
            },
            {
                4,
                "fert"
            },
            {
                5,
                "hardness"
            },
            {
                6,
                "light"
            },
            {
                7,
                "mass"
            },
            {
                8,
                "stabglue"
            },
            {
                9,
                "stabsupport"
            },
            {
                10,
                "damcat"
            },
            {
                11,
                "forgecat"
            },
            {
                12,
                "particle"
            },
            {
                13,
                "destroycat"
            },
            {
                14,
                "surfacecat"
            },
            {
                15,
                "sound"
            },
            {
                16,
                "resistance"
            },
            {
                17,
                "collide"
            },
            {
                18,
                "cover"
            },
            {
                19,
                "liquid"
            },
            {
                20,
                "plant"
            }
        };

        public string Id;

        public double Experience;

        public int MaxDamage;

        public double MovementFactor;

        public int FertileLevel;

        public double Hardness;

        public int LightOpacity;

        public int Mass;

        public int StabilityGlue;

        public bool StabilitySupport;

        public string DamageCategory;

        public string ForgeCategory;

        public string ParticleCategory;

        public string DestroyCategory;

        public string SurfaceCategory;

        public string StepSound;

        public double Resistance;

        public bool IsCollidable;

        public bool IsGroundCover;

        public bool IsLiquid;

        public bool IsPlant;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMMaterial(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            MaterialBlock materialBlock = obj as MaterialBlock;
            if (materialBlock == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "id":
                            GetId(materialBlock);
                            break;
                        case "exp":
                            GetExperience(materialBlock);
                            break;
                        case "maxdam":
                            GetMaxDamage(materialBlock);
                            break;
                        case "movement":
                            GetMovementFactor(materialBlock);
                            break;
                        case "fert":
                            GetFertileLevel(materialBlock);
                            break;
                        case "hardness":
                            GetHardness(materialBlock);
                            break;
                        case "light":
                            GetLightOpacity(materialBlock);
                            break;
                        case "mass":
                            GetMass(materialBlock);
                            break;
                        case "stabglue":
                            GetStabilityGlue(materialBlock);
                            break;
                        case "stabsupport":
                            GetStabilitySupport(materialBlock);
                            break;
                        case "damcat":
                            GetDamageCatagory(materialBlock);
                            break;
                        case "forgecat":
                            GetForgeCategory(materialBlock);
                            break;
                        case "particle":
                            GetParticleCategory(materialBlock);
                            break;
                        case "destroycat":
                            GetDestroyCategory(materialBlock);
                            break;
                        case "surfacecat":
                            GetSurfaceCategory(materialBlock);
                            break;
                        case "sound":
                            GetStepSound(materialBlock);
                            break;
                        case "resistance":
                            GetResistance(materialBlock);
                            break;
                        case "collide":
                            GetIsCollidable(materialBlock);
                            break;
                        case "cover":
                            GetIsGroundCover(materialBlock);
                            break;
                        case "liquid":
                            GetIsLiquid(materialBlock);
                            break;
                        case "plant":
                            GetIsPlant(materialBlock);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetId(materialBlock);
                GetExperience(materialBlock);
                GetMaxDamage(materialBlock);
                GetMovementFactor(materialBlock);
                GetFertileLevel(materialBlock);
                GetHardness(materialBlock);
                GetLightOpacity(materialBlock);
                GetMass(materialBlock);
                GetStabilityGlue(materialBlock);
                GetStabilitySupport(materialBlock);
                GetDamageCatagory(materialBlock);
                GetForgeCategory(materialBlock);
                GetParticleCategory(materialBlock);
                GetDestroyCategory(materialBlock);
                GetSurfaceCategory(materialBlock);
                GetStepSound(materialBlock);
                GetResistance(materialBlock);
                GetIsCollidable(materialBlock);
                GetIsGroundCover(materialBlock);
                GetIsLiquid(materialBlock);
                GetIsPlant(materialBlock);
            }
        }

        private void GetIsPlant(MaterialBlock material)
        {
            base.Bin.Add("IsPlant", IsPlant = material.IsPlant);
        }

        private void GetIsLiquid(MaterialBlock material)
        {
            base.Bin.Add("IsLiquid", IsLiquid = material.IsLiquid);
        }

        private void GetIsGroundCover(MaterialBlock material)
        {
            base.Bin.Add("IsGroundCover", IsGroundCover = material.IsGroundCover);
        }

        private void GetIsCollidable(MaterialBlock material)
        {
            base.Bin.Add("IsCollidable", IsCollidable = material.IsCollidable);
        }

        private void GetResistance(MaterialBlock material)
        {
            base.Bin.Add("Resistance", Resistance = Math.Round(material.Resistance, 3));
        }

        private void GetStepSound(MaterialBlock material)
        {
            base.Bin.Add("StepSound", StepSound = material.stepSound?.name);
        }

        private void GetSurfaceCategory(MaterialBlock material)
        {
            base.Bin.Add("SurfaceCategory", SurfaceCategory = material.SurfaceCategory);
        }

        private void GetDestroyCategory(MaterialBlock material)
        {
            base.Bin.Add("DestroyCategory", DestroyCategory = material.ParticleDestroyCategory);
        }

        private void GetParticleCategory(MaterialBlock material)
        {
            base.Bin.Add("ParticleCategory", ParticleCategory = material.ParticleCategory);
        }

        private void GetForgeCategory(MaterialBlock material)
        {
            base.Bin.Add("ForgeCategory", ForgeCategory = material.ForgeCategory);
        }

        private void GetDamageCatagory(MaterialBlock material)
        {
            base.Bin.Add("DamageCategory", DamageCategory = material.DamageCategory);
        }

        private void GetStabilitySupport(MaterialBlock material)
        {
            base.Bin.Add("StabilitySupport", StabilitySupport = material.StabilitySupport);
        }

        private void GetStabilityGlue(MaterialBlock material)
        {
            base.Bin.Add("StabilityGlue", StabilityGlue = material.StabilityGlue);
        }

        private void GetMass(MaterialBlock material)
        {
            base.Bin.Add("Mass", Mass = material.Mass.Value);
        }

        private void GetLightOpacity(MaterialBlock material)
        {
            base.Bin.Add("LightOpacity", LightOpacity = material.LightOpacity);
        }

        private void GetHardness(MaterialBlock material)
        {
            base.Bin.Add("Hardness", Hardness = material.Hardness.Value);
        }

        private void GetFertileLevel(MaterialBlock material)
        {
            base.Bin.Add("FertileLevel", FertileLevel = material.FertileLevel);
        }

        private void GetMovementFactor(MaterialBlock material)
        {
            base.Bin.Add("MovementFactor", MovementFactor = Math.Round(material.MovementFactor, 2));
        }

        private void GetMaxDamage(MaterialBlock material)
        {
            base.Bin.Add("MaxDamage", MaxDamage = material.MaxDamage);
        }

        private void GetExperience(MaterialBlock material)
        {
            base.Bin.Add("Experience", Experience = material.Experience);
        }

        private void GetId(MaterialBlock material)
        {
            base.Bin.Add("Id", Id = material.id);
        }
    }
}
