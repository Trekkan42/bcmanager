using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMRecipe : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Type = "type";

            public const string Count = "count";

            public const string CraftArea = "area";

            public const string CraftTime = "time";

            public const string CraftTool = "tool";

            public const string IsMatBased = "matbased";

            public const string IsScrappable = "scrappable";

            public const string IsWildForge = "wildforge";

            public const string Ingredients = "ingredients";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "name"
            },
            {
                1,
                "type"
            },
            {
                2,
                "count"
            },
            {
                3,
                "area"
            },
            {
                4,
                "time"
            },
            {
                5,
                "tool"
            },
            {
                6,
                "matbased"
            },
            {
                7,
                "scrappable"
            },
            {
                8,
                "wildforge"
            },
            {
                9,
                "ingredients"
            }
        };

        public string Name;

        public int Type;

        public int Count;

        public string CraftArea;

        public double CraftTime;

        public int CraftTool;

        public bool IsMatBased;

        public bool IsScrappable;

        public bool IsWildForge;

        [NotNull]
        [UsedImplicitly]
        public List<BCMIngredient> Ingredients = new List<BCMIngredient>();

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMRecipe(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            Recipe recipe = obj as Recipe;
            if (recipe == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            GetName(recipe);
                            break;
                        case "type":
                            GetType(recipe);
                            break;
                        case "count":
                            GetCount(recipe);
                            break;
                        case "area":
                            GetCraftArea(recipe);
                            break;
                        case "time":
                            GetCraftTime(recipe);
                            break;
                        case "tool":
                            GetCraftTool(recipe);
                            break;
                        case "matbased":
                            GetMatsBased(recipe);
                            break;
                        case "scrappable":
                            GetScrappable(recipe);
                            break;
                        case "wildforge":
                            GetWildForge(recipe);
                            break;
                        case "ingredients":
                            GetIngredients(recipe);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                if (!base.Options.ContainsKey("min"))
                {
                    GetName(recipe);
                }
                GetType(recipe);
                GetCount(recipe);
                GetCraftArea(recipe);
                GetCraftTime(recipe);
                GetCraftTool(recipe);
                GetMatsBased(recipe);
                GetScrappable(recipe);
                GetWildForge(recipe);
                GetIngredients(recipe);
            }
        }

        private void GetWildForge(Recipe recipe)
        {
            base.Bin.Add("IsWildForge", IsWildForge = recipe.wildcardForgeCategory);
        }

        private void GetScrappable(Recipe recipe)
        {
            base.Bin.Add("IsScrappable", IsScrappable = recipe.scrapable);
        }

        private void GetMatsBased(Recipe recipe)
        {
            base.Bin.Add("IsMatBased", IsMatBased = recipe.materialBasedRecipe);
        }

        private void GetCraftTool(Recipe recipe)
        {
            base.Bin.Add("CraftTool", CraftTool = recipe.craftingToolType);
        }

        private void GetCraftTime(Recipe recipe)
        {
            base.Bin.Add("CraftTime", CraftTime = Math.Round(recipe.craftingTime, 2));
        }

        private void GetCraftArea(Recipe recipe)
        {
            base.Bin.Add("CraftArea", CraftArea = recipe.craftingArea);
        }

        private void GetCount(Recipe recipe)
        {
            base.Bin.Add("Count", Count = recipe.count);
        }

        private void GetType(Recipe recipe)
        {
            base.Bin.Add("Type", Type = recipe.itemValueType);
        }

        private void GetName(Recipe recipe)
        {
            base.Bin.Add("Name", Name = recipe.GetName());
        }

        private void GetIngredients(Recipe recipe)
        {
            foreach (ItemStack ingredient in recipe.ingredients)
            {
                Ingredients.Add(new BCMIngredient(ingredient.itemValue.ItemClass.GetItemName(), ingredient.itemValue.type, ingredient.count));
            }
            if (base.Options.ContainsKey("min"))
            {
                base.Bin.Add("Ingredients", Ingredients.Select((BCMIngredient ing) => new int[2]
                {
                    ing.Type,
                    ing.Count
                }).Cast<object>().ToList());
            }
            else
            {
                base.Bin.Add("Ingredients", Ingredients);
            }
        }
    }
}
