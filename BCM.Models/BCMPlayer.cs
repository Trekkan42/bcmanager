using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace BCM.Models
{
    [Serializable]
    public class BCMPlayer : BCMAbstract
    {
        public static class StrFilters
        {
            public const string SteamId = "steamid";

            public const string Name = "name";

            public const string EntityId = "entityid";

            public const string Ip = "ip";

            public const string Ping = "ping";

            public const string SessionPlayTime = "session";

            public const string TotalPlayTime = "playtime";

            public const string LastOnline = "online";

            public const string Underground = "underground";

            public const string Position = "position";

            public const string Rotation = "rotation";

            public const string Health = "health";

            public const string MaxHealth = "maxhealth";

            public const string Stamina = "stamina";

            public const string MaxStamina = "maxstamina";

            public const string Food = "food";

            public const string Drink = "drink";

            public const string CoreTemp = "coretemp";

            public const string SpeedModifier = "speed";

            public const string LastZombieAttacked = "lastattack";

            public const string IsDead = "isdead";

            public const string OnGround = "onground";

            public const string IsStuck = "isstuck";

            public const string IsSafeZoneActive = "issafe";

            public const string Level = "level";

            public const string LevelProgress = "progress";

            public const string ExpToNextLevel = "tonext";

            public const string ExpForNextLevel = "fornext";

            public const string Gamestage = "gamestage";

            public const string Score = "score";

            public const string KilledPlayers = "pkill";

            public const string KilledZombies = "zkill";

            public const string Deaths = "deaths";

            public const string DistanceWalked = "walked";

            public const string ItemsCrafted = "crafted";

            public const string CurrentLife = "current";

            public const string LongestLife = "longest";

            public const string Archetype = "archetype";

            public const string DroppedPack = "pack";

            public const string RentedVendor = "vendor";

            public const string RentedVendorExpire = "vendorexpire";

            public const string Remote = "remote";

            public const string Bag = "bag";

            public const string Belt = "belt";

            public const string SelectedSlot = "selslot";

            public const string Equipment = "equip";

            public const string SkillPoints = "skillpts";

            public const string Skills = "skills";

            public const string Buffs = "buffs";

            public const string CraftingQueue = "crafting";

            public const string FavouriteRecipes = "favs";

            public const string UnlockedRecipes = "unlocks";

            public const string Quests = "quests";

            public const string Bedroll = "bedroll";

            public const string Waypoints = "waypoints";

            public const string Marker = "marker";

            public const string Friends = "friends";

            public const string LPBlocks = "lpblocks";

            public const string LastSaveSecs = "lastsave";
        }

        public class BCMIngredient
        {
            [UsedImplicitly]
            public int Type;

            [UsedImplicitly]
            public int Count;
        }

        public class BCMCraftingQueue
        {
            [UsedImplicitly]
            public int Type;

            [UsedImplicitly]
            public string Name;

            [UsedImplicitly]
            public int Count;

            [UsedImplicitly]
            public double TotalTime;

            [UsedImplicitly]
            public double CraftTime;

            [UsedImplicitly]
            public List<BCMIngredient> Ingredients;
        }

        public class BCMQuest
        {
            [UsedImplicitly]
            public string Name;

            [UsedImplicitly]
            public string Id;

            [UsedImplicitly]
            public string CurrentState;
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "steamid"
            },
            {
                1,
                "name"
            },
            {
                2,
                "entityid"
            },
            {
                3,
                "ip"
            },
            {
                4,
                "ping"
            },
            {
                5,
                "session"
            },
            {
                6,
                "playtime"
            },
            {
                7,
                "online"
            },
            {
                8,
                "underground"
            },
            {
                9,
                "position"
            },
            {
                10,
                "rotation"
            },
            {
                11,
                "health"
            },
            {
                12,
                "maxhealth"
            },
            {
                13,
                "stamina"
            },
            {
                14,
                "maxstamina"
            },
            {
                15,
                "food"
            },
            {
                16,
                "drink"
            },
            {
                17,
                "coretemp"
            },
            {
                18,
                "speed"
            },
            {
                19,
                "lastattack"
            },
            {
                20,
                "isdead"
            },
            {
                21,
                "onground"
            },
            {
                22,
                "isstuck"
            },
            {
                23,
                "issafe"
            },
            {
                24,
                "level"
            },
            {
                25,
                "progress"
            },
            {
                26,
                "tonext"
            },
            {
                27,
                "fornext"
            },
            {
                28,
                "gamestage"
            },
            {
                29,
                "score"
            },
            {
                30,
                "pkill"
            },
            {
                31,
                "zkill"
            },
            {
                32,
                "deaths"
            },
            {
                33,
                "walked"
            },
            {
                34,
                "crafted"
            },
            {
                35,
                "current"
            },
            {
                36,
                "longest"
            },
            {
                37,
                "archetype"
            },
            {
                38,
                "pack"
            },
            {
                39,
                "vendor"
            },
            {
                40,
                "vendorexpire"
            },
            {
                41,
                "remote"
            },
            {
                42,
                "bag"
            },
            {
                43,
                "belt"
            },
            {
                44,
                "selslot"
            },
            {
                45,
                "equip"
            },
            {
                46,
                "skillpts"
            },
            {
                47,
                "skills"
            },
            {
                48,
                "buffs"
            },
            {
                49,
                "crafting"
            },
            {
                50,
                "favs"
            },
            {
                51,
                "unlocks"
            },
            {
                52,
                "quests"
            },
            {
                53,
                "bedroll"
            },
            {
                54,
                "waypoints"
            },
            {
                55,
                "marker"
            },
            {
                56,
                "friends"
            },
            {
                57,
                "lpblocks"
            },
            {
                58,
                "lastsave"
            }
        };

        [UsedImplicitly]
        public string SteamId;

        [UsedImplicitly]
        public string Name;

        [UsedImplicitly]
        public int EntityId;

        [UsedImplicitly]
        public string Ip;

        [UsedImplicitly]
        public string Ping;

        [UsedImplicitly]
        public double SessionPlayTime;

        [UsedImplicitly]
        public double TotalPlayTime;

        [UsedImplicitly]
        public string LastOnline;

        [UsedImplicitly]
        public int Underground;

        [UsedImplicitly]
        public Vector3 Position;

        [UsedImplicitly]
        public Vector3 Rotation;

        [UsedImplicitly]
        public int Health;

        [UsedImplicitly]
        public int MaxHealth;

        [UsedImplicitly]
        public int Stamina;

        [UsedImplicitly]
        public int MaxStamina;

        [UsedImplicitly]
        public int Wellness;

        [UsedImplicitly]
        public int Food;

        [UsedImplicitly]
        public int Drink;

        [UsedImplicitly]
        public int CoreTemp;

        [UsedImplicitly]
        public double SpeedModifier;

        [UsedImplicitly]
        public double? LastZombieAttacked;

        [UsedImplicitly]
        public bool IsDead;

        [UsedImplicitly]
        public bool? OnGround;

        [UsedImplicitly]
        public bool? IsStuck;

        [UsedImplicitly]
        public bool? IsSafeZoneActive;

        [UsedImplicitly]
        public int Level;

        [UsedImplicitly]
        public double LevelProgress;

        [UsedImplicitly]
        public int ExpToNextLevel;

        [UsedImplicitly]
        public int ExpForNextLevel;

        [UsedImplicitly]
        public int? Gamestage;

        [UsedImplicitly]
        public int Score;

        [UsedImplicitly]
        public int KilledPlayers;

        [UsedImplicitly]
        public int KilledZombies;

        [UsedImplicitly]
        public int Deaths;

        [UsedImplicitly]
        public double DistanceWalked;

        [UsedImplicitly]
        public uint ItemsCrafted;

        [UsedImplicitly]
        public double CurrentLife;

        [UsedImplicitly]
        public double LongestLife;

        [UsedImplicitly]
        public string Archetype;

        [UsedImplicitly]
        public Vector3 DroppedPack;

        [UsedImplicitly]
        public Vector3 RentedVendor;

        [UsedImplicitly]
        public ulong RentedVendorExpire;

        [UsedImplicitly]
        public bool? Remote;

        [UsedImplicitly]
        public Dictionary<string, BCMItemStack> Bag;

        [UsedImplicitly]
        public Dictionary<string, BCMItemStack> Belt;

        [UsedImplicitly]
        public int SelectedSlot;

        [UsedImplicitly]
        public Dictionary<string, BCMItemValue> Equipment;

        [UsedImplicitly]
        public int SkillPoints;

        [UsedImplicitly]
        public List<BCMCraftingQueue> CraftingQueue;

        [UsedImplicitly]
        public List<string> FavouriteRecipes;

        [UsedImplicitly]
        public List<string> UnlockedRecipes;

        [UsedImplicitly]
        public List<BCMQuest> Quests;

        [UsedImplicitly]
        public Vector3 Bedroll;

        [UsedImplicitly]
        public List<BCMWaypoint> Waypoints;

        [UsedImplicitly]
        public Vector3 Marker;

        [UsedImplicitly]
        public List<string> Friends = new List<string>();

        [UsedImplicitly]
        public List<Vector3> LPBlocks = new List<Vector3>();

        [UsedImplicitly]
        public double? LastSaveSecs;

        private Progression progression;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMPlayer(object obj, Dictionary<string, string> options, List<string> filters)
            : base(obj, "Entity", options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            if (obj == null)
            {
                return;
            }
            PlayerInfo pInfo = (PlayerInfo)obj;
            if (pInfo.PDF == null && (pInfo.EP == null || pInfo.WPD == null))
            {
                return;
            }
            if (IsOption("filter"))
            {
                using List<string>.Enumerator enumerator = base.StrFilter.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    switch (enumerator.Current)
                    {
                        case "steamid":
                            GetSteamId(pInfo);
                            break;
                        case "name":
                            GetName(pInfo);
                            break;
                        case "entityid":
                            GetEntityId(pInfo);
                            break;
                        case "ip":
                            GetIp(pInfo);
                            break;
                        case "ping":
                            GetPing(pInfo);
                            break;
                        case "playtime":
                            GetTotalPlaytime(pInfo);
                            break;
                        case "session":
                            GetSessionPlaytime(pInfo);
                            break;
                        case "online":
                            GetLastOnline(pInfo);
                            break;
                        case "underground":
                            GetUnderground(pInfo);
                            break;
                        case "position":
                            GetPosition(pInfo);
                            break;
                        case "rotation":
                            GetRotation(pInfo);
                            break;
                        case "health":
                            GetHealth(pInfo);
                            break;
                        case "maxhealth":
                            GetMaxHealth(pInfo);
                            break;
                        case "stamina":
                            GetStamina(pInfo);
                            break;
                        case "maxstamina":
                            GetMaxStamina(pInfo);
                            break;
                        case "food":
                            GetFood(pInfo);
                            break;
                        case "drink":
                            GetDrink(pInfo);
                            break;
                        case "coretemp":
                            GetCoreTemp(pInfo);
                            break;
                        case "archetype":
                            GetArchetype(pInfo);
                            break;
                        case "walked":
                            GetDistanceWalked(pInfo);
                            break;
                        case "pack":
                            GetDroppedPack(pInfo);
                            break;
                        case "level":
                            GetLevel(pInfo);
                            break;
                        case "progress":
                            GetLevelProgress(pInfo);
                            break;
                        case "tonext":
                            GetExpToNextLevel(pInfo);
                            break;
                        case "fornext":
                            GetExpForNextLevel(pInfo);
                            break;
                        case "gamestage":
                            GetGamestage(pInfo);
                            break;
                        case "score":
                            GetScore(pInfo);
                            break;
                        case "pkill":
                            GetKilledPlayers(pInfo);
                            break;
                        case "zkill":
                            GetKilledZombies(pInfo);
                            break;
                        case "deaths":
                            GetDeaths(pInfo);
                            break;
                        case "current":
                            GetCurrentLife(pInfo);
                            break;
                        case "longest":
                            GetLongestLife(pInfo);
                            break;
                        case "crafted":
                            GetItemsCrafted(pInfo);
                            break;
                        case "isdead":
                            GetIsDead(pInfo);
                            break;
                        case "onground":
                            GetOnGround(pInfo);
                            break;
                        case "isstuck":
                            GetIsStuck(pInfo);
                            break;
                        case "issafe":
                            GetIsSafeZoneActive(pInfo);
                            break;
                        case "remote":
                            GetRemote(pInfo);
                            break;
                        case "lastattack":
                            GetLastZombieAttacked(pInfo);
                            break;
                        case "vendor":
                            GetRentedVendor(pInfo);
                            break;
                        case "vendorexpire":
                            GetRentedVendorExpire(pInfo);
                            break;
                        case "bag":
                            GetBag(pInfo);
                            break;
                        case "belt":
                            GetBelt(pInfo);
                            break;
                        case "equip":
                            GetEquipment(pInfo);
                            break;
                        case "skillpts":
                            GetSkillPoints(pInfo);
                            break;
                        case "skills":
                            GetSkills(pInfo);
                            break;
                        case "buffs":
                            GetBuffs(pInfo);
                            break;
                        case "crafting":
                            GetCraftingQueue(pInfo);
                            break;
                        case "favs":
                            GetFavouriteRecipes(pInfo);
                            break;
                        case "unlocks":
                            GetUnlockedRecipes(pInfo);
                            break;
                        case "quests":
                            GetQuests(pInfo);
                            break;
                        case "bedroll":
                            GetBedroll(pInfo);
                            break;
                        case "waypoints":
                            GetWaypoints(pInfo);
                            break;
                        case "marker":
                            GetMarker(pInfo);
                            break;
                        case "friends":
                            GetFriends(pInfo);
                            break;
                        case "lpblocks":
                            GetLpBlocks(pInfo);
                            break;
                        case "lastsave":
                            GetLastSaveSecs(pInfo);
                            break;
                    }
                }
            }
            else
            {
                GetSteamId(pInfo);
                GetName(pInfo);
                GetEntityId(pInfo);
                GetIp(pInfo);
                GetPing(pInfo);
                GetTotalPlaytime(pInfo);
                GetSessionPlaytime(pInfo);
                GetLastOnline(pInfo);
                GetUnderground(pInfo);
                GetPosition(pInfo);
                GetRotation(pInfo);
                GetHealth(pInfo);
                GetMaxHealth(pInfo);
                GetStamina(pInfo);
                GetMaxStamina(pInfo);
                GetFood(pInfo);
                GetDrink(pInfo);
                GetCoreTemp(pInfo);
                GetArchetype(pInfo);
                GetDistanceWalked(pInfo);
                GetDroppedPack(pInfo);
                GetLevel(pInfo);
                GetLevelProgress(pInfo);
                GetExpToNextLevel(pInfo);
                GetExpForNextLevel(pInfo);
                GetGamestage(pInfo);
                GetScore(pInfo);
                GetKilledPlayers(pInfo);
                GetKilledZombies(pInfo);
                GetDeaths(pInfo);
                GetCurrentLife(pInfo);
                GetLongestLife(pInfo);
                GetItemsCrafted(pInfo);
                GetIsDead(pInfo);
                GetOnGround(pInfo);
                GetIsStuck(pInfo);
                GetIsSafeZoneActive(pInfo);
                GetRemote(pInfo);
                GetLastZombieAttacked(pInfo);
                GetRentedVendor(pInfo);
                GetRentedVendorExpire(pInfo);
                GetLastSaveSecs(pInfo);
                if (base.Options.ContainsKey("full"))
                {
                    GetBag(pInfo);
                    GetBelt(pInfo);
                    GetEquipment(pInfo);
                    GetSkillPoints(pInfo);
                    GetSkills(pInfo);
                    GetCraftingQueue(pInfo);
                    GetFavouriteRecipes(pInfo);
                    GetUnlockedRecipes(pInfo);
                    GetQuests(pInfo);
                    GetBedroll(pInfo);
                    GetWaypoints(pInfo);
                    GetMarker(pInfo);
                    GetFriends(pInfo);
                    GetLpBlocks(pInfo);
                }
            }
        }

        private Progression GetProgression(PlayerInfo pInfo)
        {
            if (pInfo.EP != null || progression != null)
            {
                return progression;
            }
            progression = new Progression();
            if (pInfo.PDF == null || pInfo.PDF.progressionData.Length <= 0)
            {
                return progression;
            }
            using (PooledBinaryReader pooledBinaryReader = MemoryPools.poolBinaryReader.AllocSync(_bReset: false))
            {
                pooledBinaryReader.SetBaseStream(pInfo.PDF.progressionData);
                progression = Progression.Read(pooledBinaryReader, null);
            }
            return progression;
        }

        private void GetLpBlocks(PlayerInfo pInfo)
        {
            base.Bin.Add("LPBlocks", LPBlocks = pInfo.PPD?.LPBlocks?.Select((Vector3i lpb) => lpb.ToVector3()).ToList());
        }

        private void GetFriends(PlayerInfo pInfo)
        {
            base.Bin.Add("Friends", Friends = pInfo.PPD?.ACL?.ToList());
        }

        private void GetRotation(PlayerInfo pInfo)
        {
            base.Bin.Add("Rotation", Rotation = ((pInfo.EP != null) ? pInfo.EP.rotation : (pInfo.PDF?.ecd.rot ?? default(Vector3))));
        }

        private void GetPosition(PlayerInfo pInfo)
        {
            base.Bin.Add("Position", Position = ((pInfo.EP != null) ? pInfo.EP.position : (pInfo.PDF?.ecd.pos ?? default(Vector3))));
        }

        private void GetUnderground(PlayerInfo pInfo)
        {
            base.Bin.Add("Underground", Underground = ((pInfo.EP != null) ? ((int)pInfo.EP.position.y - GameManager.Instance.World.GetHeight((int)pInfo.EP.position.x, (int)pInfo.EP.position.z) - 1) : 0));
        }

        private void GetLastSaveSecs(PlayerInfo pInfo)
        {
            base.Bin.Add("LastSaveSecs", LastSaveSecs = ((!(pInfo.EP != null)) ? null : ((pInfo.WPD != null && pInfo.WPD.LastSaveUtc.Ticks != 0L) ? new double?((DateTime.UtcNow - pInfo.WPD.LastSaveUtc).TotalMilliseconds / 1000.0) : null)));
        }

        private void GetLastOnline(PlayerInfo pInfo)
        {
            base.Bin.Add("LastOnline", LastOnline = ((!(pInfo.EP == null)) ? "" : (pInfo.WPD?.LastOnline.ToUtcStr() ?? "")));
        }

        private void GetSessionPlaytime(PlayerInfo pInfo)
        {
            base.Bin.Add("SessionPlayTime", (pInfo.EP != null) ? (SessionPlayTime = Math.Round((Time.timeSinceLevelLoad - pInfo.EP.CreationTimeSinceLevelLoad) / 60f, 2)) : 0.0);
        }

        private void GetTotalPlaytime(PlayerInfo pInfo)
        {
            base.Bin.Add("TotalPlayTime", TotalPlayTime = Math.Round((float)(pInfo.WPD?.TotalPlayTime ?? 0) / 60f, 2));
        }

        private void GetPing(PlayerInfo pInfo)
        {
            base.Bin.Add("Ping", Ping = ((pInfo.CI != null) ? pInfo.CI.ping.ToString() : "Offline"));
        }

        private void GetIp(PlayerInfo pInfo)
        {
            base.Bin.Add("IP", Ip = ((pInfo.CI != null) ? pInfo.CI.ip : ((pInfo.WPD != null) ? pInfo.WPD.Ip : string.Empty)));
        }

        private void GetEntityId(PlayerInfo pInfo)
        {
            base.Bin.Add("EntityId", EntityId = ((pInfo.EP != null) ? pInfo.EP.entityId : (pInfo.PDF?.id ?? (-1))));
        }

        private void GetName(PlayerInfo pInfo)
        {
            base.Bin.Add("Name", Name = ((pInfo.CI != null) ? pInfo.CI.playerName : (pInfo.SPD?.Name ?? string.Empty)));
        }

        private void GetSteamId(PlayerInfo pInfo)
        {
            base.Bin.Add("SteamId", SteamId = pInfo.SteamId);
        }

        private void GetRentedVendorExpire(PlayerInfo pInfo)
        {
            base.Bin.Add("RentedVendorExpire", RentedVendorExpire = ((pInfo.EP != null) ? pInfo.EP.RentalEndTime : (pInfo.PDF?.rentalEndTime ?? 0)));
        }

        private void GetRentedVendor(PlayerInfo pInfo)
        {
            base.Bin.Add("RentedVendor", RentedVendor = ((pInfo.EP != null) ? pInfo.EP.RentedVMPosition.ToVector3() : (pInfo.PDF?.rentedVMPosition.ToVector3() ?? default(Vector3))));
        }

        private void GetLastZombieAttacked(PlayerInfo pInfo)
        {
            base.Bin.Add("LastZombieAttacked", (pInfo.EP != null) ? (LastZombieAttacked = Math.Round((float)(GameManager.Instance.World.worldTime - pInfo.EP.LastZombieAttackTime) / 600f, 2)) : null);
        }

        private void GetRemote(PlayerInfo pInfo)
        {
            base.Bin.Add("Remote", (pInfo.EP != null) ? (Remote = pInfo.EP.isEntityRemote) : null);
        }

        private void GetIsSafeZoneActive(PlayerInfo pInfo)
        {
            base.Bin.Add("IsSafeZoneActive", (pInfo.EP != null) ? (IsSafeZoneActive = pInfo.EP.IsSafeZoneActive()) : null);
        }

        private void GetIsStuck(PlayerInfo pInfo)
        {
            base.Bin.Add("IsStuck", (pInfo.EP != null) ? (IsStuck = pInfo.EP.IsStuck) : null);
        }

        private void GetOnGround(PlayerInfo pInfo)
        {
            base.Bin.Add("OnGround", (pInfo.EP != null) ? (OnGround = pInfo.EP.onGround) : null);
        }

        private void GetIsDead(PlayerInfo pInfo)
        {
            base.Bin.Add("IsDead", IsDead = ((pInfo.EP != null) ? pInfo.EP.IsDead() : (pInfo.PDF != null && pInfo.PDF.bDead)));
        }

        private void GetItemsCrafted(PlayerInfo pInfo)
        {
            base.Bin.Add("ItemsCrafted", ItemsCrafted = ((pInfo.EP != null) ? pInfo.EP.totalItemsCrafted : (pInfo.PDF?.totalItemsCrafted ?? 0)));
        }

        private void GetLongestLife(PlayerInfo pInfo)
        {
            base.Bin.Add("LongestLife", LongestLife = Math.Round((pInfo.EP != null) ? pInfo.EP.longestLife : (pInfo.PDF?.longestLife ?? 0f), 2));
        }

        private void GetCurrentLife(PlayerInfo pInfo)
        {
            base.Bin.Add("CurrentLife", CurrentLife = Math.Round((pInfo.EP != null) ? pInfo.EP.currentLife : (pInfo.PDF?.currentLife ?? 0f), 2));
        }

        private void GetDeaths(PlayerInfo pInfo)
        {
            base.Bin.Add("Deaths", Deaths = ((pInfo.EP != null) ? pInfo.EP.Died : (pInfo.PDF?.deaths ?? 0)));
        }

        private void GetKilledZombies(PlayerInfo pInfo)
        {
            base.Bin.Add("KilledZombies", KilledZombies = ((pInfo.EP != null) ? pInfo.EP.KilledZombies : (pInfo.PDF?.zombieKills ?? 0)));
        }

        private void GetKilledPlayers(PlayerInfo pInfo)
        {
            base.Bin.Add("KilledPlayers", KilledPlayers = ((pInfo.EP != null) ? pInfo.EP.KilledPlayers : (pInfo.PDF?.playerKills ?? 0)));
        }

        private void GetScore(PlayerInfo pInfo)
        {
            base.Bin.Add("Score", Score = ((pInfo.EP != null) ? pInfo.EP.Score : (pInfo.PDF?.score ?? 0)));
        }

        private void GetGamestage(PlayerInfo pInfo)
        {
            base.Bin.Add("Gamestage", Gamestage = ((pInfo.EP != null) ? new int?(pInfo.EP.gameStage) : pInfo.WPD?.Gamestage));
        }

        private void GetExpForNextLevel(PlayerInfo pInfo)
        {
            base.Bin.Add("ExpForNextLevel", ExpForNextLevel = ((pInfo.EP != null) ? pInfo.EP.Progression.GetExpForNextLevel() : GetProgression(pInfo).GetExpForNextLevel()));
        }

        private void GetExpToNextLevel(PlayerInfo pInfo)
        {
            base.Bin.Add("ExpToNextLevel", ExpToNextLevel = ((pInfo.EP != null) ? pInfo.EP.Progression.ExpToNextLevel : GetProgression(pInfo).ExpToNextLevel));
        }

        private void GetLevelProgress(PlayerInfo pInfo)
        {
            base.Bin.Add("LevelProgress", LevelProgress = Math.Round((pInfo.EP != null) ? (pInfo.EP.Progression.GetLevelProgressPercentage() * 100f) : (GetProgression(pInfo).GetLevelProgressPercentage() * 100f)));
        }

        private void GetLevel(PlayerInfo pInfo)
        {
            base.Bin.Add("Level", Level = ((pInfo.EP != null) ? pInfo.EP.Progression.GetLevel() : GetProgression(pInfo).Level));
        }

        private void GetDroppedPack(PlayerInfo pInfo)
        {
            base.Bin.Add("DroppedPack", DroppedPack = ((pInfo.EP != null) ? pInfo.EP.GetDroppedBackpackPosition().ToVector3() : (pInfo.PDF?.droppedBackpackPosition.ToVector3() ?? default(Vector3))));
        }

        private void GetDistanceWalked(PlayerInfo pInfo)
        {
            base.Bin.Add("DistanceWalked", DistanceWalked = Math.Round((pInfo.EP != null) ? pInfo.EP.distanceWalked : (pInfo.PDF?.distanceWalked ?? 0f), 1));
        }

        private void GetArchetype(PlayerInfo pInfo)
        {
            base.Bin.Add("Archetype", Archetype = ((pInfo.EP != null) ? pInfo.EP.playerProfile.Archetype : ((pInfo.PDF != null) ? pInfo.PDF.ecd.playerProfile.Archetype : string.Empty)));
        }

        private void GetEquipment(PlayerInfo pInfo)
        {
            Equipment = new Dictionary<string, BCMItemValue>();
            ItemValue[] obj = ((pInfo.EP != null) ? pInfo.EP.equipment.GetItems() : ((pInfo.PDF != null) ? pInfo.PDF.equipment.GetItems() : new ItemValue[0]));
            int num = -1;
            ItemValue[] array = obj;
            foreach (ItemValue itemValue in array)
            {
                num++;
                if (itemValue?.ItemClass != null)
                {
                    Equipment.Add($"{num}", (itemValue.type == 0) ? null : new BCMItemValue(itemValue));
                }
            }
            base.Bin.Add("Equipment", Equipment);
        }

        private void GetBelt(PlayerInfo pInfo)
        {
            Belt = new Dictionary<string, BCMItemStack>();
            ItemStack[] array;
            if (pInfo.EP != null)
            {
                array = pInfo.EP.inventory.GetSlots();
                SelectedSlot = pInfo.EP.inventory.holdingItemIdx;
            }
            else
            {
                array = ((pInfo.PDF != null) ? pInfo.PDF.inventory : new ItemStack[0]);
                SelectedSlot = pInfo.PDF?.selectedInventorySlot ?? (-1);
            }
            int num = 0;
            ItemStack[] array2 = array;
            foreach (ItemStack itemStack in array2)
            {
                BCMItemStack value = null;
                if (itemStack.itemValue.type != 0)
                {
                    value = new BCMItemStack(itemStack);
                }
                Belt.Add(num.ToString(), value);
                num++;
            }
            base.Bin.Add("SelectedSlot", SelectedSlot);
            base.Bin.Add("Belt", Belt);
        }

        private void GetSkills(PlayerInfo pInfo)
        {
            base.Bin.Add("Skills", null);
        }

        private void GetBuffs(PlayerInfo pInfo)
        {
            base.Bin.Add("Buffs", null);
        }

        private void GetBedroll(PlayerInfo pInfo)
        {
            base.Bin.Add("Bedroll", Bedroll = ((pInfo.EP != null && pInfo.EP.SpawnPoints != null && pInfo.EP.SpawnPoints.GetPos().y != int.MaxValue) ? pInfo.EP.SpawnPoints.GetPos().ToVector3() : ((pInfo.PPD != null && pInfo.PPD.BedrollPos.y != int.MaxValue) ? pInfo.PPD.BedrollPos.ToVector3() : new Vector3(0f, 0f, 0f))));
        }

        private void GetStamina(PlayerInfo pInfo)
        {
            base.Bin.Add("Stamina", Stamina = (int)((pInfo.EP != null) ? XUiM_Player.GetStamina(pInfo.EP) : (pInfo.PDF?.ecd.stats.Stamina.Value ?? 0f)));
        }

        private void GetMaxStamina(PlayerInfo pInfo)
        {
            base.Bin.Add("MaxStamina", MaxStamina = (int)((pInfo.EP != null) ? XUiM_Player.GetMaxStamina(pInfo.EP) : (pInfo.PDF?.ecd.stats.Stamina.Max ?? 0f)));
        }

        private void GetHealth(PlayerInfo pInfo)
        {
            base.Bin.Add("Health", Health = (int)((pInfo.EP != null) ? XUiM_Player.GetHealth(pInfo.EP) : (pInfo.PDF?.ecd.stats.Health.Value ?? 0f)));
        }

        private void GetMaxHealth(PlayerInfo pInfo)
        {
            base.Bin.Add("MaxHealth", MaxHealth = (int)((pInfo.EP != null) ? XUiM_Player.GetHealth(pInfo.EP) : (pInfo.PDF?.ecd.stats.Health.Value ?? 0f)));
        }

        private void GetCoreTemp(PlayerInfo pInfo)
        {
            base.Bin.Add("CoreTemp", CoreTemp = (int)((pInfo.EP != null) ? ((float)WeatherManager.BaseTemperature + pInfo.EP.Buffs.GetCustomVar("_coretemp")) : 0f));
        }

        private void GetDrink(PlayerInfo pInfo)
        {
            Dictionary<string, object> bin = base.Bin;
            float num;
            if (!(pInfo.EP != null))
            {
                PlayerDataFile pDF = pInfo.PDF;
                num = ((pDF != null) ? (pDF.ecd.stats.Water.ValuePercentUI * 100f) : 0f);
            }
            else
            {
                num = XUiM_Player.GetWater(pInfo.EP);
            }
            bin.Add("Drink", Drink = (int)num);
        }

        private void GetFood(PlayerInfo pInfo)
        {
            base.Bin.Add("Food", Food = (int)((pInfo.EP != null) ? XUiM_Player.GetFood(pInfo.EP) : ((pInfo.PDF != null) ? ((1f - (pInfo.PDF.ecd.stats.Stamina.Max - pInfo.PDF.ecd.stats.Stamina.ModifiedMax) / pInfo.PDF.ecd.stats.Stamina.Max) * 100f) : 0f)));
        }

        private void GetWaypoints(PlayerInfo pInfo)
        {
            Waypoints = new List<BCMWaypoint>();
            foreach (Waypoint item in (pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.waypoints.List : ((pInfo.PDF != null) ? pInfo.PDF.waypoints.List : new List<Waypoint>()))
            {
                Waypoints.Add(new BCMWaypoint(item));
            }
            base.Bin.Add("Waypoints", Waypoints);
        }

        private void GetMarker(PlayerInfo pInfo)
        {
            base.Bin.Add("Marker", Marker = ((pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.markerPosition.ToVector3() : (pInfo.PDF?.markerPosition.ToVector3() ?? default(Vector3))));
        }

        private void GetQuests(PlayerInfo pInfo)
        {
            Quests = new List<BCMQuest>();
            foreach (Quest item in (pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.questJournal.Clone().quests : ((pInfo.PDF != null) ? pInfo.PDF.questJournal.Clone().quests : new List<Quest>()))
            {
                BCMQuest bCMQuest = new BCMQuest();
                if (QuestClass.s_Quests.ContainsKey(item.ID))
                {
                    QuestClass questClass = QuestClass.s_Quests[item.ID];
                    bCMQuest.Name = questClass.Name;
                    bCMQuest.Id = questClass.ID;
                    bCMQuest.CurrentState = item.CurrentState.ToString();
                }
                else
                {
                    bCMQuest.Name = null;
                }
                Quests.Add(bCMQuest);
            }
            base.Bin.Add("Quests", Quests);
        }

        private void GetUnlockedRecipes(PlayerInfo pInfo)
        {
            UnlockedRecipes = new List<string>();
            foreach (string item in (pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.unlockedRecipeList : ((pInfo.PDF != null) ? pInfo.PDF.unlockedRecipeList : new List<string>()))
            {
                UnlockedRecipes.Add(item);
            }
            base.Bin.Add("UnlockedRecipes", UnlockedRecipes);
        }

        private void GetFavouriteRecipes(PlayerInfo pInfo)
        {
            FavouriteRecipes = new List<string>();
            foreach (string item in (pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.favoriteRecipeList : ((pInfo.PDF != null) ? pInfo.PDF.favoriteRecipeList : new List<string>()))
            {
                FavouriteRecipes.Add(item);
            }
            base.Bin.Add("FavouriteRecipes", FavouriteRecipes);
        }

        private void GetCraftingQueue(PlayerInfo pInfo)
        {
            CraftingQueue = new List<BCMCraftingQueue>();
            RecipeQueueItem[] array = ((pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.craftingData.RecipeQueueItems : ((pInfo.PDF != null) ? pInfo.PDF.craftingData.RecipeQueueItems : new RecipeQueueItem[0]));
            foreach (RecipeQueueItem recipeQueueItem in array)
            {
                BCMCraftingQueue bCMCraftingQueue = new BCMCraftingQueue();
                if (recipeQueueItem.Recipe != null)
                {
                    bCMCraftingQueue.Type = recipeQueueItem.Recipe.itemValueType;
                    bCMCraftingQueue.Name = recipeQueueItem.Recipe.GetName();
                    bCMCraftingQueue.Count = recipeQueueItem.Multiplier;
                    if (recipeQueueItem.IsCrafting)
                    {
                        bCMCraftingQueue.TotalTime = Math.Round(recipeQueueItem.Recipe.craftingTime * (float)(recipeQueueItem.Multiplier - 1) + recipeQueueItem.CraftingTimeLeft, 1);
                        bCMCraftingQueue.CraftTime = Math.Round(recipeQueueItem.CraftingTimeLeft, 1);
                    }
                    else
                    {
                        bCMCraftingQueue.TotalTime = Math.Round(recipeQueueItem.Recipe.craftingTime * (float)recipeQueueItem.Multiplier, 1);
                        bCMCraftingQueue.CraftTime = Math.Round(recipeQueueItem.Recipe.craftingTime, 1);
                    }
                    bCMCraftingQueue.Ingredients = new List<BCMIngredient>();
                    foreach (ItemStack item2 in recipeQueueItem.Recipe.GetIngredientsSummedUp())
                    {
                        BCMIngredient item = new BCMIngredient
                        {
                            Type = item2.itemValue.type,
                            Count = item2.count
                        };
                        bCMCraftingQueue.Ingredients.Add(item);
                    }
                }
                CraftingQueue.Add(bCMCraftingQueue);
            }
            base.Bin.Add("CraftingQueue", CraftingQueue);
        }

        private void GetSkillPoints(PlayerInfo pInfo)
        {
            base.Bin.Add("SkillPoints", SkillPoints = ((pInfo.EP != null) ? pInfo.EP.Progression.SkillPoints : GetProgression(pInfo).SkillPoints));
        }

        private void GetBag(PlayerInfo pInfo)
        {
            Bag = new Dictionary<string, BCMItemStack>();
            int num = 1;
            ItemStack[] array = ((pInfo.EP != null && pInfo.WPD != null) ? pInfo.WPD.DataCache.bag : ((pInfo.PDF != null) ? pInfo.PDF.bag : new ItemStack[0]));
            foreach (ItemStack itemStack in array)
            {
                BCMItemStack value = null;
                if (itemStack.itemValue.type != 0)
                {
                    value = new BCMItemStack(itemStack);
                }
                Bag.Add(num.ToString(), value);
                num++;
            }
            base.Bin.Add("Bag", Bag);
        }
    }
}
