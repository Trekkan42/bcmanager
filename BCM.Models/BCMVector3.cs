using System;
using UnityEngine;

namespace BCM.Models
{
    [Serializable]
    public class BCMVector3
    {
        public readonly int x;

        public readonly int y;

        public readonly int z;

        public BCMVector3(Vector3 v)
        {
            x = (int)Math.Floor(v.x);
            y = (int)Math.Floor(v.y);
            z = (int)Math.Floor(v.z);
        }

        public BCMVector3(Vector3i v)
        {
            x = v.x;
            y = v.y;
            z = v.z;
        }

        public BCMVector3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString()
        {
            return $"{x} {y} {z}";
        }

        public static BCMVector3 V3One()
        {
            return new BCMVector3(1, 1, 1);
        }

        public static bool V3One(BCMVector3 compare)
        {
            return compare.Equals(V3One());
        }

        public Vector3i V3i()
        {
            return new Vector3i(x, y, z);
        }

        public Vector3 V3()
        {
            return new Vector3((float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f);
        }

        public bool Equals(BCMVector3 obj)
        {
            if (obj != null && x == obj.x && y == obj.y)
            {
                return z == obj.z;
            }
            return false;
        }
    }
}
