using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMQuestAction
    {
        public string Type;

        public string Id;

        public string Value;

        public BCMQuestAction([NotNull] BaseQuestAction action)
        {
            Type = action.GetType().ToString();
            Id = action.ID;
            Value = action.Value;
        }
    }
}
