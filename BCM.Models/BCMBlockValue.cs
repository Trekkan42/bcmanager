using System;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMBlockValue
    {
        [UsedImplicitly]
        public int Type;

        [UsedImplicitly]
        public byte Rotation;

        [UsedImplicitly]
        public byte Meta;

        [UsedImplicitly]
        public byte Meta2;

        public BCMBlockValue(uint rawData)
        {
            BlockValue blockValue = new BlockValue(rawData);
            Type = blockValue.type;
            Rotation = blockValue.rotation;
            Meta = blockValue.meta;
            Meta2 = blockValue.meta2;
        }
    }
}
