using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMSubBiome
    {
        public byte Id;

        public string Name;

        public double Freq;

        public int Depth;

        public double Prob;

        [NotNull]
        [UsedImplicitly]
        public List<BCMBiomeLayer> Layers = new List<BCMBiomeLayer>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMBiomeBlockDecoration> DecoBlocks = new List<BCMBiomeBlockDecoration>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMBiomePrefabDecoration> DecoPrefabs = new List<BCMBiomePrefabDecoration>();

        public BCMSubBiome([NotNull] BiomeDefinition sub)
        {
            Id = sub.m_Id;
            Name = sub.m_sBiomeName;
            Freq = Math.Round(sub.freq, 6);
            Depth = sub.TotalLayerDepth;
            Prob = Math.Round(sub.prob, 6);
            foreach (BiomeLayer layer in sub.m_Layers)
            {
                Layers.Add(new BCMBiomeLayer(layer));
            }
            foreach (BiomeBlockDecoration decoBlock in sub.m_DecoBlocks)
            {
                DecoBlocks.Add(new BCMBiomeBlockDecoration(decoBlock));
            }
            foreach (BiomePrefabDecoration decoPrefab in sub.m_DecoPrefabs)
            {
                DecoPrefabs.Add(new BCMBiomePrefabDecoration(decoPrefab));
            }
        }
    }
}
