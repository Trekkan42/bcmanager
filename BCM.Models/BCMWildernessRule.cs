using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMWildernessRule
    {
        public string Name;

        public BCMVector2 MinMax;

        public int PathType;

        public int PathRadius;

        [NotNull]
        [UsedImplicitly]
        public Dictionary<string, object> PrefabRules;

        public BCMWildernessRule([NotNull] WildernessRule wildernessRule)
        {
            Name = wildernessRule.Name;
            MinMax = new BCMVector2(wildernessRule.SpawnMinMax);
            PathType = wildernessRule.PathMaterial.type;
            PathRadius = wildernessRule.PathRadius;
            PrefabRules = ((IEnumerable<KeyValuePair<string, FilterData>>)wildernessRule.PrefabSpawnRules).ToDictionary((Func<KeyValuePair<string, FilterData>, string>)((KeyValuePair<string, FilterData> r) => r.Key), (Func<KeyValuePair<string, FilterData>, object>)((KeyValuePair<string, FilterData> r) => new
            {
                Prob = Math.Round(r.Value.Probability, 3)
            }));
        }
    }
}
