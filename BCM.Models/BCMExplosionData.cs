using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMExplosionData
    {
        public int ParticleIndex;

        public int BlockRadius;

        public int EntityRadius;

        public int BuffsRadius;

        public double EntityDamage;

        public double BlockDamage;

        public List<BCMDamageMultiplier> DamageMultipliers;

        [NotNull]
        [UsedImplicitly]
        public List<BCMLootBuffAction> BuffActions = new List<BCMLootBuffAction>();

        public BCMExplosionData([NotNull] DynamicProperties _properties)
        {
            if (_properties.Values.ContainsKey("Explosion.ParticleIndex"))
            {
                int.TryParse(_properties.Values["Explosion.ParticleIndex"], out ParticleIndex);
            }
            else
            {
                ParticleIndex = 0;
            }
            if (_properties.Values.ContainsKey("Explosion.RadiusBlocks"))
            {
                int.TryParse(_properties.Values["Explosion.RadiusBlocks"], out BlockRadius);
            }
            if (_properties.Values.ContainsKey("Explosion.BlockDamage"))
            {
                double.TryParse(_properties.Values["Explosion.BlockDamage"], out BlockDamage);
            }
            else
            {
                BlockDamage = BlockRadius * BlockRadius;
            }
            if (_properties.Values.ContainsKey("Explosion.RadiusEntities"))
            {
                int.TryParse(_properties.Values["Explosion.RadiusEntities"], out EntityRadius);
            }
            if (_properties.Values.ContainsKey("Explosion.EntityDamage"))
            {
                double.TryParse(_properties.Values["Explosion.EntityDamage"], out EntityDamage);
            }
            else
            {
                EntityDamage = 20f * (float)EntityRadius;
            }
            if (_properties.Values.ContainsKey("Explosion.RadiusBuffs"))
            {
                int.TryParse(_properties.Values["Explosion.RadiusBuffs"], out BuffsRadius);
            }
            else
            {
                BuffsRadius = EntityRadius;
            }
            if (_properties.Values.ContainsKey("Explosion.Buff"))
            {
                BuffActions = GetExplosionBuffs(_properties);
            }
            DamageMultipliers = GetDamageMultiplier(_properties);
        }

        private static List<BCMLootBuffAction> GetExplosionBuffs(DynamicProperties _properties)
        {
            List<BCMLootBuffAction> list = new List<BCMLootBuffAction>();
            string[] array = _properties.Values["Explosion.Buff"].Split(',');
            string[] array2 = null;
            if (_properties.Values.ContainsKey("Explosion.Buff_chance"))
            {
                array2 = _properties.Values["Explosion.Buff_chance"].Split(',');
            }
            for (int i = 0; i < array.Length; i++)
            {
                string buffId = array[i].Trim();
                double result = 1.0;
                if (array2 != null && i < array2.Length)
                {
                    double.TryParse(array2[i].Trim(), out result);
                }
                list.Add(new BCMLootBuffAction(buffId, result));
            }
            return list;
        }

        private static List<BCMDamageMultiplier> GetDamageMultiplier(DynamicProperties _properties)
        {
            List<BCMDamageMultiplier> list = new List<BCMDamageMultiplier>();
            foreach (string key in _properties.Values.Dict.Dict.Keys)
            {
                if (key != null && key.StartsWith("Explosion.DamageBonus."))
                {
                    double.TryParse(_properties.Values[key], out var result);
                    list.Add(new BCMDamageMultiplier
                    {
                        Type = key.Substring("Explosion.DamageBonus.".Length),
                        Value = result
                    });
                }
            }
            return list;
        }
    }
}
