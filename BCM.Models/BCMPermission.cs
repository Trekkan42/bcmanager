namespace BCM.Models
{
    public class BCMPermission
    {
        public string Command;

        public int PermissionLevel;

        public BCMPermission(string command, int permissionLevel)
        {
            Command = command;
            PermissionLevel = permissionLevel;
        }
    }
}
