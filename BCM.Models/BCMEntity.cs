using System;
using System.Collections.Generic;
using UnityEngine;

namespace BCM.Models
{
    [Serializable]
    public class BCMEntity : BCMAbstract
    {
        public static class StrFilters
        {
            public const string EntityId = "id";

            public const string Type = "type";

            public const string Name = "name";

            public const string Position = "pos";

            public const string Rotation = "rot";

            public const string Lifetime = "lifetime";

            public const string IsAlive = "isalive";

            public const string CreationTime = "creationtime";

            public const string MaxHealth = "maxhealth";

            public const string Health = "health";

            public const string IsScout = "isscout";

            public const string IsSleeper = "issleeper";

            public const string IsDecoy = "isdecoy";

            public const string IsSleeping = "issleeping";

            public const string LootItems = "loot";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "id"
            },
            {
                1,
                "type"
            },
            {
                2,
                "name"
            },
            {
                3,
                "pos"
            },
            {
                4,
                "rot"
            },
            {
                5,
                "lifetime"
            },
            {
                6,
                "isalive"
            },
            {
                7,
                "creationtime"
            },
            {
                8,
                "maxhealth"
            },
            {
                9,
                "health"
            },
            {
                10,
                "isscout"
            },
            {
                11,
                "issleeper"
            },
            {
                12,
                "isdecoy"
            },
            {
                13,
                "issleeping"
            },
            {
                14,
                "loot"
            }
        };

        public int EntityId;

        public string Type;

        public string Name;

        public Vector3 Position;

        public Vector3 Rotation;

        public double? Lifetime;

        public bool IsAlive;

        public double CreationTime;

        public int MaxHealth;

        public int Health;

        public bool IsScout;

        public bool IsFeral;

        public bool IsSleeper;

        public bool IsDecoy;

        public bool IsSleeping;

        public double SpeedModifier;

        public double SpeedDay;

        public double SpeedNight;

        public List<BCMItemStack> LootItems;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMEntity(object obj, Dictionary<string, string> options, List<string> filters)
            : base(obj, "Entity", options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            Entity entity = obj as Entity;
            if (entity == null)
            {
                return;
            }
            EntityAlive entityAlive = entity as EntityAlive;
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "id":
                            GetEntityId(entity);
                            break;
                        case "type":
                            GetType(entity);
                            break;
                        case "name":
                            GetName(entity);
                            break;
                        case "pos":
                            GetPosition(entity);
                            break;
                        case "rot":
                            GetRotation(entity);
                            break;
                        case "lifetime":
                            GetLifetime(entity);
                            break;
                        case "creationtime":
                            if (!(entityAlive == null))
                            {
                                GetCreationTime(entityAlive);
                            }
                            break;
                        case "maxhealth":
                            if (!(entityAlive == null))
                            {
                                GetMaxHealth(entityAlive);
                            }
                            break;
                        case "health":
                            if (!(entityAlive == null))
                            {
                                GetHealth(entityAlive);
                            }
                            break;
                        case "isalive":
                            if (!(entityAlive == null))
                            {
                                GetIsAlive(entityAlive);
                            }
                            break;
                        case "isscout":
                            if (!(entityAlive == null))
                            {
                                GetIsScout(entityAlive);
                            }
                            break;
                        case "issleeper":
                            if (!(entityAlive == null))
                            {
                                GetIsSleeper(entityAlive);
                            }
                            break;
                        case "isdecoy":
                            if (!(entityAlive == null))
                            {
                                GetIsDecoy(entityAlive);
                            }
                            break;
                        case "issleeping":
                            if (!(entityAlive == null))
                            {
                                GetIsSleeping(entityAlive);
                            }
                            break;
                        case "loot":
                            GetLootItems(entity);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
                return;
            }
            GetEntityId(entity);
            GetType(entity);
            GetPosition(entity);
            if (base.Options.ContainsKey("full"))
            {
                GetName(entity);
                GetRotation(entity);
                GetLifetime(entity);
                if (!(entityAlive == null))
                {
                    GetCreationTime(entityAlive);
                    GetMaxHealth(entityAlive);
                    GetHealth(entityAlive);
                    GetIsAlive(entityAlive);
                    GetIsScout(entityAlive);
                    GetIsSleeper(entityAlive);
                    GetIsDecoy(entityAlive);
                    GetIsSleeping(entityAlive);
                    GetLootItems(entity);
                }
            }
        }

        private void GetIsSleeping(EntityAlive entityAlive)
        {
            base.Bin.Add("IsSleeping", IsSleeping = entityAlive.IsSleeping);
        }

        private void GetIsDecoy(EntityAlive entityAlive)
        {
            base.Bin.Add("IsDecoy", IsDecoy = entityAlive.IsSleeperDecoy);
        }

        private void GetIsSleeper(EntityAlive entityAlive)
        {
            base.Bin.Add("IsSleeper", IsSleeper = entityAlive.IsSleeper);
        }

        private void GetIsScout(EntityAlive entityAlive)
        {
            base.Bin.Add("IsScout", IsScout = entityAlive.IsScoutZombie);
        }

        private void GetIsAlive(EntityAlive entityAlive)
        {
            base.Bin.Add("IsAlive", IsAlive = !entityAlive.IsDead());
        }

        private void GetHealth(EntityAlive entityAlive)
        {
            base.Bin.Add("Health", Health = entityAlive.Health);
        }

        private void GetMaxHealth(EntityAlive entityAlive)
        {
            base.Bin.Add("MaxHealth", MaxHealth = entityAlive.GetMaxHealth());
        }

        private void GetCreationTime(EntityAlive entityAlive)
        {
            base.Bin.Add("CreationTime", CreationTime = Math.Round(entityAlive.CreationTimeSinceLevelLoad, 1));
        }

        private void GetLifetime(Entity entity)
        {
            base.Bin.Add("Lifetime", Lifetime = ((entity.lifetime < float.MaxValue) ? new double?(entity.lifetime) : null));
        }

        private void GetRotation(Entity entity)
        {
            base.Bin.Add("Rotation", Rotation = entity.rotation);
        }

        private void GetPosition(Entity entity)
        {
            base.Bin.Add("Position", Position = entity.position);
        }

        private void GetName(Entity entity)
        {
            base.Bin.Add("Name", Name = EntityClass.list[entity.entityClass]?.entityClassName);
        }

        private void GetType(Entity entity)
        {
            base.Bin.Add("Type", Type = entity.GetType().ToString());
        }

        private void GetEntityId(Entity entity)
        {
            base.Bin.Add("EntityId", EntityId = entity.entityId);
        }

        private void GetLootItems(Entity entity)
        {
            if (entity.lootContainer != null)
            {
                LootItems = new List<BCMItemStack>();
                ItemStack[] items = entity.lootContainer.items;
                foreach (ItemStack item in items)
                {
                    LootItems.Add(new BCMItemStack(item));
                }
                base.Bin.Add("LootItems", LootItems);
            }
        }
    }
}
