using System.Collections.Generic;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMHubLayout
    {
        public string Name;

        public string Type;

        [NotNull]
        [UsedImplicitly]
        public List<BCMStreetInfo> Streets = new List<BCMStreetInfo>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMLotInfo> Lots = new List<BCMLotInfo>();

        public BCMHubLayout([NotNull] HubLayout layout)
        {
            Name = layout.Name;
            Type = layout.TownshipType;
            foreach (HubLayout.StreetInfo street in layout.Streets)
            {
                Streets.Add(new BCMStreetInfo(street));
            }
            foreach (HubLayout.LotInfo lot in layout.Lots)
            {
                Lots.Add(new BCMLotInfo(lot));
            }
        }
    }
}
