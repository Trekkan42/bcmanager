namespace BCM.Models
{
    public class BCMItemMod
    {
        public string Name;

        public int Type;

        public BCMItemMod(ItemValue item)
        {
            Name = item.ItemClass.Name;
            Type = item.type;
        }
    }
}
