namespace BCM.Models
{
    public class BCMIngredient
    {
        public string Name;

        public int Type;

        public int Count;

        public BCMIngredient(string name, int type, int count)
        {
            Name = name;
            Type = type;
            Count = count;
        }
    }
}
