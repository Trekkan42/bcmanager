using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMRequirement
    {
        public List<string> Info = new List<string>();

        public BCMRequirement(IRequirement req)
        {
            req.GetInfoStrings(ref Info);
        }
    }
}
