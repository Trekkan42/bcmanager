using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMSlot
    {
        public string Name;

        public string EqSlot;

        public string EqLayer;

        public bool AltHair;

        [NotNull]
        [UsedImplicitly]
        public List<string> Textures;

        [NotNull]
        [UsedImplicitly]
        public List<string> Colors;

        [NotNull]
        [UsedImplicitly]
        public Dictionary<string, string> Masks;

        public BCMSlot([NotNull] UMASlot slot)
        {
            Name = slot.Name;
            EqSlot = slot.EquipmentSlot.ToString();
            EqLayer = slot.EquipmentLayer.ToString();
            AltHair = slot.ShowAltHair;
            Textures = slot.Textures;
            Colors = slot.Colors;
            Masks = slot.Masks.ToDictionary((UMASlot.Mask s) => s.EquipmentSlot.ToString(), (UMASlot.Mask s) => s.EquipmentLayer.ToString());
        }
    }
}
