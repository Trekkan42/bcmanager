using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMLotInfo
    {
        public string Name;

        public BCMVector2 Pos;

        public BCMVector2 Size;

        public int Rot;

        public string Prefab;

        public string Zoning;

        public string Cond;

        public string Age;

        public BCMLotInfo(HubLayout.LotInfo lotInfo)
        {
            Name = lotInfo.Name;
            Pos = new BCMVector2(lotInfo.Position);
            Size = new BCMVector2(lotInfo.Size);
            Rot = lotInfo.RotationFromNorth;
            Prefab = lotInfo.PrefabName;
            Zoning = lotInfo.Zoning;
            Cond = lotInfo.Condition;
            Age = lotInfo.Age;
        }
    }
}
