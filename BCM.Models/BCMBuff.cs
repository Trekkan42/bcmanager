using System;
using System.Collections.Generic;

namespace BCM.Models
{
    [Serializable]
    public class BCMBuff : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Icon = "icon";

            public const string Description = "desc";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                1,
                "name"
            },
            {
                2,
                "icon"
            },
            {
                3,
                "desc"
            }
        };

        public string Name;

        public string Icon;

        public string Description;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMBuff(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            BuffClass buffClass = obj as BuffClass;
            if (buffClass == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            GetName(buffClass);
                            break;
                        case "icon":
                            GetIcon(buffClass);
                            break;
                        case "desc":
                            GetDescription(buffClass);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetName(buffClass);
                if (IsOption("full"))
                {
                    GetIcon(buffClass);
                    GetDescription(buffClass);
                }
            }
        }

        private void GetName(BuffClass buff)
        {
            base.Bin.Add("Name", Name = buff.Name);
        }

        private void GetIcon(BuffClass buff)
        {
            base.Bin.Add("Icon", Icon = buff.Icon);
        }

        private void GetDescription(BuffClass buff)
        {
            base.Bin.Add("Description", Description = buff.Description);
        }
    }
}
