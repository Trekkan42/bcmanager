using System;
using System.Collections.Generic;

namespace BCM.Models
{
    [Serializable]
    public class BCMGameObject : BCMAbstract
    {
        public static class GOTypes
        {
            public const string Players = "players";

            public const string Entities = "entities";

            public const string Archetypes = "archetypes";

            public const string Blocks = "blocks";

            public const string Buffs = "buffs";

            public const string EntityClasses = "entityclasses";

            public const string EntityGroups = "entitygroups";

            public const string ItemClasses = "itemclasses";

            public const string Items = "items";

            public const string Materials = "materials";

            public const string Prefabs = "prefabs";

            public const string Quests = "quests";

            public const string Recipes = "recipes";

            public const string Skills = "skills";
        }

        public BCMGameObject(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            string typeStr = base.TypeStr;
            if (typeStr != null)
            {
                switch (typeStr)
                {
                    case "archetypes":
                        GetArchetypes(obj as Archetype);
                        break;
                    case "blocks":
                        GetItemClasses(obj as ItemClass);
                        break;
                    case "buffs":
                        GetBuffs(obj as BuffClass);
                        break;
                    case "entityclasses":
                        GetEntityClasses(obj as EntityClass);
                        break;
                    case "entitygroups":
                        GetEntityGroups((KeyValuePair<string, List<SEntityClassAndProb>>)obj);
                        break;
                    case "itemclasses":
                        GetItemClasses(obj as ItemClass);
                        break;
                    case "items":
                        GetItemClasses(obj as ItemClass);
                        break;
                    case "materials":
                        GetMaterials(obj as MaterialBlock);
                        break;
                    case "prefabs":
                        GetPrefabs(obj as string);
                        break;
                    case "quests":
                        GetQuests(obj as QuestClass);
                        break;
                    case "recipes":
                        GetRecipes(obj as Recipe);
                        break;
                    case "skills":
                        GetSkills(obj as ProgressionClass);
                        break;
                }
            }
        }

        private void GetArchetypes(Archetype obj)
        {
            base.Bin = new BCMArchetype(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetBuffs(BuffClass obj)
        {
            base.Bin = new BCMBuff(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetEntityClasses(EntityClass obj)
        {
            base.Bin = new BCMEntityClass(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetEntityGroups(KeyValuePair<string, List<SEntityClassAndProb>> obj)
        {
            base.Bin = new BCMEntityGroup(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetItemClasses(ItemClass obj)
        {
            base.Bin = new BCMItemClass(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetMaterials(MaterialBlock obj)
        {
            base.Bin = new BCMMaterial(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetPrefabs(string obj)
        {
            base.Bin = new BCMPrefab(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetQuests(QuestClass obj)
        {
            base.Bin = new BCMQuest(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetRecipes(Recipe obj)
        {
            base.Bin = new BCMRecipe(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }

        private void GetSkills(ProgressionClass obj)
        {
            base.Bin = new BCMSkill(obj, base.TypeStr, base.Options, base.StrFilter).Data();
        }
    }
}
