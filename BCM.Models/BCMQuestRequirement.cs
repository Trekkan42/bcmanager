using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMQuestRequirement
    {
        public string Type;

        public string Id;

        public string Value;

        public BCMQuestRequirement([NotNull] BaseRequirement requirement)
        {
            Type = requirement.GetType().ToString();
            Id = requirement.ID;
            Value = requirement.Value;
        }
    }
}
