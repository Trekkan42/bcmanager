using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMItemStack
    {
        public string Name;

        public int Type;

        public int Quality;

        public int Meta;

        public double UseTimes;

        public int MaxUse;

        public byte Activated;

        public byte AmmoIndex;

        public int Count;

        public List<BCMItemMod> Mods = new List<BCMItemMod>();

        public BCMItemStack(ItemStack item)
        {
            if (item == null || item.itemValue.type == 0)
            {
                return;
            }
            Name = item.itemValue.ItemClass.GetItemName();
            Type = item.itemValue.type;
            Quality = item.itemValue.Quality;
            Meta = item.itemValue.Meta;
            UseTimes = item.itemValue.UseTimes;
            MaxUse = item.itemValue.MaxUseTimes;
            Activated = item.itemValue.Activated;
            AmmoIndex = item.itemValue.SelectedAmmoTypeIndex;
            Count = item.count;
            ItemValue[] cosmeticMods;
            if (item.itemValue.CosmeticMods != null && item.itemValue.CosmeticMods.Length != 0)
            {
                cosmeticMods = item.itemValue.CosmeticMods;
                foreach (ItemValue itemValue in cosmeticMods)
                {
                    if (itemValue != null && itemValue.type != 0)
                    {
                        Mods.Add(new BCMItemMod(itemValue));
                    }
                }
            }
            if (item.itemValue.Modifications == null || item.itemValue.Modifications.Length == 0)
            {
                return;
            }
            cosmeticMods = item.itemValue.Modifications;
            foreach (ItemValue itemValue2 in cosmeticMods)
            {
                if (itemValue2 != null && itemValue2.type != 0)
                {
                    Mods.Add(new BCMItemMod(itemValue2));
                }
            }
        }
    }
}
