using System;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMPrefab : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Size = "size";

            public const string YOffset = "yoffset";

            public const string Rotation = "rotation";

            public const string AirBlocks = "airblocks";

            public const string AllowTopSoil = "topsoil";

            public const string HasMeshFile = "hasmesh";

            public const string ExcludePoiMesh = "poimesh";

            public const string DistYOffset = "distyoff";

            public const string DistOverride = "distover";

            public const string IsTrader = "trader";

            public const string TraderProtect = "protect";

            public const string TraderTpSize = "tpsize";

            public const string TraderTpCenter = "tpcenter";

            public const string Biomes = "biomes";

            public const string Townships = "townships";

            public const string Zoning = "zoning";

            public const string HasVolumes = "hasvols";

            public const string SleeperVolumes = "sleepers";

            public const string Blocks = "blocks";
        }

        private static readonly Dictionary<string, BCMPrefab> cache = new Dictionary<string, BCMPrefab>();

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "name"
            },
            {
                1,
                "size"
            },
            {
                2,
                "yoffset"
            },
            {
                3,
                "rotation"
            },
            {
                4,
                "airblocks"
            },
            {
                5,
                "topsoil"
            },
            {
                6,
                "hasmesh"
            },
            {
                7,
                "poimesh"
            },
            {
                8,
                "distyoff"
            },
            {
                9,
                "distover"
            },
            {
                10,
                "trader"
            },
            {
                11,
                "protect"
            },
            {
                12,
                "tpsize"
            },
            {
                13,
                "tpcenter"
            },
            {
                14,
                "biomes"
            },
            {
                15,
                "townships"
            },
            {
                16,
                "zoning"
            },
            {
                17,
                "hasvols"
            },
            {
                18,
                "sleepers"
            },
            {
                19,
                "blocks"
            }
        };

        public string Name;

        public BCMVector3 Size;

        public int YOffset;

        public int Rotation;

        public bool AirBlocks;

        public bool AllowTopSoil;

        public bool HasMeshFile;

        public bool ExcludePoiMesh;

        public double DistYOffset;

        public string DistOverride;

        public bool IsTrader;

        public BCMVector3 TraderProtect;

        public BCMVector3 TraderTpSize;

        public BCMVector3 TraderTpCenter;

        public string[] Biomes;

        public string[] Townships;

        public string[] Zoning;

        public bool HasVolumes;

        public List<bool> VolumesUsed;

        public List<BCMVector3> VolumesStart;

        public List<BCMVector3> VolumesSize;

        public List<string> VolumesGroup;

        public List<string> VolumeGSAdjust;

        public List<bool> VolumeIsLoot;

        [NotNull]
        [UsedImplicitly]
        public List<BCMPrefabSleeperVolume> SleeperVolumes = new List<BCMPrefabSleeperVolume>();

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMPrefab(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            string text = obj as string;
            if (text == null)
            {
                return;
            }
            if (!base.Options.ContainsKey("filter") && !base.Options.ContainsKey("info") && !base.Options.ContainsKey("blocks") && !base.Options.ContainsKey("full"))
            {
                base.Bin.Add("Name", Name = text);
                return;
            }
            if (base.Options.ContainsKey("full") && cache.ContainsKey(text))
            {
                base.Bin = cache[text].Bin;
                return;
            }
            if (base.Options.ContainsKey("filter") && cache.ContainsKey(text))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            base.Bin.Add("Name", cache[text].Name);
                            break;
                        case "size":
                            base.Bin.Add("Size", cache[text].Size);
                            break;
                        case "yoffset":
                            base.Bin.Add("YOffset", cache[text].YOffset);
                            break;
                        case "rotation":
                            base.Bin.Add("Rotation", cache[text].Rotation);
                            break;
                        case "airblocks":
                            base.Bin.Add("AirBlocks", cache[text].AirBlocks);
                            break;
                        case "topsoil":
                            base.Bin.Add("AllowTopSoil", cache[text].AllowTopSoil);
                            break;
                        case "hasmesh":
                            base.Bin.Add("HasMeshFile", cache[text].HasMeshFile);
                            break;
                        case "poimesh":
                            base.Bin.Add("ExcludePoiMesh", cache[text].ExcludePoiMesh);
                            break;
                        case "distyoff":
                            base.Bin.Add("DistYOffset", cache[text].DistYOffset);
                            break;
                        case "distover":
                            base.Bin.Add("DistOverride", cache[text].DistOverride);
                            break;
                        case "trader":
                            base.Bin.Add("IsTrader", cache[text].IsTrader);
                            break;
                        case "protect":
                            base.Bin.Add("TraderProtect", cache[text].TraderProtect);
                            break;
                        case "tpsize":
                            base.Bin.Add("TraderTpSize", cache[text].TraderTpSize);
                            break;
                        case "tpcenter":
                            base.Bin.Add("TraderTpCenter", cache[text].TraderTpCenter);
                            break;
                        case "biomes":
                            base.Bin.Add("Biomes", cache[text].Biomes);
                            break;
                        case "townships":
                            base.Bin.Add("Townships", cache[text].Townships);
                            break;
                        case "zoning":
                            base.Bin.Add("Zoning", cache[text].Zoning);
                            break;
                        case "hasvols":
                            base.Bin.Add("HasVolumes", cache[text].HasVolumes);
                            break;
                        case "sleepers":
                            base.Bin.Add("SleeperVolumes", cache[text].SleeperVolumes);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                        case "blocks":
                            break;
                    }
                }
                return;
            }
            Prefab prefab = new Prefab();
            if (!prefab.Load(text))
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item2 in base.StrFilter)
                {
                    switch (item2)
                    {
                        case "name":
                            GetName(prefab);
                            break;
                        case "size":
                            GetSize(prefab);
                            break;
                        case "yoffset":
                            GetYOffset(prefab);
                            break;
                        case "rotation":
                            GetRotation(prefab);
                            break;
                        case "airblocks":
                            GetAirBlocks(prefab);
                            break;
                        case "topsoil":
                            GetTopSoil(prefab);
                            break;
                        case "hasmesh":
                            GetHasMeshFile(prefab);
                            break;
                        case "poimesh":
                            GetExcludePoiMesh(prefab);
                            break;
                        case "distyoff":
                            GetDistYOffset(prefab);
                            break;
                        case "distover":
                            GetDistOverride(prefab);
                            break;
                        case "trader":
                            GetIsTrader(prefab);
                            break;
                        case "protect":
                            GetTraderProtect(prefab);
                            break;
                        case "tpsize":
                            GetTraderTpSize(prefab);
                            break;
                        case "tpcenter":
                            GetTraderTpCenter(prefab);
                            break;
                        case "biomes":
                            GetBiomes(prefab);
                            break;
                        case "townships":
                            GetTownships(prefab);
                            break;
                        case "zoning":
                            GetZoning(prefab);
                            break;
                        case "hasvols":
                            GetHasVolumes(prefab);
                            break;
                        case "sleepers":
                            GetSleeperVolumes(prefab);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item2);
                            break;
                        case "blocks":
                            break;
                    }
                }
            }
            else
            {
                GetName(prefab);
                if (!base.Options.ContainsKey("info") && !base.Options.ContainsKey("blocks") && !base.Options.ContainsKey("full"))
                {
                    return;
                }
                if (base.Options.ContainsKey("info") || base.Options.ContainsKey("full"))
                {
                    GetSize(prefab);
                    GetYOffset(prefab);
                    GetRotation(prefab);
                }
                if (base.Options.ContainsKey("full"))
                {
                    GetAirBlocks(prefab);
                    GetTopSoil(prefab);
                    GetHasMeshFile(prefab);
                    GetExcludePoiMesh(prefab);
                    GetDistYOffset(prefab);
                    GetDistOverride(prefab);
                    GetIsTrader(prefab);
                    GetTraderProtect(prefab);
                    GetTraderTpSize(prefab);
                    GetTraderTpCenter(prefab);
                    GetBiomes(prefab);
                    GetTownships(prefab);
                    GetZoning(prefab);
                    GetHasVolumes(prefab);
                    GetSleeperVolumes(prefab);
                }
                base.Options.ContainsKey("blocks");
            }
            if (base.Options.ContainsKey("full"))
            {
                cache.Add(text, this);
            }
        }

        private void GetZoning(Prefab prefab)
        {
            base.Bin.Add("Zoning", Zoning = prefab.GetAllowedZones());
        }

        private void GetTownships(Prefab prefab)
        {
            base.Bin.Add("Townships", Townships = prefab.GetAllowedTownships());
        }

        private void GetBiomes(Prefab prefab)
        {
            base.Bin.Add("Biomes", Biomes = prefab.GetAllowedBiomes());
        }

        private void GetSleeperVolumes(Prefab prefab)
        {
            SleeperVolumes.Clear();
            base.Bin.Add("SleeperVolumes", SleeperVolumes);
        }

        private void GetHasVolumes(Prefab prefab)
        {
            base.Bin.Add("HasVolumes", HasVolumes = prefab.bSleeperVolumes);
        }

        private void GetTraderTpCenter(Prefab prefab)
        {
            base.Bin.Add("TraderTpCenter", TraderTpCenter = new BCMVector3(prefab.TraderAreaTeleportCenter));
        }

        private void GetTraderTpSize(Prefab prefab)
        {
            base.Bin.Add("TraderTpSize", TraderTpSize = new BCMVector3(prefab.TraderAreaTeleportSize));
        }

        private void GetTraderProtect(Prefab prefab)
        {
            base.Bin.Add("TraderProtect", TraderProtect = new BCMVector3(prefab.TraderAreaProtect));
        }

        private void GetIsTrader(Prefab prefab)
        {
            base.Bin.Add("IsTrader", IsTrader = prefab.bTraderArea);
        }

        private void GetDistOverride(Prefab prefab)
        {
            base.Bin.Add("DistOverride", DistOverride = prefab.distantPOIOverride);
        }

        private void GetDistYOffset(Prefab prefab)
        {
            base.Bin.Add("DistYOffset", DistYOffset = Math.Round(prefab.distantPOIYOffset, 3));
        }

        private void GetExcludePoiMesh(Prefab prefab)
        {
            base.Bin.Add("ExcludePoiMesh", ExcludePoiMesh = prefab.bExcludeDistantPOIMesh);
        }

        private void GetHasMeshFile(Prefab prefab)
        {
            base.Bin.Add("HasMeshFile", HasMeshFile = File.Exists(Utils.GetGameDir("Data/Prefabs") + "/" + prefab.PrefabName + ".mesh"));
        }

        private void GetTopSoil(Prefab prefab)
        {
            base.Bin.Add("AllowTopSoil", AllowTopSoil = prefab.bAllowTopSoilDecorations);
        }

        private void GetAirBlocks(Prefab prefab)
        {
            base.Bin.Add("AirBlocks", AirBlocks = prefab.bCopyAirBlocks);
        }

        private void GetRotation(Prefab prefab)
        {
            base.Bin.Add("Rotation", Rotation = prefab.rotationToFaceNorth);
        }

        private void GetYOffset(Prefab prefab)
        {
            base.Bin.Add("YOffset", YOffset = prefab.yOffset);
        }

        private void GetSize(Prefab prefab)
        {
            base.Bin.Add("Size", Size = new BCMVector3(prefab.size));
        }

        private void GetName(Prefab prefab)
        {
            base.Bin.Add("Name", Name = prefab.PrefabName);
        }
    }
}
