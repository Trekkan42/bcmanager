using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMQuestReward
    {
        public string Type;

        public string Id;

        public string Value;

        public BCMQuestReward([NotNull] BaseReward reward)
        {
            Type = reward.GetType().ToString();
            Id = reward.ID;
            Value = reward.Value;
        }
    }
}
