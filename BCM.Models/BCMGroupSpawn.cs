using System;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMGroupSpawn
    {
        [UsedImplicitly]
        public int EntityClassId;

        [UsedImplicitly]
        public string Name;

        [UsedImplicitly]
        public double Prob;

        public BCMGroupSpawn(SEntityClassAndProb spawn)
        {
            EntityClassId = spawn.entityClassId;
            if (EntityClass.list.ContainsKey(spawn.entityClassId))
            {
                Name = EntityClass.list[spawn.entityClassId].entityClassName;
            }
            Prob = Math.Round(spawn.prob, 3);
        }
    }
}
