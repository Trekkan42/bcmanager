using System;
using System.Collections.Generic;

namespace BCM.Models
{
    public class BCMCraftingQueue
    {
        public int Type;

        public string Name;

        public int Count;

        public double TotalTime;

        public double CraftTime;

        public List<BCMIngredient> Ingredients;

        public BCMCraftingQueue(RecipeQueueItem item)
        {
            if (item.Recipe == null)
            {
                return;
            }
            Type = item.Recipe.itemValueType;
            Name = item.Recipe.GetName();
            Count = item.Multiplier;
            if (item.IsCrafting)
            {
                TotalTime = Math.Round(item.Recipe.craftingTime * (float)(item.Multiplier - 1) + item.CraftingTimeLeft, 1);
                CraftTime = Math.Round(item.CraftingTimeLeft, 1);
            }
            else
            {
                TotalTime = Math.Round(item.Recipe.craftingTime * (float)item.Multiplier, 1);
                CraftTime = Math.Round(item.Recipe.craftingTime, 1);
            }
            Ingredients = new List<BCMIngredient>();
            foreach (ItemStack item3 in item.Recipe.GetIngredientsSummedUp())
            {
                BCMIngredient item2 = new BCMIngredient(item3.itemValue.ItemClass.GetItemName(), item3.itemValue.type, item3.count);
                Ingredients.Add(item2);
            }
        }
    }
}
