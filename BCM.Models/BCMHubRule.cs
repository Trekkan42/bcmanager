using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMHubRule
    {
        public double DtZone;

        public string Name;

        public string TownshipType;

        public BCMVector2 Width;

        public BCMVector2 Height;

        public int PathType;

        public int PathRadius;

        public BCMStreetGenerationData Streets;

        public string Layout;

        [NotNull]
        [UsedImplicitly]
        public Dictionary<string, object> PrefabRules;

        public BCMHubRule([NotNull] HubRule hubRule)
        {
            DtZone = Math.Round(hubRule.DowntownZoningPerc, 3);
            Name = hubRule.Name;
            TownshipType = hubRule.TownshipType;
            Width = new BCMVector2(hubRule.WidthMinMax);
            Height = new BCMVector2(hubRule.HeightMinMax);
            PathType = hubRule.PathMaterial.type;
            PathRadius = hubRule.PathRadius;
            Streets = new BCMStreetGenerationData(hubRule.StreetGenData);
            Layout = hubRule.HubLayoutName;
            PrefabRules = ((IEnumerable<KeyValuePair<string, FilterData>>)hubRule.PrefabSpawnRules).ToDictionary((Func<KeyValuePair<string, FilterData>, string>)((KeyValuePair<string, FilterData> r) => r.Key), (Func<KeyValuePair<string, FilterData>, object>)((KeyValuePair<string, FilterData> r) => new
            {
                Prob = Math.Round(r.Value.Probability, 3)
            }));
        }
    }
}
