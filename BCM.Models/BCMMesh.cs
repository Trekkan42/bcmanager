using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMMesh : BCMMeshShort
    {
        public string AtlasClass;

        public string MeshType;

        public bool Shadows;

        public string BlendMode;

        public string Tag;

        public string Collider;

        public string SecShader;

        public string ShaderName;

        public string ShaderDistant;

        public string MetaName;

        public string MetaText;

        [NotNull]
        [UsedImplicitly]
        public new List<BCMMeshData> MetaData;

        public BCMMesh([NotNull] MeshDescription meshDesc, [NotNull] List<BCMMeshData> metaData)
        {
            Name = meshDesc.Name;
            AtlasClass = meshDesc.TextureAtlasClass;
            MeshType = meshDesc.meshType.ToString();
            Shadows = meshDesc.bCastShadows;
            BlendMode = meshDesc.BlendMode.ToString();
            Tag = meshDesc.Tag;
            Collider = meshDesc.ColliderLayerName;
            SecShader = meshDesc.SecondaryShader;
            ShaderName = meshDesc.ShaderName;
            ShaderDistant = meshDesc.ShaderNameDistant;
            MetaName = meshDesc.MetaData?.name;
            MetaText = meshDesc.MetaData?.text;
            MetaData = metaData;
        }
    }
}
