using System.Collections.Generic;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMStreetGenerationData
    {
        public int Level;

        public int Mult;

        public string Axiom;

        public Dictionary<string, string> Rules;

        public string[] Alts;

        public BCMStreetGenerationData([NotNull] StreetGenerationData streetGenData)
        {
            Level = streetGenData.Level;
            Mult = streetGenData.LengthMultiplier;
            Axiom = streetGenData.Axiom;
            Rules = streetGenData.Rules;
            Alts = streetGenData.AltCommands;
        }
    }
}
