using System;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMFilterData
    {
        public double Prob;

        public BCMVector2 Grid;

        public BCMFilterData([NotNull] FilterData filterData)
        {
            Prob = Math.Round(filterData.Probability, 3);
            if (filterData.HasGridPosition)
            {
                Grid = new BCMVector2(filterData.GridPosition);
            }
        }
    }
}
