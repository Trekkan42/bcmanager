using System.Xml;
using JetBrains.Annotations;
using UnityEngine;

namespace BCM.Models
{
    public class BCMMeshDataShort
    {
        public int Id;

        public string Color;

        public string Material;

        public string Texture;

        public BCMMeshDataShort([NotNull] XmlElement uv)
        {
            if (uv.HasAttribute("id"))
            {
                int.TryParse(uv.GetAttribute("id"), out Id);
            }
            if (uv.HasAttribute("color"))
            {
                string[] array = uv.GetAttribute("color").Split(',');
                if (array.Length == 3 && float.TryParse(array[0], out var result) && float.TryParse(array[1], out var result2) && float.TryParse(array[2], out var result3))
                {
                    Color = BCUtils.ColorToHex(new Color(result, result2, result3));
                }
            }
            Material = (uv.HasAttribute("material") ? uv.GetAttribute("material") : "");
            Texture = (uv.HasAttribute("texture") ? uv.GetAttribute("texture").Substring(0, uv.GetAttribute("texture").Length - 4) : "");
        }
    }
}
