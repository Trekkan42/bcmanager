using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMBiomeLayer
    {
        [UsedImplicitly]
        public BCMBiomeBlockDecoration Block;

        [UsedImplicitly]
        public int Depth;

        [UsedImplicitly]
        public int FillTo;

        [UsedImplicitly]
        public readonly List<BCMBiomeBlockDecoration> Resources = new List<BCMBiomeBlockDecoration>();

        public BCMBiomeLayer(BiomeLayer layer)
        {
            Block = new BCMBiomeBlockDecoration(layer.m_Block);
            Depth = layer.m_Depth;
            FillTo = layer.m_FillUpTo;
            foreach (BiomeBlockDecoration resource in layer.m_Resources)
            {
                Resources.Add(new BCMBiomeBlockDecoration(resource));
            }
        }
    }
}
