using System;
using System.Reflection;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMSleeperVolume
    {
        private const string VolumeGroupFieldName = "QH";

        private const string TimerFieldName = "IH";

        public int Index;

        public string Group;

        public BCMVector3 Position;

        public BCMVector3 Extent;

        public BCMVector3 Size;

        public double RespawnTimer;

        public BCMSleeperVolume(int index, [NotNull] SleeperVolume volume, [NotNull] World world)
        {
            Index = index;
            FieldInfo field = typeof(SleeperVolume).GetField("QH", BindingFlags.Instance | BindingFlags.NonPublic);
            if (field != null)
            {
                string text = field.GetValue(volume) as string;
                if (text == null)
                {
                    return;
                }
                Group = text;
            }
            Position = new BCMVector3(volume.BoxMin);
            Extent = new BCMVector3(volume.BoxMax);
            Size = new BCMVector3(volume.BoxMax - volume.BoxMin + Vector3i.one);
            FieldInfo field2 = typeof(SleeperVolume).GetField("IH", BindingFlags.Instance | BindingFlags.NonPublic);
            if (field2 == null)
            {
                return;
            }
            object value = field2.GetValue(volume);
            if (value is ulong)
            {
                ulong num = (ulong)value;
                if (num > world.worldTime)
                {
                    RespawnTimer = Math.Round((float)(num - world.worldTime) / 24000f, 2);
                }
            }
        }
    }
}
