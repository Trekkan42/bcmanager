using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMQuestObjective
    {
        public string Type;

        public string Id;

        public string Value;

        public BCMQuestObjective([NotNull] BaseObjective objective)
        {
            Type = objective.GetType().ToString();
            Id = objective.ID;
            Value = objective.Value;
        }
    }
}
