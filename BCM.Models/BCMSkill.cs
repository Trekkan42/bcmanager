using System;
using System.Collections.Generic;

namespace BCM.Models
{
    [Serializable]
    public class BCMSkill : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Level = "level";

            public const string Icon = "icon";

            public const string MaxLevel = "maxlevel";

            public const string IsPerk = "perk";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "name"
            },
            {
                1,
                "level"
            },
            {
                2,
                "icon"
            },
            {
                3,
                "maxlevel"
            },
            {
                4,
                "perk"
            }
        };

        public int Id;

        public string Name;

        public string Local;

        public double ExpGainMult;

        public string Group;

        public int ExpToNext;

        public int Level;

        public bool IsLocked;

        public string TitleKey;

        public string DescKey;

        public string Icon;

        public int MaxLevel;

        public int BaseExpTo;

        public double ExpMult;

        public bool IsPassive;

        public bool IsPerk;

        public bool IsCrafting;

        public bool AlwaysFire;

        public int CostPer;

        public double CostMult;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMSkill(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            ProgressionClass progressionClass = obj as ProgressionClass;
            if (progressionClass == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            GetName(progressionClass);
                            break;
                        case "level":
                            GetLevel(progressionClass);
                            break;
                        case "icon":
                            GetIcon(progressionClass);
                            break;
                        case "maxlevel":
                            GetMaxLevel(progressionClass);
                            break;
                        case "perk":
                            GetIsPerk(progressionClass);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetName(progressionClass);
                GetLevel(progressionClass);
                GetIcon(progressionClass);
                GetMaxLevel(progressionClass);
                GetIsPerk(progressionClass);
            }
        }

        private void GetIsPerk(ProgressionClass skill)
        {
            base.Bin.Add("IsPerk", IsPerk = skill.IsPerk);
        }

        private void GetMaxLevel(ProgressionClass skill)
        {
            base.Bin.Add("MaxLevel", MaxLevel = skill.MaxLevel);
        }

        private void GetIcon(ProgressionClass skill)
        {
            base.Bin.Add("Icon", Icon = skill.Icon);
        }

        private void GetLevel(ProgressionClass skill)
        {
            base.Bin.Add("Level", Level = skill.MinLevel);
        }

        private void GetName(ProgressionClass skill)
        {
            base.Bin.Add("Name", Name = skill.Name);
        }
    }
}
