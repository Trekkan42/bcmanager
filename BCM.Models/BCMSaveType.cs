namespace BCM.Models
{
    public enum BCMSaveType
    {
        All,
        ServerConfig,
        WorldSaveConfig,
        ServerPlayersConfig,
        WorldPlayersData
    }
}
