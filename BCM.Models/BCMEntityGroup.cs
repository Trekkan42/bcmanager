using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMEntityGroup : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Entities = "entities";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "name"
            },
            {
                1,
                "entities"
            }
        };

        public string Name;

        [NotNull]
        [UsedImplicitly]
        public List<BCMGroupSpawn> Entities = new List<BCMGroupSpawn>();

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMEntityGroup(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            KeyValuePair<string, List<SEntityClassAndProb>> entityGroups = (KeyValuePair<string, List<SEntityClassAndProb>>)obj;
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            GetName(entityGroups);
                            break;
                        case "entities":
                            GetEntities(entityGroups);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetName(entityGroups);
                GetEntities(entityGroups);
            }
        }

        private void GetName(KeyValuePair<string, List<SEntityClassAndProb>> entityGroups)
        {
            base.Bin.Add("Name", Name = entityGroups.Key);
        }

        private void GetEntities(KeyValuePair<string, List<SEntityClassAndProb>> entityGroups)
        {
            foreach (SEntityClassAndProb item in entityGroups.Value)
            {
                Entities.Add(new BCMGroupSpawn(item));
            }
            base.Bin.Add("Entities", Entities);
        }
    }
}
