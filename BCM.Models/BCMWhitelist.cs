namespace BCM.Models
{
    public class BCMWhitelist
    {
        public string SteamId;

        public string PlayerName;

        public BCMWhitelist(string steamId, string playerName)
        {
            SteamId = steamId;
            PlayerName = playerName;
        }
    }
}
