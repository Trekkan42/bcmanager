using System;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMPrefabInfo
    {
        public string Name;

        public double Prob;

        public bool List;

        public int Min;

        public int Max;

        public BCMPrefabInfo([NotNull] PrefabInfo info)
        {
            Name = info.Name;
            Prob = Math.Round(info.Prob, 3);
            List = info.FilteredList;
            Min = info.MinCount;
            Max = info.MaxCount;
        }
    }
}
