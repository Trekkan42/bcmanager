using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMArchetype : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Name = "name";

            public const string Type = "type";

            public const string IsMale = "ismale";

            public const string HairColor = "hair";

            public const string EyeColor = "eye";

            public const string SkinColor = "skin";

            public const string VoiceSets = "voice";

            public const string BaseSlots = "base";

            public const string PreviewSlots = "preview";

            public const string Dna = "dna";

            public const string Expressions = "expression";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "name"
            },
            {
                1,
                "type"
            },
            {
                2,
                "ismale"
            },
            {
                3,
                "hair"
            },
            {
                4,
                "eye"
            },
            {
                5,
                "skin"
            },
            {
                6,
                "voice"
            },
            {
                7,
                "base"
            },
            {
                8,
                "preview"
            },
            {
                9,
                "dna"
            },
            {
                10,
                "expression"
            }
        };

        public string Name;

        public bool? IsMale;

        public string HairColor;

        public string EyeColor;

        public string SkinColor;

        public string Type;

        [NotNull]
        [UsedImplicitly]
        public List<BCMSlot> BaseSlots = new List<BCMSlot>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMSlot> PreviewSlots = new List<BCMSlot>();

        [NotNull]
        [UsedImplicitly]
        public Dictionary<string, double> Dna = new Dictionary<string, double>();

        public BCMExpressionData Expressions;

        public string VoiceSet;

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMArchetype(object obj, string typeStr, Dictionary<string, string> options, List<string> filter)
            : base(obj, typeStr, options, filter)
        {
        }

        protected override void GetData(object obj)
        {
            Archetype archetype = obj as Archetype;
            if (archetype == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "name":
                            GetName(archetype);
                            break;
                        case "type":
                            GetType(archetype);
                            break;
                        case "ismale":
                            GetGender(archetype);
                            break;
                        case "eye":
                            GetEyes(archetype);
                            break;
                        case "hair":
                            GetHair(archetype);
                            break;
                        case "skin":
                            GetSkin(archetype);
                            break;
                        case "voice":
                            GetVoiceSet(archetype);
                            break;
                        case "base":
                            GetBaseSlots(archetype);
                            break;
                        case "preview":
                            GetPreviewSlots(archetype);
                            break;
                        case "dna":
                            GetDna(archetype);
                            break;
                        case "expression":
                            GetExpressions(archetype);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetName(archetype);
                GetType(archetype);
                GetGender(archetype);
                if (IsOption("full"))
                {
                    GetEyes(archetype);
                    GetHair(archetype);
                    GetSkin(archetype);
                    GetVoiceSet(archetype);
                    GetBaseSlots(archetype);
                    GetPreviewSlots(archetype);
                    GetDna(archetype);
                    GetExpressions(archetype);
                }
            }
        }

        private void GetExpressions(Archetype archetype)
        {
            base.Bin.Add("Expressions", Expressions = new BCMExpressionData(archetype.ExpressionData));
        }

        private void GetDna(Archetype archetype)
        {
            string[] names = archetype.Dna.Names;
            foreach (string text in names)
            {
                Dna.Add(text, Math.Round(archetype.Dna.GetValue(text), 3));
            }
            base.Bin.Add("Dna", Dna);
        }

        private void GetPreviewSlots(Archetype archetype)
        {
            foreach (UMASlot previewSlot in archetype.PreviewSlots)
            {
                PreviewSlots.Add(new BCMSlot(previewSlot));
            }
            base.Bin.Add("PreviewSlots", PreviewSlots);
        }

        private void GetBaseSlots(Archetype archetype)
        {
            foreach (UMASlot baseSlot in archetype.BaseSlots)
            {
                BaseSlots.Add(new BCMSlot(baseSlot));
            }
            base.Bin.Add("BaseSlots", BaseSlots);
        }

        private void GetVoiceSet(Archetype archetype)
        {
            base.Bin.Add("VoiceSets", VoiceSet = archetype.VoiceSet);
        }

        private void GetSkin(Archetype archetype)
        {
            base.Bin.Add("SkinColor", SkinColor = archetype.SkinColor.ToStringRgbHex());
        }

        private void GetHair(Archetype archetype)
        {
            base.Bin.Add("HairColor", HairColor = archetype.HairColor.ToStringRgbHex());
        }

        private void GetEyes(Archetype archetype)
        {
            base.Bin.Add("EyeColor", EyeColor = archetype.EyeColor.ToStringRgbHex());
        }

        private void GetGender(Archetype archetype)
        {
            base.Bin.Add("IsMale", IsMale = archetype.IsMale);
        }

        private void GetType(Archetype archetype)
        {
            base.Bin.Add("Type", Type = archetype.Type.ToString());
        }

        private void GetName(Archetype archetype)
        {
            base.Bin.Add("Name", Name = archetype.Name);
        }
    }
}
