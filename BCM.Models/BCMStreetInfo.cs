using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMStreetInfo
    {
        public string Name;

        public BCMVector2 StartPoint;

        public BCMVector2 EndPoint;

        public int PathType;

        public int PathRadius;

        public BCMStreetInfo(HubLayout.StreetInfo streetInfo)
        {
            Name = streetInfo.Name;
            StartPoint = new BCMVector2(streetInfo.StartPoint);
            EndPoint = new BCMVector2(streetInfo.EndPoint);
            PathType = streetInfo.PathMaterial.type;
            PathRadius = streetInfo.PathRadius;
        }
    }
}
