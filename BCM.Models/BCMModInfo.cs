using System.IO;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMModInfo
    {
        [UsedImplicitly]
        public string Name;

        [UsedImplicitly]
        public string Version;

        [UsedImplicitly]
        public string Website;

        [UsedImplicitly]
        public string Description;

        [UsedImplicitly]
        public string Author;

        [UsedImplicitly]
        public string Path;

        public BCMModInfo([NotNull] Mod mod)
        {
            Name = $"{mod.ModInfo.Name}";
            Version = $"{mod.ModInfo.Version}";
            Website = $"{mod.ModInfo.Website}";
            Description = $"{mod.ModInfo.Description}";
            Author = $"{mod.ModInfo.Author}";
            Path = System.IO.Path.GetFileName(mod.Path);
        }

        public BCMModInfo(string name, string version, string website, string description, string author)
        {
            Name = name;
            Version = version;
            Website = website;
            Description = description;
            Author = author;
        }
    }
}
