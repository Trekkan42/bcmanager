using UnityEngine;

namespace BCM.Models
{
    public class BCMWaypoint
    {
        public string Name;

        public string Icon;

        public Vector3 Pos;

        public BCMWaypoint(Waypoint waypoint)
        {
            Name = waypoint.name;
            Pos = waypoint.pos.ToVector3();
            Icon = waypoint.icon;
        }
    }
}
