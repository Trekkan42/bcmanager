using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using BCM.Commands;
using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMCmdProcessor
    {
        [NotNull]
        public readonly Dictionary<string, string> Opts;

        [NotNull]
        public readonly List<string> BPars;

        [NotNull]
        private readonly List<string> _pars;

        [NotNull]
        private readonly string _cmdName;

        public string Command = string.Empty;

        public string SteamId;

        public int? EntityId;

        public string Filter;

        public BCMVector3 Position;

        public BCMVector3 Size;

        private BCMVector3 _center;

        private BCMVector3 _maxPos;

        public BCMVector4 ChunkBounds;

        private ushort _radius;

        public bool IsSingleBlock;

        public bool IsBlockArea;

        public bool IsChunkArea;

        [Obsolete]
        public ItemStack ItemStack;

        public BCMVector3 Center => _center ?? (_center = new BCMVector3(Position.x + Size.x / 2 - 1, Position.y + Size.y / 2 - 1, Position.z + Size.z / 2 - 1));

        public BCMVector3 MaxPos => _maxPos ?? (_maxPos = new BCMVector3(Position.x + Size.x - 1, Position.y + Size.y - 1, Position.z + Size.z - 1));

        public bool IsWithinBounds(Vector3i pos)
        {
            if (Position.x <= pos.x && pos.x <= MaxPos.x && Position.y <= pos.y && pos.y <= MaxPos.y)
            {
                if (Position.z <= pos.z)
                {
                    return pos.z <= MaxPos.z;
                }
                return false;
            }
            return false;
        }

        public BCMCmdProcessor(List<string> pars, Dictionary<string, string> options, string cmdName)
        {
            Opts = options;
            BPars = (from p in pars
                     where p.StartsWith("!")
                     select p.Substring(1)).ToList();
            _pars = pars.Where((string p) => !p.StartsWith("!")).ToList();
            _cmdName = cmdName;
        }

        public static bool ProcessParams(BCMCmdProcessor command, bool hasSub = true)
        {
            if (command.Opts.ContainsKey("type"))
            {
                command.Filter = command.Opts["type"];
            }
            if (hasSub && command._pars.Count > 0)
            {
                command.Command = command._pars[0];
                command._pars.RemoveAt(0);
            }
            if (command.Opts.ContainsKey("id") && !ParseIdOption(command.Opts["id"], out command.SteamId, out command.EntityId))
            {
                BCCommandAbstract.SendOutput("Unable to find player for steam id " + command.Opts["id"]);
                return false;
            }
            if (command.SteamId == null)
            {
                command.EntityId = BCCommandAbstract.SenderInfo.RemoteClientInfo?.entityId;
                command.SteamId = BCCommandAbstract.SenderInfo.RemoteClientInfo?.playerId;
            }
            switch (command._pars.Count)
            {
                case 0:
                    return GetAreaNoParams(command);
                case 2:
                    if (!command.Opts.ContainsKey("r"))
                    {
                        return GetChunkAreaSingle(command);
                    }
                    return GetChunkAreaRadius(command);
                case 3:
                    return GetBlockAreaSingle(command);
                case 4:
                    return GetChunkAreaMultiple(command);
                case 6:
                    return GetBlockAreaMultiple(command);
                default:
                    return false;
            }
        }

        public static void ProcessTask(BCMCmdProcessor command, BCCommandAbstract cmdRef)
        {
            if (command.Opts.ContainsKey("sync"))
            {
                BCCommandAbstract.SendOutput("Processing Command synchronously...");
                ProcessCommand(command, cmdRef);
                return;
            }
            if (BCCommandAbstract.SenderInfo.NetworkConnection != null && !(BCCommandAbstract.SenderInfo.NetworkConnection is TelnetConnection))
            {
                BCCommandAbstract.SendOutput("Processing Async Command... Sending output to log");
            }
            else
            {
                BCCommandAbstract.SendOutput("Processing Async Command...");
            }
            BCTask.AddTask(command._cmdName, ThreadManager.AddSingleTask(delegate
            {
                ProcessCommand(command, cmdRef);
            }, null, delegate (ThreadManager.TaskInfo info, Exception e)
            {
                BCTask.DelTask(command._cmdName, info.GetHashCode());
            }).GetHashCode());
        }

        private static void ProcessCommand(BCMCmdProcessor command, BCCommandAbstract cmdRef)
        {
            if (command.Opts.ContainsKey("debug"))
            {
                BCCommandAbstract.SendJson(command);
            }
            Dictionary<long, Chunk> affectedChunks = GetAffectedChunks(command);
            if (affectedChunks == null)
            {
                BCCommandAbstract.SendOutput("Aborting, unable to load all chunks in area before timeout.");
                BCCommandAbstract.SendOutput("Use /timeout=#### to override the default 2000 millisecond limit, or wait for the requested chunks to finish loading and try again.");
                return;
            }
            string text = ((!command.IsBlockArea) ? "" : ($"Location {command.Position} " + (command.IsSingleBlock ? "" : $"to {command.MaxPos} "))) + ((!command.IsChunkArea) ? "" : $"Chunks {command.ChunkBounds.x} {command.ChunkBounds.y} to {command.ChunkBounds.z} {command.ChunkBounds.w}");
            if (!string.IsNullOrEmpty(text))
            {
                BCCommandAbstract.SendOutput(text);
            }
            cmdRef.ProcessCmd(command, out var reload);
            if (reload != 0 && !command.Opts.ContainsKey("noreload") && !command.Opts.ContainsKey("nr"))
            {
                BCChunks.ReloadForClients(affectedChunks, (reload == ReloadMode.Target) ? command.SteamId : string.Empty);
            }
        }

        private static void GetRadius(BCMCmdProcessor command)
        {
            ushort.TryParse(command.Opts["r"], out command._radius);
            if (command._radius > 14)
            {
                command._radius = 14;
            }
            BCCommandAbstract.SendOutput($"Setting chunk radius to +{command._radius}");
        }

        private static bool GetAreaNoParams(BCMCmdProcessor command)
        {
            EntityPlayer entityPlayer = null;
            if (command.EntityId.HasValue && GameManager.Instance.World.Players.dict.ContainsKey(command.EntityId.Value))
            {
                entityPlayer = GameManager.Instance.World.Players.dict[command.EntityId.Value];
            }
            if (entityPlayer == null)
            {
                BCCommandAbstract.SendOutput("Command requires a position when not run by a player.");
                return false;
            }
            if (command.Opts.ContainsKey("loc") || !command.Opts.ContainsKey("r"))
            {
                if (command.SteamId == null)
                {
                    BCCommandAbstract.SendOutput("Unable to find steam id for player");
                    return false;
                }
                Vector3i pos = BCLocation.GetPos(command.SteamId);
                if (pos.x == int.MinValue)
                {
                    BCCommandAbstract.SendOutput("No location stored or player not found. Use bc-loc to store a location.");
                    return false;
                }
                int num = Math.Min(pos.x, Utils.Fastfloor(entityPlayer.position.x));
                int num2 = Math.Min(pos.y, Utils.Fastfloor(entityPlayer.position.y));
                int num3 = Math.Min(pos.z, Utils.Fastfloor(entityPlayer.position.z));
                int num4 = Math.Max(pos.x, Utils.Fastfloor(entityPlayer.position.x));
                int num5 = Math.Max(pos.y, Utils.Fastfloor(entityPlayer.position.y));
                int num6 = Math.Max(pos.z, Utils.Fastfloor(entityPlayer.position.z));
                command.IsBlockArea = true;
                command.ChunkBounds = new BCMVector4(World.toChunkXZ(num), World.toChunkXZ(num3), World.toChunkXZ(num4), World.toChunkXZ(num6));
                command.Position = new BCMVector3(num, num2, num3);
                command.Size = new BCMVector3(num4 - num + 1, num5 - num2 + 1, num6 - num3 + 1);
                command.IsSingleBlock = BCMVector3.V3One(command.Size);
                return true;
            }
            if (command.Opts.ContainsKey("r"))
            {
                GetRadius(command);
                command.IsChunkArea = true;
                command.Position = new BCMVector3((World.toChunkXZ((int)Math.Floor(entityPlayer.position.x)) - command._radius) * 16, 0, (World.toChunkXZ((int)Math.Floor(entityPlayer.position.z)) - command._radius) * 16);
                command.Size = new BCMVector3(16 * (command._radius * 2 + 1), 255, 16 * (command._radius * 2 + 1));
                command.ChunkBounds = new BCMVector4
                {
                    x = World.toChunkXZ(command.Position.x),
                    y = World.toChunkXZ(command.Position.z),
                    z = World.toChunkXZ(command.MaxPos.x),
                    w = World.toChunkXZ(command.MaxPos.z)
                };
                return true;
            }
            BCCommandAbstract.SendOutput("Unable to get position. Use /loc or /r if using player entity location.");
            return false;
        }

        private static bool GetChunkAreaSingle(BCMCmdProcessor command)
        {
            if (!int.TryParse(command._pars[0], out var result) || !int.TryParse(command._pars[1], out var result2))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,z into numbers");
                return false;
            }
            command.IsChunkArea = true;
            command.ChunkBounds = new BCMVector4(result, result2, result, result2);
            command.Position = new BCMVector3(command.ChunkBounds.x * 16, 0, command.ChunkBounds.y * 16);
            command.Size = new BCMVector3(16, 255, 16);
            return true;
        }

        private static bool GetChunkAreaRadius(BCMCmdProcessor command)
        {
            if (!int.TryParse(command._pars[0], out var result) || !int.TryParse(command._pars[1], out var result2))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,z into numbers");
                return false;
            }
            GetRadius(command);
            command.IsChunkArea = true;
            command.ChunkBounds = new BCMVector4(result - command._radius, result2 - command._radius, result + command._radius, result2 + command._radius);
            command.Position = new BCMVector3(command.ChunkBounds.x * 16, 0, command.ChunkBounds.y * 16);
            command.Size = new BCMVector3(16 * (command._radius * 2 + 1), 255, 16 * (command._radius * 2 + 1));
            return true;
        }

        private static bool GetChunkAreaMultiple(BCMCmdProcessor command)
        {
            if (!int.TryParse(command._pars[0], out var result) || !int.TryParse(command._pars[1], out var result2) || !int.TryParse(command._pars[2], out var result3) || !int.TryParse(command._pars[3], out var result4))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,z x2,z2 into numbers");
                return false;
            }
            command.IsChunkArea = true;
            command.ChunkBounds = new BCMVector4(Math.Min(result, result3), Math.Min(result2, result4), Math.Max(result, result3), Math.Max(result2, result4));
            command.Position = new BCMVector3(command.ChunkBounds.x * 16, 0, command.ChunkBounds.y * 16);
            command.Size = new BCMVector3(16 * (command.ChunkBounds.z - command.ChunkBounds.x + 1), 255, 16 * (command.ChunkBounds.w - command.ChunkBounds.y + 1));
            return true;
        }

        private static bool GetBlockAreaSingle(BCMCmdProcessor command)
        {
            if (!int.TryParse(command._pars[0], out var result) || !int.TryParse(command._pars[1], out var result2) || !int.TryParse(command._pars[2], out var result3))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,y,z into numbers");
                return false;
            }
            command.IsSingleBlock = true;
            command.IsBlockArea = true;
            command.ChunkBounds = new BCMVector4(World.toChunkXZ(result), World.toChunkXZ(result3), World.toChunkXZ(result), World.toChunkXZ(result3));
            command.Position = new BCMVector3(result, result2, result3);
            command.Size = BCMVector3.V3One();
            return true;
        }

        private static bool GetBlockAreaMultiple(BCMCmdProcessor command)
        {
            if (!int.TryParse(command._pars[0], out var result) || !int.TryParse(command._pars[1], out var result2) || !int.TryParse(command._pars[2], out var result3))
            {
                BCCommandAbstract.SendOutput("Unable to parse x,y,z into numbers");
                return false;
            }
            if (!int.TryParse(command._pars[3], out var result4) || !int.TryParse(command._pars[4], out var result5) || !int.TryParse(command._pars[5], out var result6))
            {
                BCCommandAbstract.SendOutput("Unable to parse x2,y2,z2 into numbers");
                return false;
            }
            int num = Math.Min(result, result4);
            int num2 = Math.Min(result2, result5);
            int num3 = Math.Min(result3, result6);
            int num4 = Math.Max(result, result4);
            int num5 = Math.Max(result2, result5);
            int num6 = Math.Max(result3, result6);
            command.IsBlockArea = true;
            command.ChunkBounds = new BCMVector4(World.toChunkXZ(num), World.toChunkXZ(num3), World.toChunkXZ(num4), World.toChunkXZ(num6));
            command.Position = new BCMVector3(num, num2, num3);
            command.Size = new BCMVector3(num4 - num + 1, num5 - num2 + 1, num6 - num3 + 1);
            command.IsSingleBlock = BCMVector3.V3One(command.Size);
            return true;
        }

        public static bool ParseIdOption(string param, out string steamId, out int? entityId)
        {
            ClientInfo _cInfo;
            int num = ConsoleHelper.ParseParamPartialNameOrId(param, out steamId, out _cInfo, _sendError: false);
            if (_cInfo == null && num > 0 && !IsStoredPlayer(steamId))
            {
                num = 0;
            }
            entityId = _cInfo?.entityId;
            if (num == 1)
            {
                return true;
            }
            return false;
        }

        private static bool IsStoredPlayer(string steamId)
        {
            return File.Exists(GameUtils.GetPlayerDataDir() + Path.DirectorySeparatorChar + steamId + ".ttp");
        }

        public static Dictionary<int, Entity> FilterEntities(Dictionary<int, Entity> entities, Dictionary<string, string> options)
        {
            Dictionary<int, Entity> dictionary = new Dictionary<int, Entity>();
            foreach (KeyValuePair<int, Entity> entity in entities)
            {
                if (options.ContainsKey("all"))
                {
                    if (options.ContainsKey("vehicle") || !(entity.Value is EntityVehicle))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("type"))
                {
                    if (!(entity.Value == null) && !(entity.Value.GetType().ToString() != options["type"]))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("istype"))
                {
                    if (entity.Value == null)
                    {
                        continue;
                    }
                    string assemblyQualifiedName = entity.Value.GetType().AssemblyQualifiedName;
                    if (assemblyQualifiedName != null)
                    {
                        Type type = Type.GetType(assemblyQualifiedName.Replace(entity.Value.GetType().ToString(), options["istype"]));
                        if (!(type == null) && type.IsInstanceOfType(entity.Value))
                        {
                            dictionary.Add(entity.Key, entity.Value);
                        }
                    }
                }
                else if (options.ContainsKey("vehicle"))
                {
                    if (entity.Value is EntityVehicle)
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (options.ContainsKey("ecname"))
                {
                    EntityClass entityClass = EntityClass.list[entity.Value.entityClass];
                    if (entityClass != null && !(options["ecname"] != entityClass.entityClassName))
                    {
                        dictionary.Add(entity.Key, entity.Value);
                    }
                }
                else if (entity.Value is EntityEnemy || entity.Value is EntityAnimal)
                {
                    dictionary.Add(entity.Key, entity.Value);
                }
            }
            return dictionary;
        }

        public static Dictionary<long, Chunk> GetAffectedChunks(BCMCmdProcessor command)
        {
            Dictionary<long, Chunk> dictionary = new Dictionary<long, Chunk>();
            int result = 2000;
            if (command.Opts.ContainsKey("timeout") && command.Opts["timeout"] != null)
            {
                int.TryParse(command.Opts["timeout"], out result);
            }
            AddChunkObserverTask(command, result / 2000);
            for (int i = command.ChunkBounds.x; i <= command.ChunkBounds.z; i++)
            {
                for (int j = command.ChunkBounds.y; j <= command.ChunkBounds.w; j++)
                {
                    long key = WorldChunkCache.MakeChunkKey(i, j);
                    dictionary.Add(key, null);
                }
            }
            int num = (command.ChunkBounds.z - command.ChunkBounds.x + 1) * (command.ChunkBounds.w - command.ChunkBounds.y + 1);
            int num2 = 0;
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (num2 < num && stopwatch.ElapsedMilliseconds < result)
            {
                for (int k = command.ChunkBounds.x; k <= command.ChunkBounds.z; k++)
                {
                    for (int l = command.ChunkBounds.y; l <= command.ChunkBounds.w; l++)
                    {
                        long key2 = WorldChunkCache.MakeChunkKey(k, l);
                        if ((!dictionary.ContainsKey(key2) || dictionary[key2] == null) && GameManager.Instance.World.ChunkCache.ContainsChunkSync(key2))
                        {
                            dictionary[key2] = GameManager.Instance.World.GetChunkSync(key2) as Chunk;
                            if (dictionary[key2] != null)
                            {
                                num2++;
                            }
                        }
                    }
                }
            }
            stopwatch.Stop();
            if (num2 < num)
            {
                BCCommandAbstract.SendOutput($"Unable to load {num - num2}/{num} chunks");
                return null;
            }
            BCCommandAbstract.SendOutput($"Loading {num} chunks took {Math.Round((float)stopwatch.ElapsedMilliseconds / 1000f, 2)} seconds");
            return dictionary;
        }

        private static void AddChunkObserverTask(BCMCmdProcessor command, int timeoutSec)
        {
            int chunkObserverId = GameManager.Instance.World.m_ChunkManager.AddChunkObserver(command.Center.V3(), _bBuildVisualMeshAround: false, command.ChunkBounds.Radius(), -1).id;
            int timerSec = 60;
            if (command.Opts.ContainsKey("ts") && command.Opts["ts"] != null)
            {
                int.TryParse(command.Opts["ts"], out timerSec);
            }
            timerSec += timeoutSec;
            BCTask.AddTask(command._cmdName, ThreadManager.AddSingleTask(delegate (ThreadManager.TaskInfo info)
            {
                RemoveChunkObserverTask(chunkObserverId, command._cmdName, timerSec, info);
            }, null, delegate (ThreadManager.TaskInfo info, Exception e)
            {
                BCTask.DelTask(command._cmdName, info.GetHashCode(), 120);
            }).GetHashCode());
        }

        private static void RemoveChunkObserverTask(int id, string cmdName, int ts = 0, ThreadManager.TaskInfo taskInfo = null)
        {
            BCMTask task = BCTask.GetTask(cmdName, taskInfo?.GetHashCode());
            for (int i = 0; i < ts; i++)
            {
                if (task != null)
                {
                    task.Output = new
                    {
                        timer = i,
                        total = ts
                    };
                }
                Thread.Sleep(1000);
            }
            ChunkManager.ChunkObserver chunkObserver = GameManager.Instance.World.m_ChunkManager.m_ObservedEntities.FirstOrDefault((ChunkManager.ChunkObserver o) => o.id == id);
            if (chunkObserver != null)
            {
                GameManager.Instance.World.m_ChunkManager.RemoveChunkObserver(chunkObserver);
            }
        }
    }
}
