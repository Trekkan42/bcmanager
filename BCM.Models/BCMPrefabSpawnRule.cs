using System.Collections.Generic;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMPrefabSpawnRule
    {
        public string Name;

        [NotNull]
        [UsedImplicitly]
        public readonly List<BCMPrefabInfo> Prefabs = new List<BCMPrefabInfo>();

        public BCMPrefabSpawnRule([NotNull] PrefabSpawnRule spawnRule)
        {
            Name = spawnRule.Name;
            if (spawnRule.prefabs == null)
            {
                return;
            }
            foreach (PrefabInfo prefab in spawnRule.prefabs)
            {
                Prefabs.Add(new BCMPrefabInfo(prefab));
            }
        }
    }
}
