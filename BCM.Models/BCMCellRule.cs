using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using WorldGenerationEngine.Input;

namespace BCM.Models
{
    public class BCMCellRule
    {
        public string Name;

        public BCMVector2 CavesMinMax;

        public int PathType;

        public int PathRadius;

        public Dictionary<string, double> HubRules;

        public Dictionary<string, double> WildernessRules;

        public BCMCellRule([NotNull] CellRule cellRule)
        {
            Name = cellRule.Name;
            CavesMinMax = new BCMVector2(cellRule.CavesMinMax);
            PathType = cellRule.PathMaterial.type;
            PathRadius = cellRule.PathRadius;
            HubRules = cellRule.HubRules.ToDictionary((KeyValuePair<string, float> f) => f.Key, (KeyValuePair<string, float> f) => Math.Round(f.Value, 2));
            WildernessRules = cellRule.WildernessRules.ToDictionary((KeyValuePair<string, float> f) => f.Key, (KeyValuePair<string, float> f) => Math.Round(f.Value, 2));
        }
    }
}
