using JetBrains.Annotations;

namespace BCM.Models
{
    public class BCMParts
    {
        public int Type;

        public int Quality;

        public double UseTimes;

        public int MaxUse;

        public BCMParts([NotNull] ItemValue parts)
        {
            Type = parts.type;
            Quality = parts.Quality;
            UseTimes = parts.UseTimes;
            MaxUse = parts.MaxUseTimes;
        }
    }
}
