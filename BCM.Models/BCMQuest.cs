using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace BCM.Models
{
    [Serializable]
    public class BCMQuest : BCMAbstract
    {
        public static class StrFilters
        {
            public const string Id = "id";

            public const string Name = "name";

            public const string Group = "group";

            public const string SubTitle = "title";

            public const string Desc = "desc";

            public const string Offer = "offer";

            public const string Difficulty = "difficulty";

            public const string Icon = "icon";

            public const string Repeatable = "repeatable";

            public const string Category = "category";

            public const string Actions = "actions";

            public const string Requirements = "requirements";

            public const string Objectives = "objectives";

            public const string Rewards = "rewards";
        }

        private static readonly Dictionary<int, string> _filterMap = new Dictionary<int, string>
        {
            {
                0,
                "id"
            },
            {
                1,
                "name"
            },
            {
                2,
                "group"
            },
            {
                3,
                "title"
            },
            {
                4,
                "desc"
            },
            {
                5,
                "offer"
            },
            {
                6,
                "difficulty"
            },
            {
                7,
                "icon"
            },
            {
                8,
                "repeatable"
            },
            {
                9,
                "category"
            },
            {
                10,
                "actions"
            },
            {
                11,
                "requirements"
            },
            {
                12,
                "objectives"
            },
            {
                13,
                "rewards"
            }
        };

        public string Id;

        public string Name;

        public string Group;

        public string SubTitle;

        public string Desc;

        public string Offer;

        public string Difficulty;

        public string Icon;

        public bool Repeatable;

        public string Category;

        [NotNull]
        [UsedImplicitly]
        public List<BCMQuestAction> Actions = new List<BCMQuestAction>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMQuestRequirement> Requirements = new List<BCMQuestRequirement>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMQuestObjective> Objectives = new List<BCMQuestObjective>();

        [NotNull]
        [UsedImplicitly]
        public List<BCMQuestReward> Rewards = new List<BCMQuestReward>();

        public static Dictionary<int, string> FilterMap => _filterMap;

        public BCMQuest(object obj, string typeStr, Dictionary<string, string> options, List<string> filters)
            : base(obj, typeStr, options, filters)
        {
        }

        protected override void GetData(object obj)
        {
            QuestClass questClass = obj as QuestClass;
            if (questClass == null)
            {
                return;
            }
            if (IsOption("filter"))
            {
                foreach (string item in base.StrFilter)
                {
                    switch (item)
                    {
                        case "id":
                            GetId(questClass);
                            break;
                        case "name":
                            GetName(questClass);
                            break;
                        case "group":
                            GetGroup(questClass);
                            break;
                        case "title":
                            GetSubTitle(questClass);
                            break;
                        case "desc":
                            GetDesc(questClass);
                            break;
                        case "offer":
                            GetOffer(questClass);
                            break;
                        case "difficulty":
                            GetDifficulty(questClass);
                            break;
                        case "icon":
                            GetIcon(questClass);
                            break;
                        case "repeatable":
                            GetRepeatable(questClass);
                            break;
                        case "category":
                            GetCategory(questClass);
                            break;
                        case "actions":
                            GetActions(questClass);
                            break;
                        case "requirements":
                            GetRequirements(questClass);
                            break;
                        case "objectives":
                            GetObjectives(questClass);
                            break;
                        case "rewards":
                            GetRewards(questClass);
                            break;
                        default:
                            Log.Out("(BCM) Unknown filter " + item);
                            break;
                    }
                }
            }
            else
            {
                GetId(questClass);
                GetName(questClass);
                GetGroup(questClass);
                GetSubTitle(questClass);
                GetDesc(questClass);
                GetOffer(questClass);
                GetDifficulty(questClass);
                GetIcon(questClass);
                GetRepeatable(questClass);
                GetCategory(questClass);
                GetActions(questClass);
                GetRequirements(questClass);
                GetObjectives(questClass);
                GetRewards(questClass);
            }
        }

        private void GetRewards(QuestClass quest)
        {
            foreach (BaseReward reward in quest.Rewards)
            {
                Rewards.Add(new BCMQuestReward(reward));
            }
            base.Bin.Add("Rewards", Rewards);
        }

        private void GetObjectives(QuestClass quest)
        {
            foreach (BaseObjective objective in quest.Objectives)
            {
                Objectives.Add(new BCMQuestObjective(objective));
            }
            base.Bin.Add("Objectives", Objectives);
        }

        private void GetRequirements(QuestClass quest)
        {
            foreach (BaseRequirement requirement in quest.Requirements)
            {
                Requirements.Add(new BCMQuestRequirement(requirement));
            }
            base.Bin.Add("Requirements", Requirements);
        }

        private void GetActions(QuestClass quest)
        {
            foreach (BaseQuestAction action in quest.Actions)
            {
                Actions.Add(new BCMQuestAction(action));
            }
            base.Bin.Add("Actions", Actions);
        }

        private void GetCategory(QuestClass quest)
        {
            base.Bin.Add("Category", Category = quest.Category);
        }

        private void GetRepeatable(QuestClass quest)
        {
            base.Bin.Add("Repeatable", Repeatable = quest.Repeatable);
        }

        private void GetIcon(QuestClass quest)
        {
            base.Bin.Add("Icon", Icon = quest.Icon);
        }

        private void GetDifficulty(QuestClass quest)
        {
            base.Bin.Add("Difficulty", Difficulty = quest.Difficulty);
        }

        private void GetOffer(QuestClass quest)
        {
            base.Bin.Add("Offer", Offer = quest.Offer);
        }

        private void GetDesc(QuestClass quest)
        {
            base.Bin.Add("Desc", Desc = quest.Description);
        }

        private void GetSubTitle(QuestClass quest)
        {
            base.Bin.Add("SubTitle", SubTitle = quest.SubTitle);
        }

        private void GetGroup(QuestClass quest)
        {
            base.Bin.Add("Group", Group = quest.GroupName);
        }

        private void GetName(QuestClass questClass)
        {
            base.Bin.Add("Name", Name = questClass.Name);
        }

        private void GetId(QuestClass questClass)
        {
            base.Bin.Add("Id", Id = questClass.ID);
        }
    }
}
